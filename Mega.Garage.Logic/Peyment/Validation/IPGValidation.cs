﻿using FluentValidation;
using Mega.Garage.Persistence.Payment.Entities;

namespace Mega.Garage.Logic.Peyment.Validation
{
    public class IPGValidation : AbstractValidator<IPG>
    {
        public IPGValidation()
        {
            RuleFor(x => x.CallBackURL).NotEmpty().NotNull().WithMessage("آدرس بازگشتی بانک باید مشخص گردد.");
            RuleFor(x => x.Name).NotEmpty().NotNull().WithMessage("نام بانک را وارد کنید.");
            RuleFor(x => x.Password).NotEmpty().NotNull().WithMessage("رمز درگاه را وارد کنید.");
            RuleFor(x => x.TerminalId).NotEmpty().NotNull().WithMessage("شناسه ترمینال را وارد کنید.");
            RuleFor(x => x.UserName).NotEmpty().NotNull().WithMessage("نام کاربری را وارد کنید.");
        }
    }
}
