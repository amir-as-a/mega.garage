﻿using FluentValidation;
using Mega.Garage.Persistence.Payment.Entities;

namespace Mega.Garage.Logic.Payment.Validation
{
    public class PaymentValidation
    {
        public class UserPaymentEntity : AbstractValidator<UserPayment>
        {
            public UserPaymentEntity()
            {
                RuleFor(x => x.GaragesCode)
                    .MaximumLength(100).WithMessage("کد سفارش نمی تواند بیشتر از 100 کاراکتر باشد.");
            }
        }

    }
}
