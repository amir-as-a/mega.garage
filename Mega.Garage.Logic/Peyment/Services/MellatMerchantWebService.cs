﻿using Mega.Garage.Logic.Order.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Mega.Garage.Logic.Payment.Services
{
    public class MellatMerchantWebService
    {
        #region Private Properties
        public string CallBackUrl { get; private set; }
        public long TerminalId { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }
        #endregion

        public MellatMerchantWebService()
        {
            BypassCertificateError();

            // Get Mellat Merchant Data From DataBase
            var IGPData = IPGService.GetIPGByIPGEnum(Enums.IPG.Mellat);
            TerminalId = long.Parse(IGPData.TerminalId);
            UserName = IGPData.UserName;
            Password = IGPData.Password;
            CallBackUrl = IGPData.CallBackURL;
        }
        static void BypassCertificateError()
        {
            ServicePointManager.ServerCertificateValidationCallback += delegate
            {
                return true;
            };
        }
        //public static BasicHttpBinding Binding = new BasicHttpBinding()
        //{
        //    MaxBufferSize = int.MaxValue,
        //    ReceiveTimeout = new TimeSpan(0, 3, 0),
        //    SendTimeout = new TimeSpan(0, 3, 0),
        //    CloseTimeout = new TimeSpan(0, 3, 0)

        //};
        //public static EndpointAddress RemoteAddress = new EndpointAddress("https://bpm.shaparak.ir/pgwchannel/services/pgw");
        //public static MegaViewModel<(string ReferalCode, string ResCode)> PaymentRequest(int price, long orderid, long GarageCode, string dealerShenase)
        //{
        //    var Output = new MegaViewModel<(string ReferalCode, string ResCode)>()
        //    {
        //        Data = (ReferalCode: null, ResCode: null),
        //        Status = MegaStatus.Failed
        //    };
        //    try
        //    {
        //        var mellatMerchantWebService = new MellatMerchantWebService();

        //        string Result;
        //        string LocalDate = DateTime.Now.ToString("yyyyMMdd");
        //        string LocalTime = DateTime.Now.ToString("HHmmss");
        //        BypassCertificateError();

        //        var invoice = InvoiceService.GetInvoiceByOrderNumber(orderid.ToString());
        //        if (invoice == null)
        //        {
        //            Output.Messages.Add("سفارش با این شماره یافت نشد.");
        //            return Output;
        //        }

        //        if (invoice.IsPayed.GetValueOrDefault() == true)
        //        {
        //            Output.Messages.Add("سفارش مورد نظر قبلا پرداخت شده است.");
        //            return Output;
        //        }

        //        using (var bpService = new MellatWebService.PaymentGatewayClient())
        //        {
        //            string additionalData = $"{dealerShenase},{price},0;";

        //            //string additionalData =
        //            //    $"{dealerShenase},{Math.Round((double)price * 95d / 100d)},0;"
        //            //+$"1,{Math.Round((double)price * 5d / 100d)},0;";

        //            Result = bpService.bpCumulativeDynamicPayRequest(
        //                               terminalId: mellatMerchantWebService.TerminalId,
        //                               userName: mellatMerchantWebService.UserName,
        //                               userPassword: mellatMerchantWebService.Password,
        //                               orderId: GarageCode,
        //                               amount: price,
        //                               localDate: LocalDate,
        //                               localTime: LocalTime,
        //                               additionalData: additionalData,
        //                               callBackUrl: $"{HttpContext.Current.Request.Url.Scheme}://{(HttpContext.Current.Request.Url.Host != "localhost" ? HttpContext.Current.Request.Url.Host : HttpContext.Current.Request.Url.Authority)}" + mellatMerchantWebService.CallBackUrl
        //                       );

        //            String[] resultArray = Result.Split(',');
        //            string ErrorCode = resultArray[0];
        //            if (ErrorCode == "0")
        //            {
        //                Output.Data = (ReferalCode: resultArray[1], ResCode: "0");
        //                Output.Status = MegaStatus.Successfull;
        //            }
        //            else
        //            {
        //                Output.Data = (ReferalCode: null, ResCode: ErrorCode);

        //                Output.Messages.Add(ErrorCodeToText(ErrorCode));
        //            }
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Output.Messages.Add(exp.Message);
        //    }
        //    return Output;
        //}
        //public static MegaViewModel<string> VerifyPayment(long orderId, long GarageReferenceId)
        //{
        //    var Output = new MegaViewModel<string>()
        //    {
        //        Data = null,
        //        Status = MegaStatus.Failed
        //    };
        //    try
        //    {
        //        string Result;

        //        BypassCertificateError();

        //        using (var bpService = new MellatWebService.PaymentGatewayClient())
        //        {
        //            var mellatMerchantWebService = new MellatMerchantWebService();

        //            Result = bpService.bpVerifyRequest(
        //                   terminalId: mellatMerchantWebService.TerminalId,
        //                   userName: mellatMerchantWebService.UserName,
        //                   userPassword: mellatMerchantWebService.Password,
        //                   orderId: orderId,
        //                   GarageOrderId: orderId,
        //                   GarageReferenceId: GarageReferenceId
        //               );

        //            if (Result == "0")
        //            {
        //                Output.Data = Result;
        //                Output.Status = MegaStatus.Successfull;
        //            }
        //            else
        //            {
        //                Output.Messages.Add(ErrorCodeToText(Result));
        //            }
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        Output.Messages.Add(exp.Message);
        //    }
        //    return Output;
        //}
        //public static string ErrorCodeToText(string Code)
        //{
        //    switch (Code)
        //    {
        //        case "0":
        //            return "تراکنش با موفقيت انجام شد";
        //        case "11":
        //            return "شماره کارت نامعتبر است";
        //        case "12":
        //            return "موجودی کافي نيست";
        //        case "13":
        //            return "رمز نادرست است";
        //        case "14":
        //            return "تعداد دفعات وارد کردن رمز بيش از حد مجاز است";
        //        case "15":
        //            return "کارت نامعتبر است";
        //        case "16":
        //            return "دفعات برداشت وجه بيش از حد مجاز است";
        //        case "17":
        //            return "کاربر از انجام تراکنش منصرف شده است";
        //        case "18":
        //            return "تاريخ انقضای کارت گذشته است";
        //        case "19":
        //            return "مبلغ برداشت وجه بيش از حد مجاز است";
        //        case "111":
        //            return "صادر کننده کارت نامعتبر است";
        //        case "112":
        //            return "خطای سوييچ صادر کننده کارت";
        //        case "113":
        //            return "پاسخي از صادر کننده کارت دريافت نشد";
        //        case "114":
        //            return "دارنده کارت مجاز به انجام اين تراکنش نيست";
        //        case "21":
        //            return "پذيرنده نامعتبر است";
        //        case "23":
        //            return "خطای امنيتي رخ داده است";
        //        case "24":
        //            return "اطلاعات کاربری پذيرنده نامعتبر است";
        //        case "25":
        //            return "مبلغ نامعتبر است";
        //        case "31":
        //            return "پاسخ نامعتبر است";
        //        case "32":
        //            return "فرمت اطلاعات وارد شده صحيح نمي باشد";
        //        case "33":
        //            return "حساب نامعتبر است";
        //        case "34":
        //            return "خطای سيستمي";
        //        case "35":
        //            return "تاريخ نامعتبر است";
        //        case "41":
        //            return "شماره درخواست تکراری است";
        //        case "42":
        //            return "تراکنش Garage يافت نشد";
        //        case "43":
        //            return "قبلا درخواست Verify داده شده است";
        //        case "44":
        //            return "درخواست Verfiy يافت نشد";
        //        case "45":
        //            return "تراکنش Settle شده است";
        //        case "46":
        //            return "تراکنش Settle نشده است";
        //        case "47":
        //            return "تراکنش Settle يافت نشد";
        //        case "48":
        //            return "تراکنش Reverse شده است";
        //        case "49":
        //            return "تراکنش Refund يافت نشد";
        //        case "412":
        //            return "شناسه قبض نادرست است";
        //        case "413":
        //            return "شناسه پرداخت نادرست است";
        //        case "414":
        //            return "سازمان صادر کننده قبض نامعتبر است";
        //        case "415":
        //            return "زمان جلسه کاری به پايان رسيده است";
        //        case "416":
        //            return "خطا در ثبت اطلاعات";
        //        case "417":
        //            return "شناسه پرداخت کننده نامعتبر است";
        //        case "418":
        //            return "اشکال در تعريف اطلاعات مشتری";
        //        case "419":
        //            return "تعداد دفعات ورود اطلاعات از حد مجاز گذشته است";
        //        case "421":
        //            return "IP نامعتبر است";
        //        case "51":
        //            return "تراکنش تکراری است";
        //        case "54":
        //            return "تراکنش مرجع موجود نيست";
        //        case "55":
        //            return "تراکنش نامعتبر است";
        //        case "61":
        //            return "خطا در واريز";
        //    }
        //    return "خطای نامشخصی رخ داده است.لطفا با بخش پشتیبانی تماس حاصل نمائید.";
        //}
    }
}
