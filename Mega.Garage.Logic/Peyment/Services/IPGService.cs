﻿using FluentValidation;
using Mega.Garage.Logic.Peyment.Validation;
using Mega.Garage.Persistence.Payment.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Payment.Services
{
    sealed class IPGRepository : BaseRepository<IPG, int> { }
    public class IPGService
    {
        public static IPG GetIPGByIPGEnum(Enums.IPG @Enum)
        {
            return GetIPGById((int)@Enum);
        }
        public static IPG GetIPGById(int id)
        {
            var Entity = IPGRepository.Get(id);
            return Entity;
        }
        public static IEnumerable<IPG> GetAllIPGs()
        {
            var query = IPGRepository.GetAll();
            return query;
        }
        public static IPG SetIPG(IPG ipg)
        {
            var validation = new IPGValidation().Validate(ipg);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            ipg = IPGRepository.Add(ipg);
            return ipg;
        }
        public static IPG UpdateIPG(IPG iPG)
        {
            var validation = new IPGValidation().Validate(iPG);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            iPG = IPGRepository.Update(iPG);
            return iPG;
        }

        public static bool DeleteBrand(int id)
        {
            var deleted = IPGRepository.SoftDelete(id);
            return deleted;
        }
    }
}
