﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Payment.ViewModel
{
    public class PaymentSearchRequestViewModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string FullName { get; set; }
        public int? PaymentStatus { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
