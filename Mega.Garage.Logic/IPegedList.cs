﻿using System.Collections.Generic;

namespace Mega.Garage.Logic
{
    public enum SortType
    {
        ASC = 1,
        DESC = 2
    }
    public interface IPagedList<T>
    {
        int TotalRecords { get; set; }
        int CurrentPage { get; set; }
        int PageSize { get; set; }
        IEnumerable<T> Records { get; set; }
    }
    public class PagedList<T> : IPagedList<T>
    {
        public int TotalRecords { get; set; }
        public int CurrentPage { get; set; } = 1;
        public int PageSize { get; set; } = 30;
        public IEnumerable<T> Records { get; set; } = null;
        public PagedList()
        {
            Records = new List<T>();
        }
    }

}
