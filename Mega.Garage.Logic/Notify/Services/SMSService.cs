﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Notify.Services
{
    public class SMSService
    {
        public static bool AddSms(string mobile, string text)
        {
            try
            {

                string userName = "notification-webapi";
                string password = "8C8DB6B4-B869-49A4-BB2F-062BF108B8BF";

                string authParams = string.Format("{0}:{1}", userName, password);
                byte[] bytes = Encoding.UTF8.GetBytes(authParams);
                string encodedAuthParams = Convert.ToBase64String(bytes);

                var url = "http://notificationapi.iranecar.com/api/";
                var client = new RestClient(url);
                var request = new RestRequest("AddSms", Method.POST);
                request.AddHeader("Authorization", "Basic " + encodedAuthParams);
                request.AddParameter("Mobile", mobile);
                request.AddParameter("Content", text);
                request.AddParameter("Company", "SpeedySms"); //SpeedyAdvertise

                string message = string.Empty;
                foreach (var dto in request.Parameters)
                    message += dto.Name + ":" + dto.Value + "\n";

                client.ExecuteAsync(request, response => { });
                return true;
            }
            catch
            {
                return false;

            }
        }
    }
}
