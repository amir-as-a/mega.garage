﻿using System;
using System.Collections.Generic;

namespace Mega.Garage.Logic.Dealer.ViewModel
{
    public class DealerServiceViewModel
    {
        // Order
        public double Price { get; set; }
        public string OrderNumber { get; set; }
        // Service
        public string ServiceTitle { get; set; }
        public long ServiceProcessedDate { get; set; }
        // Customer
        public string CustomerName { get; set; }
        public string CustomerVehicle { get; set; }

        //Need to Add
        public List<OrderItem> OrderItems { get; set; }
        public class OrderItem
        {
            public int OrderId { get; set; }
            public string Title { get; set; }
            public double UnitPrice { get; set; }
            public string ImageUrl { get; set; }
            public int Count { get; set; }
        }
    }
}
