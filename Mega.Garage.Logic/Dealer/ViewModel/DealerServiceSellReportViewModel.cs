﻿using System.Collections.Generic;

namespace Mega.Garage.Logic.Dealer.ViewModel
{
    public class DealerServiceSellReportViewModel
    {
        public int? DealerId { get; set; }
        public string DealerTitle { get; set; }
        public double SumFinalPrice { get; set; }

        public List<DealerServiceSellMonthReportViewModel> Reports { get; set; }
    }


    public class DealerServiceSellMonthReportViewModel
    {
        public int Month { get; set; }
        public double Price { get; set; }
    }
}
