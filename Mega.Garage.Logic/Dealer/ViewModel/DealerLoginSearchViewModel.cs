﻿namespace Mega.Garage.Logic.Dealer.ViewModel
{
    public class DealerLoginSearchViewModel
    {
        public int? DealerId { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }
    }
}
