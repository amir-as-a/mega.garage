﻿namespace Mega.Garage.Logic.Dealer.ViewModel
{
    public class DealerServiceSellViewModel
    {
        /// <summary>
        /// ماه شمسی که به صورت اعداد بین 1 تا 12 بازگردانی می گردد
        /// </summary>
        public string Month { get; set; }
        /// <summary>
        /// کد شناسه مربوط به نماینده
        /// </summary>
        public int DealerId { get; set; }
        /// <summary>
        /// نام مربوط به نماینده
        /// </summary>
        public string DealerTitle { get; set; }
        /// <summary>
        /// مجموع حجم فروش نماینده در ماه جاری
        /// </summary>
        public double SumFinalPrice { get; set; }

    }
}
