﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Dealer.ViewModel
{
    public class DealerDetailViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string  PhoneNumber { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public double? Longitude { get; set; }
        public double? Latitude { get; set; }
        public string Description { get; set; }
    }
}
