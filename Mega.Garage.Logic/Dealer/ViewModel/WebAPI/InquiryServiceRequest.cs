﻿using FluentValidation;

namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class InquiryServiceRequest
    {
        public string Username { get; set; }
        public string Code { get; set; }
    }

    public class InquiryServiceRequestValidator : AbstractValidator<InquiryServiceRequest>
    {
        public InquiryServiceRequestValidator()
        {
            RuleFor(d => d.Username).NotNull().WithMessage("لطفا نام کاربری را وارد کنید.");
            RuleFor(d => d.Code).NotNull().WithMessage("لطفا شماره سفارش را وارد کنید.");
        }
    }
}