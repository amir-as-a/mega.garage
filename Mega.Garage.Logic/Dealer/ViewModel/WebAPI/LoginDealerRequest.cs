﻿using FluentValidation;

namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class LoginDealerRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string RegId { get; set; }
    }

    public class LoginDealerRequestValidator : AbstractValidator<LoginDealerRequest>
    {
        public LoginDealerRequestValidator()
        {
            RuleFor(d => d.Username).NotNull().WithMessage("لطفا نام کاربری را وارد کنید.");
            RuleFor(d => d.Password).NotNull().WithMessage("لطفا گذرواژه را وارد کنید.");
        }
    }
}