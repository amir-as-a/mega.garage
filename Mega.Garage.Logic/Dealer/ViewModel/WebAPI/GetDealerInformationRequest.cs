﻿using FluentValidation;

namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class GetDealerInformationRequest
    {
        public string Username { get; set; }
    }

    public class GetDealerInformationRequestValidator : AbstractValidator<GetDealerInformationRequest>
    {
        public GetDealerInformationRequestValidator()
        {
            RuleFor(d => d.Username).NotNull().WithMessage("لطفا نام کاربری را وارد کنید.");
        }
    }
}