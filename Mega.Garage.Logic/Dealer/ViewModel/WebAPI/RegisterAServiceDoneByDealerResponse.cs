﻿namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{ 
    public class RegisterAServiceDoneByDealerResponse
    {
        public bool SuccessfulOperation { get; set; }
    }
}