﻿using FluentValidation;

namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class RegisterAServiceDoneByDealerRequest
    {
        public string Username { get; set; }
        public string OrderNumber { get; set; }

    }

    public class RegisterAServiceDoneByDealerRequestValidator : AbstractValidator<RegisterAServiceDoneByDealerRequest>
    {
        public RegisterAServiceDoneByDealerRequestValidator()
        {
            RuleFor(d => d.Username).NotNull().WithMessage("لطفا نام کاربری را وارد کنید.");
            RuleFor(d => d.OrderNumber).NotNull().WithMessage("لطفا شماره سفارش را وارد کنید.");
        }
    }
}