﻿namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class LoginDealerResponse
    {
        public string Token { get; set; }
    }
}