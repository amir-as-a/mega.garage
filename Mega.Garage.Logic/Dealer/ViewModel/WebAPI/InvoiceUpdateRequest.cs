﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class InvoiceUpdateRequest
    {
        public string UserName { get; set; }
        public int InvoiceId { get; set; }
        public List<OrderItem> OrderItemList { get; set; } = new List<OrderItem>();

        public class OrderItem {
            public int Count { get; set; }
            public string OrderId { get; set; }
        }

    }
}