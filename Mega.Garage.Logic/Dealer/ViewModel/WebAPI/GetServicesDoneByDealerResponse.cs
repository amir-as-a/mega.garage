﻿using Mega.Garage.Logic.Dealer.ViewModel;
using System.Collections.Generic;

namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class GetServicesDoneByDealerResponse
    {
        public IList<GetServicesDoneByDealerResponseService> Services { get; set; }
    }

    public class GetServicesDoneByDealerResponseService
    {
        // Order
        public int Price { get; set; }
        public string OrderDetailNumber { get; set; }
        // Service
        public string ServiceTitle { get; set; }
        public long ServiceProcessedDate { get; set; }
        // Customer
        public string CustomerName { get; set; }
        public string CustomerVehicle { get; set; }
    }
}