﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
   public class InquiryServiceOrderItemViewModel
    {
        public string OrderDetailNumber { get; set; }
        public int OrderPrice { get; set; }
        public int OrderDiscount { get; set; }
        public string ImageUrl { get; set; }
        public string ProductCode { get; set; }
        public string ProductTitle { get; set; }
        public string ProductImage { get; set; }
    }
}
