﻿using FluentValidation;

namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class GetServicesDoneByDealerRequest
    {
        public string Username { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int CurrentPage { get; set; } = 1;
        public int PageSize { get; set; } = 10;
    }

    public class GetServicesDoneByDealerRequestValidator : AbstractValidator<GetServicesDoneByDealerRequest>
    {
        public GetServicesDoneByDealerRequestValidator()
        {
            RuleFor(d => d.Username).NotNull().WithMessage("لطفا نام کاربری را وارد کنید.");
        }
    }
}