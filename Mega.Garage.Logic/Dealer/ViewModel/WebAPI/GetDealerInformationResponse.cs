﻿namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class GetDealerInformationResponse
    {
        public string Title { get; set; }
        public string Owner { get; set; }
        public string Code { get; set; }
        public int TotalServiceCount { get; set; }
        public int TodayServiceCount { get; set; }
        public double TotalValue { get; set; }
        public double CurrentMonthValue { get; set; }
        public short Rate { get; set; } = 5;
    }
}