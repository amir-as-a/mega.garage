﻿using Mega.Garage.Logic.Dealer.ViewModel;
using System.Collections.Generic;

namespace Mega.Garage.Logic.Dealer.ViewModel.WebAPI
{
    public class InquiryServiceResponse
    {
        public InquiryServiceResponse()
        {
            Items = new List<InquiryServiceOrderItemViewModel>();
            Status = new InquiryServiceResponseStatus();
            Invoice = new InquiryServiceResponseInvoice();
            Customer = new InquiryServiceResponseCustomer();
        }
        public List<InquiryServiceOrderItemViewModel> Items { get; set; }
        public InquiryServiceResponseStatus Status { get; set; }
        public InquiryServiceResponseInvoice Invoice { get; set; }
        public InquiryServiceResponseCustomer Customer { get; set; }
    }
    
    public class InquiryServiceResponseStatus
    {
        public bool IsValid { get; set; }
        public string Message { get; set; }
        public long ServiceProcessedDate { get; set; }
    }

    public class InquiryServiceResponseInvoice
    {
        public string OrderNumber { get; set; }
        public long Date { get; set; }
        public int TotalPrice { get; set; }
        public int TotalDiscount { get; set; }
    }

    public class InquiryServiceResponseCustomer
    {
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Vehicle { get; set; }
    }
}