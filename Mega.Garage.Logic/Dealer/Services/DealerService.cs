﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Dealer.Services
{
    using FluentValidation;
    using Mega.Garage.Common;
    using Mega.Garage.Logic.Catalog.Services;
    using Mega.Garage.Logic.Dealer.Validation;
    using Mega.Garage.Logic.Dealer.ViewModel;
    using Mega.Garage.Logic.Dealer.ViewModel.WebAPI;
    using Mega.Garage.Logic.FileUpload.Services;
    using Mega.Garage.Logic.Order.Services;
    using Mega.Garage.Persistence.Dealer.Entities;
    using Mega.Garage.Persistence.Entities;
    using Mega.Garage.Persistence.Order.Entities;
    using System.Data.SqlClient;
    using System.Text;
    using Service = Persistence.Dealer.Entities.DealerService;

    public class DealerService
    {
        protected sealed class DealerRepository : BaseRepository<Dealer, int> { }
        protected sealed class DealerLoginRepository : BaseRepository<DealerLogin, int> { }
        protected sealed class DealerInventoryRepository : BaseRepository<DealerInventory, int> { }
        protected sealed class DealerServiceRepository : BaseRepository<Service, int>
        {
            public static List<DealerServiceSellViewModel> GetDealerServiceTotalSellPerMonthInAUniqueYear(int year)
            {
                var dateList = DateUtility.GetYearFirstAndLastDateOfYear(year);

                using (var context = new Persistence.GarageDbContext())
                {
                    string SqlQuery = @"
SELECT MONTH,DEALERID, SUM(FINALPRICE) SUMFINALPRICE FROM(
SELECT D.ID AS [DEALERID],I.FINALPRICE,DS.PROCESSDATE,CASE 
	WHEN INVOICEDATE BETWEEN  @M1F AND @M1E  THEN '1'
	WHEN INVOICEDATE BETWEEN  @M2F AND @M2E THEN '2'
	WHEN INVOICEDATE BETWEEN  @M3F AND @M3E THEN '3'
    WHEN INVOICEDATE BETWEEN  @M4F AND @M4E THEN '4'
	WHEN INVOICEDATE BETWEEN  @M5F AND @M5E THEN '5'
	WHEN INVOICEDATE BETWEEN  @M6F AND @M6E THEN '6'
	WHEN INVOICEDATE BETWEEN  @M7F AND @M7E THEN '7'
	WHEN INVOICEDATE BETWEEN  @M8F AND @M8E THEN '8'
	WHEN INVOICEDATE BETWEEN  @M9F AND @M9E THEN '9'
	WHEN INVOICEDATE BETWEEN  @M10F AND @M10E THEN '10'
	WHEN INVOICEDATE BETWEEN  @M11F AND @M11E THEN '11'
	WHEN INVOICEDATE BETWEEN  @M12F AND @M12E THEN '12'
	END AS [MONTH] FROM [DEALER].[DEALER] D
JOIN [DEALER].[DEALERSERVICE] DS ON D.ID = DS.DEALERID
JOIN [Garage].[INVOICE] I ON  DS.INVOICEID = I.ID
WHERE I.ISACTIVE = 1 AND DS.ISACTIVE = 1
) T
GROUP BY MONTH,DEALERID
HAVING MONTH IS NOT NULL
ORDER BY MONTH ASC
";
                    var result = context.Database.SqlQuery<DealerServiceSellViewModel>(SqlQuery,
                    new SqlParameter("M1F", dateList[0].StartOfMonth),
                    new SqlParameter("M1E", dateList[0].EndOfMonth),
                    new SqlParameter("M2F", dateList[1].StartOfMonth),
                    new SqlParameter("M2E", dateList[1].EndOfMonth),
                    new SqlParameter("M3F", dateList[2].StartOfMonth),
                    new SqlParameter("M3E", dateList[2].EndOfMonth),
                    new SqlParameter("M4F", dateList[3].StartOfMonth),
                    new SqlParameter("M4E", dateList[3].EndOfMonth),
                    new SqlParameter("M5F", dateList[4].StartOfMonth),
                    new SqlParameter("M5E", dateList[4].EndOfMonth),
                    new SqlParameter("M6F", dateList[5].StartOfMonth),
                    new SqlParameter("M6E", dateList[5].EndOfMonth),
                    new SqlParameter("M7F", dateList[6].StartOfMonth),
                    new SqlParameter("M7E", dateList[6].EndOfMonth),
                    new SqlParameter("M8F", dateList[7].StartOfMonth),
                    new SqlParameter("M8E", dateList[7].EndOfMonth),
                    new SqlParameter("M9F", dateList[8].StartOfMonth),
                    new SqlParameter("M9E", dateList[8].EndOfMonth),
                    new SqlParameter("M10F", dateList[9].StartOfMonth),
                    new SqlParameter("M10E", dateList[9].EndOfMonth),
                    new SqlParameter("M11F", dateList[10].StartOfMonth),
                    new SqlParameter("M11E", dateList[10].EndOfMonth),
                    new SqlParameter("M12F", dateList[11].StartOfMonth),
                    new SqlParameter("M12E", dateList[11].EndOfMonth)
                    ).ToList();

                    return result;
                }

            }

        }
        protected sealed class DealerTransactionHistoryRepository : BaseRepository<DealerTransactionHistory, long> { }
        #region Dealer Service
        public static Dealer AddDealer(Dealer dealer)
        {
            var validation = new DealerValidation.DealerEntity().Validate(dealer);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            return DealerRepository.Add(dealer);
        }
        public static bool DeleteDealer(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealer = DealerRepository.Get(id);

            if (dealer == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = DealerRepository.SoftDelete(dealer);
            return IsDeleted;
        }

        public static IEnumerable<Dealer> GetAllDealers()
        {

            return DealerRepository.GetAll();
        }
        public static IEnumerable<ViewModel.DealerDetailViewModel> GetDealersList()
        {
            return DealerRepository.GetQuery(x => x.IsActive && x.IsOnSite).OrderBy(x => x.Id)
                .Select(dealer => new DealerDetailViewModel
                {
                    Id = dealer.Id,
                    Title = dealer.Title,
                    PhoneNumber = dealer.Phone,
                    Address = dealer.Address,
                    Province = dealer.Province,
                    City = dealer.City,
                    Longitude = dealer.Longitude,
                    Latitude = dealer.Latitude,
                    Description = dealer.Description
                });
        }
        public static Dealer GetDealerById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealer = DealerRepository.Get(id);

            if (dealer == null || !dealer.IsActive)
                return null;

            return dealer;
        }

        public static Dealer UpdateDealer(Dealer dealer)
        {
            if (dealer == null)
                throw new ArgumentNullException();

            var oldEntity = DealerRepository.Get(dealer.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            dealer = DealerRepository.Update(dealer);
            return dealer;
        }

        public static Dealer GetDealerByUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException();

            var login = GetDealerLoginByUsername(username);

            if (login == null)
                return null;

            var dealer = DealerRepository.Get((int)login.DealerId);

            if (dealer == null || !dealer.IsActive)
                return null;

            return dealer;
        }
        public static double? GetTotalSellForADealerByDealerId(int dealerId)
        {
            using (var db = new Persistence.GarageDbContext())
            {
                var dealerSell = from invoice in db.Invoices
                                 join dealer in db.DealerServices on invoice.Id equals dealer.InvoiceId into leftQuery
                                 from joined in leftQuery.DefaultIfEmpty()
                                 where joined.DealerId == dealerId && invoice.IsActive && joined.IsActive
                                 select new { invoice.FinalPrice };
                return dealerSell.Sum(x => x.FinalPrice);
            }
        }
        public static double? GetThisMonthSellForADealerByDealerId(int dealerId)
        {
            using (var db = new Persistence.GarageDbContext())
            {
                DateTime CurrentMonthFirstDayFromJalaliDate = DateUtility.GetDateOfFirstDayOfCurrentJalaliMonth();

                var dealerSell = from invoice in db.Invoices
                                 join dealer in db.DealerServices on invoice.Id equals dealer.InvoiceId into leftQuery
                                 from joined in leftQuery.DefaultIfEmpty()
                                 where joined.DealerId == dealerId && invoice.IsActive && joined.IsActive &&
                                       joined.ProcessDate >= CurrentMonthFirstDayFromJalaliDate
                                 select new { invoice.FinalPrice };
                return dealerSell.Sum(x => x.FinalPrice);
            }
        }
        public static int GetTodayServicesDoneByDealerId(int dealerId)
        {
            DateTime Today = DateTime.Now.Date;
            DateTime TodayEnd = DateTime.Now.Date.AddDays(1).AddMilliseconds(-1);
            return DealerServiceRepository.GetQuery(x => x.IsActive && x.ProcessDate >= Today && x.ProcessDate <= TodayEnd && x.DealerId == dealerId).Count();
        }
        public static int GetTotalservicesByDealerId(int dealerId)
        {
            return DealerServiceRepository.Count(x => x.IsActive && x.DealerId == dealerId);
        }

        public static double? GetTodaySellForADealerByDealerId(int dealerId)
        {
            using (var db = new Persistence.GarageDbContext())
            {
                DateTime Today = DateTime.Now.Date;
                DateTime TodayEnd = DateTime.Now.Date.AddDays(1).AddMilliseconds(-1);

                var dealerSell = from invoice in db.Invoices
                                 join dealer in db.DealerServices on invoice.Id equals dealer.InvoiceId into leftQuery
                                 from joined in leftQuery.DefaultIfEmpty()
                                 where joined.DealerId == dealerId && invoice.IsActive && joined.IsActive &&
                                       joined.ProcessDate >= Today && joined.ProcessDate <= TodayEnd
                                 select new { invoice.FinalPrice };
                return dealerSell.Sum(x => x.FinalPrice);
            }
        }
        public static GetDealerInformationResponse GetDealerInformation(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException();

            var dealer = GetDealerByUsername(username);
            if (dealer == null)
                throw new NullReferenceException("نمایندگی یافت نشد.");

            var ServiceCount = GetTotalservicesByDealerId(dealer.Id);
            var totalValue = GetTotalSellForADealerByDealerId(dealer.Id) ?? 0;
            var currentMonthValue = GetThisMonthSellForADealerByDealerId(dealer.Id) ?? 0;
            var todayServiceCount = GetTodayServicesDoneByDealerId(dealer.Id);

            var information = new GetDealerInformationResponse
            {
                Title = dealer.Title,
                Code = dealer.Code,
                Owner = dealer.OwnerFirstName + " " + dealer.OwnerLastName,
                TotalValue = totalValue,
                CurrentMonthValue = currentMonthValue,
                TotalServiceCount = ServiceCount,
                TodayServiceCount = todayServiceCount
            };

            return information;
        }

        public static IEnumerable<DealerServiceSellReportViewModel> GetDealerServiceTotalSellPerMonthInAUniqueYear(int year)
        {
            var dealers = GetAllDealers();
            var result = DealerServiceRepository.GetDealerServiceTotalSellPerMonthInAUniqueYear(year);

            var response = result.GroupBy(x => x.DealerId).Select(x => new DealerServiceSellReportViewModel
            {
                DealerId = x.Key,
                DealerTitle = dealers.FirstOrDefault(y => y.Id == x.Key).Title,
                SumFinalPrice = x.Sum(y => y.SumFinalPrice),
                Reports = x.Select(y => new DealerServiceSellMonthReportViewModel()
                {
                    Month = int.Parse(y.Month),
                    Price = y.SumFinalPrice
                }).ToList()
            }).ToList();


            response.ForEach(item =>
            {
                for (int i = 0; i < 12; i++)
                {
                    var month = item.Reports.FirstOrDefault(x => x.Month == (i + 1));
                    if (month == null)
                    {
                        item.Reports.Add(new DealerServiceSellMonthReportViewModel()
                        {
                            Month = (i + 1),
                            Price = 0
                        });
                    }
                }

                item.Reports = item.Reports.OrderBy(x => x.Month).ToList();
            });

            var total = new DealerServiceSellReportViewModel()
            {
                DealerTitle = "جمع کل",
                Reports = new List<DealerServiceSellMonthReportViewModel>()
            };
            for (int i = 0; i < 12; i++)
            {
                total.Reports.Add(new DealerServiceSellMonthReportViewModel()
                {
                    Month = (i + 1),
                    Price = (double?)result.Where(x => x.Month == (i + 1).ToString())?.Sum(x => x.SumFinalPrice) ?? (double)0
                });
            }
            total.SumFinalPrice = total.Reports.Sum(x => x.Price);
            response.Add(total);

            return response;
        }
        #endregion

        #region Dealer Inventory
        public static DealerInventory AddDealerInventory(DealerInventory dealerInventory)
        {
            if (dealerInventory == null)
                throw new ArgumentNullException();

            dealerInventory = DealerInventoryRepository.Add(dealerInventory);
            return dealerInventory;
        }

        public static bool DeleteDealerInventory(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealerInventory = DealerInventoryRepository.Get(id);

            if (dealerInventory == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = DealerInventoryRepository.SoftDelete(dealerInventory);
            return IsDeleted;
        }

        public static IEnumerable<DealerInventory> GetAllDealerInventories()
        {
            return DealerInventoryRepository.GetAll();
        }

        public static DealerInventory GetDealerInventoryById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealerInventory = DealerInventoryRepository.Get(id);

            if (dealerInventory == null || !dealerInventory.IsActive)
                return null;

            return dealerInventory;
        }

        public static DealerInventory UpdateDealerInventory(DealerInventory dealerInventory)
        {
            if (dealerInventory == null)
                throw new ArgumentNullException();

            var oldEntity = DealerInventoryRepository.Get(dealerInventory.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            dealerInventory = DealerInventoryRepository.Update(dealerInventory);

            return dealerInventory;
        }

        #endregion

        #region Dealer Transaction History
        public static DealerTransactionHistory AddDealerTransactionHistory(DealerTransactionHistory dealerTransactionHistory)
        {
            if (dealerTransactionHistory == null)
                throw new ArgumentNullException();

            dealerTransactionHistory = DealerTransactionHistoryRepository.Add(dealerTransactionHistory);
            return dealerTransactionHistory;
        }

        public static bool DeleteDealerTransactionHistory(long id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealerTransactionHistory = DealerTransactionHistoryRepository.Get(id);

            if (dealerTransactionHistory == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = DealerTransactionHistoryRepository.SoftDelete(dealerTransactionHistory);
            return IsDeleted;
        }

        public static IEnumerable<DealerTransactionHistory> GetAllDealerTransactionHistories()
        {
            return DealerTransactionHistoryRepository.GetAll();
        }

        public static DealerTransactionHistory GetDealerTransactionHistoryById(long id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealerTransactionHistory = DealerTransactionHistoryRepository.Get(id);

            if (dealerTransactionHistory == null || !dealerTransactionHistory.IsActive)
                return null;

            return dealerTransactionHistory;
        }

        public static IList<DealerTransactionHistory> GetDealerTransactionHistoriesByDealerId(int dealerId)
        {
            if (dealerId == 0)
                throw new ArgumentNullException();

            var dealerTransactionHistory = DealerTransactionHistoryRepository.GetQuery(d => d.DealerId == dealerId && d.IsActive).ToList();
            return dealerTransactionHistory;
        }

        public static DealerTransactionHistory UpdateDealerTransactionHistory(DealerTransactionHistory dealerTransactionHistory)
        {
            if (dealerTransactionHistory == null)
                throw new ArgumentNullException();

            var oldEntity = DealerTransactionHistoryRepository.Get(dealerTransactionHistory.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            dealerTransactionHistory = DealerTransactionHistoryRepository.Update(dealerTransactionHistory);
            return dealerTransactionHistory;
        }

        #endregion

        #region Service

        public static Service AddDealerService(Service dealerService)
        {
            if (dealerService == null)
                throw new ArgumentNullException();

            return DealerServiceRepository.Add(dealerService);
        }

        public static bool DeleteDealerService(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealerService = DealerServiceRepository.Get(id);

            if (dealerService == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = DealerServiceRepository.SoftDelete(dealerService);
            return IsDeleted;
        }

        public static IEnumerable<Service> GetAllDealerServices()
        {
            return DealerServiceRepository.GetAll();
        }

        public static Service GetDealerServiceById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealerService = DealerServiceRepository.Get(id);

            if (dealerService == null || !dealerService.IsActive)
                return null;

            return dealerService;
        }

        public static Service GetDealerServiceByOrderDetailNumber(string orderDetailNumber)
        {
            if (string.IsNullOrEmpty(orderDetailNumber))
                throw new ArgumentNullException();

            var dealerService = DealerServiceRepository.GetQuery(
                sl => sl.IsActive &&
                sl.OrderDetailNumber.Equals(orderDetailNumber)
                ).FirstOrDefault();

            return dealerService;
        }

        public static Service UpdateDealerService(Service dealerService)
        {
            if (dealerService == null)
                throw new ArgumentNullException();

            var oldEntity = DealerServiceRepository.Get(dealerService.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            dealerService = DealerServiceRepository.Update(dealerService);
            return dealerService;
        }

        public static PagedList<DealerServiceViewModel> GetServicesDoneByDealer(string username, DateTime? fromdate, DateTime? toDate, int pageSize, int currentPage)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException();

            var dealer = GetDealerByUsername(username);

            if (dealer == null)
                throw new InvalidOperationException();

            using (var db = new Persistence.GarageDbContext())
            {
                var result = new List<DealerServiceViewModel>();

                var dealerSell =
                    (from dlrSrv in db.DealerServices
                     join dlr in db.Dealers on dlrSrv.DealerId equals dlr.Id
                     join dlrLogin in db.DealerLogins on dlr.Id equals dlrLogin.DealerId
                     join invc in db.Invoices on dlrSrv.InvoiceId equals invc.Id
                     join cus in db.Customers on invc.CustomerId equals cus.Id
                     join carGrp in db.CarGroups on cus.CarGroupCode equals carGrp.Id
                     where dlrLogin.UserName == username && dlrSrv.IsActive && dlr.IsActive && dlrLogin.IsActive && invc.IsActive && cus.IsActive && carGrp.IsActive
                     select new
                     {
                         dlrLogin.UserName,
                         customerFullname = cus.FirstName + " " + cus.LastName,
                         cargroupTitle = carGrp.Title,
                         invc.FinalPrice,
                         invc.OrderNumber,
                         dlrSrv.ProcessDate,
                         orderItems = db.OrderItems
                                      .Where(ord => ord.IsActive && !ord.IsCancelled && ord.InvoiceId == invc.Id)
                                      .Select(ord =>
                                      new
                                      {
                                          ord.Product.CatalogImage,
                                          ord.Product.CatalogTitle,
                                          ord.UnitPrice,
                                          ord.Id
                                      })
                     });
                if (fromdate.HasValue)
                {
                    DateTime FromDate = fromdate.Value;
                    dealerSell = dealerSell.Where(x => x.ProcessDate >= FromDate);
                }
                if (toDate.HasValue)
                {
                    DateTime ToDate = toDate.Value;
                    dealerSell = dealerSell.Where(x => x.ProcessDate <= ToDate);
                }
                var ResultCount = dealerSell.Count();
                var Result = dealerSell.OrderByDescending(x => x.ProcessDate).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
                var OutputResult = new List<DealerServiceViewModel>();
                Result.ForEach(item =>
                {
                    OutputResult.Add(new DealerServiceViewModel
                    {
                        CustomerName = item.customerFullname,
                        CustomerVehicle = item.cargroupTitle,
                        OrderNumber = item.OrderNumber,
                        Price = item.FinalPrice ?? 0,
                        ServiceProcessedDate = (item.ProcessDate.HasValue ? DateUtility.GetUnixTimestamp(item.ProcessDate.Value) : -1),
                        ServiceTitle = "",
                        OrderItems = item.orderItems.ToList().Select(x => new DealerServiceViewModel.OrderItem
                        {
                            OrderId = x.Id,
                            Count = 1,
                            ImageUrl = UploadService.GetCDNFilePath(x.CatalogImage),
                            Title = x.CatalogTitle,
                            UnitPrice = x.UnitPrice
                        }).ToList()
                    }); ;
                });
                return new PagedList<DealerServiceViewModel>()
                {
                    CurrentPage = currentPage,
                    PageSize = pageSize,
                    TotalRecords = ResultCount,
                    Records = OutputResult
                };
            }
        }
        public static IList<Service> GetDealerServicesByDealerId(int dealerId)
        {
            if (dealerId == 0)
                throw new ArgumentNullException();

            var services = DealerServiceRepository.GetQuery(s =>
                s.IsActive &&
                s.DealerId == dealerId
            ).ToList();

            return services;
        }
        public static string UpdateInvoiceByDealer(ViewModel.WebAPI.InvoiceUpdateRequest invoiceUpdateRequest)
        {
            if (invoiceUpdateRequest == null)
                throw new ArgumentNullException("ورودی اطلاعات ناقص است.");

            var InvoiceEntity = InvoiceService.GetInvoiceByOrderNumber(invoiceUpdateRequest.InvoiceId.ToString());

            if (InvoiceEntity == null)
                throw new ArgumentNullException("سفارشی با این کد یافت نشد.");

            if (InvoiceEntity.CurrentProcessId != 1)
                throw new NullReferenceException("این سفارش در وضعیت ثبت اولیه نیست و غیرقابل ویرایش است.");

            var OrderItemsInDataBase = OrderService.GetAllOrderItemsByInvoiceId(InvoiceEntity.Id);
            var OrderItemsInInput = invoiceUpdateRequest.OrderItemList?.ToList();

            foreach (var OrderItem in OrderItemsInInput)
            {
                switch (OrderItem.Count)
                {
                    default:
                        break;
                    case 0:
                        OrderService.SetOrderItemAsCanceledByOrderIdAndInvoiceOrderNumber(OrderItem.OrderId, invoiceUpdateRequest.InvoiceId.ToString());
                        var ItemPrice = OrderItemsInDataBase.FirstOrDefault(x => x.OrderDetailNumber == OrderItem.OrderId).UnitPrice;
                        InvoiceEntity.FinalPrice -= ItemPrice;
                        break;
                }
            }
            InvoiceService.UpdateInvoice(InvoiceEntity, true, false);
            //SEND SMS
            return InvoiceEntity.OrderNumber;
        }
        public static InquiryServiceResponse GetInquiryService(string orderlNumber)
        {
            if (string.IsNullOrEmpty(orderlNumber))
                throw new ArgumentNullException();

            var inquiryVM = new InquiryServiceResponse();

            var invoice = InvoiceService.GetInvoiceByOrderNumber(orderlNumber);

            if (invoice == null)
                throw new NullReferenceException("فاکتور یافت نشد.");

            inquiryVM.Invoice.OrderNumber = invoice.OrderNumber;
            inquiryVM.Invoice.Date = DateUtility.GetUnixTimestamp(invoice.InvoiceDate);

            var orderItems = OrderService.GetOrderItemsByInvoiceId(invoice.Id);

            if (!orderItems.Any())
            {
                inquiryVM.Status.IsValid = false;
                inquiryVM.Status.Message = "هیچ سفارشی برای فاکتور مذکور یافت نشد.";

                return inquiryVM;
            }

            inquiryVM.Items = new List<InquiryServiceOrderItemViewModel>();

            foreach (var item in orderItems)
            {
                var dealerService = GetDealerServiceByOrderDetailNumber(item.OrderDetailNumber);

                if (dealerService != null)
                {
                    inquiryVM.Status.IsValid = false;
                    inquiryVM.Status.ServiceProcessedDate = DateUtility.GetUnixTimestamp(dealerService.ProcessDate.Value);
                    inquiryVM.Status.Message = "این خدمت پیشتر در تاریخ مذکور انجام شده.";

                    return inquiryVM;
                }

                var orderItemVM = new InquiryServiceOrderItemViewModel
                {
                    OrderDetailNumber = item.OrderDetailNumber,
                    OrderPrice = (int)item.UnitPrice,
                    OrderDiscount = (int)item.Discount,
                    ImageUrl = ProductService.GetImageUrlByProductId(item.ProductId)
                };

                var product = ProductService.GetProductById((long)item.ProductId);

                if (product == null)
                    throw new NullReferenceException("محصول یافت نشد.");

                orderItemVM.ProductCode = product.Code;
                orderItemVM.ProductTitle = product.CatalogTitle;
                orderItemVM.ProductImage = UploadService.GetCDNFilePath(product.CatalogImage);
                inquiryVM.Items.Add(orderItemVM);

                inquiryVM.Invoice.TotalPrice += orderItemVM.OrderPrice;
            }

            inquiryVM.Invoice.TotalDiscount = (int)invoice.TotalDiscount;

            var customer = CustomerService.GetCustomerById(invoice.CustomerId);

            if (customer == null)
                throw new NullReferenceException("مشتری یافت نشد.");

            inquiryVM.Customer.Name = customer.FirstName + " " + customer.LastName;
            inquiryVM.Customer.Mobile = customer.Mobile;

            var vehicle = CarGroupService.GetCarGroupByCode(customer.CarGroupCode);

            if (vehicle == null)
                throw new NullReferenceException("گروه خودرو یافت نشد.");

            inquiryVM.Customer.Vehicle = vehicle.Title;

            inquiryVM.Status.IsValid = true;
            inquiryVM.Status.Message = "خدمت قابل انجام است.";
            return inquiryVM;
        }

        public static bool IsOrderDetailNumberValidForDealerService(OrderItem orderItem)
        {
            // orderDetailNumber is not valid.
            if (orderItem == null)
                return false;

            // the service has been fulfilled or canceled before.
            // TODO use enums instead.
            if (orderItem.CurrentOrderProcessId != 1)
                return false;

            var dealerService = GetDealerServiceByOrderDetailNumber(orderItem.OrderDetailNumber);

            // the service has been fulfilled for this order item before.
            if (dealerService != null)
                return false;

            return true;
        }

        public static RegisterAServiceDoneByDealerResponse RegisterAServiceDoneByDealer(string username, string orderNumber)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(orderNumber))
                throw new ArgumentNullException();
            using (var db = new Persistence.GarageDbContext())
            {
                var dbTransaction = db.Database.BeginTransaction();
                var InvoiceEntity = db.Invoices.Where(invoice => invoice.OrderNumber == orderNumber && invoice.IsActive).FirstOrDefault();

                if (InvoiceEntity == null)
                    throw new NullReferenceException("فاکتوری با این شماره وجود ندارد.");


                /// if CurrentProcessId = 1 then 'ثبت اولیه'
                /// if CurrentProcessId = 2 then 'تکمیل ارائه صنعت'
                /// if CurrentProcessId = 3 then 'لغو سفارش'

                if (InvoiceEntity.CurrentProcessId != 1)
                    throw new InvalidOperationException("این سفارش قبلا بررسی شده و در وضعیتی نیست که بتواند مجدد تغییر یابد");

                var dealer = GetDealerByUsername(username);

                if (dealer == null)
                    throw new NullReferenceException("نمایندگی یافت نشد.");

                ///Done flag in [InvoiceProcess] table has '2' ID
                InvoiceEntity.CurrentProcessId = 2;
                InvoiceEntity.DealerId = dealer.Id;

                foreach (var OrderItem in InvoiceEntity.OrderItems)
                {
                    OrderItem.CurrentOrderProcessId = 2; // BUG isn't "OrderStatusId" same as "CurrentOrderProcessId"?
                    OrderItem.DealerId = dealer.Id;
                    var orderProcessHistory = new OrderProcessHistory
                    {
                        OrderId = OrderItem.Id,
                        CreatedBy = 1,
                        CreatedDate = DateTime.Now,
                        IsActive = true,
                        ModifiedBy = 1,
                        ModifiedDate = DateTime.Now,
                        OrderProcessId = 2 // TODO use enum
                    };
                    db.OrderProcessHistories.Add(orderProcessHistory);
                }



                var DealerService = new Service
                {
                    DealerId = dealer.Id,
                    InvoiceId = InvoiceEntity.Id,
                    OrderDetailNumber = InvoiceEntity.OrderNumber,
                    OrderItemId = null,
                    ProcessDate = DateTime.Now,
                    CreatedBy = 1,
                    CreatedDate = DateTime.Now,
                    Description = "",
                    ModifiedDate = DateTime.Now,
                    ModifiedBy = 1,
                    IsActive = true
                };
                db.DealerServices.Add(DealerService);
                try
                {
                    var Errors = db.GetValidationErrors();
                    int RowsAffected = db.SaveChanges();
                    dbTransaction.Commit();
                    return new RegisterAServiceDoneByDealerResponse { SuccessfulOperation = true };
                }
                catch
                {
                    dbTransaction.Rollback();
                    return new RegisterAServiceDoneByDealerResponse { SuccessfulOperation = false };
                }
            }
        }

        #endregion

        #region Authorization

        public static PagedList<DealerLogin> GetDealerLogins(DealerLoginSearchViewModel parameters)
        {
            var query = DealerLoginRepository.GetQuery(item => item.IsActive == true);

            if (parameters.DealerId.HasValue)
                query = query.Where(invoice => invoice.DealerId == parameters.DealerId);

            query = query.OrderByDescending(x => x.CreatedDate);

            return new PagedList<DealerLogin>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = parameters.PageSize > 0 ? query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList() : query.ToList(),
            };
        }

        public static DealerLogin AddDealerLogin(DealerLogin dealerLogin)
        {
            var validation = new DealerLoginValidation.DealerLoginEntity().Validate(dealerLogin);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            return DealerLoginRepository.Add(dealerLogin);
        }

        public static DealerLogin GetDealerLoginById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealerLogin = DealerLoginRepository.Get(id);

            if (dealerLogin == null || !dealerLogin.IsActive)
                return null;

            return dealerLogin;
        }

        public static bool DeleteDealerLogin(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var dealerLogin = DealerLoginRepository.Get(id);

            if (dealerLogin == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = DealerLoginRepository.SoftDelete(dealerLogin);
            return IsDeleted;
        }

        public static DealerLogin UpdateDealerLogin(DealerLogin dealerLogin)
        {
            if (dealerLogin == null)
                throw new ArgumentNullException();

            var oldEntity = DealerLoginRepository.Get(dealerLogin.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            dealerLogin = DealerLoginRepository.Update(dealerLogin);
            return dealerLogin;
        }

        public static bool LogoutDealer(string token, string userName)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(token))
                throw new ArgumentNullException();

            var LogedinUser = GetDealerLoginByUsername(userName);

            if (LogedinUser == null)
                throw new InvalidOperationException("نمایندگی یافت نشد.");

            if ((bool)LogedinUser.IsLocked)
                throw new InvalidOperationException("نمایندگی غیرفعال است.");

            // To make sure that there is an active dealer for this login.
            var dealer = GetDealerById((int)LogedinUser.DealerId);
            if (dealer == null)
                throw new InvalidOperationException("نمایندگی یافت نشد.");


            LogedinUser.Token = null;
            LogedinUser.TokenExpireDate = DateTime.Now.Date;

            DealerLoginRepository.Update(LogedinUser, needAuthorization: false);

            return true;
        }

        public static LoginDealerResponse GetDealerTokenByUserNameAndPassword(string username, string password, string regId = "")
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                throw new ArgumentNullException();

            var login = GetDealerLoginByUsername(username);

            if (login == null)
                throw new InvalidOperationException("نمایندگی یافت نشد.");

            if ((bool)login.IsLocked)
                throw new InvalidOperationException("نمایندگی غیرفعال است.");

            // To make sure that there is an active dealer for this login.
            var dealer = GetDealerById((int)login.DealerId);
            if (dealer == null)
                throw new InvalidOperationException("نمایندگی یافت نشد.");

            if (!login.Password.Equals(password))
                throw new InvalidOperationException("نام کاربری یا گذرواژه اشتباه است.");

            // Fix null RegId issue.
            if (login.RegId == null)
                login.RegId = string.Empty;

            // Update user's device.
            if (!string.IsNullOrEmpty(regId) && !login.RegId.Equals(regId))
                login.RegId = regId;

            // TODO: Genrate a proper token
            var token = Guid.NewGuid().ToString();
            login.Token = token;
            login.TokenExpireDate = DateTime.Now.AddDays(7);

            login = DealerLoginRepository.Update(login, needAuthorization: false);

            var clientToken = EncodeClientToken(login.UserName, login.Token);
            return new LoginDealerResponse { Token = clientToken };
        }

        private static string EncodeClientToken(string username, string token)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(token))
                throw new ArgumentNullException();

            var text = username + ":" + token;
            var bytes = Encoding.UTF8.GetBytes(text);
            var result = Convert.ToBase64String(bytes);

            return result;
        }

        public static DealerLogin GetDealerLoginByUsername(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException();

            // TODO do somthing with ".Trim().ToLower()"
            var dealerLogin = DealerLoginRepository.GetQuery(
                dl => dl.IsActive &&
                dl.UserName.Trim().ToLower().Equals(username.Trim().ToLower())
                ).FirstOrDefault();

            return dealerLogin;
        }

        public static void ExtendDealerLoginExpirationDate(string username)
        {
            if (string.IsNullOrEmpty(username))
                throw new ArgumentNullException();

            var login = GetDealerLoginByUsername(username);
            login.TokenExpireDate = DateTime.Now.AddDays(7);
            DealerLoginRepository.Update(login, needAuthorization: false);
        }

        public static bool IsDealerLoginTokenValid(string username, string token)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(token))
                throw new ArgumentNullException();

            var dealerLogin = GetDealerLoginByUsername(username);

            if (dealerLogin == null)
                return false;

            if (!dealerLogin.Token.Equals(token))
                return false;

            if (dealerLogin.TokenExpireDate < DateTime.Now)
                return false;

            // To make sure that there is an active dealer for this login.
            var dealer = GetDealerById((int)dealerLogin.DealerId);
            if (dealer == null)
                return false;

            return true;
        }

        #endregion
    }
}
