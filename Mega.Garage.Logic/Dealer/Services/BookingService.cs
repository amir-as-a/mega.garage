﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Dealer.Validation;
using Mega.Garage.Persistence.Dealer.Entities;

namespace Mega.Garage.Logic.Dealer.Services
{
    public class BookingService
    {
        protected class BookingRepository : BaseRepository<Booking, int> { }
        protected class BookingCapacityRepository : BaseRepository<BookingCapacity, int> { }

        #region Booking
        public static Booking AddBooking(Booking booking)
        {
            var validation = new BookingValidation.BookingEntity().Validate(booking);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            booking = BookingRepository.Add(booking);
            return booking;
        }

        public static bool DeleteBooking(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var booking = BookingRepository.Get(id);

            if (booking == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = BookingRepository.SoftDelete(booking);
            return IsDeleted;
        }

        public static IEnumerable<Booking> GetAllBookings()
        {
            return BookingRepository.GetAll();
        }


        public static Booking GetBookingById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var booking = BookingRepository.Get(id);

            if (booking == null || !booking.IsActive)
                return null;

            return booking;
        }

        public static Booking UpdateBooking(Booking booking)
        {
            if (booking == null)
                throw new ArgumentNullException();

            var oldEntity = BookingRepository.Get(booking.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            booking = BookingRepository.Update(booking);
            return booking;
        }
        #endregion
        #region Booking Capacity
        public static BookingCapacity AddBookingCapacity(BookingCapacity bookingCapacity)
        {
            if (bookingCapacity == null)
                throw new ArgumentNullException();

            bookingCapacity = BookingCapacityRepository.Add(bookingCapacity);
            return bookingCapacity;
        }

        public static bool DeleteBookingCapacity(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var bookingCapacity = BookingCapacityRepository.Get(id);

            if (bookingCapacity == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = BookingCapacityRepository.SoftDelete(bookingCapacity);
            return IsDeleted;
        }

        public static IEnumerable<BookingCapacity> GetAllBookingCapacities()
        {
            return BookingCapacityRepository.GetAll();
        }
        public static IEnumerable<int> GetAllBookingCapacitiesIds()
        {
            return BookingCapacityRepository.GetQuery(x => x.IsActive).Where(x => x.TimeBoxId != null).Select(x => x.TimeBoxId.Value);
        }

        public static BookingCapacity GetBookingCapacityById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var bookingCapacity = BookingCapacityRepository.Get(id);

            if (bookingCapacity == null || !bookingCapacity.IsActive)
                return null;

            return bookingCapacity;
        }

        public static BookingCapacity UpdateBookingCapacity(BookingCapacity bookingCapacity)
        {
            if (bookingCapacity == null)
                throw new ArgumentNullException();

            var oldEntity = BookingCapacityRepository.Get(bookingCapacity.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            bookingCapacity = BookingCapacityRepository.Update(bookingCapacity);
            return bookingCapacity;
        }
        #endregion
    }
}
