﻿using Mega.Garage.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mega.Garage.Logic.Dealer.Services
{
    using Mega.Garage.Persistence.Dealer.Entities;

    public class TimeBoxService
    {
        protected class TimeBoxRepository : BaseRepository<TimeBox, int> { }
        protected class TimeBoxTypeRepository : BaseRepository<TimeBoxType, int> { }

        #region TimeBox
        public static TimeBox AddTimeBox(TimeBox timeBox)
        {
            if (timeBox == null)
                throw new ArgumentNullException();

            timeBox = TimeBoxRepository.Add(timeBox);
            return timeBox;
        }

        public static bool DeleteTimeBox(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var timeBox = TimeBoxRepository.Get(id);

            if (timeBox == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = TimeBoxRepository.SoftDelete(timeBox);
            return IsDeleted;
        }

        public static IEnumerable<TimeBox> GetAllTimeBoxes()
        {
            return TimeBoxRepository.GetAll();
        }

        public static ICollection<TimeBox> GetFreeTimeBoxes()
        {
            var bookinCapacities = BookingService.GetAllBookingCapacitiesIds().ToList();
            var timeBox = TimeBoxRepository.GetAll();
            var freeTimeBoxes = timeBox.Where(t => !bookinCapacities.Contains(t.Id));
            return freeTimeBoxes.ToList();
        }

        public static TimeBox GetTimeBoxById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var timeBox = TimeBoxRepository.Get(id);

            if (timeBox == null || !timeBox.IsActive)
                return null;

            return timeBox;
        }

        public static TimeBox UpdateTimeBox(TimeBox timeBox)
        {
            if (timeBox == null)
                throw new ArgumentNullException();

            var oldEntity = TimeBoxRepository.Get(timeBox.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            timeBox = TimeBoxRepository.Update(timeBox);
            return timeBox;
        }
        #endregion

        #region TimeBox Type
        public static TimeBoxType AddTimeBoxType(TimeBoxType timeBoxType)
        {
            if (timeBoxType == null)
                throw new ArgumentNullException();

            timeBoxType = TimeBoxTypeRepository.Add(timeBoxType);
            return timeBoxType;
        }

        public static bool DeleteTimeBoxType(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var timeBoxType = TimeBoxTypeRepository.Get(id);

            if (timeBoxType == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = TimeBoxTypeRepository.SoftDelete(timeBoxType);
            return IsDeleted;
        }

        public static IEnumerable<TimeBoxType> GetAllTimeBoxTypes()
        {
            return TimeBoxTypeRepository.GetAll();
        }

        public static TimeBoxType GetTimeBoxTypeById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var timeBoxType = TimeBoxTypeRepository.Get(id);

            if (timeBoxType == null || !timeBoxType.IsActive)
                return null;

            return timeBoxType;
        }

        public static TimeBoxType UpdateTimeBoxType(TimeBoxType timeBoxType)
        {
            if (timeBoxType == null)
                throw new ArgumentNullException();

            var oldEntity = TimeBoxTypeRepository.Get(timeBoxType.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            timeBoxType = TimeBoxTypeRepository.Update(timeBoxType);
            return timeBoxType;
        }

        #endregion
    }
}
