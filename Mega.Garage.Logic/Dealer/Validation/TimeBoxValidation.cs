﻿using FluentValidation;
using Mega.Garage.Persistence.Dealer.Entities;

namespace Mega.Garage.Logic.Dealer.Validation
{
    internal class TimeBoxValidation
    {
        internal class TimeBoxEntity : AbstractValidator<TimeBox>
        {
            public TimeBoxEntity()
            {
                RuleFor(x => x.Id).GreaterThan(0).WithMessage("شناسه وارد شده معتبر نیست!");
                RuleFor(x => x.Title).MaximumLength(250).WithMessage("عنوان نمی تواند دارای بیشتر از 250 حرف باشد!");
                RuleFor(x => x.Code).MaximumLength(50).WithMessage("شناسه نمی تواند دارای بیشتر از 50 حرف باشد!");
            }
        }
    }
}
