﻿using FluentValidation;
using Mega.Garage.Persistence.Dealer.Entities;

namespace Mega.Garage.Logic.Dealer.Validation
{
    internal class DealerLoginValidation
    {
        internal class DealerLoginEntity : AbstractValidator<DealerLogin>
        {
            public DealerLoginEntity()
            {
                RuleFor(x => x.UserName)
                    .NotEmpty().WithMessage("وارد کردن نام کاربری اجباری است.");

                RuleFor(x => x.UserName)
                    .NotNull().WithMessage("وارد کردن نام کاربری اجباری است.");

                RuleFor(x => x.Password)
                    .NotNull().WithMessage("وارد کردن رمزعبور اجباری است.");

                RuleFor(x => x.Password)
                    .NotNull().WithMessage("وارد کردن رمزعبور اجباری است.")
                    .MinimumLength(6).WithMessage("!طول رمزعبور حداقل بایستی 6 حرف باشد");

                RuleFor(x => x.DealerId)
                    .NotNull().WithMessage("انتخاب نمایندگی اجباری است")
                    .GreaterThan(0).WithMessage("انتخاب نمایندگی اجباری است");
            }
        }

    }
}
