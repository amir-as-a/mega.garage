﻿using FluentValidation;
using Mega.Garage.Persistence.Dealer.Entities;
using System;

namespace Mega.Garage.Logic.Dealer.Validation
{
    internal class BookingValidation
    {
        internal class BookingEntity : AbstractValidator<Booking>
        {
            public BookingEntity()
            {
                RuleFor(x => x.Day)
                    .NotEmpty().WithMessage("تاریخ نمی تواند خالی باشد")
                    .GreaterThanOrEqualTo(DateTime.Now).WithMessage("تاریخ وارد شده معتبر نمی باشد");

                RuleFor(x => x.CustomerFirstName)
                    .MaximumLength(150).WithMessage("نام نمی تواند دارای بیشتر از 150 حرف باشد")
                    .Must(Common.Validate.IsPersianName).WithMessage("نام وارد شده معتبر نمی باشد");

                RuleFor(x => x.CustomerLastName)
                    .MaximumLength(150).WithMessage("نام خانوادگی نمی تواند دارای بیشتر از 150 حرف باشد")
                    .Must(Common.Validate.IsPersianName).WithMessage("نام خانوادگی وارد شده معتبر نمی باشد");
            }
        }

        internal class BookingCapacitygEntity : AbstractValidator<BookingCapacity>
        {
            public BookingCapacitygEntity()
            {
            }
        }
    }
}
