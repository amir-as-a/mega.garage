﻿using FluentValidation;
using Mega.Garage.Persistence.Dealer.Entities;

namespace Mega.Garage.Logic.Dealer.Validation
{
    internal class DealerValidation
    {
        internal class DealerEntity : AbstractValidator<Persistence.Dealer.Entities.Dealer>
        {
            public DealerEntity()
            {
                RuleFor(x => x.Title)
                    .MaximumLength(250).WithMessage("عنوان نمی تواند دارای بیشتر از ۲۵۰ حرف باشد!")
                    .NotEmpty().WithMessage("عنوان نمایندگی معتبر نیست")
                    .Must(Common.Validate.IsPersianText).WithMessage("عنوان نمایندگی معتبر نیست");

                RuleFor(x => x.OwnerFirstName)
                    .MaximumLength(150).WithMessage("!نام نمی تواند دارای بیشتر از 150 حرف باشد")
                    .Must(Common.Validate.IsPersianName).WithMessage("نام وارد شده معتبر نمی باشد");

                RuleFor(x => x.OwnerLastName)
                    .MaximumLength(150).WithMessage("!نام خانوادگی نمی تواند دارای بیشتر از 150 حرف باشد")
                    .Must(Common.Validate.IsPersianName).WithMessage("نام وارد شده معتبر نمی باشد");

                RuleFor(x => x.OwnerNationalCode)
                    .Must(Common.Validate.IsNationalCode).WithMessage("کد ملی معتبر نمی باشد!");

                RuleFor(x => x.OwnerPhone)
                    .Must(Common.Validate.IsMobile).WithMessage("شماره تلفن همراه معتبر نمی باشد!");

                RuleFor(x => x.Phone)
                    .Must(Common.Validate.IsPhone).WithMessage("شماره تلفن معتبر نمی باشد!");

                RuleFor(x => x.Image)
                    .MaximumLength(300).WithMessage("!مسیر تصویر نمی تواند دارای بیشتر از 300 حرف باشد");

                RuleFor(x => x.Icon)
                    .MaximumLength(300).WithMessage("!مسیر لوگو نمی تواند دارای بیشتر از 300 حرف باشد");

                RuleFor(x => x.DocumentImage)
                    .MaximumLength(300).WithMessage("!مسیر مستندات نمی تواند دارای بیشتر از 300 حرف باشد");
            }
        }

        internal class DealerInventoryEntity : AbstractValidator<DealerInventory>
        {
            public DealerInventoryEntity()
            {
                RuleFor(x => x.Id).GreaterThan(0).WithMessage("شناسه وارد شده معتبر نیست!");
            }
        }

        internal class DealerTransactionHistoryEntity : AbstractValidator<DealerTransactionHistory>
        {
            public DealerTransactionHistoryEntity()
            {
                RuleFor(x => x.Id).GreaterThan(0).WithMessage("شناسه وارد شده معتبر نیست!");
            }
        }

    }
}
