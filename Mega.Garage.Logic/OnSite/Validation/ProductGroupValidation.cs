﻿using FluentValidation;
using Mega.Garage.Persistence.OnSite.Entities;

namespace Mega.Garage.Logic.OnSite.Validation
{
    internal class ProductGroupValidation
    {
        internal class ProductGroupEntity : AbstractValidator<ProductGroup>
        {
            public ProductGroupEntity()
            {

            }

        }

        internal class ProductGroupProductEntity : AbstractValidator<ProductGroupProduct>
        {
            public ProductGroupProductEntity()
            {

            }

        }
    }
}
