﻿namespace Mega.Garage.Logic.OnSite.Services
{
    using Mega.Garage.Common;
    using Mega.Garage.Persistence.OnSite.Entities;
    using System;
    using System.Collections.Generic;

    public class LandingPageService
    {
        protected class LandingPageRepository : BaseRepository<LandingPage, int> { }

        public static LandingPage AddLandingPage(LandingPage landingPage)
        {
            if (landingPage == null)
                throw new ArgumentNullException();

            landingPage = LandingPageRepository.Add(landingPage);
            return landingPage;
        }

        public static bool DeleteLandingPage(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var landingPage = LandingPageRepository.Get(id);
            if (landingPage == null)
                throw new InvalidDeleteOperationException();
            bool IsDeleted = LandingPageRepository.SoftDelete(landingPage);
            return IsDeleted;
        }

        public static IEnumerable<LandingPage> GetAllLandingPages()
        {
            return LandingPageRepository.GetAll();
        }

        public static LandingPage GetLandingPageById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var landingPage = LandingPageRepository.Get(id);

            if (landingPage == null || !landingPage.IsActive)
                return null;

            return landingPage;
        }

        public static LandingPage UpdateLandingPage(LandingPage landingPage)
        {
            if (landingPage == null)
                throw new ArgumentNullException();

            var oldEntity = LandingPageRepository.Get(landingPage.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            landingPage = LandingPageRepository.Update(landingPage);
            return landingPage;
        }

    }
}
