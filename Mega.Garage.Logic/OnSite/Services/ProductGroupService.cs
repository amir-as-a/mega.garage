﻿namespace Mega.Garage.Logic.OnSite.Services
{
    using ClosedXML.Excel;
    using Mega.Garage.Common;
    using Mega.Garage.Logic.OnSite.ViewModel;
    using Mega.Garage.Logic.Order.Services;
    using Mega.Garage.Persistence.OnSite.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ProductGroupService
    {
        protected class ProductGroupRepository : BaseRepository<ProductGroup, int> { }
        protected class ProductGroupProductRepository : BaseRepository<ProductGroupProduct, int> { }

        #region Product Groups
        public static ProductGroup AddProductGroup(ProductGroup productGroup)
        {
            if (productGroup == null)
                throw new ArgumentNullException();

            productGroup = ProductGroupRepository.Add(productGroup);
            return productGroup;
        }

        public static bool DeleteProductGroup(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var productGroup = ProductGroupRepository.Get(id);
            if (productGroup == null)
                throw new InvalidDeleteOperationException();
            bool IsDeleted = ProductGroupRepository.SoftDelete(productGroup);
            return IsDeleted;
        }

        public static IEnumerable<ProductGroup> GetAllProductGroups()
        {
            return ProductGroupRepository.GetAll();
        }

        public static PagedList<ProductGroup> GetAllProductGroups(ProductGroupSearchViewModel parameters)
        {

            var query = ProductGroupRepository.GetQuery(x => x.IsActive);

            if (!string.IsNullOrEmpty(parameters.Title))
                query = query.Where(category => category.Title.Contains(parameters.Title));

            query = query.OrderByDescending(x => x.CreatedDate);

            return new PagedList<ProductGroup>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList(),
            };
        }

        public static ProductGroup GetProductGroupById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var productGroup = ProductGroupRepository.Get(id);

            if (productGroup == null || !productGroup.IsActive)
                return null;

            return productGroup;
        }

        public static ProductGroup GetProductGroupByEnglishTitle(string englishTitle)
        {
            if (string.IsNullOrEmpty(englishTitle))
                throw new ArgumentNullException();

            var productGroup = ProductGroupRepository.GetQuery(item => item.IsActive == true && item.EnglishTitle == englishTitle).FirstOrDefault();

            if (productGroup == null || !productGroup.IsActive)
                return null;

            return productGroup;
        }

        public static ProductGroup UpdateProductGroup(ProductGroup productGroup)
        {
            if (productGroup == null)
                throw new ArgumentNullException();

            var oldEntity = ProductGroupRepository.Get(productGroup.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            productGroup = ProductGroupRepository.Update(productGroup);
            return productGroup;
        }
        public static int GetProductGroups()
        {
            return ProductGroupRepository.Count(x => x.IsActive);
        }
        public static IList<int?> GetProductGroupIdsByProductId(long productId)
        {
            return ProductGroupProductRepository.GetQuery(x => x.IsActive && x.ProductId == productId)
                .Select(i => i.ProductGroupId).ToList();
        }


        public static XLWorkbook GenerateExcelReport(IEnumerable<ProductGroup> catalogs)
        {
            var report = new List<ProductGroupReportViewModel>();

            foreach (var item in catalogs)
            {
                var vm = new ProductGroupReportViewModel
                {
                    Title = item.Title,
                    EnglishTitle = item.EnglishTitle,
                    Description = item.Description
                };

                report.Add(vm);
            }

            var dataTable = report.ToDataTable("عنوان گروه محصول", "عنوان انگلیسی", "توضیحات");

            var workbook = new XLWorkbook();
            workbook.Worksheets.Add(dataTable, "Report");

            return workbook;
        }
        #endregion

        #region Product Group Products
        public static ProductGroupProduct AddProductGroupProduct(ProductGroupProduct productGroupProduct)
        {
            if (productGroupProduct == null)
                throw new ArgumentNullException();

            productGroupProduct = ProductGroupProductRepository.Add(productGroupProduct);
            return productGroupProduct;
        }

        public static bool DeleteProductGroupProduct(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var productGroupProduct = ProductGroupProductRepository.Get(id);
            if (productGroupProduct == null)
                throw new InvalidDeleteOperationException();
            bool IsDeleted = ProductGroupProductRepository.SoftDelete(productGroupProduct);
            return IsDeleted;
        }

        public static IEnumerable<ProductGroupProduct> GetAllProductGroupProducts()
        {
            return ProductGroupProductRepository.GetAll();
        }

        public static ProductGroupProduct GetProductGroupProductById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var productGroupProduct = ProductGroupProductRepository.Get(id);

            if (productGroupProduct == null || !productGroupProduct.IsActive)
                return null;

            return productGroupProduct;
        }

        public static ProductGroupProduct UpdateProductGroupProduct(ProductGroupProduct productGroupProduct)
        {
            if (productGroupProduct == null)
                throw new ArgumentNullException();

            var oldEntity = ProductGroupProductRepository.Get(productGroupProduct.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            productGroupProduct = ProductGroupProductRepository.Update(productGroupProduct);
            return productGroupProduct;
        }

        public static IEnumerable<ProductGroupProduct> GetProductGroupProductsByProductGroupId(int productGroupId)
        {
            return ProductGroupProductRepository.GetQuery(productGroupProduct => productGroupProduct.IsActive == true && productGroupProduct.ProductGroupId == productGroupId).ToList();
        }
        #endregion

        public static MegaViewModel<ProductGroupViewModel> GetProductByProductGroupEnglishTitle(string englishTitle)
        {
            var OutPut = new MegaViewModel<ProductGroupViewModel>
            {
                Status = MegaStatus.Successfull
            };

            var productGroupEntity =
                ProductGroupRepository
                .GetQuery(productGroup => productGroup.IsActive == true && productGroup.EnglishTitle == englishTitle)
                .FirstOrDefault();

            if (productGroupEntity == null)
            {
                OutPut.Status = MegaStatus.Failed;
                OutPut.Messages.Add($"{englishTitle} به عنوان کد دسته بندی محصول وجود ندارد");

            }
            else
            {
                var productIds =
                    ProductGroupProductRepository
                    .GetQuery(x => x.ProductGroupId == productGroupEntity.Id && x.IsActive == true).Select(x => x.ProductId).ToList();

                var products = ProductService.GetProductsByIdList(productIds).ToList();

                OutPut.Data = new ProductGroupViewModel()
                {
                    ProductGroup = productGroupEntity,
                    Products = products
                };
            }

            return OutPut;
        }
    }
}
