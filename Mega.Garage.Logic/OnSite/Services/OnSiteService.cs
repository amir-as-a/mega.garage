﻿//using Mega.Garage.Logic.Catalog.Services;
//using Mega.Garage.Logic.FileUpload.Services;
//using Mega.Garage.Logic.Order.Services;
//using Mega.Garage.Mongo.Helpers;
//using Mega.Garage.Mongo.Models;
//using Mega.Garage.Mongo.Services;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Brand = Mega.Garage.Persistence.Catalog.Entities.Brand;
//using Category = Mega.Garage.Persistence.Catalog.Entities.Category;
//using Color = Mega.Garage.Persistence.Catalog.Entities.Color;
//using Product = Mega.Garage.Persistence.Order.Entities.Product;
//using ProductGroup = Mega.Garage.Persistence.OnSite.Entities.ProductGroup;

//namespace Mega.Garage.Logic.OnSite.Services
//{
//    public class OnSiteService
//    {
//        #region Brand

//        private static Taxonomy CreateBrandTaxonomy(Brand brand)
//        {
//            var taxonomy = new Taxonomy
//            {
//                Id = MongoDocumentIdPrefix.Brand + brand.Id,
//                Title = brand.Title,
//                TaxonomyTypes = new List<string> { MongoDocumentType.Brand },
//                Images = new List<Image>()
//            };

//            try
//            {
//                var uri = new Uri(UploadService.GetCDNFilePath(brand.LogoImagePath));
//                var image = new Image { Source = uri };
//                taxonomy.Images.Add(image);
//            }
//            catch (Exception)
//            {
//            }

//            return taxonomy;
//        }

//        public static void AddBrandTaxonomy(Brand brand)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                var taxonomy = CreateBrandTaxonomy(brand);
//                mongo.AddTaxonomy(taxonomy);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void DeleteBrandTaxonomy(string brandId)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                mongo.DeleteTaxonomy(MongoDocumentIdPrefix.Brand + brandId);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void UpdateBrandTaxonomy(Brand brand)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                var taxonomy = CreateBrandTaxonomy(brand);
//                mongo.UpdateTaxonomy(taxonomy);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void SyncBrandTaxonomies()
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                mongo.DeleteTaxonomiesByType(MongoDocumentType.Brand);

//                var brands = BrandService.GetAllBrands();
//                var taxonomies = brands.Select(CreateBrandTaxonomy).ToList();
//                mongo.BatchAddTaxonomies(taxonomies);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        #endregion

//        #region Color

//        private static Taxonomy CreateColorTaxonomy(Color color)
//        {
//            return new Taxonomy
//            {
//                Id = MongoDocumentIdPrefix.Color + color.Id,
//                Title = color.Title,
//                TaxonomyTypes = new List<string> { MongoDocumentType.Color },
//                Hex = color.ColorHexCode
//            };
//        }

//        public static void AddColorTaxonomy(Color color)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                var taxonomy = CreateColorTaxonomy(color);
//                mongo.AddTaxonomy(taxonomy);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void DeleteColorTaxonomy(string colorId)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                mongo.DeleteTaxonomy(MongoDocumentIdPrefix.Color + colorId);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void UpdateColorTaxonomy(Color color)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                var taxonomy = CreateColorTaxonomy(color);
//                mongo.UpdateTaxonomy(taxonomy);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void SyncColorTaxonomies()
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                mongo.DeleteTaxonomiesByType(MongoDocumentType.Color);

//                var colors = ColorService.GetAllColors();
//                var taxonomies = colors.Select(CreateColorTaxonomy).ToList();
//                mongo.BatchAddTaxonomies(taxonomies);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        #endregion

//        #region Product

//        private static Mongo.Models.Product CreateMongoProduct(Product product)
//        {
//            var catalog = CatalogService.GetCatalogById((int)product.CatalogId);

//            var result = new Mongo.Models.Product();

//            result.Id = MongoDocumentIdPrefix.Product + product.Id.ToString();
//            result.Description = catalog.Description;
//            result.Images = new List<Image>();
//            result.Price = (long)product.BasePrice;
//            result.SoldOut = false;
//            result.Title = catalog.Title;
//            result.WebsiteBoostScore = catalog.WebsiteBoostScore ?? 0;
//            result.Categories = new List<string> { MongoDocumentIdPrefix.Category + catalog.CategoryId.ToString() };

//            result.VariationTypes = new List<VariationType>
//                {
//                    new VariationType
//                    {
//                        Max = 1,
//                        Min = 1,
//                        TaxonomyTypes = new List<string> { MongoDocumentType.Color }
//                    }
//                };

//            result.Taxonomies = new List<string>
//                {
//                    MongoDocumentIdPrefix.Brand + catalog.BrandId.ToString(),
//                    MongoDocumentIdPrefix.Color + product.ColorId.ToString()
//                };

//            if (product.ColorId != null && product.ColorAdditiveCost != null)
//            {
//                result.Variations = new List<Variation>
//                {
//                    new Variation
//                    {
//                        Id = product.ColorId.ToString(),
//                        Taxonomies = MongoDocumentIdPrefix.Color + product.ColorId.ToString(),
//                        Price = (long)product.ColorAdditiveCost,
//                        Disabled = false,
//                        Selected = true,
//                        Images =  new List<Image>(),
//                    }
//                };

//                // Trying to add product variation color image.
//                try
//                {
//                    var uri = new Uri(UploadService.GetCDNFilePath(product.ColorImage));
//                    var image = new Image { Source = uri };
//                    result.Variations.FirstOrDefault().Images.Add(image);
//                }
//                catch (Exception)
//                {
//                }
//            }

//            // Trying to add product image.
//            try
//            {
//                var uri = new Uri(UploadService.GetCDNFilePath(catalog.ImagePath));
//                var image = new Image { Source = uri };
//                result.Images.Add(image);
//            }
//            catch (Exception)
//            {
//            }

//            // Adding product groups.
//            var productGroups = ProductGroupService.GetProductGroupIdsByProductId(product.Id);

//            foreach (var item in productGroups)
//            {
//                result.Taxonomies.Add(MongoDocumentIdPrefix.ProductGroup + item);
//            }

//            return result;
//        }

//        public static void AddMongoProduct(Product product)
//        {
//            try
//            {
//                var mongoProduct = CreateMongoProduct(product);
//                IMongoService mongo = new MongoService();
//                mongo.AddProduct(mongoProduct);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void UpdateMongoProduct(Product product)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                var mongoProduct = CreateMongoProduct(product);
//                mongo.UpdateProduct(mongoProduct);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void DeleteMongoProduct(long productId)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                mongo.DeleteProduct(MongoDocumentIdPrefix.Product + productId);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void SyncMongoProducts()
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();

//                // TODO: delete all products at once. BatchDelete in Garage.Mongo.
//                var mongoProducts = mongo.GetAllProducts();
//                foreach (var item in mongoProducts)
//                {
//                    mongo.DeleteProduct(item.Id);
//                }

//                // TODO: insert all products at once. BatchAdd in Garage.Mongo. 
//                var sqlProducts = ProductService.GetAllProducts();
//                foreach (var item in sqlProducts)
//                {
//                    var product = CreateMongoProduct(item);
//                    mongo.AddProduct(product);
//                }
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        #endregion

//        #region Product Group

//        private static Taxonomy CreateProductGroupTaxonomy(ProductGroup productGroup)
//        {
//            var taxonomy = new Taxonomy
//            {
//                Id = MongoDocumentIdPrefix.ProductGroup + productGroup.Id,
//                Title = productGroup.Title,
//                TaxonomyTypes = new List<string> { MongoDocumentType.ProductGroup },
//                Images = new List<Image>()
//            };

//            try
//            {
//                var uri = new Uri(UploadService.GetCDNFilePath(productGroup.ImagePath));
//                var image = new Image { Source = uri };
//                taxonomy.Images.Add(image);
//            }
//            catch (Exception)
//            {
//            }

//            return taxonomy;
//        }

//        public static void AddProductGroupTaxonomy(ProductGroup productGroup)
//        {
//            try
//            {
//                var mongoProductGroup = CreateProductGroupTaxonomy(productGroup);
//                IMongoService mongo = new MongoService();
//                mongo.AddTaxonomy(mongoProductGroup);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void UpdateProductGroupTaxonomy(ProductGroup productGroup)
//        {
//            try
//            {
//                var mongoProductGroup = CreateProductGroupTaxonomy(productGroup);
//                IMongoService mongo = new MongoService();
//                mongo.UpdateTaxonomy(mongoProductGroup);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void DeleteProductGroupTaxonomy(long productGroupId)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                mongo.DeleteTaxonomy(MongoDocumentIdPrefix.ProductGroup + productGroupId);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void SyncProductGroupTaxonomies()
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                mongo.DeleteTaxonomiesByType(MongoDocumentType.ProductGroup);

//                var productGroups = ProductGroupService.GetAllProductGroups();
//                var taxonomies = productGroups.Select(CreateProductGroupTaxonomy).ToList();
//                mongo.BatchAddTaxonomies(taxonomies);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        #endregion

//        #region Category

//        private static Mongo.Models.Category CreateMongoCategory(Category category)
//        {
//            var result = new Mongo.Models.Category
//            {
//                Id = MongoDocumentIdPrefix.Category + category.Id,
//                Title = category.Title,
//                EnglishTitle = category.EnglishTitle,
//                AddressLink = category.AddressLink,
//                CategoryCode = category.CategoryCode,
//                Description = category.Description,
//                IsMainMenu = category.IsMainMenu,
//                LevelNumber = category.LevelNumber ?? 0,
//                ParentCategoryId = MongoDocumentIdPrefix.Category + category.ParentCategoryId,
//                SortNumber = category.SortNumber ?? 0,
//                ThumbImagePath = new List<Image>(),
//                WideImagePath = new List<Image>()
//            };

//            try
//            {
//                var uri = new Uri(UploadService.GetCDNFilePath(category.ThumbImagePath));
//                var image = new Image { Source = uri };
//                result.ThumbImagePath.Add(image);
//            }
//            catch (Exception)
//            {
//            }

//            try
//            {
//                var uri = new Uri(UploadService.GetCDNFilePath(category.WideImagePath));
//                var image = new Image { Source = uri };
//                result.WideImagePath.Add(image);
//            }
//            catch (Exception)
//            {
//            }

//            return result;
//        }

//        public static void AddMongoCategory(Category category)
//        {
//            try
//            {
//                var mongoCategory = CreateMongoCategory(category);
//                IMongoService mongo = new MongoService();
//                mongo.AddCategory(mongoCategory);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void UpdateMongoCategory(Category category)
//        {
//            try
//            {
//                var mongoCategory = CreateMongoCategory(category);
//                IMongoService mongo = new MongoService();
//                mongo.UpdateCategory(mongoCategory);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void DeleteMongoCategory(int categoryId)
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();
//                mongo.DeleteCategory(MongoDocumentIdPrefix.Category + categoryId);
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        public static void SyncMongoCategories()
//        {
//            try
//            {
//                IMongoService mongo = new MongoService();

//                // TODO: delete all categories at once. BatchDelete in Garage.Mongo.
//                var mongoCategories = mongo.GetAllCategories();
//                foreach (var item in mongoCategories)
//                {
//                    mongo.DeleteCategory(item.Id);
//                }

//                // TODO: insert all categories at once. BatchAdd in Garage.Mongo. 
//                var sqlCategories = CategoryService.GetAllCategories();
//                foreach (var item in sqlCategories)
//                {
//                    var category = CreateMongoCategory(item);
//                    mongo.AddCategory(category);
//                }
//            }
//            catch (Exception exception)
//            {
//                throw exception;
//            }
//        }

//        #endregion
//    }
//}
