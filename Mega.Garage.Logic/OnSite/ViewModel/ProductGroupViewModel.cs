﻿using Mega.Garage.Persistence.OnSite.Entities;
using Mega.Garage.Persistence.Order.Entities;
using System.Collections.Generic;

namespace Mega.Garage.Logic.OnSite.ViewModel
{
    public class ProductGroupViewModel
    {
       
        public ProductGroup ProductGroup { get; set; }
        public List<Product> Products { get; set; }
    }
}
