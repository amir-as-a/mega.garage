﻿namespace Mega.Garage.Logic.OnSite.ViewModel
{
    public class ProductGroupReportViewModel
    {
        public string Title { get; set; }
        public string EnglishTitle { get; set; }
        public string Description { get; set; }
    }
}
