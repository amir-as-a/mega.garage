﻿namespace Mega.Garage.Logic.OnSite.ViewModel
{
    public class ProductGroupSearchViewModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string Title { get; set; }
    }
}
