﻿using System;

namespace Mega.Garage.Logic.Order.ViewModel
{
    public class InvoiceSearchViewModel
    {
        public int? CustomerId { get; set; }
        public bool? IsPayed { get; set; }
        public int? ProductGroup { get; set; }
        public string CustomerName { get; set; }        
        public string CustomerMobile { get; set; }
        public string OrderNumber { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string DiscountCode { get; set; }
        public int? PaymentTypeId { get; set; }
        public int? DealerId { get; set; }
        public int? CurrentProcessId { get; set; }
        public int PageSize { get; set; }
        public int CurrentPage { get; set; }

    }
}
