﻿namespace Mega.Garage.Logic.Order.ViewModel
{
    public class ProductReportViewModel
    {
        public string CatalogTitle { get; set; }
        public int Count { get; set; }
    }
}
