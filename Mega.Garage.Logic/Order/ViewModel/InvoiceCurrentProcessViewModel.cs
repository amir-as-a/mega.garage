﻿using System;

namespace Mega.Garage.Logic.Order.ViewModel
{
    public class InvoiceCurrentProcessRequestViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class InvoiceCurrentProcessResponseViewModel
    {
        public int CurrentProcess { get; set; }
        public string CurrentProcessTitle { get; set; }
        public int Count { get; set; }
    }
}
