﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mega.Garage.Persistence.Order.Entities;

namespace Mega.Garage.Logic.Order.ViewModel
{
    public class BulkDiscountViewModel
    {
        [DisplayName("تعداد کد تخفیف")]
        public int Count { get; set; }
        [DisplayName("پیشوند کد تخفیف")]
        public string Prefix { get; set; }
        [DisplayName("کمترین عدد کد تخفیف")]
        public int Min { get; set; }
        [DisplayName("بیشترین عدد کد تخفیف")]
        public int Max { get; set; }
        public Persistence.Order.Entities.Discount Discount { get; set; }
    }
}
