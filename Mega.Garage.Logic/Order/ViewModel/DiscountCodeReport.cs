﻿namespace Mega.Garage.Logic.Order.ViewModel
{
    public class DiscountCodeReport
    {
        public string DiscountCode { get; set; }
        public int Count { get; set; }
    }
}
