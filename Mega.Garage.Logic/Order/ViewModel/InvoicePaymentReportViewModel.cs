﻿using System;

namespace Mega.Garage.Logic.Order.ViewModel
{
    public class InvoicePaymentReportRequestViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class InvoicePaymentReportResponseViewModel
    {
        public int PaymentType { get; set; }
        public string PaymentTypeTitle { get; set; }
        public int Count { get; set; }
    }
}
