﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Order.ViewModel
{
   public class InvoiceTotalFinalPricePerMonthViewModel
    {
        public string Month { get; set; }
        public int FinalPrice { get; set; }
        public override string ToString()
        {
            return $"Month = {Month} , FinalPrice = {FinalPrice}";
        }
    }
}
