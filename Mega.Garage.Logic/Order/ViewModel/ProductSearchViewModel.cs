﻿namespace Mega.Garage.Logic.Order.ViewModel
{
    public class ProductSearchViewModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string Title { get; set; }
        public int? Catalog { get; set; }
        public double? FromPrice { get; set; }
        public double? ToPrice { get; set; }
    }
}
