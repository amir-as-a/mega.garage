﻿namespace Mega.Garage.Logic.Order.ViewModel
{
    public class InvoiceCountPerMonthReportViewModel
    {
        public string Month { get; set; }
        public int Count { get; set; }
        public override string ToString()
        {
            return $"Month = {Month} , Count = {Count}";
        }
    }
}
