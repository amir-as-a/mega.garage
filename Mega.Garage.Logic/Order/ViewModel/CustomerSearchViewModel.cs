﻿using System;

namespace Mega.Garage.Logic.Order.ViewModel
{
    public class CustomerSearchViewModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string DiscountCode { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
    }
}
