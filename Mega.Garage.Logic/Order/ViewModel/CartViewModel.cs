﻿namespace Mega.Garage.Logic.Order.ViewModel
{
    public class CartViewModel
    {
        public int ProductGroupId { get; set; }
        public long ProductId { get; set; }
        public int Count { get; set; }
    }
}