﻿using System.Collections.Generic;

namespace Mega.Garage.Logic.Order.ViewModel
{
    public class InvoiceViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string CarGroup { get; set; }
        public string DiscountCode { get; set; }
        public int? Dealer { get; set; }
        /// <summary>
        /// روش پرداخت
        /// 1. در محل
        /// 2. آنلاین
        /// </summary>
        public int Payment { get; set; }
        /// <summary>
        /// نوع پلت فرم کاربر
        /// 1. وب
        /// 2. اپ موبایل
        /// </summary>
        public int Platform { get; set; }
        public int IPGId { get; set; } = 1;
        public List<CartViewModel> Carts { get; set; }
    }
}
