﻿using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Persistence.Order.Entities;
using System.Collections.Generic;

namespace Mega.Garage.Logic.Order.ViewModel
{
    using Persistence.Dealer.Entities;
    public class FullReportByCustomerViewModel
    {
        public FullReportByCustomerViewModel()
        {
            Invoices = new List<Invoice>();
            Dealers = new List<Dealer>();
            DealerServices = new List<DealerService>();
        }
        public List<Invoice> Invoices { get; set; }
        public List<Dealer> Dealers { get; set; }
        public List<DealerService> DealerServices { get; set; }
    }
}
