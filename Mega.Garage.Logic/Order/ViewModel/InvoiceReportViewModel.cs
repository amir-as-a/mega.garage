﻿using Mega.Garage.Persistence.Order.Entities;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Mega.Garage.Logic.Order.ViewModel
{
    public class InvoiceReportViewModel
    {
        [Display(Name = "کاربر")]
        public string CustomerName { get; set; }
        
        [Display(Name = "کد فاکتور")]
        public string InvoiceCode { get; set; }
        [Display(Name = "تاریخ فاکتور")]
        public DateTime InvoiceDate { get; set; }
        
        [Display(Name = "شماره سفارش")]
        public string OrderNumber { get; set; }
       
        [Display(Name = "مبلغ کل")]
        public double? TotalAmount { get; set; }
       
        [Display(Name = "کد تخفیف")]
        public string DiscountCode { get; set; }
        [Display(Name = "تخفیف کل")]
        public double? TotalDiscount { get; set; }
        [Display(Name = "قیمت نهایی")]
        public double? FinalPrice { get; set; }
    }
}
