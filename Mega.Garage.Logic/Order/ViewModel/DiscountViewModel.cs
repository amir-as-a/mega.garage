﻿namespace Mega.Garage.Logic.Order.ViewModel
{
    public class DiscountViewModel
    {
        public DiscountViewModel(string message)
        {
            Message = message;
        }

        public DiscountViewModel(double value)
        {
            Value = value;
        }

        public string Message { get; set; }
        public double? Value { get; set; }
    }
}
