﻿using System.ComponentModel;

namespace Mega.Garage.Logic.Order.ViewModel
{
    public class CustomerReportViewModel
    {
        [DisplayName("شناسه")]
        public int Id { get; set; }
        [DisplayName("نام")]
        public string FirstName { get; set; }
        [DisplayName("نام خانوادگی")]
        public string LastName { get; set; }
        [DisplayName("موبایل")]
        public string Mobile { get; set; }
        [DisplayName("ایمیل")]
        public string Email { get; set; }
    }
}
