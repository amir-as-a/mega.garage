﻿using FluentValidation;
using Mega.Garage.Persistence.Order.Entities;

namespace Mega.Garage.Logic.Order.Validation
{
    internal class ProductValidation
    {
        internal class ProductEntity : AbstractValidator<Product>
        {
            public ProductEntity()
            {
                RuleFor(x => x.Code)
                    .NotEmpty().WithMessage("وارد کردن کد محصول اجباری است.");

                RuleFor(x => x.CatalogId)
                    .NotNull().WithMessage("محصول به درستی انتخاب نشده است.")
                    .NotEmpty().WithMessage("محصول به درستی انتخاب نشده است.")
                    .GreaterThan(0).WithMessage("محصول به درستی انتخاب نشده است.");

                //RuleFor(x => x.CatalogTitle)
                //    .NotNull().WithMessage("عنوان محصول به درستی وارد نشده است.")
                //    .NotEmpty().WithMessage("عنوان محصول به درستی وارد نشده است.");

                //RuleFor(x => x.CatalogCode)
                //    .NotNull().WithMessage("کد محصول به درستی وارد نشده است.")
                //    .NotEmpty().WithMessage("کد محصول به درستی وارد نشده است.");

                RuleFor(x => x.BasePrice)
                    .NotNull().WithMessage("قیمت پایه محصول به درستی وارد نشده است.")
                    .NotEmpty().WithMessage("قیمت پایه محصول به درستی وارد نشده است.")
                    .GreaterThan(0).WithMessage("قیمت پایه محصول به درستی وارد نشده است.");

                RuleFor(x => x.ColorId)
                    .NotNull().WithMessage("رنگ محصول به درستی انتخاب نشده است.")
                    .NotEmpty().WithMessage("رنگ محصول به درستی انتخاب نشده است.");
            }
        }
    }
}
