﻿using FluentValidation;
using Mega.Garage.Persistence.Order.Entities;

namespace Mega.Garage.Logic.Order.Validation
{
    internal class CartValidation
    {
        internal class CartEntity : AbstractValidator<Cart>
        {
            public CartEntity()
            {
                RuleFor(x => x.UserId)
                    .NotNull().WithMessage("شناسه کاربر وارد شده معتبر نیست.")
                    .GreaterThan(0).WithMessage("شناسه کاربر وارد شده معتبر نیست.");

                RuleFor(x => x.CustomerId)
                    .NotNull().WithMessage("شناسه مشتری وارد شده صحیح نیست.")
                    .GreaterThan(0).WithMessage("شناسه مشتری وارد شده صحیح نیست.");
            }
        }

        internal class CartItemEntity : AbstractValidator<CartItem>
        {
            public CartItemEntity()
            {
                RuleFor(x => x.ProductId)
                    .NotNull().WithMessage("شناسه محصول وارد شده معتبر نیست.")
                    .GreaterThan(0).WithMessage("شناسه محصول وارد شده معتبر نیست.");

                RuleFor(x => x.OrderQty)
                    .NotNull().WithMessage("تعداد وارد شده صحیح نیست.")
                    .GreaterThan(0).WithMessage("تعداد وارد شده صحیح نیست.");

                RuleFor(x => x.FinalPrice)
                    .NotNull().WithMessage("قیمت نهایی به درستی وارد نشده است.")
                    .GreaterThan(0).WithMessage("قیمت نهایی به درستی وارد نشده است.");

                RuleFor(x => x.ProductTitle)
                    .NotNull().WithMessage("عنوان محصول به درستی وارد نشده است.")
                    .NotEmpty().WithMessage("عنوان محصول به درستی وارد نشده است.");
            }
        }

        internal class CartViewModelEntity : AbstractValidator<ViewModel.InvoiceViewModel>
        {
            public CartViewModelEntity()
            {
                RuleFor(x => x.FirstName)
                    .NotNull().WithMessage("وارد کردن نام الزامی است.")
                    .NotEmpty().WithMessage("وارد کردن نام الزامی است.");

                RuleFor(x => x.LastName)
                    .NotNull().WithMessage("وارد کردن نام خانوادگی الزامی است.")
                    .NotEmpty().WithMessage("وارد کردن نام خانوادگی الزامی است.");

                RuleFor(x => x.Mobile)
                    .NotNull().WithMessage("وارد کردن تلفن همراه الزامی است.")
                    .NotEmpty().WithMessage("وارد کردن تلفن همراه الزامی است.")
                    .Must(Common.Validate.IsMobile).WithMessage("تلفن همراه وارد شده صحیح نیست.");

                //RuleFor(x => x.CarGroup)
                //    .NotNull().WithMessage("انتخاب کردن گروه خودرو الزامی است.")
                //    .NotEmpty().WithMessage("انتخاب کردن گروه خودرو الزامی است.");

                RuleForEach(x => x.Carts)
                    .SetValidator(new CartValidator());
            }

            internal class CartValidator : AbstractValidator<ViewModel.CartViewModel>
            {
                public CartValidator()
                {
                    RuleFor(x => x.Count)
                        .NotNull().WithMessage("تعداد وارد شده برای محصول صحیح نیست.")
                        .GreaterThan(0).WithMessage("تعداد وارد شده باید بیشتر از 0 باشد.");

                    RuleFor(x => x.ProductId)
                        .NotNull().WithMessage("شناسه محصول وارد شده صحیح نیست.")
                        .GreaterThan(0).WithMessage("شناسه محصول وارد شده صحیح نیست.");

                }
            }

        }
    }
}
