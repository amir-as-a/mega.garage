﻿using FluentValidation;
using Mega.Garage.Persistence.Order.Entities;

namespace Mega.Garage.Logic.Order.Validation
{
    internal class DiscountValidation
    {

        internal class DiscountEntity : AbstractValidator<Discount>
        {
            public DiscountEntity()
            {
                RuleFor(x => x.Id)
                    .GreaterThanOrEqualTo(0).WithMessage("شناسه وارد شده معتبر نیست");

                RuleFor(x => x.Code)
                    .NotEmpty().WithMessage("کد تخفیف نمی تواند خالی باشد");

                RuleFor(x => x.Code)
                    .MaximumLength(50).WithMessage("کد تخفیف نمی تواند بیشتر از 50 حرف باشد");

                RuleFor(x => x.AmountPrice)
                    .GreaterThan(0).WithMessage("مبلغ وارد شده باید بیشتر از صفر باشد");

                RuleFor(x => x.FromDate)
                    .LessThan(x => x.ToDate).WithMessage("بازه زمانی کد تخفیف معتبر نیست");

                RuleFor(x => x.Percent)
                    .ExclusiveBetween(1, 100).WithMessage("درصد تخفیف باید بین 1 تا 100 درصد باشد");

                RuleFor(x => x.MaximumAmountLimit)
                    .GreaterThan(0).WithMessage("محدوده قیمت نباید کمتر از صفر باشد");
            }
        }

        internal class BulkDiscountEntity : AbstractValidator<ViewModel.BulkDiscountViewModel>
        {
            public BulkDiscountEntity()
            {
                RuleFor(x => x.Count)
                    .NotNull().NotEmpty().WithMessage("تعداد کد تخفیف می بایستی پر شود و بزرگتر از 0 باشد.");

                RuleFor(x => x.Prefix)
                    .MaximumLength(4).WithMessage("پیشوند نمی توانند بیشتر از 4 حرف باشد.");

                RuleFor(x => x.Min)
                    .InclusiveBetween(0, 99998).WithMessage("کمترین عدد کد تخفیف می توانند بین 0 تا 99998 باشد.")
                    .NotEqual(x => x.Max).WithMessage("کمترین عدد و بیشترین عدد کد تخفیف نباید یکسان باشد.");

                RuleFor(x => x.Max)
                    .InclusiveBetween(1, 99999).WithMessage("بیشترین رقم کد تخفیف می توانند بین 1 تا 99999 باشد.");

                RuleFor(x => x.Discount.Id)
                        .GreaterThanOrEqualTo(0).WithMessage("شناسه وارد شده معتبر نیست");

                RuleFor(x => x.Discount.Code)
                    .MaximumLength(50).WithMessage("کد تخفیف نمی تواند بیشتر از 50 حرف باشد");

                RuleFor(x => x.Discount.AmountPrice)
                    .GreaterThan(0).WithMessage("مبلغ وارد شده باید بیشتر از صفر باشد");

                RuleFor(x => x.Discount.FromDate)
                    .LessThan(x => x.Discount.ToDate).WithMessage("بازه زمانی کد تخفیف معتبر نیست");

                RuleFor(x => x.Discount.Percent)
                    .ExclusiveBetween(1, 100).WithMessage("درصد تخفیف باید بین 1 تا 100 درصد باشد");

                RuleFor(x => x.Discount.MaximumAmountLimit)
                    .GreaterThan(0).WithMessage("محدوده قیمت نباید کمتر از صفر باشد");

            }
        }
    }
}
