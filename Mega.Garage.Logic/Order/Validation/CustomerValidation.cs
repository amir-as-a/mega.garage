﻿using FluentValidation;
using Mega.Garage.Persistence.Order.Entities;

namespace Mega.Garage.Logic.Order.Validation
{
    internal class CustomerValidation
    {
        internal class CustomerEntity : AbstractValidator<Customer>
        {
            public CustomerEntity()
            {

                RuleFor(x => x.FirstName)
                    .NotNull().WithMessage("وارد کردن نام مشتری اجباری است.")
                    .MinimumLength(3).WithMessage("وارد کردن نام مشتری اجباری است.");

                RuleFor(x => x.LastName)
                    .NotNull().WithMessage("وارد کردن نام خانوادگی مشتری اجباری است.")
                    .MinimumLength(3).WithMessage("وارد کردن نام خانوادگی مشتری اجباری است.");

                RuleFor(x => x.Mobile)
                    .NotNull().WithMessage("وارد کردن تلفن همراه اجباری است.")
                    .NotEmpty().WithMessage("وارد کردن تلفن همراه اجباری است.")
                    .Must(Common.Validate.IsMobile).WithMessage("تلفن همراه وارد شده صحیح نیست.");

                //RuleFor(x => x.NationalCode)
                //    .NotNull().WithMessage("کد ملی وارد شده صحیح نیست.")
                //    .Must(Common.Validate.IsNationalCode).WithMessage("کد ملی وارد شده صحیح نیست.");
            }
        }

    }
}
