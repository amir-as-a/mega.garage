﻿using FluentValidation;
using Mega.Garage.Persistence.Entities;
using Mega.Garage.Persistence.Order.Entities;

namespace Mega.Garage.Logic.Order.Validation
{
    internal class OrderValidation
    {
        internal class InvoiceEntity : AbstractValidator<Invoice>
        {
            public InvoiceEntity()
            {
                RuleFor(x => x.CartId)
                    .NotNull().WithMessage("شناسه سبدخرید وارد شده معتبر نیست.")
                    .GreaterThan(0).WithMessage("شناسه سبدخرید وارد شده معتبر نیست.");

                RuleFor(x => x.CustomerId)
                    .NotNull().WithMessage("شناسه کاربر وارد شده معتبر نیست.")
                    .GreaterThan(0).WithMessage("شناسه کاربر وارد شده معتبر نیست.");

                RuleFor(x => x.DealerId)
                    .NotNull().WithMessage("شناسه نمایندگی وارد شده معتبر نیست.")
                    .GreaterThan(0).WithMessage("شناسه نمایندگی وارد شده معتبر نیست.");

                RuleFor(x => x.InvoiceCode)
                    .NotNull().WithMessage("کد فاکتور وارد شده معتبر نیست.")
                    .NotEmpty().WithMessage("کد فاکتور وارد شده معتبر نیست.")
                    .MinimumLength(3).WithMessage("کد فاکتور وارد شده معتبر نیست.");

                RuleFor(x => x.OrderNumber)
                    .NotNull().WithMessage("شماره سفارش وارد شده معتبر نیست.")
                    .NotEmpty().WithMessage("شماره سفارش وارد شده معتبر نیست.")
                    .MinimumLength(3).WithMessage("شماره سفارش وارد شده معتبر نیست.");
            }
        }

        internal class OrderItemEntity : AbstractValidator<OrderItem>
        {
            public OrderItemEntity()
            {

            }
        }

        internal class OrderProcessHistoryEntity : AbstractValidator<OrderProcessHistory>
        {
            public OrderProcessHistoryEntity()
            {

            }
        }
    }
}
