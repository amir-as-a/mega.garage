﻿using FluentValidation;
using Mega.Garage.Persistence.Order.Entities;

namespace Mega.Garage.Logic.Order.Validation
{
    internal class CampaignValidation
    {
        internal class CampaignEntity : AbstractValidator<Campaign>
        {
            public CampaignEntity()
            {
                RuleFor(x => x.Title)
                    .NotEmpty().WithMessage("وارد کردن عنوان کمپین اجباری است.")
                    .MinimumLength(3).WithMessage("وارد کردن عنوان کمپین اجباری است.");

                RuleFor(x => x.Code)
                    .NotEmpty().WithMessage("وارد کردن کد کمپین اجباری است.");

                RuleFor(x => x.CampaignTypeId)
                    .GreaterThan(0).WithMessage("انتخاب کردن نوع کمپین اجباری است.");
            }
        }

        internal class CampaignTypeEntity : AbstractValidator<CampaignType>
        {
            public CampaignTypeEntity()
            {
                RuleFor(x => x.Title)
                    .NotEmpty().WithMessage("وارد کردن عنوان نوع کمپین اجباری است.")
                    .MinimumLength(3).WithMessage("عنوان باید بیشتر از 3 حرف باشد.");

                RuleFor(x => x.Code)
                    .NotEmpty().WithMessage("وارد کردن کد نوع کمپین اجباری است.")
                    .MaximumLength(5).WithMessage("طول رشته نمی تواند بیشتر از 5 حرف باشد.");
            }
        }

        internal class CampaignProductEntity : AbstractValidator<CampaignProduct>
        {

            public CampaignProductEntity()
            {
                RuleFor(x => x.CampaignId)
                    .NotNull().WithMessage("کمپین وارد شده معتبر نیست.")
                    .NotEmpty().WithMessage("کمپین وارد شده معتبر نیست.")
                    .GreaterThan(0).WithMessage("کمپین وارد شده معتبر نیست.");

                RuleFor(x => x.ProductId)
                    .NotNull().WithMessage("محصول وارد شده معتبر نیست.")
                    .NotEmpty().WithMessage("محصول وارد شده معتبر نیست.")
                    .GreaterThan(0).WithMessage("محصول وارد شده معتبر نیست.");

                RuleFor(x => x.MaxOrderQty)
                    .NotNull().WithMessage("حداکثر تعداد سفارش وارد شده معتبر نیست.")
                    .NotEmpty().WithMessage("حداکثر تعداد سفارش وارد شده معتبر نیست.")
                    .GreaterThan(0).WithMessage("حداکثر تعداد سفارش وارد شده معتبر نیست.");

                RuleFor(x => x.MinOrderQty)
                    .NotNull().WithMessage("حداقل تعداد سفارش وارد شده معتبر نیست.")
                    .NotEmpty().WithMessage("حداقل تعداد سفارش وارد شده معتبر نیست.")
                    .GreaterThan(0).WithMessage("حداقل تعداد سفارش وارد شده معتبر نیست.");

                RuleFor(x => x.Qty)
                    .NotNull().WithMessage("تعداد وارد شده صحیح نیست")
                    .NotEmpty().WithMessage("تعداد وارد شده صحیح نیست")
                    .GreaterThan(0).WithMessage("تعداد وارد شده صحیح نیست");

                RuleFor(x => x.MaxOrderQty)
                    .GreaterThanOrEqualTo(x => x.MinOrderQty).WithMessage("حداقل تعداد سفارش نمی تواند از تعداد حداکثر بیشتر باشد.");
            }
        }
    }
}
