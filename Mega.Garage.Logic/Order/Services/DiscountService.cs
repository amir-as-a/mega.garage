﻿using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Order.Validation;
using Mega.Garage.Logic.Order.ViewModel;
using Mega.Garage.Persistence.Order.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Mega.Garage.Logic.Order.Services
{
    public class DiscountService
    {
        protected class DiscountRepository : BaseRepository<Discount, int> { }
        protected class DiscountProductRepository : BaseRepository<DiscountProduct, int> { }
        protected class DiscountBulkRepository : Repository<Discount, int> { }

        public static Discount AddDiscount(Discount discount)
        {
            var validation = new DiscountValidation.DiscountEntity().Validate(discount);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            discount = DiscountRepository.Add(discount);
            return discount;
        }

        public static bool DeleteDiscount(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var discount = DiscountRepository.Get(id);
            if (discount == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = DiscountRepository.SoftDelete(discount);
            return isDeleted;
        }
        public static IPagedList<Discount> GetAllPagedListDiscounts(DateTime? fromDate, DateTime? toDate, string code, int currentPage, int pageSize)
        {

            IQueryable<Discount> queryableDiscount = DiscountRepository.GetQuery();
            queryableDiscount = queryableDiscount.Where(x => x.IsActive);
            if (fromDate.HasValue)
            {
                DateTime FromDateValue = fromDate.Value;
                queryableDiscount = queryableDiscount.Where(x => x.FromDate >= FromDateValue);
            }
            if (toDate.HasValue)
            {
                DateTime ToDateValue = toDate.Value.AddDays(1).AddSeconds(-1);
                queryableDiscount = queryableDiscount.Where(x => x.FromDate <= ToDateValue);
            }
            if (!string.IsNullOrEmpty(code))
            {
                code = code.Trim();
                if (code.Contains('*'))
                {
                    queryableDiscount = queryableDiscount.Where(x => x.Code.Contains(code));
                }
                else
                {
                    queryableDiscount = queryableDiscount.Where(x => x.Code == code);
                }
            }
            return new PagedList<Discount>
            {
                CurrentPage = currentPage,
                PageSize = pageSize,
                Records = queryableDiscount.OrderBy(x => x.CreatedDate).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList(),
                TotalRecords = queryableDiscount.Count()
            };
        }
        public static IEnumerable<Discount> GetAllDiscounts()
        {
            return DiscountRepository.GetAll();
        }

        public static Discount GetDiscountById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var discount = DiscountRepository.Get(id);

            if (discount == null || !discount.IsActive)
                return null;

            return discount;
        }

        public static Discount UpdateDiscount(Discount discount)
        {
            var validation = new DiscountValidation.DiscountEntity().Validate(discount);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            var oldEntity = DiscountRepository.Get(discount.Id);
            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            discount = DiscountRepository.Update(discount, needAuthorization: false);
            return discount;
        }

        public static DiscountProduct AddDiscountProduct(DiscountProduct discountProduct)
        {
            if (discountProduct == null)
                throw new ArgumentNullException();
            discountProduct = DiscountProductRepository.Add(discountProduct);
            return discountProduct;
        }

        public static bool DeleteDiscountProduct(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var discountProduct = DiscountProductRepository.Get(id);
            if (discountProduct == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = DiscountProductRepository.SoftDelete(discountProduct);
            return isDeleted;
        }

        public static IEnumerable<DiscountProduct> GetAllDiscountProducts()
        {
            return DiscountProductRepository.GetAll();
        }

        public static DiscountProduct GetDiscountProductById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var discountProduct = DiscountProductRepository.Get(id);

            if (discountProduct == null || !discountProduct.IsActive)
                return null;

            return discountProduct;
        }

        public static DiscountProduct UpdateDiscountProduct(DiscountProduct discountProduct)
        {
            if (discountProduct == null)
                throw new ArgumentNullException();

            var oldEntity = DiscountProductRepository.Get(discountProduct.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();
            discountProduct = DiscountProductRepository.Update(discountProduct);
            return discountProduct;
        }

        /// <summary>
        /// بررسی یک کد تخفیف
        /// با استفاده از کد تخفیف ارسال شده به متد و بررسی شرط های اعمال شده 
        /// در صورت معتبر بودن کد تخفیف، مقدار آن باز گردانده می شود
        /// و در صورت هرگونه عدم تطابق علت آن در شی خروجی ارسال می شود.
        /// </summary>
        /// <param name="code">کد تخفیف مورد نظر</param>
        /// <param name="amount">مبلغ موجود در سبد خرید</param>
        /// <returns>نتیجه بررسی کد تخفیف</returns>
        public static MegaViewModel<double> CheckDiscount(string code, double amount)
        {

            if (amount <= 0)
                throw new Exception($"\"Amount\" cannot be less than or equal to zero.");

            var entity = DiscountRepository.GetQuery(discount => discount.IsActive && discount.Code == code).SingleOrDefault();

            // عدم یافت شدن کد تخفیف
            if (entity == null)
                return new MegaViewModel<double>("کد تخفیف مورد نظر یافت نشد.");

            // بررسی منقضی بودن کارت تخفیف
            if (entity.HasExpired)
                return new MegaViewModel<double>("کارت تخفیف مورد نظر منقضی شده است.");

            // بررسی اعتبار کد تخفیف از تاریخ
            if (entity.FromDate >= DateTime.Now)
                return new MegaViewModel<double>("تاریخ استفاده از این کد تخفیف هنوز نرسیده است.");

            // بررسی اعتبار کد تخفیف تا تاریخ
            if (entity.ToDate <= DateTime.Now)
                return new MegaViewModel<double>("تاریخ استفاده از این کد تخفیف گذشته است.");

            // بررسی مقدار قیمت سبد خرید، که بایستی که از مقدار مجاز حداقل سفارش، برای استفاده از این کد تخفیف بیشتر باشد
            if (amount < entity.MinimumBasketSize)
                return new MegaViewModel<double>($"حداقل سفارش برای استفاده از این کد تخفیف مبلغ {entity.MinimumBasketSize.Value:N0} می باشد");

            // بررسی تعداد میزان استفاده از این کد تخفیف با حداکثر مقدار مجاز برای استفاده
            if (entity.NumberOfUsageLimit <= entity.NumberOfUsed)
                return new MegaViewModel<double>("تعداد استفاده از این کد تخفیف اتمام یافته است.");

            // محاسبه قیمت بر اساس درصد تخفیف
            if (entity.Percent != null && entity.Percent > 0)
            {
                // محاسبه مقدار تخفیف بر اساس درصد
                var discount = (amount / 100 * entity.Percent).Value;

                // در صورت بیشتر شدن میزان تخفیف از حد مجاز کارت تخفیف، حد مجاز به عنوان میزان تخفیف برگردانده می شود.
                if (discount > entity.MaximumAmountLimit)
                    discount = entity.MaximumAmountLimit.Value;

                // رند گردن تخفیف با سه رقم ثابت
                if (discount % 1000 > 0)
                    discount -= (discount % 1000);

                return new MegaViewModel<double>(discount);
            }
            // مقدار ثابت تخفیف
            else
            {
                // اگر مقدار تخفیف، بیشتر از میزان سبد خرید بود، یعنی تخفیف 100درصد
                if (amount <= entity.AmountPrice)
                    return new MegaViewModel<double>(amount);

                return new MegaViewModel<double>(entity.AmountPrice.Value);
            }

        }

        public static void AddUsedNumber(string discountCode)
        {
            var discountEntity = DiscountRepository.GetQuery(discount => discount.Code == discountCode).FirstOrDefault();
            discountEntity.NumberOfUsed += 1;

            UpdateDiscount(discountEntity);
        }

        public static bool AddMultiple(BulkDiscountViewModel vm)
        {

            var bulkValidation = new DiscountValidation.BulkDiscountEntity().Validate(vm);
            if (!bulkValidation.IsValid)
                throw new ValidationException(bulkValidation.Errors);

            DiscountBulkRepository bulk = new DiscountBulkRepository();

            var uniqueNumbers = FileID.GetUniqueNumber(vm.Count, vm.Min, vm.Max);
            List<Discount> discounts = new List<Discount>();
            for (int i = 0; i < vm.Count; i++)
            {
                discounts.Add(new Discount
                {
                    AmountPrice = vm.Discount.AmountPrice,
                    FromDate = vm.Discount.FromDate,
                    ToDate = vm.Discount.ToDate,
                    HasExpired = vm.Discount.HasExpired,
                    IsActive = vm.Discount.IsActive,
                    IsJustForTransportation = vm.Discount.IsJustForTransportation,
                    MaximumAmountLimit = vm.Discount.MaximumAmountLimit,
                    MinimumBasketSize = vm.Discount.MinimumBasketSize,
                    Percent = vm.Discount.Percent,
                    Title = vm.Discount.Title,
                    NumberOfUsed = vm.Discount.NumberOfUsed,
                    NumberOfUsageLimit = vm.Discount.NumberOfUsageLimit,
                    IsForWeb = vm.Discount.IsForWeb,
                    IsForMobile = vm.Discount.IsForMobile,
                    IsJustForOnlinePayment = vm.Discount.IsJustForOnlinePayment,
                    Code = $"{vm.Prefix}{uniqueNumbers[i]}"
                });
            }
            bulk.AddRange(discounts, false);
            return bulk.Commit() != 0;
        }
    }
}
