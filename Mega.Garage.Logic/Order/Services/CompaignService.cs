﻿using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Order.Validation;
using Mega.Garage.Persistence.Order.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mega.Garage.Logic.Order.Services
{
    public class CampaignService
    {
        protected class CampaignRepository : BaseRepository<Campaign, int> { }
        protected class CampaignTypeRepository : BaseRepository<CampaignType, int> { }
        protected class CampaignProductRepository : BaseRepository<CampaignProduct, int> { }

        public static Campaign AddCampaign(Campaign campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException();

            var validation = new CampaignValidation.CampaignEntity().Validate(campaign);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            campaign = CampaignRepository.Add(campaign);
            return campaign;
        }

        public static bool DeleteCampaign(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var campaign = CampaignRepository.Get(id);
            if (campaign == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = CampaignRepository.SoftDelete(campaign);
            return isDeleted;
        }

        public static IEnumerable<Campaign> GetAllCampaigns()
        {
            return CampaignRepository.GetQuery(x => x.IsActive, new List<string>() { "CampaignType" }).ToList();
        }
        public static PagedList<Campaign> GetAllCampaignsWithFilter(DateTime? fromDate, DateTime? toDate, bool? isOneSite, bool? isSpecialGarage, int currentPage, int pageSize = 25)
        {
            IQueryable<Campaign> queryableCampaign = CampaignRepository.GetQuery(campaign => campaign.IsActive);
            if (fromDate.HasValue)
            {
                DateTime FromDateFixed = fromDate.Value.Date;
                queryableCampaign = queryableCampaign.Where(campaign => campaign.StartDate >= FromDateFixed);
            }

            if (toDate.HasValue)
            {
                DateTime toDateFixed = toDate.Value.AddDays(1).AddSeconds(-1);
                queryableCampaign = queryableCampaign.Where(campaign => campaign.EndDate <= toDateFixed);
            }
            if (isOneSite.HasValue)
            {
                bool IsOneSiteFixed = isOneSite.Value;
                queryableCampaign = queryableCampaign.Where(campaign => campaign.IsOnsite == IsOneSiteFixed);
            }
            if (isSpecialGarage.HasValue)
            {
                bool IsSpecialGarageFixed = isSpecialGarage.Value;
                queryableCampaign = queryableCampaign.Where(campaign => campaign.IsSpecialGarage == IsSpecialGarageFixed);
            }
            queryableCampaign = queryableCampaign.OrderByDescending(x => x.CreatedBy);
            return new PagedList<Campaign>
            {
                CurrentPage = currentPage,
                PageSize = pageSize,
                Records = queryableCampaign.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList(),
                TotalRecords = queryableCampaign.Count()
            };
        }
        public static Campaign GetCampaignById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var campaign = CampaignRepository.Get(id);

            if (campaign == null || !campaign.IsActive)
                return null;

            return campaign;
        }

        public static Campaign UpdateCampaign(Campaign campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException();

            var oldEntity = CampaignRepository.Get(campaign.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new CampaignValidation.CampaignEntity().Validate(campaign);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            campaign = CampaignRepository.Update(campaign);
            return campaign;
        }

        public static CampaignType AddCampaignType(CampaignType campaignType)
        {
            if (campaignType == null)
                throw new ArgumentNullException();

            var validation = new CampaignValidation.CampaignTypeEntity().Validate(campaignType);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            campaignType = CampaignTypeRepository.Add(campaignType);
            return campaignType;
        }

        public static bool DeleteCampaignType(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var campaignType = CampaignTypeRepository.Get(id);

            if (campaignType == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = CampaignTypeRepository.SoftDelete(campaignType);
            return isDeleted;
        }

        public static IEnumerable<CampaignType> GetAllCampaignTypes()
        {
            return CampaignTypeRepository.GetAll();
        }

        public static CampaignType GetCampaignTypeById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var campaignType = CampaignTypeRepository.Get(id);

            if (campaignType == null || !campaignType.IsActive)
                return null;

            return campaignType;
        }

        public static CampaignType UpdateCampaignType(CampaignType campaignType)
        {
            if (campaignType == null)
                throw new ArgumentNullException();

            var oldEntity = CampaignTypeRepository.Get(campaignType.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new CampaignValidation.CampaignTypeEntity().Validate(campaignType);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            campaignType = CampaignTypeRepository.Update(campaignType);
            return campaignType;
        }

        public static CampaignProduct AddCampaignProduct(CampaignProduct campaignProduct)
        {
            if (campaignProduct == null)
                throw new ArgumentNullException();

            var validation = new CampaignValidation.CampaignProductEntity().Validate(campaignProduct);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            campaignProduct = CampaignProductRepository.Add(campaignProduct);
            return campaignProduct;
        }

        public static bool DeleteCampaignProduct(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var campaignProduct = CampaignProductRepository.Get(id);

            if (campaignProduct == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = CampaignProductRepository.SoftDelete(campaignProduct);
            return isDeleted;
        }

        public static IEnumerable<CampaignProduct> GetAllCampaignProducts()
        {
            return CampaignProductRepository.GetAll();
        }

        public static CampaignProduct GetCampaignProductById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var campaignProduct = CampaignProductRepository.Get(id);

            if (campaignProduct == null || !campaignProduct.IsActive)
                return null;

            return campaignProduct;
        }

        public static CampaignProduct UpdateCampaignProduct(CampaignProduct campaignProduct)
        {
            if (campaignProduct == null)
                throw new ArgumentNullException();

            var oldEntity = CampaignProductRepository.Get(campaignProduct.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            campaignProduct = CampaignProductRepository.Update(campaignProduct);
            return campaignProduct;
        }

        public static IEnumerable<CampaignProduct> GetCampaignProductsByCampaignId(int campaignId)
        {
            return CampaignProductRepository.GetQuery(campaign => campaign.CampaignId == campaignId && campaign.IsActive).ToList();
        }
    }
}
