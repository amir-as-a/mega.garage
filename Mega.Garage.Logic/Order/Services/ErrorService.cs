﻿using System;
using System.Collections.Generic;
using Mega.Garage.Common;
using Mega.Garage.Persistence.Order.Entities;


namespace Mega.Garage.Logic.Order.Services
{
    public class ErrorService
    {
        protected class ErrorRepository : BaseRepository<Error, int> { }
        public static Error AddError(Error error)
        {
            if (error == null)
                throw new ArgumentNullException();
            error = ErrorRepository.Add(error);
            return error;
        }

        public static bool DeleteError(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var error = ErrorRepository.Get(id);
            if (error == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = ErrorRepository.SoftDelete(error);
            return isDeleted;
        }

        public static IEnumerable<Error> GetAllErrors()
        {
            return ErrorRepository.GetAll();
        }

        public static Error GetErrorById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var error = ErrorRepository.Get(id);

            if (error == null || !error.IsActive)
                return null;

            return error;
        }

        public static Error UpdateError(Error error)
        {
            if (error == null)
                throw new ArgumentNullException();

            var oldEntity = ErrorRepository.Get(error.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();
            error = ErrorRepository.Update(error);
            return error;
        }
    }
}
