﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using ClosedXML.Excel;
using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Logic.Order.Validation;
using Mega.Garage.Logic.Order.ViewModel;
using Mega.Garage.Logic.Payment.Services;
using Mega.Garage.Persistence.Order.Entities;


namespace Mega.Garage.Logic.Order.Services
{
    public class InvoiceService
    {
        protected class InvoiceRepository : BaseRepository<Invoice, int>
        {
            public static List<InvoiceTotalFinalPricePerMonthViewModel> GetTotalInvoicesFinalPricePerMonthReport(int year)
            {
                var dateList = DateUtility.GetYearFirstAndLastDateOfYear(year);

                using (var context = new Persistence.GarageDbContext())
                {
                    var result = context.Database.SqlQuery<InvoiceTotalFinalPricePerMonthViewModel>(@"
SELECT T.[Month] AS [Month], sum(t.FinalPrice) as [totalPrice]
FROM (
  SELECT CASE 
	WHEN INVOICEDATE BETWEEN  @M1F AND @M1E  THEN '1'
	WHEN INVOICEDATE BETWEEN  @M2F AND @M2E THEN '2'
	WHEN INVOICEDATE BETWEEN  @M3F AND @M3E THEN '3'
    WHEN INVOICEDATE BETWEEN  @M4F AND @M4E THEN '4'
	WHEN INVOICEDATE BETWEEN  @M5F AND @M5E THEN '5'
	WHEN INVOICEDATE BETWEEN  @M6F AND @M6E THEN '6'
	WHEN INVOICEDATE BETWEEN  @M7F AND @M7E THEN '7'
	WHEN INVOICEDATE BETWEEN  @M8F AND @M8E THEN '8'
	WHEN INVOICEDATE BETWEEN  @M9F AND @M9E THEN '9'
	WHEN INVOICEDATE BETWEEN  @M10F AND @M10E THEN '10'
	WHEN INVOICEDATE BETWEEN  @M11F AND @M11E THEN '11'
	WHEN INVOICEDATE BETWEEN  @M12F AND @M12E THEN '12'
	END AS [Month] , FinalPrice
  FROM [Garage].[INVOICE]) T
GROUP BY T.[Month]",
                    new SqlParameter("M1F", dateList[0].StartOfMonth),
                    new SqlParameter("M1E", dateList[0].EndOfMonth),
                    new SqlParameter("M2F", dateList[1].StartOfMonth),
                    new SqlParameter("M2E", dateList[1].EndOfMonth),
                    new SqlParameter("M3F", dateList[2].StartOfMonth),
                    new SqlParameter("M3E", dateList[2].EndOfMonth),
                    new SqlParameter("M4F", dateList[3].StartOfMonth),
                    new SqlParameter("M4E", dateList[3].EndOfMonth),
                    new SqlParameter("M5F", dateList[4].StartOfMonth),
                    new SqlParameter("M5E", dateList[4].EndOfMonth),
                    new SqlParameter("M6F", dateList[5].StartOfMonth),
                    new SqlParameter("M6E", dateList[5].EndOfMonth),
                    new SqlParameter("M7F", dateList[6].StartOfMonth),
                    new SqlParameter("M7E", dateList[6].EndOfMonth),
                    new SqlParameter("M8F", dateList[7].StartOfMonth),
                    new SqlParameter("M8E", dateList[7].EndOfMonth),
                    new SqlParameter("M9F", dateList[8].StartOfMonth),
                    new SqlParameter("M9E", dateList[8].EndOfMonth),
                    new SqlParameter("M10F", dateList[9].StartOfMonth),
                    new SqlParameter("M10E", dateList[9].EndOfMonth),
                    new SqlParameter("M11F", dateList[10].StartOfMonth),
                    new SqlParameter("M11E", dateList[10].EndOfMonth),
                    new SqlParameter("M12F", dateList[11].StartOfMonth),
                    new SqlParameter("M12E", dateList[11].EndOfMonth)
                    ).ToList();

                    return result;
                }

            }

            public static List<InvoiceCountPerMonthReportViewModel> GetTotalInvoicesCountPerMonthReport(int year)
            {
                var dateList = DateUtility.GetYearFirstAndLastDateOfYear(year);

                using (var context = new Persistence.GarageDbContext())
                {
                    var result = context.Database.SqlQuery<InvoiceCountPerMonthReportViewModel>(@"
SELECT T.[Month] AS [Month], COUNT(*) AS [Count]
FROM (
  SELECT CASE 
	WHEN INVOICEDATE BETWEEN  @M1F AND @M1E  THEN '1'
	WHEN INVOICEDATE BETWEEN  @M2F AND @M2E THEN '2'
	WHEN INVOICEDATE BETWEEN  @M3F AND @M3E THEN '3'
    WHEN INVOICEDATE BETWEEN  @M4F AND @M4E THEN '4'
	WHEN INVOICEDATE BETWEEN  @M5F AND @M5E THEN '5'
	WHEN INVOICEDATE BETWEEN  @M6F AND @M6E THEN '6'
	WHEN INVOICEDATE BETWEEN  @M7F AND @M7E THEN '7'
	WHEN INVOICEDATE BETWEEN  @M8F AND @M8E THEN '8'
	WHEN INVOICEDATE BETWEEN  @M9F AND @M9E THEN '9'
	WHEN INVOICEDATE BETWEEN  @M10F AND @M10E THEN '10'
	WHEN INVOICEDATE BETWEEN  @M11F AND @M11E THEN '11'
	WHEN INVOICEDATE BETWEEN  @M12F AND @M12E THEN '12'
	END AS [Month]
  FROM [sale].[INVOICE]) T
GROUP BY T.[Month]",
                    new SqlParameter("M1F", dateList[0].StartOfMonth),
                    new SqlParameter("M1E", dateList[0].EndOfMonth),
                    new SqlParameter("M2F", dateList[1].StartOfMonth),
                    new SqlParameter("M2E", dateList[1].EndOfMonth),
                    new SqlParameter("M3F", dateList[2].StartOfMonth),
                    new SqlParameter("M3E", dateList[2].EndOfMonth),
                    new SqlParameter("M4F", dateList[3].StartOfMonth),
                    new SqlParameter("M4E", dateList[3].EndOfMonth),
                    new SqlParameter("M5F", dateList[4].StartOfMonth),
                    new SqlParameter("M5E", dateList[4].EndOfMonth),
                    new SqlParameter("M6F", dateList[5].StartOfMonth),
                    new SqlParameter("M6E", dateList[5].EndOfMonth),
                    new SqlParameter("M7F", dateList[6].StartOfMonth),
                    new SqlParameter("M7E", dateList[6].EndOfMonth),
                    new SqlParameter("M8F", dateList[7].StartOfMonth),
                    new SqlParameter("M8E", dateList[7].EndOfMonth),
                    new SqlParameter("M9F", dateList[8].StartOfMonth),
                    new SqlParameter("M9E", dateList[8].EndOfMonth),
                    new SqlParameter("M10F", dateList[9].StartOfMonth),
                    new SqlParameter("M10E", dateList[9].EndOfMonth),
                    new SqlParameter("M11F", dateList[10].StartOfMonth),
                    new SqlParameter("M11E", dateList[10].EndOfMonth),
                    new SqlParameter("M12F", dateList[11].StartOfMonth),
                    new SqlParameter("M12E", dateList[11].EndOfMonth)
                    ).ToList();

                    return result;
                }
            }

        }
        protected class InvoiceAdditiveRepository : BaseRepository<InvoiceAdditive, int> { }
        protected class InvoiceAdditiveTypeRepository : BaseRepository<InvoiceAdditiveType, int> { }
        protected class InvoiceProcessRepository : BaseRepository<InvoiceProcess, int> { }
        protected class InvoiceProcessHistoryRepository : BaseRepository<InvoiceProcessHistory, int> { }
        protected class PaymentTypeRepository : BaseRepository<PaymentType, int> { }

        internal static readonly string baseUrl = $"{HttpContext.Current.Request.Url.Scheme}://{(HttpContext.Current.Request.Url.Host != "localhost" ? HttpContext.Current.Request.Url.Host : HttpContext.Current.Request.Url.Authority)}/revieworder/check/";
        internal static readonly string CreateInvoiceSMSText = @"
شماره سفارش درخواست شما: {0}
اطلاعات سفارش به همراه آدرس اتوسرویس انتخابی:
{1}
با تشکر
اسپیدی
";

        internal static readonly string OnlineCreateInvoiceSmsText = @"
سفارش اسپیدی

سفارش جدید با شماره {0}
{1}
مبلغ {2} ریال

به صورت اینترنتی پرداخت شده است و نیازی به پرداخت وجه در محل نیست.";

        internal static readonly string CashCreateInvoiceSmsText = @"
سفارش اسپیدی

سفارش جدید با شماره {0}
{1}
مبلغ {2} ریال

مبلغ {3} ریال می بایست در هنگام سرویس دهی دریافت شود.";

        internal static readonly string ProductItemSmsText = "تعداد {0} محصول {1}";

        public static Invoice AddInvoice(Invoice invoice)
        {
            if (invoice == null)
                throw new ArgumentNullException();

            invoice = InvoiceRepository.Add(invoice, needAuthorization: false);
            return invoice;
        }

        public static int GetTotalInvoices()
        {
            return InvoiceRepository.Count(x => x.IsActive);
        }

        public static bool DeleteInvoice(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoice = InvoiceRepository.Get(id);

            if (invoice == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = InvoiceRepository.SoftDelete(invoice);
            return isDeleted;

        }
        public static IQueryable<Invoice> GetQuery()
        {
            return InvoiceRepository.GetQuery();
        }

        public static IEnumerable<Invoice> GetAllInvoices(InvoiceSearchViewModel parameters)
        {
            var query = InvoiceRepository.GetQuery().Include(x => x.PaymentType);
            if (parameters.CustomerId.HasValue)
                query = query.Where(invoice =>
                    invoice.CustomerId == parameters.CustomerId);

            if (parameters.FromDate.HasValue)
                query = query.Where(invoice =>
                    invoice.CreatedDate >= parameters.FromDate);

            if (parameters.ToDate.HasValue)
                query = query.Where(invoice =>
                    invoice.CreatedDate <= DbFunctions.AddDays(parameters.ToDate, 1));

            if (parameters.IsPayed.HasValue)
                query = query.Where(invoice =>
                    invoice.IsPayed == true);

            return query.ToList();
        }

        public static Invoice GetInvoiceById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoice = InvoiceRepository.Get(id);

            if (invoice == null || !invoice.IsActive)
                return null;

            return invoice;
        }

        public static Invoice GetInvoiceByOrderNumber(string orderNumber)
        {
            if (string.IsNullOrEmpty(orderNumber))
                throw new ArgumentNullException();

            return InvoiceRepository.GetQuery(invoice => invoice.IsActive && invoice.OrderNumber == orderNumber).SingleOrDefault();
        }

        public static List<int> GetInvoiceIdListByCusomerId(int customerId)
        {
            if (customerId == 0)
                throw new ArgumentNullException();

            var invoiceIdList = InvoiceRepository.GetQuery(i => i.CustomerId == customerId && i.IsActive).Select(i => i.Id).ToList();

            if (invoiceIdList == null)
                return null;

            return invoiceIdList;
        }

        public static PagedList<Invoice> GetInvoiceListByDealerId(InvoiceSearchViewModel parameters)
        {
            var query = InvoiceRepository.GetQuery(i => i.DealerId == parameters.DealerId && i.IsPayed == true && i.PaymentTypeId == 2);

            if (parameters.FromDate.HasValue)
                query = query.Where(invoice =>
                    invoice.CreatedDate >= parameters.FromDate);

            if (parameters.ToDate.HasValue)
                query = query.Where(invoice =>
                    invoice.CreatedDate <= DbFunctions.AddDays(parameters.ToDate, 1));


            query = query.OrderByDescending(x => x.CreatedDate);

            return new PagedList<Invoice>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = parameters.PageSize > 0 ? query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList() : query.ToList(),
            };

        }

        public static List<Invoice> GetInvoiceListByCusomerId(int customerId)
        {
            if (customerId == 0)
                throw new ArgumentNullException();

            var invoiceIdList = InvoiceRepository.GetQuery(i => i.CustomerId == customerId && i.IsActive).ToList();

            if (invoiceIdList == null)
                return null;

            return invoiceIdList;
        }

        public static List<Invoice> GetInvoiceListDetailByInvoiceIds(List<int> invoices)
        {
            if (invoices.Count == 0)
                throw new ArgumentNullException();

            var invoiceIdList = InvoiceRepository.GetQuery(i => invoices.Contains(i.Id) && i.IsActive).ToList();

            if (invoiceIdList == null)
                return null;

            return invoiceIdList;
        }
        public static List<int?> GetCartIdByCusomerId(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var cartIdList = InvoiceRepository.GetQuery(p => p.CustomerId == id && p.IsActive).Select(p => p.CartId).ToList();

            if (cartIdList == null)
                return null;

            return cartIdList;
        }

        public static Invoice UpdateInvoice(Invoice invoice, bool needCommit = true, bool needAuthorization = true)
        {
            if (invoice == null)
                throw new ArgumentNullException();

            var oldEntity = InvoiceRepository.Get(invoice.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            invoice = InvoiceRepository.Update(invoice, needCommit, needAuthorization);
            return invoice;
        }

        public static InvoiceAdditive AddInvoiceAdditive(InvoiceAdditive invoiceAdditive)
        {
            if (invoiceAdditive == null)
                throw new ArgumentNullException();

            invoiceAdditive = InvoiceAdditiveRepository.Add(invoiceAdditive);
            return invoiceAdditive;
        }

        public static bool DeleteInvoiceAdditive(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoiceAdditive = InvoiceAdditiveRepository.Get(id);

            if (invoiceAdditive == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = InvoiceAdditiveRepository.SoftDelete(invoiceAdditive);
            return isDeleted;
        }

        public static IEnumerable<InvoiceAdditive> GetAllInvoiceAdditives()
        {
            return InvoiceAdditiveRepository.GetAll();
        }

        public static InvoiceAdditive GetInvoiceAdditiveById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoiceAdditive = InvoiceAdditiveRepository.Get(id);

            if (invoiceAdditive == null || !invoiceAdditive.IsActive)
                return null;

            return invoiceAdditive;
        }

        public static InvoiceAdditive UpdateInvoiceAdditive(InvoiceAdditive invoiceAdditive)
        {
            if (invoiceAdditive == null)
                throw new ArgumentNullException();

            var oldEntity = InvoiceAdditiveRepository.Get(invoiceAdditive.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            invoiceAdditive = InvoiceAdditiveRepository.Update(invoiceAdditive);
            return invoiceAdditive;
        }

        public static InvoiceAdditiveType AddInvoiceAdditiveType(InvoiceAdditiveType invoiceAdditiveType)
        {
            if (invoiceAdditiveType == null)
                throw new ArgumentNullException();

            invoiceAdditiveType = InvoiceAdditiveTypeRepository.Add(invoiceAdditiveType);
            return invoiceAdditiveType;
        }

        public static bool DeleteInvoiceAdditiveType(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoiceAdditiveType = InvoiceAdditiveTypeRepository.Get(id);

            if (invoiceAdditiveType == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = InvoiceAdditiveTypeRepository.SoftDelete(invoiceAdditiveType);
            return isDeleted;
        }

        public static IEnumerable<InvoiceAdditiveType> GetAllInvoiceAdditiveTypes()
        {
            return InvoiceAdditiveTypeRepository.GetAll();
        }

        public static InvoiceAdditiveType GetInvoiceAdditiveTypeById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoiceAdditiveType = InvoiceAdditiveTypeRepository.Get(id);

            if (invoiceAdditiveType == null || !invoiceAdditiveType.IsActive)
                return null;

            return invoiceAdditiveType;
        }

        public static InvoiceAdditiveType UpdateInvoiceAdditiveType(InvoiceAdditiveType invoiceAdditiveType)
        {
            if (invoiceAdditiveType == null)
                throw new ArgumentNullException();

            var oldEntity = InvoiceAdditiveTypeRepository.Get(invoiceAdditiveType.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            invoiceAdditiveType = InvoiceAdditiveTypeRepository.Update(invoiceAdditiveType);
            return invoiceAdditiveType;
        }

        public static InvoiceProcess AddInvoiceProcess(InvoiceProcess invoiceProcess)
        {
            if (invoiceProcess == null)
                throw new ArgumentNullException();

            invoiceProcess = InvoiceProcessRepository.Add(invoiceProcess);
            return invoiceProcess;
        }

        public static bool DeleteInvoiceProcess(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoiceProcess = InvoiceProcessRepository.Get(id);

            if (invoiceProcess == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = InvoiceProcessRepository.SoftDelete(invoiceProcess);
            return isDeleted;
        }

        public static IEnumerable<InvoiceProcess> GetAllInvoiceProcesses()
        {
            return InvoiceProcessRepository.GetAll();
        }

        public static InvoiceProcess GetInvoiceProcessById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoiceProcess = InvoiceProcessRepository.Get(id);

            if (invoiceProcess == null || !invoiceProcess.IsActive)
                return null;

            return invoiceProcess;
        }
        public static InvoiceProcess GetInvoiceProcessByCode(string code)
        {
            if (string.IsNullOrEmpty(code))
                throw new ArgumentNullException();

            var invoiceProcess = InvoiceProcessRepository.GetQuery(item => item.Code == code).FirstOrDefault();

            if (invoiceProcess == null || !invoiceProcess.IsActive)
                return null;

            return invoiceProcess;
        }

        public static InvoiceProcess UpdateInvoiceProcess(InvoiceProcess invoiceProcess)
        {
            if (invoiceProcess == null)
                throw new ArgumentNullException();

            var oldEntity = InvoiceProcessRepository.Get(invoiceProcess.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();


            return invoiceProcess;
        }

        public static InvoiceProcessHistory AddInvoiceProcessHistory(InvoiceProcessHistory invoiceProcessHistory)
        {
            if (invoiceProcessHistory == null)
                throw new ArgumentNullException();

            invoiceProcessHistory = InvoiceProcessHistoryRepository.Add(invoiceProcessHistory, needAuthorization: false);
            return invoiceProcessHistory;
        }

        public static bool DeleteInvoiceProcessHistory(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoiceProcessHistory = InvoiceProcessHistoryRepository.Get(id);

            if (invoiceProcessHistory == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = InvoiceProcessHistoryRepository.SoftDelete(invoiceProcessHistory);
            return isDeleted;
        }

        public static IEnumerable<InvoiceProcessHistory> GetAllInvoiceProcessHistories()
        {
            return InvoiceProcessHistoryRepository.GetAll();
        }

        public static InvoiceProcessHistory GetInvoiceProcessHistoryById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var invoiceProcessHistory = InvoiceProcessHistoryRepository.Get(id);

            if (invoiceProcessHistory == null || !invoiceProcessHistory.IsActive)
                return null;

            return invoiceProcessHistory;
        }

        public static InvoiceProcessHistory UpdateInvoiceProcessHistory(InvoiceProcessHistory invoiceProcessHistory)
        {
            if (invoiceProcessHistory == null)
                throw new ArgumentNullException();

            var oldEntity = InvoiceProcessHistoryRepository.Get(invoiceProcessHistory.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();
            invoiceProcessHistory = InvoiceProcessHistoryRepository.Update(invoiceProcessHistory);
            return invoiceProcessHistory;
        }

        public static IEnumerable<InvoiceAdditive> GetAllInvoiceAdditivesByInvoiceId(int invoiceId)
        {
            return InvoiceAdditiveRepository.GetQuery(invoiceAdditive => invoiceAdditive.IsActive && invoiceAdditive.InvoiceId == invoiceId).ToList();
        }

        public static IEnumerable<InvoiceProcessHistory> GetAllInvoiceProcessHistoriesByInvoiceId(int invoiceId)
        {
            return InvoiceProcessHistoryRepository.GetQuery(invoiceProcessHistory => invoiceProcessHistory.IsActive && invoiceProcessHistory.InvoiceId == invoiceId).ToList();
        }

        //public static bool MakeInvoiceFromCart(Cart cart)
        //{
        //    #region Load Data
        //    List<CartItem> cartItems = CartService.GetCartItemByCartId(cart.Id).ToList();

        //    List<long> productIds = cartItems.Select(cartItem => cartItem.ProductId.Value).ToList();
        //    IEnumerable<Product> products = ProductService.GetProductsByIdList(productIds);
        //    #endregion

        //    #region Invoice Code
        //    var lastInvoiceItem = InvoiceRepository.GetQuery().OrderBy(inv => inv.InvoiceCode).FirstOrDefault();
        //    var invoiceCode = "1";
        //    if (lastInvoiceItem == null)
        //        invoiceCode = (int.Parse(lastInvoiceItem.InvoiceCode) + 1).ToString();
        //    #endregion

        //    #region Order Number
        //    var orderNumber = new Random().Next(11111, 99999).ToString();
        //    while (InvoiceRepository.GetQuery().Count(inv => inv.OrderNumber == orderNumber) > 0)
        //    {
        //        orderNumber = new Random().Next(11111, 99999).ToString();
        //    }
        //    #endregion

        //    decimal price = cartItems.Sum(cartItem => cartItem.FinalPrice * cartItem.OrderQty);

        //    Invoice invoice = new Invoice()
        //    {
        //        CartId = cart.Id,
        //        CustomerId = cart.CustomerId.Value,
        //        InvoiceCode = invoiceCode,
        //        InvoiceDate = DateTime.Now,
        //        ExpireDate = DateTime.Now.AddDays(7),
        //        OrderNumber = orderNumber,
        //        HasAdditive = false,
        //        AdditiveAmount = 0,
        //        TotalAmount = price,
        //        TotalQty = cartItems.Count(),
        //        TotalDiscount = 0,
        //        FinalPrice = price,
        //        HasDelivery = false,
        //        DeliveryPrice = 0
        //    };

        //    invoice = InvoiceService.AddInvoice(invoice);

        //    List<OrderItem> orderItems = new List<OrderItem>();
        //    for (int i = 0; i < cartItems.Count; i++)
        //    {
        //        var cartItem = cartItems[i];

        //        var orderItem = new OrderItem()
        //        {
        //            InvoiceId = invoice.Id,
        //            ProductId = cartItem.ProductId,
        //            UnitPrice = cartItem.FinalPrice,
        //            OrderDetailNumber = invoice.OrderNumber + ((i + 1).ToString("D3")),
        //            Discount = 0,
        //            Guid = Guid.NewGuid(),
        //            IsDelivered = false,
        //            DeliveryPrice = 0,
        //            IsShipped = false,
        //            IsCancelled = false
        //        };

        //        orderItem = OrderService.AddOrderItem(orderItem);
        //        orderItems.Add(orderItem);
        //    }

        //    return true;
        //}

        public static MegaViewModel<Invoice> CreateInvoice(InvoiceViewModel model)
        {
            var validation = new CartValidation.CartViewModelEntity().Validate(model);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            if (General.Services.SettingsService.IsMandatoryDealer() && (model.Dealer == null || model.Dealer <= 0))
            {
                return new MegaViewModel<Invoice>("انتخاب اتوسرویس الزامی می باشد.");
            }

            if (model.Payment == 2 && (model.Dealer == null || model.Dealer <= 0))
            {
                return new MegaViewModel<Invoice>("برای پرداخت آنلاین می بایست اتوسرویس انتخاب شده باشد.");
            }

            Persistence.Dealer.Entities.Dealer dealerObject = null;
            if (model.Dealer != null && model.Dealer > 0)
            {
                dealerObject = DealerService.GetDealerById(model.Dealer.GetValueOrDefault());
                if (dealerObject == null && model.Payment == 2) // در پرداخت آنلاین انتخاب اتوسرویس الزامی است
                {
                    return new MegaViewModel<Invoice>("انتخاب اتوسرویس برای پرداخت آنلاین الزامی است.");
                }

                if (dealerObject != null && model.Payment == 2 && string.IsNullOrEmpty(dealerObject.BankAccountNumber)) // باید برای اتوسرویس انتخاب شده بود، شناسه پرداخت براش ثبت شده باشد
                {
                    return new MegaViewModel<Invoice>("برای این اتوسرویس نمی توان پرداخت اینترنتی داشت.");
                }
            }
            var customer = CustomerService.GetCustomerByMobile(model.Mobile);
            if (customer == null)
            {
                customer = CustomerService.AddCustomer(new Customer()
                {
                    FirstName = model.FirstName.GetStandardFarsiString(),
                    LastName = model.LastName.GetStandardFarsiString(),
                    Mobile = model.Mobile,
                    CarGroupCode = int.Parse(model.CarGroup ?? "100")
                }, false);
            }
            else
            {
                customer.CarGroupCode = int.Parse(model.CarGroup ?? "100");
                CustomerService.UpdateCustomer(customer, false);
            }

            #region Invoice Code
            var lastInvoiceItem = InvoiceRepository.GetQuery().OrderByDescending(inv => inv.InvoiceCode).FirstOrDefault();
            var invoiceCode = "1";
            if (lastInvoiceItem != null)
                invoiceCode = (double.Parse(lastInvoiceItem.InvoiceCode) + 1).ToString();
            #endregion

            #region Order Number
            var orderNumber = new Random().Next(11111, 99999).ToString();
            while (InvoiceRepository.Count(inv => inv.OrderNumber == orderNumber) > 0)
            {
                orderNumber = new Random().Next(11111, 99999).ToString();
            }
            #endregion

            List<long> productIds = model.Carts.Select(x => x.ProductId).ToList();
            IEnumerable<Product> products = ProductService.GetProductsByIdList(productIds);

            var price = 0;
            foreach (var item in model.Carts)
            {
                price += (item.Count * products.SingleOrDefault(x => x.Id == item.ProductId)?.UnitPrice.Value ?? 0);
            }

            var discount = DiscountService.CheckDiscount(model.DiscountCode, price);
            if (discount.Status != MegaStatus.Successfull)
            {
                model.DiscountCode = null;
            }

            Invoice invoice = new Invoice()
            {
                CustomerId = customer.Id,
                CustomerFirstName = customer.FirstName,
                CustomerLastName = customer.LastName,
                CustomerMobile = customer.Mobile,
                InvoiceCode = invoiceCode,
                InvoiceDate = DateTime.Now,
                ExpireDate = DateTime.Now.AddDays(7),
                OrderNumber = orderNumber,
                HasAdditive = false,
                AdditiveAmount = 0,
                TotalAmount = price,
                TotalQty = model.Carts.Count(),
                DiscountCode = model.DiscountCode,
                TotalDiscount = discount.Data,
                FinalPrice = price - discount.Data,
                HasDelivery = false,
                CurrentProcessId = 1,
                PaymentTypeId = model.Payment,
                IsPayed = false,
                IsAccountingAccepted = false,
                DeliveryPrice = 0,
                DealerId = model.Dealer
            };

            invoice = InvoiceService.AddInvoice(invoice);
            var process = GetInvoiceProcessByCode("1");
            var invoiceProcessHistory = new InvoiceProcessHistory()
            {
                InvoiceId = invoice.Id,
                NewInvoiceProcessId = process.Id
            };
            AddInvoiceProcessHistory(invoiceProcessHistory);

            /// در صورتی که پرداخت توسط کاربر از نوع پرداخت آنلاین باشد مراحل ذیل انجام می گردد 
            /// ابتدا درخواست پرداخت به سمت بانک ارسال می گردد
            /// در صورتی که بانک پاسخگو باشد یک شماره ارجا برای فاکتور از بانک ملت دریافت می گردد
            /// سپس در جدول *پرداخت کاربر* یک رکورد برای فاکتور درج می شود و تمام اطلاعات دریافتی از بانک در آن ذخیره می گردد
            /// اگر بانک پاسخ مناسبی به درخواست ندهد به کاربر پیغامی مبنی بر وجود ایراد در درگاه بانکی داده می شود و می تواند ر فرم قبلی نوع پرداخت در محل را انتخاب کنید و به
            /// فرایند ثبت سفارش ادامه دهد
            //if (model.Payment == 2)
            //{
            //    var paymentPrice = int.Parse(invoice.FinalPrice.ToString());
            //    if (invoice.CustomerMobile == "09357214976")
            //        paymentPrice = 20000;

            //    string GarageCode = $"{invoice.OrderNumber}{DateTime.Now:yyMMddHHmmss}";
            //    var bankObject =
            //        MellatMerchantWebService
            //        .PaymentRequest(
            //            price: paymentPrice,
            //            orderid: long.Parse(invoice.OrderNumber),
            //            GarageCode: long.Parse(GarageCode),
            //            dealerShenase: dealerObject.BankAccountNumber);

            //    if (bankObject.Status == MegaStatus.Successfull)
            //    {
            //        PaymentService.AddUserPayment(new Persistence.Payment.Entities.UserPayment()
            //        {
            //            GaragesCode = GarageCode,
            //            ReferalCode = bankObject.Data.ReferalCode,
            //            ResponseCode = bankObject.Data.ResCode,
            //            IPGId = model.IPGId,
            //            InvoiceId = invoice.Id,
            //            PaymentDate = DateTime.Now,
            //            PaymentStatusId = PaymentService.GetPaymentStatusByCode(Enums.PaymentStatus.PaymentRequest).Id,
            //            HasSplit = false
            //        });
            //    }
            //    else
            //        throw new Exception(bankObject.Messages.FirstOrDefault());
            //}

            var orderStaus = OrderService.GetOrderProcessByCode("1");

            /// ذخیره سازی اقلام سفارش در جدول
            List<OrderItem> orderItems = new List<OrderItem>();
            var index = 1;
            for (int i = 0; i < model.Carts.Count; i++)
            {
                var cartItem = model.Carts[i];
                var product = products.FirstOrDefault(x => x.Id == cartItem.ProductId);

                if (product != null)
                {
                    for (int j = 0; j < cartItem.Count; j++)
                    {
                        string orderItemNumber = string.Join("", invoice.OrderNumber, FileID.ID(2), index.ToString("D3"), FileID.ID(3));
                        var orderItem = new OrderItem()
                        {
                            InvoiceId = invoice.Id,
                            ProductId = cartItem.ProductId,
                            BasePrice = product.BasePrice.Value,
                            UnitPrice = product.UnitPrice.Value,
                            OrderDetailNumber = orderItemNumber,
                            Discount = product.BasePrice.Value - product.UnitPrice.Value,
                            DicountCode = invoice.DiscountCode,
                            Guid = Guid.NewGuid(),
                            IsDelivered = false,
                            DeliveryPrice = 0,
                            CurrentOrderProcessId = 1,
                            OrderStatusId = orderStaus.Id,
                            IsShipped = false,
                            IsCancelled = false
                        };

                        orderItem = OrderService.AddOrderItem(orderItem);
                        orderItems.Add(orderItem);

                        var history = new OrderProcessHistory()
                        {
                            OrderId = orderItem.Id,
                            OrderProcessId = orderStaus.Id
                        };
                        OrderService.AddOrderProcessHistory(history);
                        index += 1;
                    }
                }
            }

            /// در صورتی که سفارش ثبت شده شامل یک کد تخفیف باشد
            /// به تعداد استفاده آن کد تخفیف یک واحد افزوده می شود تا در صورت تغییر شرایط و اتمام تعداد مصرف منقضی گردد
            if (!string.IsNullOrEmpty(model.DiscountCode))
                DiscountService.AddUsedNumber(model.DiscountCode);

            /// ارسال پیامک فقط در صورتی امکان دارد که پرداخت انلاین نباشد
            /// در صورتی که پرداخت آنلاین باشد پیامک بعد از موفقیت آمیز بودن پرداخت انجام می شود
            if (model.Payment != 2)
            {
                if (customer.Mobile != "09357214976") // در صورتی که شماره عابدی بود، اس ام اس ثبت فاکتور ارسال نشود
                    Notify.Services.SMSService.AddSms(customer.Mobile, string.Format(CreateInvoiceSMSText, invoice.OrderNumber, baseUrl + invoice.OrderNumber));

                if (dealerObject != null && General.Services.SettingsService.IsSendSmsToDealer())
                {

                    var productsText = new StringBuilder();
                    var productListForSms = invoice.OrderItems.GroupBy(x => x.Product);
                    foreach (var item in productListForSms)
                    {
                        productsText.Append(string.Format(ProductItemSmsText, item.Count(), item.Key.ShortTitle) + "\n");
                    }

                    Notify.Services.SMSService.AddSms(dealerObject.OwnerPhone,
                        string.Format(
                            CashCreateInvoiceSmsText,
                            invoice.OrderNumber,
                            productsText.ToString(),
                            invoice.FinalPrice.Value.ToString("N0")
                        )
                    );
                }
            }

            return new MegaViewModel<Invoice>(invoice);
        }

        #region Payment Type

        public static IEnumerable<PaymentType> GetAllPaymentTypes()
        {
            return PaymentTypeRepository.GetAll();
        }

        #endregion

        #region Reports
        public static PagedList<FullReportByCustomerViewModel> FullReportByCustomerViewModel(InvoiceSearchViewModel parameter, int currentPage, int pageSize)
        {
            using (var db = new Persistence.GarageDbContext())
            {
                var Result = new FullReportByCustomerViewModel();
                var customerInvoicesQuery = db.Invoices
                    .Where(i => i.IsActive).Include(x => x.OrderItems).Include("OrderItems.OrderProcess").Include(x => x.PaymentType).Include("OrderItems.Product");

                #region ApplySearchParameters

                if (parameter.CustomerId.HasValue)
                    customerInvoicesQuery = customerInvoicesQuery.Where(invoice =>
                        invoice.CustomerId == parameter.CustomerId);

                if (parameter.IsPayed != null)
                    customerInvoicesQuery = customerInvoicesQuery.Where(invoice =>
                        invoice.IsPayed == parameter.IsPayed);

                if (!string.IsNullOrEmpty(parameter.CustomerName))
                    customerInvoicesQuery = customerInvoicesQuery.Where(invoice =>
                        invoice.CustomerFirstName.Contains(parameter.CustomerName) ||
                        invoice.CustomerLastName.Contains(parameter.CustomerName));

                if (parameter.ProductGroup > 0)
                    switch (parameter.ProductGroup)
                    {
                        case 1:
                            customerInvoicesQuery = customerInvoicesQuery.Where(invoice => invoice.ProductGroup == null);
                            break;
                        case 2:
                            customerInvoicesQuery = customerInvoicesQuery.Where(invoice => invoice.ProductGroup == "snapp");
                            break;
                        default:
                            break;
                    }


                if (!string.IsNullOrEmpty(parameter.OrderNumber))
                    customerInvoicesQuery = customerInvoicesQuery.Where(invoice =>
                        invoice.OrderNumber == parameter.OrderNumber);

                if (!string.IsNullOrEmpty((parameter.DiscountCode)))
                    customerInvoicesQuery = customerInvoicesQuery.Where(invoice =>
                        invoice.DiscountCode == parameter.DiscountCode);

                if (parameter.FromDate.HasValue)
                    customerInvoicesQuery = customerInvoicesQuery.Where(invoice =>
                        invoice.InvoiceDate >= parameter.FromDate);

                if (parameter.ToDate.HasValue)
                    customerInvoicesQuery = customerInvoicesQuery.Where(invoice =>
                        invoice.InvoiceDate <= DbFunctions.AddDays(parameter.ToDate, 1));

                if (parameter.PaymentTypeId.HasValue && parameter.PaymentTypeId.Value > 0)
                    customerInvoicesQuery = customerInvoicesQuery.Where(invoice => invoice.PaymentTypeId == parameter.PaymentTypeId);

                if (parameter.CurrentProcessId.HasValue && parameter.CurrentProcessId.Value > 0)
                    customerInvoicesQuery = customerInvoicesQuery.Where(invoice => invoice.CurrentProcessId == parameter.CurrentProcessId);
                #endregion

                var customerInvoices = customerInvoicesQuery.OrderByDescending(x => x.CreatedDate).Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();

                Result.Invoices = customerInvoices;
                var invoiceIds = Result.Invoices.Select(x => x.Id).ToArray();
                Result.DealerServices = db.DealerServices.Where(ds => ds.IsActive && (ds.InvoiceId != null && invoiceIds.Contains(ds.InvoiceId.Value))).ToList();
                var dealerIds = Result.DealerServices.Select(x => x.DealerId).Distinct().ToArray();

                Result.Dealers = db.Dealers.Where(d => d.IsActive && dealerIds.Contains(d.Id)).ToList();

                return new PagedList<FullReportByCustomerViewModel>()
                {
                    CurrentPage = currentPage,
                    PageSize = pageSize,
                    Records = new List<FullReportByCustomerViewModel>() { Result },
                    TotalRecords = customerInvoicesQuery.Count()
                };
            }
        }

        public static XLWorkbook GenerateExcelReport(IEnumerable<Invoice> invoices)
        {
            var report = new List<InvoiceReportViewModel>();

            foreach (var item in invoices)
            {
                var vm = new InvoiceReportViewModel
                {
                    CustomerName = item.CustomerFirstName + " " + item.CustomerLastName,
                    InvoiceCode = item.InvoiceCode,
                    OrderNumber = item.OrderNumber,
                    InvoiceDate = item.InvoiceDate,
                    DiscountCode = item.DiscountCode,
                    TotalAmount = item.TotalAmount,
                    TotalDiscount = item.TotalDiscount,
                    FinalPrice = item.FinalPrice
                };

                report.Add(vm);
            }

            var dataTable = report.ToDataTable("مشتری", "شماره فاکتور", "تاریخ فاکتور", "شماره سفارش", "مبلغ کل", "کدتخفیف", "تخفیف کل", "قیمت نهایی");

            var workbook = new XLWorkbook();
            workbook.Worksheets.Add(dataTable, "Report");

            return workbook;
        }

        public static IEnumerable<DiscountCodeReport> DiscountCodeReport()
        {
            var result = InvoiceRepository.GetQuery(x => x.IsActive && !string.IsNullOrEmpty(x.DiscountCode)).GroupBy(x => x.DiscountCode).Select(x => new DiscountCodeReport
            {
                DiscountCode = x.Key,
                Count = x.Count()
            }).ToList();

            return result;
        }

        public static IEnumerable<InvoiceCountPerMonthReportViewModel> GetTotalInvoicesCountPerMonthReport(int year)
        {
            var result = InvoiceRepository.GetTotalInvoicesCountPerMonthReport(year).ToList();

            const int months = 12;
            for (int i = 0; i < months; i++)
            {
                yield return
                    new InvoiceCountPerMonthReportViewModel
                    {
                        Count = result.FirstOrDefault(x => x.Month == (i + 1).ToString())?.Count ?? 0,
                        Month = (i + 1).ToString()
                    };
            }
        }

        public static IEnumerable<InvoicePaymentReportResponseViewModel> GetInvoiceCountByPaymentTypeReport(InvoicePaymentReportRequestViewModel parameters)
        {
            var result = InvoiceRepository.GetQuery(x => x.IsActive && x.InvoiceDate >= parameters.FromDate && x.InvoiceDate <= parameters.ToDate)
                .GroupBy(x => x.PaymentTypeId).Select(x => new InvoicePaymentReportResponseViewModel()
                {
                    PaymentType = x.Key.Value,
                    PaymentTypeTitle = x.FirstOrDefault().PaymentType.Title,
                    Count = x.Count()
                }).ToList();

            result.Insert(0, new InvoicePaymentReportResponseViewModel()
            {
                PaymentTypeTitle = "مجموع",
                Count = result.Sum(x => x.Count)
            });

            return result;
        }

        public static IEnumerable<InvoiceCurrentProcessResponseViewModel> GetInvoiceProcessStatusReport(InvoiceCurrentProcessRequestViewModel parameters)
        {
            var result = InvoiceRepository.GetQuery(x => x.IsActive && x.InvoiceDate >= parameters.FromDate && x.InvoiceDate <= parameters.ToDate)
                .GroupBy(x => x.CurrentProcessId).Select(x => new InvoiceCurrentProcessResponseViewModel()
                {
                    CurrentProcess = x.Key,
                    CurrentProcessTitle = x.FirstOrDefault().InvoiceProcess.Title,
                    Count = x.Count()
                }).ToList();

            result.Insert(0, new InvoiceCurrentProcessResponseViewModel()
            {
                CurrentProcessTitle = "مجموع",
                Count = result.Sum(x => x.Count)
            });

            return result;
        }

        public static PagedList<Invoice> GetAllInvoice(InvoiceSearchViewModel parameters)
        {
            var query = InvoiceRepository.GetQuery();

            if (parameters.FromDate.HasValue)
                query = query.Where(invoice =>
                    invoice.CreatedDate >= parameters.FromDate);

            if (parameters.ToDate.HasValue)
                query = query.Where(invoice =>
                    invoice.CreatedDate <= DbFunctions.AddDays(parameters.ToDate, 1));


            query = query.OrderByDescending(x => x.CreatedDate);

            return new PagedList<Invoice>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = parameters.PageSize > 0 ? query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList() : query.ToList(),
            };

        }

        #endregion
    }
}