﻿using Mega.Garage.Persistence.Order.Entities;
using Mega.Garage.Common;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using Mega.Garage.Logic.Order.ViewModel;
using System.Data.Entity;
using ClosedXML.Excel;
using Mega.Garage.Logic.Order.Validation;
using FluentValidation;


namespace Mega.Garage.Logic.Order.Services
{
    public class CustomerService
    {
        protected class CustomerRepository : BaseRepository<Customer, int> { }

        public static int GetTotalCustomers()
        {
            return CustomerRepository.Count(x => x.IsActive);
        }

        public static Customer AddCustomer(Customer customer, bool needAuthorization = true)
        {
            var validation = new CustomerValidation.CustomerEntity().Validate(customer);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            if (customer == null)
                throw new ArgumentNullException();

            customer.ip = Network.GetIP();

            customer = CustomerRepository.Add(customer, needAuthorization: needAuthorization);

            return customer;
        }

        public static bool DeleteCustomer(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var customer = CustomerRepository.Get(id);

            if (customer == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = CustomerRepository.SoftDelete(customer);

            return isDeleted;
        }

        public static IEnumerable<Customer> GetAllCustomers()
        {
            return CustomerRepository.GetAll();
        }

        public static Customer GetCustomerById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var customer = CustomerRepository.Get(id);

            if (customer == null || !customer.IsActive)
                return null;

            return customer;
        }

        public static PagedList<Customer> GetAllCustomers(CustomerSearchViewModel parameters)
        {
            var query = CustomerRepository.GetQuery();

            if (!string.IsNullOrEmpty(parameters.Name))
                query = query.Where(customer =>
                    customer.LastName.Contains(parameters.Name) ||
                    customer.FirstName.Contains(parameters.Name));

            if (!string.IsNullOrEmpty(parameters.Mobile))
                query = query.Where(customer =>
                    customer.Mobile.Contains(parameters.Mobile));

            if (!string.IsNullOrEmpty(parameters.DiscountCode))
                query = query.Where(customer =>
                    customer.Invoices.Any(c => c.DiscountCode.Contains(parameters.DiscountCode)));

            if (parameters.FromDate.HasValue)
                query = query.Where(customer =>
                    customer.CreatedDate >= parameters.FromDate);

            if (parameters.ToDate.HasValue)
                query = query.Where(customer =>
                    customer.CreatedDate <= DbFunctions.AddDays(parameters.ToDate, 1));


            query = query.OrderByDescending(x => x.CreatedDate);

            return new PagedList<Customer>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = parameters.PageSize > 0 ? query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList() : query.ToList(),
            };

        }

        public static XLWorkbook GenerateExcelReport(IEnumerable<Customer> customers)
        {
            var report = new List<CustomerReportViewModel>();

            foreach (var item in customers)
            {
                var vm = new CustomerReportViewModel
                {
                    Id = item.Id,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Mobile = item.Mobile,
                    Email = item.Email
                };

                report.Add(vm);
            }

            var dataTable = report.ToDataTable("شناسه", "نام", "نام خانوادگی", "موبایل", "ایمیل");

            var workbook = new XLWorkbook();
            workbook.Worksheets.Add(dataTable, "Report");

            return workbook;
        }

        public static IEnumerable<Customer> GetCustomersByIdList(IEnumerable<int> customerIds)
        {
            var customers = CustomerRepository.GetQuery(customer => customerIds.Contains(customer.Id) && customer.IsActive).ToList();

            return customers;
        }

        public static IEnumerable<Customer> GetCustomerInvoicesByCustomerId(int customerId)
        {
            var customers = CustomerRepository.GetQuery(customer => customer.Invoices.Any(c => c.CustomerId == customerId) && customer.IsActive).ToList();

            return customers;
        }

        public static DataTable ReportCustomersHaveDiscountCode()
        {
            using (var db = new Persistence.GarageDbContext())
            {
                IQueryable<Customer> customerQuery =
                    db.Customers
                    .Where(customer => customer.IsActive && customer.Invoices.Any(invoice => invoice.DiscountCode != null));

                var customerList = customerQuery.Select(customer => new CustomerReportViewModel
                {
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    Mobile = customer.Mobile,
                    Email = customer.Email
                }).ToList();

                return customerList.ToDataTable();
            }
        }

        public static Customer UpdateCustomer(Customer customer, bool needAuthorization = true)
        {
            if (customer == null)
                throw new ArgumentNullException();

            var oldEntity = CustomerRepository.Get(customer.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            customer = CustomerRepository.Update(customer, needAuthorization: needAuthorization);
            return customer;
        }

        public static Customer GetCustomerByMobile(string mobile)
        {
            return CustomerRepository.GetQuery(customer => customer.Mobile == mobile && customer.IsActive == true).SingleOrDefault();
        }
    }
}
