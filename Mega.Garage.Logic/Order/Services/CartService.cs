﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mega.Garage.Common;
using Mega.Garage.Persistence.Order.Entities;

namespace Mega.Garage.Logic.Order
{
    public class CartService
    {
        protected class CartRepository : BaseRepository<Cart, int> { }
        protected class CartItemRepository : BaseRepository<CartItem, int> { }

        public static Cart AddCart(Cart cart)
        {
            if (cart == null)
                throw new ArgumentNullException();
            cart = CartRepository.Add(cart);
            return cart;
        }

        public static bool DeleteCart(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var cart = CartRepository.Get(id);

            if (cart == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = CartRepository.SoftDelete(cart);
            return isDeleted;
        }

        public static IEnumerable<Cart> GetAllCarts()
        {
            return CartRepository.GetAll();
        }

        public static Cart GetCartById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var cart = CartRepository.Get(id);

            if (cart == null || !cart.IsActive)
                return null;

            return cart;
        }

        public static Cart UpdateCart(Cart cart)
        {
            if (cart == null)
                throw new ArgumentNullException();

            var oldEntity = CartRepository.Get(cart.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            cart = CartRepository.Update(cart);
            return cart;
        }

        public static CartItem AddCartItem(CartItem cartItem)
        {
            if (cartItem == null)
                throw new ArgumentNullException();

            cartItem = CartItemRepository.Add(cartItem);
            return cartItem;
        }

        public static bool DeleteCartItem(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var cartItem = CartItemRepository.Get(id);

            if (cartItem == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = CartItemRepository.SoftDelete(id);
            return isDeleted;
        }

        public static IEnumerable<CartItem> GetAllCartItems()
        {
            return CartItemRepository.GetAll();
        }

        public static CartItem GetCartItemById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var cartItem = CartItemRepository.Get(id);

            if (cartItem == null || !cartItem.IsActive)
                return null;

            return cartItem;
        }

        public static CartItem UpdateCartItem(CartItem cartItem)
        {
            if (cartItem == null)
                throw new ArgumentNullException();
            var oldEntity = CartItemRepository.Get(cartItem.Id);
            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();
            cartItem = CartItemRepository.Update(cartItem);
            return cartItem;
        }

        public static IEnumerable<Cart> GetCartByUserId(int userId)
        {
            return CartRepository.GetQuery(cart => cart.UserId == userId).ToList();
        }

        public static IEnumerable<CartItem> GetCartItemByCartId(int cartId)
        {
            if (cartId == 0)
                throw new ArgumentNullException();

            var cartItems = CartItemRepository.GetQuery(cartItem => cartItem.IsActive == true && cartItem.CartId == cartId).ToList();

            return cartItems;
        }

    }
}
