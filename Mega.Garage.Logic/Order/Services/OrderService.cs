﻿using Mega.Garage.Common;
using Mega.Garage.Logic.Order.ViewModel;
using Mega.Garage.Persistence.Entities;
using Mega.Garage.Persistence.Order.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Mega.Garage.Logic.Order.Services
{
    public class OrderService
    {
        protected class OrderItemRepository : BaseRepository<OrderItem, int> { }
        protected class OrderProcessRepository : BaseRepository<OrderProcess, int> { }
        protected class OrderProcessHistoryRepository : BaseRepository<OrderProcessHistory, int> { }

        public static OrderItem AddOrderItem(OrderItem orderItem)
        {

            if (orderItem == null)
                throw new ArgumentNullException();

            orderItem = OrderItemRepository.Add(orderItem, needAuthorization: false);
            return orderItem;
        }
        public static bool SetOrderItemAsCanceledByOrderIdAndInvoiceOrderNumber(string orderId, string orderNumber)
        {
            if (string.IsNullOrEmpty(orderId))
                throw new ArgumentNullException();

            var orderItem = OrderItemRepository.GetQuery(order => order.OrderDetailNumber == orderId && order.Invoice.OrderNumber == orderNumber).FirstOrDefault();
            if (orderItem == null)
                throw new ArgumentNullException("این سفارش متعلق به شما نیست");

            orderItem.IsCancelled = true;
            orderItem.CancelReasonErrorID = 1;
            _ = OrderItemRepository.Update(orderItem, true, false);
            return true;
        }

        public static bool DeleteOrderItem(int id, bool needAuthorization = true)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var orderItem = OrderItemRepository.Get(id);
            if (orderItem == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = OrderItemRepository.SoftDelete(orderItem, needAuthorization);
            return isDeleted;
        }

        public static IEnumerable<OrderItem> GetAllOrderItems()
        {
            return OrderItemRepository.GetAll();
        }

        public static OrderItem GetOrderItemById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var orderItem = OrderItemRepository.Get(id);

            if (orderItem == null || !orderItem.IsActive)
                return null;

            return orderItem;
        }

        public static OrderItem GetOrderItemByOrderNumber(string orderNumber)
        {
            if (string.IsNullOrEmpty(orderNumber))
                throw new ArgumentNullException();

            var InvoiceEntity = InvoiceService.GetInvoiceByOrderNumber(orderNumber);

            var orderItem = OrderItemRepository
                .GetQuery(oi => oi.IsActive && oi.InvoiceId == InvoiceEntity.Id)
                .FirstOrDefault();
            return orderItem;
        }

        public static IEnumerable<OrderItem> GetOrderItemByDiscountValue()
        {
            var orderItem = OrderItemRepository.GetQuery(item => item.Discount > 0).ToList();

            if (orderItem == null)
                return null;

            return orderItem;
        }

        public static IEnumerable<OrderItem> GetOrderItemsByInvoiceId(int id)
        {
            var orderItem = OrderItemRepository.GetQuery(item => item.InvoiceId == id && item.IsActive && !item.IsCancelled).ToList();

            if (orderItem == null)
                return null;

            return orderItem;
        }

        public static IEnumerable<OrderItem> GetOrderItemsByInvoiceIdList(IEnumerable<int> InvoiceIds)
        {
            var orderItem = OrderItemRepository.GetQuery(item => InvoiceIds.Contains(item.Id) && item.IsActive).ToList();

            if (orderItem == null)
                return null;

            return orderItem;
        }

        public static DataTable ReportCustomerOrderItemsByCustomerId(int customerId)
        {
            using (var db = new Persistence.GarageDbContext())
            {
                IQueryable<OrderItem> orderItemQuery =
                    db.OrderItems
                    .Where(orderItem => orderItem.IsActive && orderItem.Invoice.CustomerId == customerId);

                var orderItemListList = orderItemQuery.Select(orderItem => new OrderItemReportViewModel
                {
                    ProductTitle = orderItem.Product.CatalogTitle
                }).ToList();

                return orderItemQuery.ToDataTable();
            }
        }

        public static OrderItem UpdateOrderItem(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException();

            var oldEntity = OrderItemRepository.Get(orderItem.Id);
            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            orderItem = OrderItemRepository.Update(orderItem);
            return orderItem;
        }

        public static IEnumerable<OrderItem> GetAllOrderItemsByInvoiceId(int invoiceId)
        {
            return OrderItemRepository.GetQuery(orderItem => orderItem.IsActive == true && orderItem.InvoiceId == invoiceId).ToList();
        }

        public static OrderProcess AddOrderProcess(OrderProcess orderProcess)
        {
            if (orderProcess == null)
                throw new ArgumentNullException();

            orderProcess = OrderProcessRepository.Add(orderProcess);
            return orderProcess;
        }

        public static bool DeleteOrderProcess(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var orderProcess = OrderProcessRepository.Get(id);

            if (orderProcess == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = OrderProcessRepository.SoftDelete(orderProcess);
            return isDeleted;
        }

        public static IEnumerable<OrderProcess> GetAllOrderProcesses()
        {
            return OrderProcessRepository.GetAll();
        }

        public static OrderProcess GetOrderProcessById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var orderProcess = OrderProcessRepository.Get(id);

            if (orderProcess == null || !orderProcess.IsActive)
                return null;

            return orderProcess;
        }

        public static OrderProcess GetOrderProcessByCode(string code)
        {
            if (string.IsNullOrEmpty(code))
                throw new ArgumentNullException();

            var orderProcess = OrderProcessRepository.GetQuery(item => item.Code == code).FirstOrDefault();

            if (orderProcess == null || !orderProcess.IsActive)
                return null;

            return orderProcess;
        }

        public static OrderProcess UpdateOrderProcess(OrderProcess orderProcess)
        {
            if (orderProcess == null)
                throw new ArgumentNullException();

            var oldEntity = OrderProcessRepository.Get(orderProcess.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();


            return orderProcess;
        }

        public static OrderProcessHistory AddOrderProcessHistory(OrderProcessHistory orderProcessHistory)
        {
            if (orderProcessHistory == null)
                throw new ArgumentNullException();

            orderProcessHistory = OrderProcessHistoryRepository.Add(orderProcessHistory, needAuthorization: false);
            return orderProcessHistory;
        }

        public static bool DeleteOrderProcessHistory(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var orderProcessHistory = OrderProcessHistoryRepository.Get(id);

            if (orderProcessHistory == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = OrderProcessHistoryRepository.SoftDelete(orderProcessHistory);
            return isDeleted;
        }

        public static IEnumerable<OrderProcessHistory> GetAllOrderProcessHistories()
        {
            return OrderProcessHistoryRepository.GetAll();
        }

        public static OrderProcessHistory GetOrderProcessHistoryById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var orderProcessHistory = OrderProcessHistoryRepository.Get(id);

            if (orderProcessHistory == null || !orderProcessHistory.IsActive)
                return null;

            return orderProcessHistory;
        }

        public static OrderProcessHistory UpdateOrderProcessHistory(OrderProcessHistory orderProcessHistory)
        {
            if (orderProcessHistory == null)
                throw new ArgumentNullException();

            var oldEntity = OrderProcessHistoryRepository.Get(orderProcessHistory.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();
            orderProcessHistory = OrderProcessHistoryRepository.Update(orderProcessHistory);
            return orderProcessHistory;
        }

        public static IEnumerable<OrderProcessHistory> GetAllOrderProcessHistoriesByOrderItemId(int orderItemId)
        {
            return OrderProcessHistoryRepository.GetQuery(orderProcessHistory => orderProcessHistory.IsActive && orderProcessHistory.OrderId == orderItemId).ToList();
        }

        #region Report

        /// <summary>
        /// گزارش فروش محصولات
        /// </summary>
        /// <returns>گزارش فروش محصولات</returns>
        public static List<ProductReportViewModel> ProductSellReport()
        {
            var result = OrderItemRepository.GetQuery(x => x.IsActive).GroupBy(x => x.ProductId).Select(x => new ProductReportViewModel()
            {
                CatalogTitle = x.FirstOrDefault().Product.ShortTitle,
                Count = x.Count()
            }).ToList();

            return result;
        }
        #endregion

    }
}
