﻿using Mega.Garage.Common;
using Mega.Garage.Persistence.Order.Entities;
using System;
using System.Collections.Generic;
using Mega.Garage.Logic.Order.Validation;
using FluentValidation;
using System.Linq;
using Mega.Garage.Logic.Order.ViewModel;

namespace Mega.Garage.Logic.Order.Services
{
    public class ProductService
    {
        protected class ProductRepository : BaseRepository<Product, long>
        {
        }
        public static IEnumerable<Product> GetAllProducts()
        {
            return ProductRepository.GetAll();
        }
        public static IPagedList<Product> GetPagedListAllProducts(ProductSearchViewModel parameters)
        {
            var query = ProductRepository.GetQuery(product => product.IsActive);

            if (!string.IsNullOrEmpty(parameters.Title))
                query = query.Where(product => product.CatalogTitle.Contains(parameters.Title));

            if (parameters.Catalog.HasValue && parameters.Catalog > 0)
                query = query.Where(product => product.CatalogId == parameters.Catalog);

            if (parameters.FromPrice.HasValue)
                query = query.Where(product => product.UnitPrice >= parameters.FromPrice);

            if (parameters.ToPrice.HasValue)
                query = query.Where(product => product.UnitPrice <= parameters.ToPrice);

            query = query.OrderByDescending(x => x.CreatedDate);

            return new PagedList<Product>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList(),
            };
        }

        /// <summary>
        /// تعداد تمام محصولات فعال
        /// </summary>
        /// <returns></returns>
        public static int GetTotalProducts()
        {
            return ProductRepository.Count(x => x.IsActive);
        }
        public static Product GetProductById(long id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var product = ProductRepository.Get(id);

            if (product == null || !product.IsActive)
                return null;

            return product;
        }

        public static Product AddProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException();

            if (product.CatalogId == null)
                throw new InvalidAddOperationException("انتخاب طبقه محصول اجباری است");

            Persistence.Catalog.Entities.Catalog referencedCatalog = Catalog.Services.CatalogService.GetCatalogById((int)product.CatalogId);

            if (referencedCatalog == null)
                throw new InvalidAddOperationException("نام محصول وجود ندارد");
            product.CatalogCode = referencedCatalog.ConfigCode;
            product.CatalogTitle = referencedCatalog.Title;
            product.ShortTitle = referencedCatalog.ShortTitle;
            product.CatalogImage = referencedCatalog.ImagePath;
            product.SizeId = (product.SizeId != null) ? (int)product.SizeId : 1;
            product.SizeAdditiveCost = (product.SizeAdditiveCost != null) ? (int)product.SizeAdditiveCost : 0;
            product.GuaranteeId = (product.GuaranteeId != null) ? (int)product.GuaranteeId : 1;
            product.AvailableQty = (product.AvailableQty != null) ? (int)product.AvailableQty : 0;
            product.SellerId = (product.SellerId != null) ? (int)product.SellerId : 1;

            product.ViewCount = 0;
            product.VisitCount = 0;
            product.LikeCount = 0;

            var validation = new ProductValidation.ProductEntity().Validate(product);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            var entity = ProductRepository.Add(product);
            return entity;
        }

        public static Product UpdateProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException();

            var oldEntity = ProductRepository.Get(product.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new ProductValidation.ProductEntity().Validate(product);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            product = ProductRepository.Update(product);
            return product;
        }

        public static bool DeleteProduct(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var product = ProductRepository.Get(id);
            if (product == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = ProductRepository.SoftDelete(product);
            return isDeleted;
        }

        public static IEnumerable<Product> GetProductsByIdList(IEnumerable<long> listId)
        {
            return ProductRepository.GetQuery(product => product.IsActive && listId.Contains(product.Id)).ToList();
        }

        public static IEnumerable<Product> GetAllProductsByCatalogId(int? catalogId)
        {
            return ProductRepository.GetQuery(item => item.CatalogId == catalogId && item.IsActive).ToList();
        }

        internal static string GetImageUrlByProductId(long? productId)
        {
            if (productId == null)
                throw new ArgumentNullException();

            var Entity =
                ProductRepository
                .GetQuery(x => x.Id == productId.Value)
                .Select(x => new { x.ColorImage, x.CatalogImage }).FirstOrDefault();

            if (Entity == null)
                throw new ArgumentNullException();

            return FileUpload.Services.UploadService.GetCDNFilePath(Entity.ColorImage);
        }
    }
}
