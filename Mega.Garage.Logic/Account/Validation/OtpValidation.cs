﻿using FluentValidation;
using Mega.Garage.Logic.Account.ViewModel;
using Mega.Garage.Persistence.Account.Entities;

namespace Mega.Garage.Logic.Account.Validation
{
    public class OtpValidation
    {
        public class OtpEntity : AbstractValidator<OTP>
        {
            public OtpEntity()
            {
                RuleFor(x => x.UserMobile)
                    .NotNull().WithMessage("وارد کردن تلفن همراه الزامی می باشد.");

                RuleFor(x => x.UserMobile)
                    .Must(Common.Validate.IsMobile).WithMessage("تلفن همراه وارد شده معتبر نمی باشد.");

            }

        }

        public class OtpVerifyEntity : AbstractValidator<OtpVerifyViewModel>
        {
            public OtpVerifyEntity()
            {
                RuleFor(x => x.Mobile)
                    .NotNull().WithMessage("وارد کردن تلفن همراه الزامی می باشد.")
                    .NotEmpty().WithMessage("وارد کردن تلفن همراه الزامی می باشد.")
                    .Must(Common.Validate.IsMobile).WithMessage("تلفن همراه وارد شده معتبر نمی باشد.");

                RuleFor(x => x.Code)
                    .NotNull().WithMessage("وارد کردن کد الزامی است.")
                    .NotEmpty().WithMessage("وارد کردن کد الزامی است.")
                    .Length(5).WithMessage("کد وارد شده صحیح نیست.");
            }
        }
    }
}
