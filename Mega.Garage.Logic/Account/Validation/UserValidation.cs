﻿using FluentValidation;
using Mega.Garage.Persistence.Account.Entities;

namespace Mega.Garage.Logic.Account.Validation
{
    internal class UserValidation
    {
        internal class UserEntity : AbstractValidator<User>
        {
            public UserEntity()
            {
                RuleFor(x => x.Id).GreaterThan(0).WithMessage("شناسه وارد شده معتبر نمی باشد!");
                RuleFor(x => x.UserName).MaximumLength(50).WithMessage("طول رشته ی نام کاربری نمی تواند بیشتر از 50 حرف باشد.");
                RuleFor(x => x.UserName).MinimumLength(5).WithMessage("طول رشته ی نام کاربری نمی تواند کمتر از 5 حرف باشد");
                RuleFor(x => x.PasswordHash).NotEmpty().NotNull().WithMessage("برای حساب کاربری خود رمزی وارد کنید.");
                RuleFor(x => x.FirstName).Length(3, 30).WithMessage("نام بین 3 تا 30 حرف باید باشد.");
                RuleFor(x => x.LastName).Length(3, 100).WithMessage("نام خانوادگی بین 3 تا 100 حرف باید باشد.");
                RuleFor(x => x.Email).Must(Common.Validate.IsEmail).WithMessage("رشته ی وارد شده شبیه به یک ایمیل نیست.");
                RuleFor(x => x.ClientId).ExclusiveBetween(1, 5).WithMessage("کد کلاینت اشتباه وارد شده است.");
                RuleFor(x => x.UserMobile).Must(Common.Validate.IsMobile).WithMessage("رشته وارد شده موبایل معتبری نیست.");
            }
        }
    }
}
