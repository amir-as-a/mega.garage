﻿namespace Mega.Garage.Logic.Account.ViewModel
{
    public class OtpVerifyViewModel
    {
        public string Mobile { get; set; }
        public string Code { get; set; }
    }
}
