﻿using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Account.Validation;
using Mega.Garage.Logic.Account.ViewModel;
using Mega.Garage.Persistence.Account.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mega.Garage.Logic.Account.Servies
{
    public class OtpService
    {
        protected class OtpRepository : BaseRepository<OTP, long> { }
        internal static readonly string OneTimeUseSMSText = $"کد تائید شما : {{0}} {Environment.NewLine} ایرانی کار - اسپیدی";
        public static OTP AddOtp(OTP otp)
        {
            var validation = new OtpValidation.OtpEntity().Validate(otp);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            otp = OtpRepository.Add(otp, needAuthorization: false);

            return otp;
        }

        public static bool DeleteOtp(long id)
        {
            var deleted = OtpRepository.SoftDelete(id);
            return deleted;
        }

        public static IEnumerable<OTP> GetAllOtps()
        {
            return OtpRepository.GetAll();
        }

        public static OTP GetOtpById(long id)
        {
            return OtpRepository.Get(id);
        }

        public static OTP GetOtpByUserMobile(string Mobile)
        {
            var entity =
                OtpRepository
                .GetQuery(otp => otp.UserMobile == Mobile && !otp.IsEntered).SingleOrDefault();
            return entity;
        }

        public static OTP UpdateOtp(OTP otp)
        {
            otp = OtpRepository.Update(otp, needAuthorization: false);
            return otp;
        }

        // ثبت یک تلفن در جدول OTP
        public static bool RegisterOtp(string mobile)
        {
            mobile = mobile.GetStandardFarsiString();
            var entity = OtpRepository.GetQuery(otp => otp.UserMobile == mobile && otp.IsEntered == false).FirstOrDefault();

            // یافت نشدن شماره تلفن در جدول OTP
            if (entity == null)
            {
                entity = new OTP()
                {
                    UserMobile = mobile,
                    OneTimePassword = new Random().Next(11111, 99999).ToString(),
                    GenerateDate = DateTime.Now,
                    SendDate = DateTime.Now,
                    ExpireDate = DateTime.Now.AddMinutes(15),
                    IsEntered = false,
                    EnteredDate = null,
                    ClientId = 1
                };

                entity = OtpService.AddOtp(entity);
                Notify.Services.SMSService.AddSms(entity.UserMobile, string.Format(OneTimeUseSMSText, entity.OneTimePassword));

                return entity.Id > 0;
            }
            else
            {
                // اگر کد وارد را کاربر یک مرتبه وارد کرده بود و یا از تاریخ انقضای آن گذشته بود، مجددا رمز تولید می شود
                if (entity.ExpireDate <= DateTime.Now)
                    entity.OneTimePassword = new Random().Next(11111, 99999).ToString();

                // به روز کردن تاریخ انقضاء
                entity.ExpireDate = DateTime.Now.AddMinutes(15);
                entity.IsEntered = false;
                entity = OtpService.UpdateOtp(entity);

                Notify.Services.SMSService.AddSms(entity.UserMobile, string.Format(OneTimeUseSMSText, entity.OneTimePassword));

                return entity.Id > 0;
            }
        }

        public static MegaViewModel<bool> Verify(OtpVerifyViewModel otpVerifyViewModel)
        {
            var validation = new OtpValidation.OtpVerifyEntity().Validate(otpVerifyViewModel);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            otpVerifyViewModel.Mobile = otpVerifyViewModel.Mobile.GetStandardFarsiString();
            var entity = OtpRepository.GetQuery(otp => otp.OneTimePassword == otpVerifyViewModel.Code && otp.UserMobile == otpVerifyViewModel.Mobile).FirstOrDefault();

            if (entity != null)
            {
                if (entity.IsEntered)
                    return new MegaViewModel<bool>("از این کد قبلا استفاده شده و اعتبار دوباره ندارد. لطفا کد جدید از سامانه بگیرید.");

                if (entity.ExpireDate < DateTime.Now)
                    return new MegaViewModel<bool>("کد وارد شده منقضی شده است، لطفا مجددا تلاش کنید.");

                if (entity.OneTimePassword != otpVerifyViewModel.Code)
                    return new MegaViewModel<bool>("کد وارد شده صحیح نیست.");


                entity.IsEntered = true;
                entity.EnteredDate = DateTime.Now;

                OtpService.UpdateOtp(entity);

                return new MegaViewModel<bool>(true);
            }
            else
                return new MegaViewModel<bool>("شماره تلفن همراه یافت نشد.");
        }
    }
}
