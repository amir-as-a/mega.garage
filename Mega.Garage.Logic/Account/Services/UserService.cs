﻿using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Account.Validation;
using Mega.Garage.Persistence.Account.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mega.Garage.Logic.Account.Services
{


    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizationAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var UserData = UserService.IsAuthenticated();
            if (UserData == null || !UserData.IsActive || UserData.IsLocked)
            {

                string[] Roles = this.Roles?.Split(',').Where(x => !string.IsNullOrEmpty(x)).ToArray();
                string actionName = filterContext.ActionDescriptor.ActionName;
                string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new
                    {
                        action = "Login",
                        controller = "Accounting",
                        area = "",
                        data = filterContext.HttpContext.Request.Url.AbsoluteUri
                    }));
            }

        }
    }

    public class UserService
    {
        protected class UserRepository : BaseRepository<User, int> { }
        protected const string AuthenticationCookieName = "Apa_Co_Auth";
        protected const string Delimeter = "___";
        protected static string SecretKey
        {
            get
            {
                string browserName = Network.BrowserName();
                string IP = Network.GetIP();
                return Cryptography.Encrypt(string.Join(".", IP, browserName));
            }
        }

        public static User IsAuthenticated()
        {
            var cookieValue = ReadAuthCookie();
            if (cookieValue.HasValue)
            {
                return GetUserData(cookieValue.Value.userName, cookieValue.Value.passWord);
            }
            return null;
        }

        public static (string userName, string passWord)? ReadAuthCookie()
        {
            string cookieValue = Cookie.ReadCookie(AuthenticationCookieName);
            if (string.IsNullOrEmpty(cookieValue))
                return null;

            if (Cryptography.TryDecrypt(cookieValue, SecretKey, out string decryptedCookie))
            {
                string[] decryptedArray = System.Text.RegularExpressions.Regex.Split(decryptedCookie, Delimeter);
                string userName = decryptedArray[0];
                string passWord = decryptedArray[1];

                return (userName, passWord);
            }
            else
            {
                return null;
            }
        }

        public static void SetAuthenticationCookie(string userName, string passWord, bool rememberMe)
        {
            if (IsUserDataValid(userName, passWord))
            {
                string cookieValue = Cryptography.EncryptByUV(userName + Delimeter + passWord, SecretKey);
                if (rememberMe)
                    Cookie.SetCookie(AuthenticationCookieName, cookieValue, DateTime.Now.AddDays(30));
                else
                    Cookie.SetCookie(AuthenticationCookieName, cookieValue);
            }
        }

        public static User GetUserByUserId(long userId)
        {
            var userEntity = UserRepository.GetQuery(x => x.Id == userId && x.IsActive).FirstOrDefault();
            return userEntity;
        }

        public static User GetUserByUserName(string userName)
        {

            var UserEntity = UserRepository.GetQuery(x => x.UserName.ToLower() == userName.ToLower() && x.IsActive).FirstOrDefault();
            return UserEntity;
        }

        public static User GetUserByUserNameAndEmail(string userName, string email)
        {
            var UserEntity = UserRepository.GetQuery(x => x.UserName.ToLower() == userName.ToLower() && x.Email == email && x.IsActive).FirstOrDefault();
            return UserEntity;
        }

        public static User GetUserByUserNameAndMobile(string userName, string mobile)
        {
            var UserEntity =
                UserRepository
                .GetQuery(x => x.UserName.ToLower() == userName.ToLower() && x.Email == mobile && x.IsActive)
                .FirstOrDefault();

            return UserEntity;
        }
        public static User GetUserByUserNameAndMobileOrEmail(string userName, string mobile, string email)
        {
            var UserEntity =
                UserRepository
                .GetQuery(x => x.UserName.ToLower() == userName.ToLower() && (x.Email == mobile || x.Email == email) && x.IsActive)
                .FirstOrDefault();

            return UserEntity;
        }

        public static bool IsUserValidByUserName(string userName)
        {
            var UserExists = UserRepository.GetQuery(x => x.UserName.ToLower() == userName.ToLower() && x.IsActive).Count();
            return UserExists == 1;
        }

        public static bool IsUserValidByUserId(long userId)
        {
            var UserExists = UserRepository.GetQuery(x => x.Id == userId && x.IsActive).Count();
            return UserExists == 1;
        }

        public static bool IsUserDataValid(string userName, string passWord)
        {
            string EncryptedPassword = EncryptPassword(userName, passWord);

            var UserExists =
                UserRepository
                .GetQuery(x => x.UserName.ToLower() == userName.ToLower() && x.PasswordHash == EncryptedPassword && x.IsActive)
                .Count();

            return UserExists == 1;
        }

        public static User GetUserData(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return null;

            string EncryptedPassword = EncryptPassword(userName, password);

            var UserExists =
                UserRepository
                .GetQuery(x => x.UserName.ToLower() == userName.ToLower() && x.PasswordHash == EncryptedPassword && x.IsActive)
                .FirstOrDefault();
            return UserExists;
        }

        public static bool Register(string userName, string firstName, string lastName, string passWord, string mobileNo, string email, Enums.Sex sex)
        {
            User UserEntity = new User()
            {
                CreatedBy = 1,
                CreatedDate = DateTime.Now,
                FirstName = firstName,
                IsActive = true,
                Guid = Guid.NewGuid().ToString(),
                IsLocked = false,
                UserMobile = mobileNo,
                ModifiedBy = 1,
                LastName = lastName,
                ModifiedDate = DateTime.Now,
                Reserve1 = true,
                Reserve2 = true,
                Email = email,
                PasswordHash = EncryptPassword(userName, passWord),
                LastLoginDate = DateTime.Now,
                UserName = userName,
                UserCode = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15),
                UserTypeId = 1,
                ClientId = 1 // Speedy
            };

            var validation = new UserValidation.UserEntity().Validate(UserEntity);
            if (validation.IsValid)
            {
                _ = UserRepository.Add(UserEntity);
                return true;
            }
            throw new ValidationException(validation.Errors);

        }

        public static bool UpdateProfile(int id, string userName, string firstName, string lastName, string mobileNo, string email, Enums.Sex sex)
        {
            var currentUser = GetUserById(id);
            if (currentUser == null)
                throw MegaException.ThrowException("چنین کاربری در سامانه پیدا نشد.");

            if (currentUser.IsLocked)
                throw MegaException.ThrowException("حساب کاربری این کاربر قفل است برای ویرایش ابتدا کاربر را فعال کنید.");

            currentUser.FirstName = firstName;
            currentUser.LastName = lastName;
            currentUser.Email = email;
            currentUser.UserMobile = mobileNo;
            var validation = new UserValidation.UserEntity().Validate(currentUser);
            if (validation.IsValid)
            {
                _ = UserRepository.Update(currentUser);
                return true;
            }
            throw new ValidationException(validation.Errors);
        }

        public static bool LockUser(int id)
        {
            var userEntity = GetUserById(id);
            if (userEntity == null)
                throw MegaException.ThrowException("کاربری با این شناسه در پایگاه داده وجود ندارد");

            userEntity.IsLocked = true;
            UserService.UpdateUser(userEntity);
            return true;
        }

        public static bool UnlockUser(int id)
        {
            var userEntity = GetUserById(id);
            if (userEntity == null)
                throw MegaException.ThrowException("کاربری با این شناسه در پایگاه داده وجود ندارد");

            userEntity.IsLocked = false;
            UserService.UpdateUser(userEntity);
            return true;
        }

        public static bool Logout()
        {
            Cookie.ExpireCookie(AuthenticationCookieName);
            return true;
        }

        public static bool ChangeUserPassword(string userName, string oldPassword, string newPassword)
        {
            string encryptedOldPassword = EncryptPassword(userName, oldPassword);
            var userData = GetUserData(userName, encryptedOldPassword);
            if (userData == null)
                throw MegaException.ThrowException("چنین کاربری پیدا نشد.");

            string encryptedNewPassword = EncryptPassword(userName, newPassword);
            userData.PasswordHash = encryptedNewPassword;
            UserRepository.Update(userData);
            return true;
        }

        public static string GetUserPasswordByMobileOrEmail(string userName, string mobile, string email)
        {
            var userPassword =
                UserRepository
                .GetQuery(x => x.UserName.ToLower() == userName.ToLower() && (x.Email == mobile || x.Email == email) && x.IsActive)
                .Select(x => x.PasswordHash)
                .FirstOrDefault();

            if (string.IsNullOrEmpty(userPassword))
                throw Common.MegaException.ThrowException("چنین کاربری پیدا نشد.");

            string decryptedPassword = DecryptPassword(userName, userPassword);
            return decryptedPassword;
        }

        public static string EncryptPassword(string userName, string passWord)
        {
            string userNameKey = userName.GetTypeCode().ToString();
            return Cryptography.EncryptByUV(userNameKey, passWord);
        }

        internal static string DecryptPassword(string userName, string encryptedPassword)
        {
            string userNameKey = userName.GetTypeCode().ToString();
            return Cryptography.Decrypt(encryptedPassword, userNameKey);
        }

        public static User AddUser(User user)
        {
            var validation = new UserValidation.UserEntity().Validate(user);
            if (validation.IsValid)
            {
                user = UserRepository.Add(user);
                return user;
            }
            throw new ValidationException(validation.Errors);
        }

        public static bool DeleteUser(int id)
        {
            var deleted = UserRepository.SoftDelete(id);
            return deleted;
        }

        public static IEnumerable<User> GetAllUsers()
        {
            return UserRepository.GetAll();
        }

        public static User GetUserById(int id)
        {
            return UserRepository.Get(id);
        }

        public static string GetFullnameUserById(int id)
        {
            return UserRepository.GetQuery(x => x.Id == id).Select(x => x.FirstName + " " + x.LastName).FirstOrDefault();
        }

        public static User UpdateUser(User user)
        {
            var currentUser = GetUserById(user.Id);
            currentUser.FirstName = user.FirstName;
            currentUser.LastName = user.LastName;
            currentUser.UserMobile = user.UserMobile;
            currentUser.Email = user.Email;

            currentUser = UserRepository.Update(currentUser);
            return currentUser;
        }
        public static User ResetPassword(int userId, string newPassword, string confirmNewPassword)
        {
            if (newPassword != confirmNewPassword)
                throw Common.MegaException.ThrowException("رمزعبور و تکرار آن با هم برابر نیستند.");

            var currentUser = GetUserById(userId);

            currentUser.PasswordHash = EncryptPassword(currentUser.UserName, newPassword);

            currentUser = UserRepository.Update(currentUser);
            return currentUser;
        }
    }
}

