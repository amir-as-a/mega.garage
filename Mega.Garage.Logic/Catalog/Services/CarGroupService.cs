﻿using Mega.Garage.Logic;
using Mega.Garage.Persistence.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mega.Garage.Logic.Catalog.Services
{
    public static class CarGroupService
    {
        protected class CarGroupRepository : BaseRepository<CarGroup, int>
        {
        }

        public static CarGroup AddCarGroup(CarGroup carGroup)
        {
            if (carGroup == null)
                throw new ArgumentNullException();

            carGroup = CarGroupRepository.Add(carGroup);
            return carGroup;
        }

        public static bool DeleteCarGroup(int id)
        {
            var deleted = CarGroupRepository.SoftDelete(id);
            return deleted;
        }

        public static IEnumerable<CarGroup> GetAllCarGroups()
        {
            return CarGroupRepository.GetAll();
        }

        public static CarGroup GetCarGroupById(int id)
        {
            return CarGroupRepository.Get(id);
        }

        public static CarGroup GetCarGroupByCode(int code)
        {
            if (code == 0)
                throw new ArgumentNullException();

            var carGroup = CarGroupRepository.GetQuery(cg =>
                cg.IsActive &&
                cg.Code == code
                ).FirstOrDefault();

            return carGroup;
        }

        public static CarGroup UpdateCarGroup(CarGroup carGroup)
        {
            if (carGroup == null)
                throw new ArgumentNullException();

            carGroup = CarGroupRepository.Update(carGroup);
            return carGroup;
        }
    }
}
