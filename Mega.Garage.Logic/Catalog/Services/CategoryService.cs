﻿using System;
using Mega.Garage.Common;
using Mega.Garage.Persistence.Catalog.Entities;
using System.Collections.Generic;
using Mega.Garage.Logic.Catalog.Validation;
using FluentValidation;
using Mega.Garage.Logic.Catalog.ViewModel;
using System.Linq;

namespace Mega.Garage.Logic.Catalog.Services
{
    public class CategoryService
    {
        protected class CategoryRepository : BaseRepository<Category, int> { }

        public static Category AddCategory(Category category)
        {
            if (category == null)
                throw new ArgumentNullException();

            var validation = new CategoryValidation.CategoryEntity().Validate(category);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            category = CategoryRepository.Add(category);
            return category;
        }

        public static bool DeleteCategory(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var category = CategoryRepository.Get(id);

            if (category == null)
                throw new InvalidDeleteOperationException();

            if (CatalogService.GetCatalogCountByCategoryId(category.Id) > 0)
                throw new Exception("برای این دسته بندی کاتالوگ وجود دارد.");

            bool IsDeleted = CategoryRepository.SoftDelete(category);
            return IsDeleted;
        }

        public static IEnumerable<Category> GetAllCategories()
        {
            return CategoryRepository.GetAll();
        }

        public static IPagedList<Category> GetAllCategoriesByPage(CategorySearchViewModel parameters)
        {
            var query = CategoryRepository.GetQuery();

            if (!string.IsNullOrEmpty(parameters.Title))
                query = query.Where(category => category.Title.Contains(parameters.Title));

            if (!string.IsNullOrEmpty(parameters.CategoryCode))
                query = query.Where(category => category.CategoryCode.Contains(parameters.CategoryCode));

            query = query.OrderByDescending(x => x.CreatedDate);

            return new PagedList<Category>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList(),
            };
        }

        public static Category GetCategoryById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var category = CategoryRepository.Get(id);

            if (category == null || !category.IsActive)
                return null;

            return category;
        }

        public static Category UpdateCategory(Category category)
        {
            if (category == null)
                throw new ArgumentNullException();

            var validation = new CategoryValidation.CategoryEntity().Validate(category);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            var oldEntity = CategoryRepository.Get(category.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            category = CategoryRepository.Update(category);
            return category;
        }

        public static IEnumerable<Category> GetSubjectCategories()
        {
            const int subjectsCategoryId = 3;
            return CategoryRepository.GetQuery(category => category.IsActive && category.ParentCategoryId == subjectsCategoryId);
        }
    }
}
