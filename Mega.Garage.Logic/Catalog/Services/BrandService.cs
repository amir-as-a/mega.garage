﻿using System;
using System.Collections.Generic;
using Mega.Garage.Logic.Catalog.Validation;
using Mega.Garage.Logic.OnSite.Services;
using Mega.Garage.Persistence.Catalog.Entities;
using FluentValidation;
using Mega.Garage.Logic.Catalog.ViewModel;
using System.Linq;
using ClosedXML.Excel;
using Mega.Garage.Common;
using Mega.Garage.Logic;

namespace Mega.Garage.Logic.Catalog.Services
{
    public class BrandService
    {
        protected class BrandRepository : BaseRepository<Brand, int> { }
        public static Brand AddBrand(Brand brand)
        {
            var validation = new BrandValidation.BrandEntity().Validate(brand);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            brand = BrandRepository.Add(brand);

          
            return brand;
        }

        public static bool DeleteBrand(int id)
        {
            var deleted = BrandRepository.SoftDelete(id);
     
            return deleted;
        }

        public static IEnumerable<Brand> GetAllBrands()
        {
            return BrandRepository.GetAll();
        }

        public static IPagedList<Brand> GetAllBrands(BrandSearchViewModel parameters)
        {
            var query = BrandRepository.GetQuery();

            if (!string.IsNullOrEmpty(parameters.Title))
                query = query.Where(brand => brand.Title.Contains(parameters.Title));

            if (!string.IsNullOrEmpty(parameters.Country))
                query = query.Where(brand => brand.Country.Contains(parameters.Country));

            query = query.OrderByDescending(x => x.CreatedDate);

            return new PagedList<Brand>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList(),
            };
        }

        public static Brand GetBrandById(int id)
        {
            return BrandRepository.Get(id);
        }

        public static IEnumerable<Brand> GetBrandByCountry(string country)
        {
            if (country == null)
                throw new ArgumentNullException();

            var brand = BrandRepository.GetQuery(c => c.IsActive && c.Country == country);

            return brand;
        }

        public static Brand UpdateBrand(Brand brand)
        {
            var validation = new BrandValidation.BrandEntity().Validate(brand);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            brand = BrandRepository.Update(brand);
            return brand;
        }
        public static XLWorkbook GenerateExcelReport(IEnumerable<Brand> catalogs)
        {
            var report = new List<BrandReportViewModel>();

            foreach (var item in catalogs)
            {
                var vm = new BrandReportViewModel
                {
                    Title = item.Title,
                    EnglishTitle = item.EnglishTitle,
                    Description = item.Description,
                    Country = item.Country
                };

                report.Add(vm);
            }

            var dataTable = report.ToDataTable("عنوان", "عنوان انگلیسی", "توضیحات", "کشور");

            var workbook = new XLWorkbook();
            workbook.Worksheets.Add(dataTable, "Report");

            return workbook;
        }
    }
}
