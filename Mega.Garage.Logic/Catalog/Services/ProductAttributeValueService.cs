﻿using System;
using Mega.Garage.Common;
using Mega.Garage.Persistence.Catalog.Entities;
using System.Collections.Generic;

namespace Mega.Garage.Logic.Catalog.Services
{
    public class ProductAttributeValueService
    {
        protected class ProductAttributeValueRepository : BaseRepository<ProductAttributeValue, int> { }
        public ProductAttributeValue AddProductAttributeValue(ProductAttributeValue productAttributeValue)
        {
            if (productAttributeValue == null)
                throw new ArgumentNullException();

            productAttributeValue = ProductAttributeValueRepository.Add(productAttributeValue);
            return productAttributeValue;
        }

        public static bool DeleteProductAttributeValue(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var productAttributeValue = ProductAttributeValueRepository.Get(id);

            if (productAttributeValue == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = ProductAttributeValueRepository.SoftDelete(productAttributeValue);
            return IsDeleted;
        }

        public static IEnumerable<ProductAttributeValue> GetAllProductAttributeValues()
        {
            return ProductAttributeValueRepository.GetAll();
        }

        public static IEnumerable<ProductAttributeValue> GetProductAttributeValuesByAttributeKeyId(int keyId)
        {
            var values = ProductAttributeValueRepository.GetQuery(p => p.IsActive && p.ProductAttributeKeyId == keyId);
            return values;
        }

        public static ProductAttributeValue GetProductAttributeValueById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var productAttributeValue = ProductAttributeValueRepository.Get(id);

            if (productAttributeValue == null || !productAttributeValue.IsActive)
                return null;

            return productAttributeValue;
        }

        public static ProductAttributeValue UpdateProductAttributeValue(ProductAttributeValue productAttributeValue)
        {
            if (productAttributeValue == null)
                throw new ArgumentNullException();

            var oldEntity = ProductAttributeValueRepository.Get(productAttributeValue.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            productAttributeValue = ProductAttributeValueRepository.Update(productAttributeValue);
            return productAttributeValue;
        }

    }
}
