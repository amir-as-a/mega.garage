﻿using System;
using Mega.Garage.Common;
using Mega.Garage.Persistence.Catalog.Entities;

using System.Collections.Generic;

namespace Mega.Garage.Logic.Catalog.Services
{
    public class ProductAttributeKeyService
    {
        protected class ProductAttributeKeyRepository : BaseRepository<ProductAttributeKey, int> { }
        public static ProductAttributeKey AddProductAttributeKey(ProductAttributeKey productAttributeKey)
        {
            if (productAttributeKey == null)
                throw new ArgumentNullException();

            productAttributeKey = ProductAttributeKeyRepository.Add(productAttributeKey);
            return productAttributeKey;
        }

        public static bool DeleteProductAttributeKey(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var productAttributeKey = ProductAttributeKeyRepository.Get(id);

            if (productAttributeKey == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = ProductAttributeKeyRepository.SoftDelete(productAttributeKey);
            return IsDeleted;
        }

        public static IEnumerable<ProductAttributeKey> GetAllProductAttributeKeys()
        {
            return ProductAttributeKeyRepository.GetAll();
        }

        public static ProductAttributeKey GetProductAttributeKeyById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var productAttributeKey = ProductAttributeKeyRepository.Get(id);

            if (productAttributeKey == null || !productAttributeKey.IsActive)
                return null;

            return productAttributeKey;
        }

        public static ProductAttributeKey UpdateProductAttributeKey(ProductAttributeKey productAttributeKey)
        {
            if (productAttributeKey == null)
                throw new ArgumentNullException();

            var oldEntity = ProductAttributeKeyRepository.Get(productAttributeKey.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            productAttributeKey = ProductAttributeKeyRepository.Update(productAttributeKey);
            return productAttributeKey;
        }

    }
}
