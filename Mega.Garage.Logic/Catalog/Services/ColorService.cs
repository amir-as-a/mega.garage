﻿using System;
using Mega.Garage.Common;
using System.Collections.Generic;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Logic.OnSite.Services;
using Mega.Garage.Logic.Catalog.Validation;
using FluentValidation;
using Mega.Garage.Logic.Catalog.ViewModel;
using System.Linq;
using ClosedXML.Excel;

namespace Mega.Garage.Logic.Catalog.Services
{
    public class ColorService
    {
        protected class ColorRepository : BaseRepository<Color, int>
        {
        }

        public static Color AddColor(Color color)
        {
            if (color == null)
                throw new ArgumentNullException();

            var validation = new ColorValidation.ColorEntity().Validate(color);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            color = ColorRepository.Add(color);

            //if (color != null)
            //{
            //    OnSiteService.AddColorTaxonomy(color);
            //}

            return color;
        }

        public static bool DeleteColor(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var color = ColorRepository.Get(id);

            if (color == null)
                throw new InvalidDeleteOperationException();

            var deleted = ColorRepository.SoftDelete(color);

            //if (deleted)
            //{
            //    OnSiteService.DeleteColorTaxonomy(id.ToString());
            //}

            return deleted;
        }

        public static IEnumerable<Color> GetAllColors()
        {
            return ColorRepository.GetAll();
        }

        public static IPagedList<Color> GetAllColorsByPage(ColorSearchViewModel parameters)
        {
            var query = ColorRepository.GetQuery();

            if (!string.IsNullOrEmpty(parameters.Title))
                query = query.Where(brand => brand.Title.Contains(parameters.Title));

            query = query.OrderByDescending(x => x.CreatedDate);

            return new PagedList<Color>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList(),
            };
        }

        public static Color GetColorById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var color = ColorRepository.Get(id);

            if (color == null || !color.IsActive)
                return null;

            return color;
        }

        public static Color UpdateColor(Color color)
        {
            if (color == null)
                throw new ArgumentNullException();

            var validation = new ColorValidation.ColorEntity().Validate(color);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            var oldEntity = ColorRepository.Get(color.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            color = ColorRepository.Update(color);

            //if (color != null)
            //{
            //    OnSiteService.UpdateColorTaxonomy(color);
            //}

            return color;
        }
        public static XLWorkbook GenerateColorsExcelReport(IEnumerable<Color> colors)
        {
            var report = new List<ColorReportViewModel>();

            foreach (var item in colors)
            {
                var vm = new ColorReportViewModel
                {
                    Title = item.Title,
                    EnglishTitle = item.EnglishTitle,
                    ColorHexCode = item.ColorHexCode
                };

                report.Add(vm);
            }

            var dataTable = report.ToDataTable("عنوان", "عنوان انگلیسی", "کدهگزا دسیمال");

            var workbook = new XLWorkbook();
            workbook.Worksheets.Add(dataTable, "Report");

            return workbook;
        }
    }
}