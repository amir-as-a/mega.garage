﻿using System;
using Mega.Garage.Common;
using Mega.Garage.Persistence.Catalog.Entities;
using System.Collections.Generic;
using System.Data.Entity;
using Mega.Garage.Logic.Catalog.Validation;
using FluentValidation;
using System.Linq.Expressions;
using System.Linq;
using Mega.Garage.Logic.Catalog.ViewModel;
using ClosedXML.Excel;
using Mega.Garage.Persistence.Order.Entities;
using Mega.Garage.Logic.Order.Services;

namespace Mega.Garage.Logic.Catalog.Services
{
    public class CatalogService
    {
        protected class CatalogRepository : BaseRepository<Persistence.Catalog.Entities.Catalog, int>
        {

        }
        internal class CategoryRepository : BaseRepository<Category, int> { }
        internal class CatalogCategoryRespository : BaseRepository<CatalogCategory, int> { }
        internal class CatalogPropertyRespository : BaseRepository<CatalogProperty, int> { }
        internal class CatalogStatusRespository : BaseRepository<CatalogStatus, int> { }
        internal class CatalogTypeRespository : BaseRepository<CatalogType, int> { }

        #region Catalog Category
        public static CatalogCategory AddCatalogCategory(CatalogCategory catalogCategory)
        {
            return CatalogCategoryRespository.Add(catalogCategory);
        }
        public static bool DeleteCatalogCategory(int id)
        {
            return CatalogCategoryRespository.SoftDelete(id);
        }
        public static IEnumerable<CatalogCategory> GetAllCatalogCategories()
        {
            return CatalogCategoryRespository.GetQuery(x => x.IsActive).Include(x => x.Category).Include(x => x.Catalog);
        }
        public static CatalogCategory GetCatalogCategoryById(int id)
        {
            return CatalogCategoryRespository.Get(id);
        }
        public static CatalogCategory UpdateCatalogCategory(CatalogCategory catalogCategory)
        {
            if (catalogCategory == null)
                throw new ArgumentNullException();

            var oldEntity = CatalogCategoryRespository.Get(catalogCategory.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            catalogCategory = CatalogCategoryRespository.Update(catalogCategory);

            return catalogCategory;
        }

        public static XLWorkbook GenerateCategoriesExcelReport(IEnumerable<Category> categories)
        {
            var report = new List<CategoriesReportViewModel>();

            foreach (var item in categories)
            {
                var vm = new CategoriesReportViewModel
                {
                    Title = item.Title,
                    EnglishTitle = item.EnglishTitle,
                    Description = item.Description,
                    CategoryCode = item.CategoryCode
                };

                report.Add(vm);
            }

            var dataTable = report.ToDataTable("عنوان", "عنوان انگلیسی", "توضیحات", "شناسه طبقه");

            var workbook = new XLWorkbook();
            workbook.Worksheets.Add(dataTable, "Report");

            return workbook;
        }
        #endregion

        #region Catalog Property
        public static CatalogProperty AddCatalogProperty(CatalogProperty catalogProperty)
        {
            if (catalogProperty == null)
                throw new ArgumentNullException();

            catalogProperty = CatalogPropertyRespository.Add(catalogProperty);
            return catalogProperty;
        }

        public static bool DeleteCatalogProperty(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var catalogProperty = CatalogPropertyRespository.Get(id);

            if (catalogProperty == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = CatalogPropertyRespository.SoftDelete(catalogProperty);
            return IsDeleted;
        }

        public static IEnumerable<CatalogProperty> GetAllCatalogProperties()
        {
            return CatalogPropertyRespository.GetAll();
        }

        public static CatalogProperty GetCatalogPropertyById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var catalogProperty = CatalogPropertyRespository.Get(id);

            if (catalogProperty == null || !catalogProperty.IsActive)
                return null;

            return catalogProperty;
        }

        public static CatalogProperty UpdateCatalogProperty(CatalogProperty catalogProperty)
        {
            if (catalogProperty == null)
                throw new ArgumentNullException();

            var oldEntity = CatalogPropertyRespository.Get(catalogProperty.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            catalogProperty = CatalogPropertyRespository.Update(catalogProperty);
            return catalogProperty;
        }

        #endregion

        #region Catalog

        /// <summary>
        /// تعداد تمام کاتالوگهای ثبت شده
        /// </summary>
        /// <returns></returns>
        public static int GetTotalCatalogs()
        {
            return CatalogRepository.Count(x => x.IsActive);
        }

        public static Persistence.Catalog.Entities.Catalog AddCatalog(Persistence.Catalog.Entities.Catalog catalog)
        {
            var validation = new CatalogValidation.CatalogEntity().Validate(catalog);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            catalog = CatalogRepository.Add(catalog);

            return catalog;
        }

        public static bool DeleteCatalog(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var catalog = CatalogRepository.Get(id);

            if (catalog == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = CatalogRepository.SoftDelete(catalog);
            return isDeleted;
        }

        /// <summary>
        /// دریافت لیست تعداد کاتالوگ ها براساس ردیف دسته بندی
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public static int GetCatalogCountByCategoryId(int categoryId)
        {
            return CatalogRepository.Count(item => item.CategoryId == categoryId);
        }

        public static IEnumerable<Persistence.Catalog.Entities.Catalog> GetAllCatalogs()
        {
            return CatalogRepository.GetAll();
        }

        public static IPagedList<Persistence.Catalog.Entities.Catalog> GetAllCatalogsByPage(CatalogSearchViewModel parameters)
        {
            var query = CatalogRepository.GetQuery(catalog => catalog.IsActive);

            if (!string.IsNullOrEmpty(parameters.Title))
                query = query.Where(catalog => catalog.Title.Contains(parameters.Title));

            if (!string.IsNullOrEmpty(parameters.ConfigCode))
                query = query.Where(catalog => catalog.ConfigCode.Contains(parameters.ConfigCode));

            query = query.OrderByDescending(x => x.WebsiteBoostScore);

            return new PagedList<Persistence.Catalog.Entities.Catalog>()
            {
                TotalRecords = query.Count(),
                PageSize = parameters.PageSize,
                CurrentPage = parameters.CurrentPage,
                Records = query.Skip((parameters.CurrentPage - 1) * parameters.PageSize).ToList(),
            };
        }

        public static Persistence.Catalog.Entities.Catalog GetCatalogById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var catalog = CatalogRepository.Get(id);

            if (catalog == null || !catalog.IsActive)
                return null;

            return catalog;
        }

        public static Persistence.Catalog.Entities.Catalog UpdateCatalog(Persistence.Catalog.Entities.Catalog catalog)
        {
            if (catalog == null)
                throw new ArgumentNullException();

            var oldEntity = CatalogRepository.Get(catalog.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            catalog = CatalogRepository.Update(catalog);

            List<Product> products = ProductService.GetAllProductsByCatalogId(catalog.Id).ToList();
            products.ForEach(item =>
            {
                item.CatalogTitle = catalog.Title;
                item.CatalogCode = catalog.ConfigCode;
                item.ShortTitle = catalog.ShortTitle;

                ProductService.UpdateProduct(item);
            });

            return catalog;
        }

        public static CatalogCategory AddCategoryCatalog(CatalogCategory categoryCatalog)
        {
            if (categoryCatalog == null)
                throw new ArgumentNullException();

            categoryCatalog = CatalogCategoryRespository.Add(categoryCatalog);
            return categoryCatalog;
        }

        public static bool DeleteCategoryCatalog(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var categoryCatalog = CatalogCategoryRespository.Get(id);

            if (categoryCatalog == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = CatalogCategoryRespository.SoftDelete(categoryCatalog);
            return IsDeleted;
        }

        public static IEnumerable<CatalogCategory> GetAllCategoryCatalogs()
        {
            return CatalogCategoryRespository.GetAll();
        }

        public static CatalogCategory GetCategoryCatalogById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var categoryCatalog = CatalogCategoryRespository.Get(id);

            if (categoryCatalog == null || !categoryCatalog.IsActive)
                return null;

            return categoryCatalog;
        }

        public static CatalogCategory UpdateCategoryCatalog(CatalogCategory categoryCatalog)
        {
            if (categoryCatalog == null)
                throw new ArgumentNullException();

            var oldEntity = CatalogCategoryRespository.Get(categoryCatalog.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            categoryCatalog = CatalogCategoryRespository.Update(categoryCatalog);
            return categoryCatalog;
        }

        public static string GetNewCatalogCode(int categoryId)
        {
            var lastCatalog = CatalogRepository.GetQuery(catalog => catalog.CategoryId == categoryId).OrderByDescending(catalog => catalog.ConfigCode).FirstOrDefault();
            var catalogCode = lastCatalog?.ConfigCode?.Split('-');
            if (catalogCode != null && catalogCode.Length == 3)
            {
                return $"SPD-{catalogCode[1]}-{(int.Parse(catalogCode[2]) + 1).ToString("D3")}";
            }
            else
            {
                var category = CategoryService.GetCategoryById(categoryId);
                return $"SPD-{category.CategoryCode}-{(1).ToString("D3")}";
            }
        }

        public static XLWorkbook GenerateCatalogExcelReport(IEnumerable<Persistence.Catalog.Entities.Catalog> catalogs)
        {
            var report = new List<CatalogReportViewModel>();

            foreach (var item in catalogs)
            {
                var vm = new CatalogReportViewModel
                {
                    Title = item.Title,
                    EnglishTitle = item.EnglishTitle,
                    ConfigCode = item.ConfigCode,
                    FullConfigCode = item.FullConfigCode,
                    Description = item.Description
                };

                report.Add(vm);
            }

            var dataTable = report.ToDataTable("عنوان", "عنوان انگلیسی", "شناسه پیکره بندی", "شناسه طبقه بندی کلی", "توضیحات");

            var workbook = new XLWorkbook();
            workbook.Worksheets.Add(dataTable, "Report");

            return workbook;
        }
        #endregion

        #region Catalog Status
        public static CatalogStatus AddCatalogStatus(CatalogStatus CatalogStatus)
        {
            if (CatalogStatus == null)
                throw new ArgumentNullException();

            CatalogStatus = CatalogStatusRespository.Add(CatalogStatus);
            return CatalogStatus;
        }

        public static bool DeleteCatalogStatus(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var CatalogStatus = CatalogStatusRespository.Get(id);

            if (CatalogStatus == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = CatalogStatusRespository.SoftDelete(CatalogStatus);
            return IsDeleted;
        }

        public static IEnumerable<CatalogStatus> GetAllCatalogStatuses()
        {
            return CatalogStatusRespository.GetAll();
        }

        public static CatalogStatus GetCatalogStatusById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var CatalogStatus = CatalogStatusRespository.Get(id);

            if (CatalogStatus == null || !CatalogStatus.IsActive)
                return null;

            return CatalogStatus;
        }

        public static CatalogStatus UpdateCatalogStatus(CatalogStatus CatalogStatus)
        {
            if (CatalogStatus == null)
                throw new ArgumentNullException();

            var oldEntity = CatalogStatusRespository.Get(CatalogStatus.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            CatalogStatus = CatalogStatusRespository.Update(CatalogStatus);
            return CatalogStatus;
        }
        #endregion

        #region Catalog Type
        public static CatalogType AddCatalogType(CatalogType CatalogType)
        {
            if (CatalogType == null)
                throw new ArgumentNullException();

            CatalogType = CatalogTypeRespository.Add(CatalogType);
            return CatalogType;
        }

        public static bool DeleteCatalogType(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var CatalogType = CatalogTypeRespository.Get(id);

            if (CatalogType == null)
                throw new InvalidDeleteOperationException();

            bool IsDeleted = CatalogTypeRespository.SoftDelete(CatalogType);
            return IsDeleted;
        }

        public static IEnumerable<CatalogType> GetAllCatalogTypes()
        {
            return CatalogTypeRespository.GetAll();
        }

        public static CatalogType GetCatalogTypeById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var CatalogType = CatalogTypeRespository.Get(id);

            if (CatalogType == null || !CatalogType.IsActive)
                return null;

            return CatalogType;
        }

        public static CatalogType UpdateCatalogType(CatalogType CatalogType)
        {
            if (CatalogType == null)
                throw new ArgumentNullException();

            var oldEntity = CatalogTypeRespository.Get(CatalogType.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            CatalogType = CatalogTypeRespository.Update(CatalogType);
            return CatalogType;
        }
        #endregion
    }



}
