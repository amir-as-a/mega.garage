﻿using FluentValidation;
using Mega.Garage.Persistence.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Catalog.Validation
{
    internal class ProductAttributeKeyValidation
    {
        internal class ProductAttributeKeyEntity : AbstractValidator<ProductAttributeKey>
        {
            public ProductAttributeKeyEntity()
            {
                RuleFor(x => x.Title)
                    .NotEmpty().WithMessage("عنوان معتبر نیست");
            }
        }
    }
}
