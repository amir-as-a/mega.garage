﻿using FluentValidation;
using Mega.Garage.Persistence.Catalog.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Catalog.Validation
{
    internal class ColorValidation
    {
        internal class ColorEntity : AbstractValidator<Color>
        {
            public ColorEntity()
            {
                RuleFor(x => x.Title)
                    .NotEmpty().WithMessage("عنوان معتبر نیست");
            }
        }
    }
}