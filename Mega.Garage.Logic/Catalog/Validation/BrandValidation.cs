﻿using FluentValidation;
using Mega.Garage.Persistence.Catalog.Entities;

namespace Mega.Garage.Logic.Catalog.Validation
{
    internal class BrandValidation
    {
        internal class BrandEntity : AbstractValidator<Brand>
        {
            public BrandEntity()
            {
                RuleFor(x => x.Title)
                    .MaximumLength(150).WithMessage("عنوان نمی تواند دارای بیشتر از 150 حرف باشد")
                    .NotEmpty().WithMessage("عنوان برند به درستی وارد نشده است")
                    .Must(Common.Validate.IsPersianText).WithMessage("عنوان وارد شده معتبر نمی باشد");

                RuleFor(x => x.EnglishTitle)
                    .MaximumLength(150).WithMessage("عنوان انگلیسی نمی تواند دارای بیشتر از 150 حرف باشد")
                    .Must(Common.Validate.IsText).WithMessage("عنوان انگلیسی وارد شده معتبر نمی باشد");

                RuleFor(x => x.WideImagePath)
                    .MaximumLength(650).WithMessage("مسیر تصویر نمی تواند دارای بیشتر از 650 حرف باشد");

                RuleFor(x => x.LogoImagePath)
                    .MaximumLength(650).WithMessage("مسیر لوگو نمی تواند دارای بیشتر از 650 حرف باشد");
            }
        }
    }
}
