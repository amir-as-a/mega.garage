﻿using FluentValidation;
using Mega.Garage.Persistence.Catalog.Entities;

namespace Mega.Garage.Logic.Catalog.Validation
{
    internal class CatalogValidation
    {
        internal class CatalogEntity : AbstractValidator<Persistence.Catalog.Entities.Catalog>
        {
            public CatalogEntity()
            {
                RuleFor(x => x.Title)
                    .MaximumLength(250).WithMessage("عنوان نمی تواند دارای بیشتر از 250 حرف باشد")
                    .NotEmpty().WithMessage("عنوان کاتالوگ معتبر نیست");

                RuleFor(x => x.EnglishTitle)
                    .MaximumLength(250).WithMessage("عنوان انگلیسی نمی تواند دارای بیشتر از 250 حرف باشد");

                RuleFor(x => x.CatalogTypeId)
                    .GreaterThan(0).WithMessage("دسته بندی به درستی انتخاب نشده است");

                RuleFor(x => x.ConfigCode)
                    .NotEmpty().WithMessage("شناسه پیکره بندی معتبر نیست")
                    .MinimumLength(1).WithMessage("شناسه پیکره بندی معتبر نیست");

                RuleFor(x => x.ImagePath)
                    .MaximumLength(250).WithMessage("مسیر تصویر نمی تواند دارای بیشتر از 250 حرف باشد");

                RuleFor(x => x.ThumbImagePath)
                    .MaximumLength(250).WithMessage("مسیر لوگو نمی تواند دارای بیشتر از 250 حرف باشد");
            }
        }

        internal class CategoryEntity : AbstractValidator<Category>
        {
            public CategoryEntity()
            {
                RuleFor(x => x.Title)
                    .NotEmpty().WithMessage("عنوان معتبر نیست");
            }
        }

        internal class CatalogTypeEntity : AbstractValidator<CatalogType>
        {
            public CatalogTypeEntity()
            {
                RuleFor(x => x.Title)
                    .NotEmpty().WithMessage("عنوان معتبر نیست");
            }
        }

        internal class CatalogStatusEntity : AbstractValidator<CatalogStatus>
        {
            public CatalogStatusEntity()
            {
                RuleFor(x => x.Title)
                    .NotEmpty().WithMessage("عنوان معتبر نیست");
            }
        }

        internal class CatalogPropertyEntity : AbstractValidator<CatalogProperty>
        {
            public CatalogPropertyEntity()
            {
            }
        }
    }
}
