﻿using FluentValidation;
using Mega.Garage.Persistence.Catalog.Entities;

namespace Mega.Garage.Logic.Catalog.Validation
{
    internal class CategoryValidation
    {
        internal class CategoryEntity : AbstractValidator<Category>
        {
            public CategoryEntity()
            {
                RuleFor(x => x.Title)
                    .MaximumLength(250).WithMessage("عنوان نمی تواند دارای بیشتر از 250 حرف باشد")
                    .NotEmpty().WithMessage("عنوان کاتالوگ معتبر نیست");

                RuleFor(x => x.EnglishTitle)
                    .MaximumLength(250).WithMessage("عنوان انگلیسی نمی تواند دارای بیشتر از 250 حرف باشد");

                RuleFor(x => x.CategoryCode)
                    .NotEmpty().WithMessage("شناسه معتبر نیست")
                    .MinimumLength(1).WithMessage("شناسه معتبر نیست");
            }
        }
    }
}

