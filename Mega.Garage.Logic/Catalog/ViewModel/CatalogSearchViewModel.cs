﻿namespace Mega.Garage.Logic.Catalog.ViewModel
{
    public class CatalogSearchViewModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string Title { get; set; }
        public string ConfigCode { get; set; }
    }
}
