﻿namespace Mega.Garage.Logic.Catalog.ViewModel
{
    public class CategoriesReportViewModel
    {
        public string Title { get; set; }
        public string EnglishTitle { get; set; }
        public string Description { get; set; }
        public string CategoryCode { get; set; }
    }
}
