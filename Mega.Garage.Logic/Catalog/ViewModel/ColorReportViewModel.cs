﻿namespace Mega.Garage.Logic.Catalog.ViewModel
{
    public class ColorReportViewModel
    {
        public string Title { get; set; }
        public string EnglishTitle { get; set; }
        public string ColorHexCode { get; set; }
    }
}
