﻿namespace Mega.Garage.Logic.Catalog.ViewModel
{
    public class CatalogReportViewModel
    {
        public string Title { get; set; }
        public string EnglishTitle { get; set; }
        public string ConfigCode { get; set; }
        public string FullConfigCode { get; set; }
        public string Description { get; set; }
    }
}
