﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Catalog.ViewModel
{
    public class BrandReportViewModel
    {
        public string Title { get; set; }
        public string EnglishTitle { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
    }
}
