﻿namespace Mega.Garage.Logic.Catalog.ViewModel
{
    public class BrandSearchViewModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public string Title { get; set; }
        public string Country { get; set; }
    }
}
