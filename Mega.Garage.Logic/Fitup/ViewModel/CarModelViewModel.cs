﻿namespace Mega.Garage.Logic.Fitup.ViewModel
{
    public class CarModelViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
    }
}
