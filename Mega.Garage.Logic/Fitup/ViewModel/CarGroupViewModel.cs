﻿using System.Collections.Generic;

namespace Mega.Garage.Logic.Fitup.ViewModel
{
    public class CarGroupViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string BrandCode { get; set; }
        public string SPic { get; set; }
        public virtual IList<CarModelViewModel> Models { get; set; }
    }
}
