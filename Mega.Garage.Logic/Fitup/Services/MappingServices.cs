﻿using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Fitup.Validation;
using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Persistence.Fitup.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mega.Garage.Logic.Fitup.Services
{
    public class MappingService
    {
        protected class MappingRepository : BaseRepository<Mapping, int> { }

        #region Car Mapping
        public static Mapping AddMapping(Mapping entity)
        {
            if (entity == null)
                throw new ArgumentNullException();

            var validation = new MappingValidation().Validate(entity);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            entity = MappingRepository.Add(entity);
            return entity;
        }

        public static Mapping UpdateMapping(Mapping entity)
        {
            if (entity == null)
                throw new ArgumentNullException();

            var oldEntity = MappingRepository.Get(entity.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new MappingValidation().Validate(entity);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            entity = MappingRepository.Update(entity);
            return entity;
        }

        public static bool DeleteMapping(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var Entity = MappingRepository.Get(id);
            if (Entity == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = MappingRepository.SoftDelete(Entity);
            return isDeleted;
        }

        public static IEnumerable<Mapping> GetAllMapping()
        {
            return MappingRepository.GetQuery(x => x.IsActive).ToList();
        }

        public static IEnumerable<Mapping> GetAllMappingByProductId(long productId)
        {
            return MappingRepository.GetQuery(x => x.IsActive && x.ProductId == productId).ToList();
        }

        public static IEnumerable<Mapping> GetAllMappingByCarModelId(int modelId)
        {
            return MappingRepository.GetQuery(x => x.IsActive && x.CarModelId == modelId).ToList();
        }

        public static Mapping AddMapping(long productId, int carModelId)
        {
            var item = MappingRepository.GetQuery(x => x.IsActive && x.ProductId == productId && x.CarModelId == carModelId).FirstOrDefault();
            if (item == null)
            {
                var product = ProductService.GetProductById(productId);
                var carModel = CarModelsService.GetCarModelById(carModelId);

                if (product == null)
                    throw new ArgumentNullException();

                if (carModel == null)
                    throw new ArgumentNullException();

                Mapping mapping = new Mapping()
                {
                    CarGroupId = carModel.CarGroupId,
                    CarModelId = carModel.Id,
                    ProductId = product.Id,
                    ProductCode = product.Code,
                    CatalogTitle = product.CatalogTitle
                };
                return AddMapping(mapping);
            }
            else
            {
                return item;
            }
        }

        public static bool RemoveMapping(long productId, int carModelId)
        {
            var item = MappingRepository.GetQuery(x => x.IsActive && x.ProductId == productId && x.CarModelId == carModelId).FirstOrDefault();

            if (item == null)
                throw new ArgumentNullException();

            return DeleteMapping(item.Id);
        }

        public static Mapping GetMappingById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var Entity = MappingRepository.Get(id);

            if (Entity == null || !Entity.IsActive)
                return null;

            return Entity;
        }

        #endregion
    }
}
