﻿using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Fitup.Validation;
using Mega.Garage.Persistence.Fitup.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mega.Garage.Logic.Fitup.Services
{
    public class CarModelsService
    {
        protected class CarModelsRepository : BaseRepository<CarModel, int> { }

        #region Car Models
        public static dynamic GetAllCarModelsByCarGroupId(int carGroupId)
        {
            var entity = CarModelsRepository.GetQuery(x => x.IsActive && x.CarGroupId == carGroupId)
                 .Select(x =>
                 new
                 {
                     x.Id,
                     x.Title,
                     x.Code
                 }).ToList();
            return entity;
        }

        public static CarModel AddCarModel(CarModel entity)
        {
            if (entity == null)
                throw new ArgumentNullException();

            var validation = new CarModelsValidation().Validate(entity);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            entity = CarModelsRepository.Add(entity);
            return entity;
        }
        public static CarModel UpdateCarModel(CarModel entity)
        {
            if (entity == null)
                throw new ArgumentNullException();

            var oldEntity = CarModelsRepository.Get(entity.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new CarModelsValidation().Validate(entity);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            entity = CarModelsRepository.Update(entity);
            return entity;
        }

        public static bool DeleteCarModel(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var Entity = CarModelsRepository.Get(id);
            if (Entity == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = CarModelsRepository.SoftDelete(Entity);
            return isDeleted;
        }

        public static IEnumerable<CarModel> GetAllCarModels()
        {
            return CarModelsRepository.GetQuery(x => x.IsActive).ToList();
        }
        public static CarModel GetCarModelById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var Entity = CarModelsRepository.Get(id);

            if (Entity == null || !Entity.IsActive)
                return null;

            return Entity;
        }

        #endregion
    }
}