﻿using System;


namespace Mega.Garage.Logic.Fitup.Services
{
    using FluentValidation;
    using Mega.Garage.Common;
    using Mega.Garage.Logic.Fitup.Validation;
    using Mega.Garage.Logic.Fitup.ViewModel;
    using Mega.Garage.Persistence.Fitup.Entities;
    using System.Collections.Generic;
    using System.Linq;
    public class CarGroupService
    {
        protected class CarGroupsRepository : BaseRepository<CarGroups, int> { }

        #region CarGroups
        /// <summary>
        /// لیست تمام گروه های خودرو به همراه تمامی مدل های زیر مجموعه آن
        /// </summary>
        /// <returns></returns>
        public static List<CarGroupViewModel> GetAllCarGroupsWithModels()
        {
            List<CarGroupViewModel> entity = CarGroupsRepository.GetQuery(x => x.IsActive)
                 .Select(x => new CarGroupViewModel()
                 {
                     Id = x.Id,
                     Title = x.Title,
                     SPic = x.SPic,
                     BrandCode = x.BrandCode,
                     Models = x.CarModel.Where(y => y.IsActive).Select(y => new CarModelViewModel()
                     {
                         Id = y.Id,
                         Title = y.Title,
                         Code = y.Code
                     }).ToList()
                 }).ToList();

            return entity;
        }

        public static dynamic GetAllCarGroups()
        {
            var entity = CarGroupsRepository.GetQuery(x => x.IsActive)
                 .Select(x =>
                 new
                 {
                     x.Id,
                     x.Title,
                     x.SPic,
                     x.BrandCode
                 }).ToList();
            return entity;
        }

        public static CarGroups AddCarGroup(CarGroups entity)
        {
            if (entity == null)
                throw new ArgumentNullException();

            var validation = new CarGroupsValidation().Validate(entity);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            entity = CarGroupsRepository.Add(entity);
            return entity;
        }

        public static CarGroups UpdateCarGroup(CarGroups entity)
        {
            if (entity == null)
                throw new ArgumentNullException();

            var oldEntity = CarGroupsRepository.Get(entity.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new CarGroupsValidation().Validate(entity);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            entity = CarGroupsRepository.Update(entity);
            return entity;
        }

        public static bool DeleteCarGroup(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var Entity = CarGroupsRepository.Get(id);
            if (Entity == null)
                throw new InvalidDeleteOperationException();

            bool isDeleted = CarGroupsRepository.SoftDelete(Entity);
            return isDeleted;
        }


        public static CarGroups GetCarGroupsById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var Entity = CarGroupsRepository.Get(id);

            if (Entity == null || !Entity.IsActive)
                return null;

            return Entity;
        }

        #endregion
    }
}