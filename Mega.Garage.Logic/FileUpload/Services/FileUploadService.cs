﻿using Mega.Garage.Persistence.FileUpload.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mega.Garage.Common;
using System.Web;

namespace Mega.Garage.Logic.FileUpload.Services
{
    public class UploadService
    {
        protected static string DomainPath = "";
        private static readonly string TOKEN_ID = "SpeedyUploader";
        static UploadService()
        {
            var FilePathAddress = System.Web.Configuration.WebConfigurationManager.AppSettings["DomainPath"];
            if (string.IsNullOrEmpty(FilePathAddress))
                throw new Exception("Domain Path in Configuration is not defined");
            else
            {
                /// default value for domain path for speedy is like string assigned below
                if (FilePathAddress.ToLower() == "default")
                    DomainPath = "https://speedy-admin.iranecar.com/";
                //DomainPath = "https://cdn-iranecar.ir/Speedy/";
                else
                {
                    /// Standard Domain URL must starts with http:// or https:// 
                    /// if this condition be false system stops wotking to repare it
                    if (!FilePathAddress.ToLower().StartsWith("http://") || FilePathAddress.ToLower().StartsWith("https://"))
                    {
                        throw new Exception("DomainPath must starts with http:// or https://");
                    }
                    /// if domain path directory does not end with / we add it automatically
                    if (!FilePathAddress.EndsWith("/"))
                    {
                        FilePathAddress += '/';
                    }
                    DomainPath = FilePathAddress.ToString();
                }
            }
        }
        protected class UserFileRepository : BaseRepository<UserFile, Guid> { }
        public static string GetCDNFilePath(string fileJson)
        {
            if (string.IsNullOrEmpty(fileJson))
                return string.Empty;
            try
            {
                var FileData = JSON.Desrialize<ViewModel.FileData>(fileJson);
                return GetCDNAddressByFileId(FileData.FileId);
            }
            catch
            {
                return string.Empty;
            }
        }
        public static string GetCDNAddressByFileId(Guid fileId)
        {
            string CDNAddress = UserFileRepository.GetQuery(x => x.IsActive && x.Id == fileId).Select(x => x.CDNAddress).FirstOrDefault();
            return ((DomainPath.EndsWith("/") && CDNAddress.StartsWith("/")) ? DomainPath.Trim('/') + CDNAddress : DomainPath + CDNAddress);
        }
        public static List<ViewModel.FileDetailResult> FileList()
        {
            return UserFileRepository.GetQuery(x => x.IsActive)
                .Select(x => new ViewModel.FileDetailResult { FileId = x.Id, FileName = x.FileName }).ToList();
        }
        static ViewModel.UploadResult UploadWithFileStream(string fileName, string remotePath, System.IO.Stream fileStream)
        {
            byte[] fileArray = new byte[fileStream.Length];
            fileStream.Read(fileArray, 0, (int)fileStream.Length);
            return UploadWithByteArray(fileName, remotePath, fileArray);
        }
        public static ViewModel.UploadResult Upload(HttpPostedFileBase fileBase, string remotePath = "")
        {
            var Result = new ViewModel.UploadResult(null, Guid.Empty);
            if (fileBase == null)
            {
                Result.ErrorMessage = "";
            }
            else
            {
                if (fileBase.ContentLength == 0)
                {
                    Result.ErrorMessage = "";
                }
                else
                {
                    Result = UploadWithFileStream(fileBase.FileName, remotePath, fileBase.InputStream);
                }
            }
            return Result;
        }

        /// <summary>
        /// Uploads a file to database
        /// </summary>
        /// <param name="UserToken">user token which is granted to upload a file</param>
        /// <param name="FileName">Name of the file to upload</param>
        /// <param name="FileArray">An array of file with byte[] format</param>
        /// <returns></returns>
        public static ViewModel.UploadResult UploadWithByteArray(string fileName, string remotePath, byte[] fileArray)
        {
            fileName = System.IO.Path.GetFileName(fileName);
            ViewModel.UploadResult Output = new ViewModel.UploadResult(fileName, Guid.Empty);
            var TokenData = UserTokenService.IsUserValid(TOKEN_ID);
            if (!TokenData.IsValid)
            {
                Output.ErrorMessage = TokenData.ErrorMessage;
            }
            else
            {
                if (string.IsNullOrEmpty(remotePath))
                {
                    remotePath = "/Speedy/";
                }
                Guid FileID = FileUpload(fileName, remotePath, TokenData.UserId, fileArray);
                if (FileID == null)
                {
                    Output.ErrorMessage = "File coud not be upload for unacceptable reason";
                }
                else
                {
                    Output.IsUploaded = true;
                    Output.FileID = FileID;
                    Output.FileData = $"{{ \"FileName\" : \"{fileName}\", \"FileId\" : \"{FileID}\" }}";
                }
            }
            return Output;
        }

        public static ViewModel.DownloadResult Download(Guid fileID, string FileName)
        {
            var TokenData = UserTokenService.IsUserValid(TOKEN_ID);
            if (!TokenData.IsValid)
            {
                return new ViewModel.DownloadResult
                {
                    ErrorMessage = TokenData.ErrorMessage
                };
            }
            else
            {
                {
                    var FileData =
                        UserFileRepository.GetQuery(x => x.IsActive && x.Id == fileID && x.FileName.ToLower() == FileName.ToLower())
                        .Select(x => new { x.MimeType, x.FileName, x.CDNAddress, x.Width, x.Height, x })
                        .FirstOrDefault();
                    if (FileData == null)
                    {
                        return new ViewModel.DownloadResult
                        {
                            ErrorMessage = "File does not exist"
                        };
                    }
                    else
                    {


                        if (!FileData.CDNAddress.IsValidUrl())
                        {
                            return new ViewModel.DownloadResult
                            {
                                ErrorMessage = "File does not exist"
                            };
                        }
                        else
                        {
                            using (var wc = new System.Net.WebClient())
                            {
                                byte[] FileArray = wc.DownloadData(DomainPath + FileData.CDNAddress);
                                return new ViewModel.DownloadResult
                                {
                                    FileArray = FileArray,
                                    FileURL = FileData.FileName,
                                    IsAvailble = true,
                                    MimeType = FileData.MimeType
                                };
                            }
                        }
                    }
                }
            }

        }
        public static ViewModel.DownloadResult DownloadImage(Guid fileID, string FileName, int? width, int? height)
        {
            var Result = Download(fileID, FileName);
            if (Result.IsAvailble)
            {
                if (Image.IsFileAnImage(Result.FileArray))
                {
                    var img = Image.ResizeImage(Result.FileArray, (width ?? -1), (height ?? -1));
                    Result.FileArray = Image.ImageToByte(img);
                }
            }
            return Result;
        }
        public static bool DeleteFile(Guid fileId)
        {

            return UserFileRepository.SoftDelete(fileId);
        }

        private static Guid FileUpload(string fileName, string remotePath, Guid userId, byte[] fileArray)
        {
            string mimeType = System.Web.MimeMapping.GetMimeMapping(fileName);
            var IsFileAnImage = Common.Image.IsFileAnImage(fileArray);
            Tuple<int, int> FileDeminision = new Tuple<int, int>(-1, -1);
            if (IsFileAnImage)
            {
                var Image = Common.Image.ByteToImage(fileArray);
                FileDeminision = new Tuple<int, int>(Image.Width, Image.Height);
            }
            Guid FileId = Guid.NewGuid();
            string FileName = FileId + System.IO.Path.GetExtension(fileName);
            string FullFilePath = remotePath + FileName;
            //System.IO.File.WriteAllBytes(FullFilePath, fileArray);
            _ = FTPClient.UploadFile(FullFilePath, fileArray);
            var FileUpload = UserFileRepository.Add(new UserFile
            {
                FilePath = FullFilePath,
                FileName = fileName,
                FileSize = fileArray.Length,
                Height = FileDeminision.Item2,
                Width = FileDeminision.Item1,
                Id = FileId,
                IsActive = true,
                MimeType = mimeType,
                UploadDate = DateTime.Now,
                UserTokenId = userId,
                CreatedBy = 1,
                CreatedDate = DateTime.Now,
                ModifiedBy = 1,
                ModifiedDate = DateTime.Now,
                CDNAddress = FullFilePath.Replace("//", "/")
            });

            return FileUpload.Id;
        }

    }

}
