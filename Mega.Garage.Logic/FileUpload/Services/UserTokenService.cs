﻿using Mega.Garage.Persistence.FileUpload.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.FileUpload.Services
{
    public class UserTokenService
    {
        protected class UserTokenRepository : BaseRepository<UserToken, Guid> { }
        public static ViewModel.UserResult IsUserValid(string UserToken)
        {
            var Result = new ViewModel.UserResult();
            try
            {
                var UserObject = UserTokenRepository.GetQuery(x => x.Token == UserToken).FirstOrDefault();
                if (UserObject == null)
                {
                    Result = new ViewModel.UserResult
                    {
                        ErrorMessage = "There is no user with this token",
                        IsValid = false
                    };
                }
                else
                {
                    if (UserObject.IsActive)
                    {
                        Result = new ViewModel.UserResult
                        {
                            ErrorMessage = string.Empty,
                            IsValid = true,
                            UserId = UserObject.Id
                        };
                    }
                    else
                    {
                        Result = new ViewModel.UserResult
                        {
                            ErrorMessage = "This token is not available any more",
                            IsValid = false
                        };
                    }
                }
            }
            catch
            {
                /// Log ex Data for proccess result later
            }
            return Result;
        }
    }
}
