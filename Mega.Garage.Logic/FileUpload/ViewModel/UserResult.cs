﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.FileUpload.ViewModel
{
    public partial class UserResult
    {
        public Guid UserId { get; set; }
        public bool IsValid { get; set; } = false;

        public string ErrorMessage { get; set; }
    }
}
