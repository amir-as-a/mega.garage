﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.FileUpload.ViewModel
{
    public class FileDetailResult
    {
        public Guid FileId { get; set; }
        public string FileName { get; set; }
    }
}
