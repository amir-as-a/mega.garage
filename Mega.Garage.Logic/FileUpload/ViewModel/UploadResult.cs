﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.FileUpload.ViewModel
{
    /// <summary>
    /// Output class for result of upload trying
    /// </summary>
    public class UploadResult
    {
        public UploadResult(string fileName, Guid fileId)
        {
            FileID = fileId;
            FileName = fileName;
        }
        /// <summary>
        /// Is file Uploaded?
        /// </summary>
        public bool IsUploaded { get; set; } = false;
        /// <summary>
        /// Original File Name uploaded by user
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// If file is uploaded this field will fill to access the file in future
        /// </summary>
        public Guid FileID { get; set; }
        /// <summary>
        /// If uploading the file fails this property will return the result of failture;
        /// </summary>
        public string ErrorMessage { get; set; }

        public object FileData { get; set; }
    }
}
