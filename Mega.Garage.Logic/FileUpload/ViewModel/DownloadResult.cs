﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.FileUpload.ViewModel
{
    public class DownloadResult
    {
        public bool IsAvailble { get; set; } = false;
        public byte[] FileArray { get; set; }
        public string ErrorMessage { get; set; }
        public string FileURL { get; set; }
        public string MimeType { get; set; }
    }
}
