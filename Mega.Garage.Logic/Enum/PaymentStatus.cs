﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Enums
{
    public enum PaymentStatus
    {
        PaymentRequest = 1,
        ApprovedPayment = 2,
        CancelledPayment = 3
    }
}
