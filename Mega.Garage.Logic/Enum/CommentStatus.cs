﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Enums
{
    public enum CommentStatus
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3
    }
}
