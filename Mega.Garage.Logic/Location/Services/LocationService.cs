﻿using Mega.Garage.Persistence.Location.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mega.Garage.Logic.Location.Services
{
    public class LocationService
    {
        protected class CityRepository : BaseRepository<City, int> { }
        protected class ProvinceRepository : BaseRepository<Province, int> { }
        protected class RegionRepository : BaseRepository<Region, int> { }

        #region City
        public static IEnumerable<City> GetCitiesByProvinceId(int provinceId)
        {
            if (provinceId == 0)
                throw new ArgumentNullException();

            return CityRepository.GetQuery(c => c.IsActive).Where(c => c.ProvinceId == provinceId);
        }

        public static IEnumerable<ViewModel.CityDetail> GetCityListByProvinceId(int provinceId)
        {
            return
                ProvinceRepository.GetQuery(x => x.IsActive)
                .Select(x => new ViewModel.CityDetail { Id = x.Id, Title = x.Title, ProvinceId = provinceId })
                .OrderBy(x => x.Title)
                .ToList();
        }
        #endregion

        #region Province
        public static IEnumerable<ViewModel.ProvinceDetail> GetProvinceList()
        {
            return
                ProvinceRepository.GetQuery(x => x.IsActive)
                .Select(x => new ViewModel.ProvinceDetail { Id = x.Id, Title = x.Title, CityCount = x.Cities.Count })
                .OrderBy(x => x.Title)
                .ToList();
        }

        public static IEnumerable<Province> GetAllProvinces()
        {
            return ProvinceRepository.GetAll();
        }

        #endregion

        #region Region
        public static IEnumerable<Region> GetRegionsByCityId(int cityId)
        {
            if (cityId == 0)
                throw new ArgumentNullException();

            return RegionRepository.GetQuery(r => r.IsActive).Where(r => r.CityId == cityId);
        }
        #endregion
    }
}
