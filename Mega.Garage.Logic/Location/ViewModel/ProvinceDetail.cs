﻿namespace Mega.Garage.Logic.Location.ViewModel
{
    public class ProvinceDetail
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? CityCount { get; set; }
    }
}
