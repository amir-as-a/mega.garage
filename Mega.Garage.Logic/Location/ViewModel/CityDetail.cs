﻿namespace Mega.Garage.Logic.Location.ViewModel
{
    public class CityDetail
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int ProvinceId { get; set; }
        public string ProvinceName { get; set; }
    }
}
