﻿using Mega.Garage.Persistence;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Mega.Garage.Common;
using System.Data.Entity.Migrations;
using Mega.Garage.Persistence.Account.Entities;


namespace Mega.Garage.Logic
{
    public class BaseRepository<TEntity, PK> where TEntity : BaseEntity<PK> where PK : struct
    {
        public static IEnumerable<TEntity> GetAll()
        {
            var Result = new Repository<TEntity, PK>().GetAll();
            return Result;
        }

        public static TEntity Get(PK id)
        {
            var Result = new Repository<TEntity, PK>().Get(id);
            return Result;
        }

        public static IQueryable<TEntity> GetQuery()
        {
            var Result = new Repository<TEntity, PK>().GetQuery();
            return Result;
        }

        public static IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> criteria)
        {
            var Result = new Repository<TEntity, PK>().GetQuery(criteria);
            return Result;
        }
        public static IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> criteria, List<string> includs)
        {
            var Result = new Repository<TEntity, PK>().GetQuery(criteria, includs);
            return Result;
        }
        public static bool HasRecord(PK id)
        {
            var Result = new Repository<TEntity, PK>().HasRecord(id);
            return Result;
        }
        /// <summary>
        /// Count of records with an special criteria
        /// </summary>
        /// <param name="criteria">Criteria which is expressed to an unique entity</param>
        /// <returns>Count of records</returns>

        public static int Count(Expression<Func<TEntity, bool>> criteria)
        {
            var Result = new Repository<TEntity, PK>().Count(criteria);
            return Result;
        }
        public static int Count()
        {
            var Result = new Repository<TEntity, PK>().Count();
            return Result;
        }
        /// <summary>
        /// Paginates a Query
        /// </summary>
        /// <typeparam name="TOrder"></typeparam>
        /// <param name="page">Must be started from 1</param>
        /// <param name="pageSize">An integer between 1 to 1000 </param>
        /// <param name="isDescending"></param>
        /// <param name="criteria"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public static IPagedList<TEntity> GetPagedList<TOrder>
            (
            Expression<Func<TEntity, bool>> criteria,
            Expression<Func<TEntity, DateTime>> order,
            int page = 1, int pageSize = 30, bool isDescending = true)
        {
            var Result = new Repository<TEntity, PK>().GetPagedList(criteria, order, page, pageSize, isDescending);
            return Result;
        }
        /// <summary>
        /// This method adds an Entity to database and sets default 
        /// </summary>
        /// <param name="entity">Entity to save in database</param>
        /// <returns>Returns saved entity with actual default data added from framework like Id, IsActive, CreatedBy, ...</returns>
        public static TEntity Add(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {
            var Result = new Repository<TEntity, PK>().Add(entity, needCommit, needAuthorization);
            return Result;
        }
        /// <summary>
        /// This method adds an Entity to database and sets default 
        /// </summary>
        /// <param name="entity">Entity to save in database</param>
        /// <returns>Returns saved entity with actual default data added from framework like Id, IsActive, CreatedBy, ...</returns>
        public static int AddEntity(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {
            var Result = new Repository<TEntity, PK>().AddEntity(entity, needCommit, needAuthorization);
            return Result;
        }
        /// <summary>
        /// Updates an entity in database
        /// </summary>
        /// <param name="entity">Entered Entity to update</param>
        /// <returns>Returns updated entity after modification</returns>
        public static TEntity Update(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {
            var Repo = new Repository<TEntity, PK>();
            var Result = Repo.Update(entity, false, needAuthorization);
            if (needCommit)
                Repo.Commit();
            Repo.Dispose();
            return Result;
        }
        /// <summary>
        /// Updates an entity in database
        /// </summary>
        /// <param name="entity">Entered Entity to update</param>
        /// <returns>Returns true if modification was successful</returns>
        public static bool UpdateEntity(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {
            var Repo = new Repository<TEntity, PK>();
            var Result = Repo.UpdateEntity(entity, needCommit, needAuthorization);
            return Result;
        }
        /// <summary>
        /// This method changes IsActive flag to false 
        /// This base repository only supports Logical Delete
        /// </summary>
        /// <param name="id">PK for TEntity injected</param>
        /// <returns></returns>
        public static bool SoftDelete(PK id)
        {
            var Repo = new Repository<TEntity, PK>();
            var Result = Repo.SoftDelete(id);
            return Result;
        }
        /// <summary>
        /// Deletes a set of records with criteria
        /// </summary>
        /// <param name="criteria">criteria attached to a TEntity</param>
        public static void SoftDelete(Expression<Func<TEntity, bool>> criteria, bool needCommit = true, bool needAuthorization = true)
        {
            var Repo = new Repository<TEntity, PK>();
            Repo.SoftDelete(criteria, needCommit, needAuthorization);
        }
        /// <summary>
        /// This method changes IsActive flag to false 
        /// This base repository only supports Logical Delete
        /// </summary>
        /// <param name="id">Assigned entity to TEntity as input</param>
        /// <returns></returns>
        public static bool SoftDelete(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {

            var Repo = new Repository<TEntity, PK>();
            var Result = Repo.SoftDelete(entity, needCommit, needAuthorization);
            return Result;

        }
        /// <summary>
        /// After INSERT, DELETE, UPDATE save changes to database and calls Flush Method
        /// </summary>
        /// <returns></returns>

    }
    public class Repository<TEntity, PK> where TEntity : BaseEntity<PK> where PK : struct
    {
        bool disposed = false;
        private readonly GarageDbContext Context;
        private User ActiveUser;
        public Repository()
        {
            Context = new GarageDbContext();
        }
        public void Dispose()
        {
            Context.Dispose();
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                Context.Dispose();
                GC.SuppressFinalize(this);
            }
            disposed = true;
        }
        public IEnumerable<TEntity> GetAll()
        {
            var query = Context.Set<TEntity>().Where(entity => entity.IsActive).ToList();
            return query;
        }

        public TEntity Get(PK id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        public IQueryable<TEntity> GetQuery()
        {
            return Context.Set<TEntity>().AsQueryable<TEntity>();
        }
        public bool HasRecord(PK id)
        {
            return Context.Set<TEntity>().Count<TEntity>(x => x.Id.Equals(id)) == 1;
        }
        public IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery().Where(criteria);
        }
        public IQueryable<TEntity> GetQuery(Expression<Func<TEntity, bool>> criteria, List<string> includs)
        {
            IQueryable<TEntity> query = GetQuery().Where(criteria);
            includs.ForEach(tableName =>
            {
                query = query.Include(tableName);
            });
            return query;
        }
        /// <summary>
        /// Count of records with an special criteria
        /// </summary>
        /// <param name="criteria">Criteria which is expressed to an unique entity</param>
        /// <returns>Count of records</returns>
        public int Count(Expression<Func<TEntity, bool>> criteria)
        {
            return GetQuery(criteria).Count(criteria);
        }
        public int Count()
        {
            return Context.Set<TEntity>().AsQueryable<TEntity>().Count();
        }
        /// <summary>
        /// Paginates a Query
        /// </summary>
        /// <typeparam name="TOrder"></typeparam>
        /// <param name="page">Must be started from 1</param>
        /// <param name="pageSize">An integer between 1 to 1000 </param>
        /// <param name="isDescending"></param>
        /// <param name="criteria"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public IPagedList<TEntity> GetPagedList<TOrder>
            (Expression<Func<TEntity, bool>> criteria, Expression<Func<TEntity, TOrder>> order, int page = 1, int pageSize = 30, bool isDescending = true)
        {
            if (page < 1)
                throw new OutOfRangeException();

            if (1000 < pageSize && pageSize < 1)
                throw new OutOfRangeException();

            return new PagedList<TEntity>()
            {
                CurrentPage = page,
                PageSize = pageSize,
                TotalRecords = Count(criteria),
                Records = Context.Set<TEntity>().AsQueryable<TEntity>().Where(criteria).OrderByWithDirection(order, isDescending).Skip((page - 1) * pageSize).Take(pageSize).ToList()
            };
        }
        /// <summary>
        /// This method adds an Entity to database and sets default 
        /// </summary>
        /// <param name="entity">Entity to save in database</param>
        /// <returns>Returns saved entity with actual default data added from framework like Id, IsActive, CreatedBy, ...</returns>
        public TEntity Add(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {
            if (needAuthorization)
                ActiveUser = Account.Services.UserService.IsAuthenticated();

            if (entity == null)
                throw new ArgumentNullException("entity");

            if (needAuthorization && ActiveUser == null)
                throw new Exception("User must be signed in to modify data");

            entity.CreatedBy = (needAuthorization || ActiveUser != null) ? ActiveUser.Id : 1;
            entity.ModifiedBy = entity.CreatedBy;


            entity.IsActive = true;
            entity.CreatedDate = DateTime.Now;
            entity.ModifiedDate = DateTime.Now;
            TEntity SavedEntity = Context.Set<TEntity>().Add(entity);

            if (needCommit)
                Commit();

            return SavedEntity;
        }

        /// <summary>
        /// This method adds Bulk Entity to database and sets default 
        /// </summary>
        /// <param name="entity">Entity to save in database</param>
        /// <returns>Returns saved entity with actual default data added from framework like Id, IsActive, CreatedBy, ...</returns>
        public IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities, bool needCommit = true, bool needAuthorization = true)
        {
            if (needAuthorization)
                ActiveUser = Account.Services.UserService.IsAuthenticated();

            if (entities == null)
                throw new ArgumentNullException("entity");

            if (needAuthorization && ActiveUser == null)
                throw new Exception("User must be signed in to modify data");

            foreach (var entity in entities)
            {
                entity.CreatedBy = (needAuthorization || ActiveUser != null) ? ActiveUser.Id : 1;
                entity.ModifiedBy = entity.CreatedBy;
                entity.IsActive = true;
                entity.CreatedDate = DateTime.Now;
                entity.ModifiedDate = DateTime.Now;
            }
            IEnumerable<TEntity> SavedEntity = Context.Set<TEntity>().AddRange(entities);
            if (needCommit)
                Commit();

            return SavedEntity;
        }

        /// <summary>
        /// This method adds an Entity to database and sets default 
        /// </summary>
        /// <param name="entity">Entity to save in database</param>
        /// <returns>Returns saved entity with actual default data added from framework like Id, IsActive, CreatedBy, ...</returns>
        public int AddEntity(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {
            if (needAuthorization)
                ActiveUser = Account.Services.UserService.IsAuthenticated();

            if (entity == null)
                throw new ArgumentNullException("entity");

            if (needAuthorization && ActiveUser == null)
                throw new Exception("User must be signed in to modify data");

            entity.CreatedBy = (needAuthorization || ActiveUser != null) ? ActiveUser.Id : 1;
            entity.ModifiedBy = entity.CreatedBy;
            entity.IsActive = true;
            entity.CreatedDate = DateTime.Now;
            entity.ModifiedDate = DateTime.Now;
            Context.Set<TEntity>().Add(entity);

            if (needCommit)
                return Commit();
            else
                return 0;
        }
        /// <summary>
        /// Updates an entity in database
        /// </summary>
        /// <param name="entity">Entered Entity to update</param>
        /// <returns>Returns updated entity after modification</returns>
        public TEntity Update(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {
            if (needAuthorization)
                ActiveUser = Account.Services.UserService.IsAuthenticated();

            if (entity == null)
                throw new ArgumentNullException(entity.GetType().Name);

            if (needAuthorization && ActiveUser == null)
                throw new Exception("User must be signed in to modify data");

            TEntity ContextEntity = Context.Set<TEntity>().Find(entity.Id);
            if (ContextEntity == null)
                throw new ArgumentNullException(entity.GetType().Name);

            if (ContextEntity.IsActive == false)
                throw new InvalidUpdateOperationException();

            entity.ModifiedBy = (needAuthorization || ActiveUser != null) ? ActiveUser.Id : 1;
            entity.CreatedBy = ContextEntity.CreatedBy;
            entity.CreatedDate = ContextEntity.CreatedDate;
            entity.IsActive = ContextEntity.IsActive;
            entity.ModifiedDate = DateTime.Now;
            Context.Set<TEntity>().AddOrUpdate(entity);

            if (needCommit)
                Commit();

            return entity;
        }
        /// <summary>
        /// Updates an entity in database
        /// </summary>
        /// <param name="entity">Entered Entity to update</param>
        /// <returns>Returns true if modification was successful</returns>
        public bool UpdateEntity(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {
            if (needAuthorization)
                ActiveUser = Account.Services.UserService.IsAuthenticated();

            if (entity == null)
                throw new ArgumentNullException(entity.GetType().Name);

            if (needAuthorization && ActiveUser == null)
                throw new Exception("User must be signed in to modify data");

            TEntity ContextEntity = Context.Set<TEntity>().Find(entity.Id);
            if (ContextEntity == null)
                throw new ArgumentNullException(entity.GetType().Name);

            if (ContextEntity.IsActive == false)
                throw new InvalidUpdateOperationException();

            entity.ModifiedBy = (needAuthorization || ActiveUser != null) ? ActiveUser.Id : 1;
            entity.ModifiedDate = DateTime.Now;
            Context.Set<TEntity>().AddOrUpdate(entity);

            if (needCommit)
                return Commit() == 1;
            else
                return false;
        }
        /// <summary>
        /// This method changes IsActive flag to false 
        /// This base repository only supports Logical Delete
        /// </summary>
        /// <param name="id">PK for TEntity injected</param>
        /// <returns></returns>
        public bool SoftDelete(PK id)
        {
            TEntity record = Context.Set<TEntity>().Find(id);
            return SoftDelete(record);
        }
        /// <summary>
        /// Deletes a set of records with criteria
        /// </summary>
        /// <param name="criteria">criteria attached to a TEntity</param>
        public void SoftDelete(Expression<Func<TEntity, bool>> criteria, bool needCommit = true, bool needAuthorization = true)
        {
            IEnumerable<TEntity> records = GetQuery(criteria);
            foreach (TEntity record in records)
                SoftDelete(record, false, needAuthorization);
            if (needCommit)
                Commit();
        }
        /// <summary>
        /// This method changes IsActive flag to false 
        /// This base repository only supports Logical Delete
        /// </summary>
        /// <param name="id">Assigned entity to TEntity as input</param>
        /// <returns></returns>
        public bool SoftDelete(TEntity entity, bool needCommit = true, bool needAuthorization = true)
        {
            if (needAuthorization)
                ActiveUser = Account.Services.UserService.IsAuthenticated();
            if (needAuthorization && ActiveUser == null)
                throw new Exception("User must be signed in to modify data");

            if (entity == null)
                throw new ArgumentNullException("entity");

            entity.ModifiedBy = (needAuthorization || ActiveUser != null) ? ActiveUser.Id : 1;
            entity.ModifiedDate = DateTime.Now;
            entity.IsActive = false;
            Context.Set<TEntity>().AddOrUpdate(entity);
            if (needCommit)
                return Commit() == 1;
            else
                return false;

        }
        /// <summary>
        /// After INSERT, DELETE, UPDATE save changes to database and calls Flush Method
        /// </summary>
        /// <returns></returns>
        public int Commit()
        {
            int rowsAffected = Context.SaveChanges();
            return rowsAffected;
        }
        /// <summary>
        /// This method garbage unmanaged memory and renew the context for next activities
        /// </summary>


    }
}
