﻿using Mega.Garage.Persistence.Polling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Polling.ViewModel
{
    public class UserSurveyViewModel
    {
        public UserSurvey UserSurvey { get; set; }
        public List<UserSurveyQuestion> UserSurveyQuestions { get; set; } = new List<UserSurveyQuestion>();
        public List<Question> Questions { get; set; } = new List<Question>();
    }
}
