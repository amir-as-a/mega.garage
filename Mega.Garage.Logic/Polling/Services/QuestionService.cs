﻿using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Polling.Validation;
using Mega.Garage.Persistence.Polling.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Polling.Services
{
    public class QuestionService
    {
        protected class SurveyRepository : BaseRepository<Survey, long> { }
        protected class QuestionRepository : BaseRepository<Question, int> { }
        protected class SurveyTypeRepository : BaseRepository<SurveyType, int> { }

        public static SurveyType AddSurveyType(SurveyType surveyType)
        {
            if (surveyType == null)
                throw new ArgumentNullException();
            var validation = new QuestionValidation.SurveyTypeEntity().Validate(surveyType);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);
            surveyType = SurveyTypeRepository.Add(surveyType);
            return surveyType;
        }
        public static SurveyType UpdateSurveyType(SurveyType surveyType)
        {
            if (surveyType == null)
                throw new ArgumentNullException();

            var oldEntity = SurveyTypeRepository.Get(surveyType.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new QuestionValidation.SurveyTypeEntity().Validate(surveyType);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            surveyType = SurveyTypeRepository.Update(surveyType);
            return surveyType;
        }
        public static bool DeleteSurveyType(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var SurveyType = SurveyTypeRepository.Get(id);
            if (SurveyType == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = SurveyTypeRepository.SoftDelete(SurveyType);
            return isDeleted;
        }
        public static SurveyType GetSurveyTypeById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var surveyType = SurveyTypeRepository.Get(id);

            if (surveyType == null || !surveyType.IsActive)
                return null;

            return surveyType;
        }
        public static IEnumerable<SurveyType> GetAllSurveyTypes()
        {
            return SurveyTypeRepository.GetAll();
        }
        public static Survey AddSurvey(Survey survey)
        {
            if (survey == null)
                throw new ArgumentNullException();

            var isCodeExists = SurveyRepository.GetQuery(row => row.Code == survey.Code).Any();
            if (isCodeExists)
                MegaException.ThrowException("این کد قبلا ثبت شده است");

            var isDateExists = SurveyRepository.GetQuery(row => row.StartDate >= survey.StartDate || row.EndDate <= survey.EndDate).Any();
            if (isDateExists)
                MegaException.ThrowException("تاریخ درج شده همپوشانی دارد");

            var validation = new QuestionValidation.SurveyEntity().Validate(survey);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);
            survey = SurveyRepository.Add(survey);
            return survey;
        }
        public static Survey UpdateSurvey(Survey survey)
        {
            if (survey == null)
                throw new ArgumentNullException();

            var codeEntity = GetSurveyByCode(survey.Code);
            if (codeEntity != null)
                MegaException.ThrowException("این کد قبلا ثبت شده است");

            var oldEntity = SurveyRepository.Get(survey.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new QuestionValidation.SurveyEntity().Validate(survey);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            survey = SurveyRepository.Update(survey);
            return survey;
        }
        public static bool DeleteSurvey(long id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var Survey = SurveyRepository.Get(id);
            if (Survey == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = SurveyRepository.SoftDelete(Survey);
            return isDeleted;
        }
        public static Survey GetSurveyById(long id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var Survey = SurveyRepository.Get(id);

            if (Survey == null || !Survey.IsActive)
                return null;

            return Survey;
        }
        public static Survey GetSurveyByCode(string code)
        {
            if (string.IsNullOrEmpty(code))
                throw new ArgumentNullException();

            var Survey = SurveyRepository.GetQuery(x => x.Code == code).FirstOrDefault();

            if (Survey == null || !Survey.IsActive)
                return null;

            return Survey;
        }
        public static IEnumerable<Survey> GetAllSurveys()
        {
            return SurveyRepository.GetAll();
        }
        public static PagedList<Survey> GetAllSurveysWithFilter(DateTime? fromDate, DateTime? toDate, int? surveyTypeId, string title, string code, int? questionType, int? clientId, int currentPage, int pageSize = 25)
        {
            IQueryable<Survey> queryableSurvey = SurveyRepository.GetQuery(campaign => campaign.IsActive);
            if (fromDate.HasValue)
            {
                DateTime FromDateFixed = fromDate.Value.Date;
                queryableSurvey = queryableSurvey.Where(campaign => campaign.StartDate >= FromDateFixed);
            }
            if (toDate.HasValue)
            {
                DateTime toDateFixed = toDate.Value.AddDays(1).AddSeconds(-1);
                queryableSurvey = queryableSurvey.Where(survey => survey.EndDate <= toDateFixed);
            }
            if (surveyTypeId.HasValue)
                queryableSurvey = queryableSurvey.Where(survey => survey.SurveyTypeId == surveyTypeId.Value);
            if (!string.IsNullOrEmpty(title))
                queryableSurvey = queryableSurvey.Where(survey => survey.Title.Contains(title));
            if (!string.IsNullOrEmpty(code))
                queryableSurvey = queryableSurvey.Where(survey => survey.Code.Contains(code));
            if (questionType.HasValue)
                queryableSurvey = queryableSurvey.Where(survey => survey.QuestionType == questionType.Value);
            if (clientId.HasValue)
                queryableSurvey = queryableSurvey.Where(survey => survey.ClientId == clientId.Value);

            queryableSurvey = queryableSurvey.OrderByDescending(x => x.CreatedBy);
            return new PagedList<Survey>
            {
                CurrentPage = currentPage,
                PageSize = pageSize,
                Records = queryableSurvey.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList(),
                TotalRecords = queryableSurvey.Count()
            };
        }
        public static Question AddQuestion(Question Question)
        {
            if (Question == null)
                throw new ArgumentNullException();
            var validation = new QuestionValidation.QuestionEntity().Validate(Question);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);
            Question = QuestionRepository.Add(Question);
            return Question;
        }
        public static Question UpdateQuestion(Question Question)
        {
            if (Question == null)
                throw new ArgumentNullException();

            var oldEntity = QuestionRepository.Get(Question.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new QuestionValidation.QuestionEntity().Validate(Question);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            Question = QuestionRepository.Update(Question);
            return Question;
        }
        public static bool DeleteQuestion(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var Question = QuestionRepository.Get(id);
            if (Question == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = QuestionRepository.SoftDelete(Question);
            return isDeleted;
        }
        public static Question GetQuestionById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var Question = QuestionRepository.Get(id);

            if (Question == null || !Question.IsActive)
                return null;

            return Question;
        }
        public static List<Question> GetQuestionBySurveyId(long surveyId)
        {
            var Questions = QuestionRepository.GetQuery(e => e.SurveyId == surveyId).ToList();
            return Questions;
        }
        public static IEnumerable<Question> GetAllQuestions()
        {
            return QuestionRepository.GetAll();
        }
        public static PagedList<Question> GetAllQuestionsWithFilter(DateTime? fromDate, DateTime? toDate, string title, int? surveyId, string code, int? questionType, int? clientId, int currentPage, int pageSize = 25)
        {
            IQueryable<Question> queryableQuestion = QuestionRepository.GetQuery(campaign => campaign.IsActive);
            if (fromDate.HasValue)
            {
                DateTime FromDateFixed = fromDate.Value.Date;
                queryableQuestion = queryableQuestion.Include(row => row.Survey)
                    .Where(question => question.Survey.StartDate >= FromDateFixed);
            }

            if (toDate.HasValue)
            {
                DateTime toDateFixed = toDate.Value.AddDays(1).AddSeconds(-1);
                queryableQuestion = queryableQuestion.Include(row => row.Survey)
                    .Where(question => question.Survey.EndDate <= toDateFixed);
            }

            if (!string.IsNullOrEmpty(code))
            {
                queryableQuestion = queryableQuestion.Include(row => row.Survey)
                    .Where(survey => survey.Survey.Code.Contains(code));
            }
            if (questionType.HasValue)
            {
                queryableQuestion = queryableQuestion.Include(row => row.Survey)
                    .Where(survey => survey.Survey.QuestionType == questionType.Value);
            }
            if (!string.IsNullOrEmpty(title))
            {
                queryableQuestion = queryableQuestion.Where(question => question.Title.Contains(title));
            }
            if (surveyId.HasValue)
            {
                queryableQuestion = queryableQuestion.Where(question => question.SurveyId == surveyId.Value);
            }
            if (clientId.HasValue)
            {
                queryableQuestion = queryableQuestion.Include(row => row.Survey)
                    .Where(question => question.Survey.ClientId == clientId.Value);
            }

            queryableQuestion = queryableQuestion.OrderByDescending(x => x.CreatedBy);
            return new PagedList<Question>
            {
                CurrentPage = currentPage,
                PageSize = pageSize,
                Records = queryableQuestion.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList(),
                TotalRecords = queryableQuestion.Count()
            };
        }
        public static List<Question> GetAllValidQuestion(DateTime? toDate, int? clientId, long? surveyId)
        {
            IQueryable<Question> queryableQuestion = QuestionRepository.GetQuery(row => row.IsActive).Include(row => row.Survey);
            if (toDate.HasValue)
            {
                DateTime toDateFixed = toDate.Value.AddDays(1).AddSeconds(-1);
                queryableQuestion = queryableQuestion.Where(question => question.Survey.EndDate <= toDateFixed);
            }
            if (clientId.HasValue)
            {
                queryableQuestion = queryableQuestion.Where(survey => survey.Survey.ClientId == clientId.Value);
            }
            if (surveyId.HasValue)
            {
                queryableQuestion = queryableQuestion.Where(row => row.SurveyId == surveyId.Value);
            }

            return queryableQuestion.ToList();
        }
    }
}
