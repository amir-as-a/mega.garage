﻿using FluentValidation;
using Mega.Garage.Common;
using Mega.Garage.Logic.Polling.Validation;
using Mega.Garage.Logic.Polling.ViewModel;
using Mega.Garage.Persistence.Polling.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Logic.Polling.Services
{
    public class UserSurveyService
    {
        protected class UserSurveyRepository : BaseRepository<UserSurvey, long> { }
        protected class UserSurveyQuestionRepository : BaseRepository<UserSurveyQuestion, long> { }
        public static UserSurvey AddUserSurvey(UserSurvey userSurvey)
        {
            if (userSurvey == null)
                throw new ArgumentNullException();
            userSurvey = UserSurveyRepository.Add(userSurvey);
            return userSurvey;
        }
        public static UserSurvey UpdateUserSurvey(UserSurvey userSurvey)
        {
            if (userSurvey == null)
                throw new ArgumentNullException();

            var oldEntity = UserSurveyRepository.Get(userSurvey.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new UserSurveyValidation.UserSurveyEntity().Validate(userSurvey);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            userSurvey = UserSurveyRepository.Update(userSurvey);
            return userSurvey;
        }
        public static bool DeleteUserSurvey(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var userSurvey = UserSurveyRepository.Get(id);
            if (userSurvey == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = UserSurveyRepository.SoftDelete(userSurvey);
            return isDeleted;
        }
        public static UserSurvey GetUserSurveyById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var userSurvey = UserSurveyRepository.Get(id);

            if (userSurvey == null || !userSurvey.IsActive)
                return null;

            return userSurvey;
        }
        public static IEnumerable<UserSurvey> GetAllUserSurveys()
        {
            return UserSurveyRepository.GetAll();
        }
        // this method needs a revised 
        public static void AddUserSurveyQuestion(long userSurveyId, List<UserSurveyQuestion> userSurveyQuestions)
        {
            if (userSurveyQuestions == null)
                throw new ArgumentNullException();
            userSurveyQuestions.ForEach(x => x.UserSurveyId = userSurveyId);
            foreach (var item in userSurveyQuestions)
            {
                UserSurveyQuestionRepository.Add(item);
            }
        }
        public static UserSurveyQuestion AddUserSurveyQuestion(UserSurveyQuestion userSurveyQuestion)
        {
            if (userSurveyQuestion == null)
                throw new ArgumentNullException();
            var validation = new UserSurveyValidation.UserSurveyQuestionEntity().Validate(userSurveyQuestion);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);
            userSurveyQuestion = UserSurveyQuestionRepository.Add(userSurveyQuestion);
            return userSurveyQuestion;
        }
        public static UserSurveyQuestion UpdateUserSurveyQuestion(UserSurveyQuestion userSurveyQuestion)
        {
            if (userSurveyQuestion == null)
                throw new ArgumentNullException();

            var oldEntity = UserSurveyQuestionRepository.Get(userSurveyQuestion.Id);

            if (oldEntity == null || !oldEntity.IsActive)
                throw new InvalidUpdateOperationException();

            var validation = new UserSurveyValidation.UserSurveyQuestionEntity().Validate(userSurveyQuestion);
            if (!validation.IsValid)
                throw new ValidationException(validation.Errors);

            userSurveyQuestion = UserSurveyQuestionRepository.Update(userSurveyQuestion);
            return userSurveyQuestion;
        }
        public static bool DeleteUserSurveyQuestion(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();
            var userSurveyQuestion = UserSurveyQuestionRepository.Get(id);
            if (userSurveyQuestion == null)
                throw new InvalidDeleteOperationException();
            bool isDeleted = UserSurveyQuestionRepository.SoftDelete(userSurveyQuestion);
            return isDeleted;
        }
        public static UserSurveyQuestion GetUserSurveyQuestionById(int id)
        {
            if (id == 0)
                throw new ArgumentNullException();

            var userSurveyQuestion = UserSurveyQuestionRepository.Get(id);

            if (userSurveyQuestion == null || !userSurveyQuestion.IsActive)
                return null;

            return userSurveyQuestion;
        }
        public static IEnumerable<UserSurveyQuestion> GetAllUserSurveyQuestions()
        {
            return UserSurveyQuestionRepository.GetAll();
        }

        public static UserSurveyViewModel GetUserSurveyByOrderNumber(string orderNumber)
        {
            if (string.IsNullOrEmpty(orderNumber))
                throw new ArgumentNullException();
            var vm = new UserSurveyViewModel();
            vm.UserSurvey = UserSurveyRepository.GetQuery(x => x.OrderNumber == orderNumber).FirstOrDefault();
            if (vm.UserSurvey != null)
            {
                vm.UserSurveyQuestions = UserSurveyQuestionRepository.GetQuery(x => x.UserSurveyId == vm.UserSurvey.Id)
                 .Include(row => row.Question).ToList();
                vm.UserSurveyQuestions.ForEach(x => x.Question.Survey = QuestionService.GetSurveyById(x.Question.SurveyId.Value));
            }
            return vm;
        }
        public static UserSurveyViewModel GetUserSurveyByUserServeyId(int id)
        {
            var vm = new UserSurveyViewModel();
            vm.UserSurvey = UserSurveyRepository.GetQuery(x => x.Id == id).FirstOrDefault();
            vm.Questions = QuestionService.GetQuestionBySurveyId(vm.UserSurvey.SurveyId);
            if (vm.UserSurvey != null)
            {
                vm.UserSurveyQuestions = UserSurveyQuestionRepository.GetQuery(x => x.UserSurveyId == vm.UserSurvey.Id)
                 .Include(row => row.Question).ToList();
                vm.UserSurveyQuestions.ForEach(x => x.Question.Survey = QuestionService.GetSurveyById(x.Question.SurveyId.Value));
            }
            return vm;
        }
    }
}
