﻿using FluentValidation;
using Mega.Garage.Persistence.Polling.Entities;

namespace Mega.Garage.Logic.Polling.Validation
{
    internal class QuestionValidation
    {
        internal class SurveyTypeEntity : AbstractValidator<SurveyType>
        {
            public SurveyTypeEntity()
            {
                RuleFor(x => x.Title)
                    .NotNull().WithMessage("وارد کردن عنوان اجباری است.")
                    .MaximumLength(150).WithMessage("عنوان می توانند حداکثر 150 حرف داشته باشد.");
                RuleFor(x => x.Description)
                    .NotNull().WithMessage("وارد کردن توضیحات اجباری است.");
            }
        }
        internal class SurveyEntity : AbstractValidator<Survey>
        {
            public SurveyEntity()
            {
                RuleFor(x => x.Title)
                    .NotNull().WithMessage("وارد کردن عنوان اجباری است.")
                    .MaximumLength(150).WithMessage("عنوان می توانند حداکثر 150 حرف داشته باشد.");
                RuleFor(x => x.Description)
                    .NotNull().WithMessage("وارد کردن توضیحات اجباری است.");
                RuleFor(x => x.StartDate)
                    .LessThan(x => x.EndDate).WithMessage("بازه زمانی نظرسنجی معتبر نیست");

            }
        }
        internal class QuestionEntity : AbstractValidator<Question>
        {
            public QuestionEntity()
            {
                RuleFor(x => x.Title)
                    .NotNull().WithMessage("وارد کردن عنوان اجباری است.")
                    .MaximumLength(250).WithMessage("عنوان می توانند حداکثر 250 حرف داشته باشد.");
            }
        }
    }
}
