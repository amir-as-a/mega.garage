﻿using FluentValidation;
using Mega.Garage.Persistence.Polling.Entities;

namespace Mega.Garage.Logic.Polling.Validation
{
    internal class UserSurveyValidation
    {
        internal class UserSurveyEntity : AbstractValidator<UserSurvey>
        {
            public UserSurveyEntity()
            {

            }
        }
        internal class UserSurveyQuestionEntity : AbstractValidator<UserSurveyQuestion>
        {
            public UserSurveyQuestionEntity()
            {

            }
        }
    }
}
