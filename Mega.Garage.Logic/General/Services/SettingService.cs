﻿using Mega.Garage.Persistence.General.Entities;
using System.Linq;

namespace Mega.Garage.Logic.General.Services
{
    public class SettingsService
    {
        protected class SettingsRepository : BaseRepository<Setting, int> { }

        #region Mandatory Select Dealer
        public static bool IsMandatoryDealer()
        {
            var settings = SettingsRepository.GetQuery(c => c.IsActive && c.KeyTitle == "IsMandatoryDealer").FirstOrDefault();

            if (settings == null)
                return false;

            return bool.Parse(settings.ValueTitle);
        }

        public static bool SaveMandatoryDealer(bool mandatory)
        {
            var settings = SettingsRepository.GetQuery(c => c.IsActive && c.KeyTitle == "IsMandatoryDealer").FirstOrDefault();
            if (settings == null)
            {
                settings = new Setting()
                {
                    KeyTitle = "IsMandatoryDealer",
                    ValueTitle = mandatory.ToString()
                };

                return SettingsRepository.Add(settings).Id > 0;
            }
            else
            {
                settings.ValueTitle = mandatory.ToString();
                return SettingsRepository.Update(settings).Id > 0;
            }
        }
        #endregion

        #region Send Sms To Dealer Active
        public static bool IsSendSmsToDealer()
        {
            var settings = SettingsRepository.GetQuery(c => c.IsActive && c.KeyTitle == "IsSendSmsToDealer").FirstOrDefault();

            if (settings == null)
                return false;

            return bool.Parse(settings.ValueTitle);
        }

        public static bool SaveSendSmsToDealer(bool status)
        {
            var settings = SettingsRepository.GetQuery(c => c.IsActive && c.KeyTitle == "IsSendSmsToDealer").FirstOrDefault();
            if (settings == null)
            {
                settings = new Setting()
                {
                    KeyTitle = "IsSendSmsToDealer",
                    ValueTitle = status.ToString()
                };

                return SettingsRepository.Add(settings).Id > 0;
            }
            else
            {
                settings.ValueTitle = status.ToString();
                return SettingsRepository.Update(settings).Id > 0;
            }
        }
        #endregion

    }
}
