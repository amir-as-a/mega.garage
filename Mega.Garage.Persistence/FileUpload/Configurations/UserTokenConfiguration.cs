﻿using Mega.Garage.Persistence.FileUpload.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.FileUpload.Configurations
{
    public class UserTokenConfiguration : EntityTypeConfiguration<UserToken>
    {
        public UserTokenConfiguration()
        {
            ToTable("UserToken", "fileupload");

            Property(c => c.UserName)
                .HasMaxLength(200);

            Property(c => c.Token)
                .HasMaxLength(30);

            Property(c => c.TokenPassword)
                .HasMaxLength(200);

            Property(c => c.RowVersion)
                .IsFixedLength();
        }
    }
}
