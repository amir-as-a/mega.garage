﻿using Mega.Garage.Persistence.FileUpload.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.FileUpload.Configurations
{
    public class UserFileConfiguration : EntityTypeConfiguration<UserFile>
    {
        public UserFileConfiguration() {
            ToTable("UserFile", "fileupload");
            
            Property(c => c.FileName)
                .HasMaxLength(256);
            
            Property(c => c.MimeType)
                .HasMaxLength(200);

            Property(c => c.FilePath)
                .HasMaxLength(2000);

            Property(c => c.CDNAddress)
                .HasMaxLength(2000);

            Property(c => c.RowVersion)
                .IsFixedLength();
        }
    }
}
