using Mega.Garage.Persistence.FileUpload;
using Mega.Garage.Persistence.FileUpload.Configurations;
using Mega.Garage.Persistence.FileUpload.Entities;
using System.Data.Entity;

namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public virtual DbSet<UserFile> UserFile { get; set; }
        public virtual DbSet<UserToken> UserToken { get; set; }

        public static void ConfigureFileUpload(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserTokenConfiguration());
            modelBuilder.Configurations.Add(new UserFileConfiguration());
        }
    }

}
