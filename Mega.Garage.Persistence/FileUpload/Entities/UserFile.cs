using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.FileUpload.Entities
{
    
    public partial class UserFile : BaseEntity<Guid>
    {
        public string FileName { get; set; }

        public int FileSize { get; set; }

        public string FilePath { get; set; }
        public string CDNAddress { get; set; }

        public string MimeType { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public DateTime UploadDate { get; set; }

        public Guid UserTokenId { get; set; }

        public virtual UserToken UserToken { get; set; }
    }
}
