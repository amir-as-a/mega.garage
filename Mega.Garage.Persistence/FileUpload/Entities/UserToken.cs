using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.FileUpload.Entities
{
   
    public class UserToken : BaseEntity<Guid>
    {
        public string UserName { get; set; }

        public string Token { get; set; }

        public string TokenPassword { get; set; }


        public virtual ICollection<UserFile> UserFile { get; set; }

    }
}
