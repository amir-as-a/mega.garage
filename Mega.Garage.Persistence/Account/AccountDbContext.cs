using Mega.Garage.Persistence.Account.Configurations;
using System.Data.Entity;
using Mega.Garage.Persistence.Account;
using Mega.Garage.Persistence.Account.Entities;

namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public virtual DbSet<OTP> Otps { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }

        public static void ConfigureAccount(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new OtpConfiguration());
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new UserTypeConfiguration());
        }
    }
}
