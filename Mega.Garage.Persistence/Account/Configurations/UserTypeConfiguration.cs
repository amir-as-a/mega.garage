﻿using Mega.Garage.Persistence.Account.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Account.Configurations
{
    public class UserTypeConfiguration : EntityTypeConfiguration<UserType>
    {
        public UserTypeConfiguration()
        {
            ToTable("UserType", "auth");

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
