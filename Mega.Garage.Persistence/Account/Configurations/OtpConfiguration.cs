﻿using Mega.Garage.Persistence.Account.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Account.Configurations
{
    public class OtpConfiguration : EntityTypeConfiguration<OTP>
    {
        public OtpConfiguration()
        {
            ToTable("OTP", "auth");

            Property(p => p.UserMobile)
                .HasMaxLength(20)
                .HasColumnName("UserPhone");
            

            Property(p => p.OneTimePassword)
                .HasMaxLength(15);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
