﻿using Mega.Garage.Persistence.Account.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Account.Configurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("User", "auth");

            Property(p => p.UserName)
                .HasMaxLength(50);

            Property(p => p.FirstName)
                .HasMaxLength(150);

            Property(p => p.UserMobile)
               .HasMaxLength(20)
               .HasColumnName("PhoneNumber");

            Property(p => p.LastName)
                .HasMaxLength(150);

            Property(p => p.Email)
                .HasMaxLength(150);
            
            Property(p => p.UserCode)
                .HasMaxLength(20);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
