﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Account.Entities
{
    
    public partial class OTP : BaseEntity<long>
    {
        [DisplayName("تلفن همراه کاربر")]
        public string UserMobile { get; set; }
        [DisplayName("رمز یکبار مصرف")]
        public string OneTimePassword { get; set; }
        [DisplayName("تاریخ ایجاد")]
        public DateTime GenerateDate { get; set; }
        [DisplayName("تاریخ ارسال")]
        public DateTime SendDate { get; set; }
        [DisplayName("تاریخ انقضاء")]
        public DateTime ExpireDate { get; set; }
        [DisplayName("کد وارد شده یا خیر")]
        public bool IsEntered { get; set; }
        [DisplayName("تاریخ ورود")]
        public DateTime? EnteredDate { get; set; }
        [DisplayName("ردیف نوع مشتری")]
        public int ClientId { get; set; }
    }
}
