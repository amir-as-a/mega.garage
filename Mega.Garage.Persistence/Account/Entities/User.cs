﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Account.Entities
{

    
    public class User : BaseEntity<int>
    {
        [DisplayName("نام کاربری")]
        public string UserName { get; set; }
        [DisplayName("رمزعبور")]
        public string PasswordHash { get; set; }
        [DisplayName("نام")]
        public string FirstName { get; set; }
        [DisplayName("نام خانوادگی")]
        public string LastName { get; set; }
        [DisplayName("موبایل")]
        public string UserMobile { get; set; }
        [DisplayName("ایمیل")]
        public string Email { get; set; }
        [DisplayName("نوع مشتری")]
        public int ClientId { get; set; }
        [DisplayName("نوع کاربر")]
        public int UserTypeId { get; set; }
        [DisplayName("کد کاربر")]
        public string UserCode { get; set; }
        [DisplayName("شناسه یکتا کاربر")]
        public string Guid { get; set; }
        [DisplayName("تاریخ آخرین لاگین")]
        public DateTime? LastLoginDate { get; set; }
        [DisplayName("کاربر مسدود شده")]
        public bool IsLocked { get; set; }
        [DisplayName("فیلد رزرو 1")]
        public bool? Reserve1 { get; set; }
        [DisplayName("فیلد رزرو 2")]
        public bool? Reserve2 { get; set; }
    }
}
