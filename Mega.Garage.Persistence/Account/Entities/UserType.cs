﻿using System.ComponentModel;

namespace Mega.Garage.Persistence.Account.Entities
{
    public class UserType : BaseEntity<int>
    {
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [DisplayName("کد")]
        public string Code { get; set; }
    }
}
