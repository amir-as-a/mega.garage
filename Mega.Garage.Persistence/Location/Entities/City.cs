﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Location.Entities
{
    
    public class City : BaseEntity<int>
    {
        [DisplayName("شهر")]
        public string Title { get; set; }
        [DisplayName("شناسه شهر")]
        public string Code { get; set; }
        [DisplayName("استان")]
        public int? ProvinceId { get; set; }

        public virtual Province Province { get; set; }

        public virtual ICollection<Region> Regions { get; } = new HashSet<Region>();
    }
}
