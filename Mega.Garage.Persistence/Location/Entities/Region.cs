﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Location.Entities
{
   
    public class Region : BaseEntity<int>
    {
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("شناسه")]
        public string Code { get; set; }
        [DisplayName("شهر")]
        public int CityId { get; set; }
        public virtual City City { get; set; }
    }
}
