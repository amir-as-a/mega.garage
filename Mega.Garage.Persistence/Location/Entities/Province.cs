﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Location.Entities
{
   
    public class Province : BaseEntity<int>
    {
        [DisplayName("استان")]
        public string Title { get; set; }
        public virtual ICollection<City> Cities { get; } = new HashSet<City>();
    }
}
