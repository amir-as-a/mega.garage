﻿using Mega.Garage.Persistence.Location.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Location.Configurations
{
    public class CityConfiguration : EntityTypeConfiguration<City>
    {
        public CityConfiguration()
        {
            ToTable("City", "location");

            Property(p => p.Code)
                .HasMaxLength(50);
            
            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}

