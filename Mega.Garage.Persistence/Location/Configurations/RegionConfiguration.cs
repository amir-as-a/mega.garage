﻿using Mega.Garage.Persistence.Location.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Location.Configurations
{
    public class RegionConfiguration : EntityTypeConfiguration<Region>
    { 
        public RegionConfiguration()
        {
            ToTable("Region", "location");

            Property(p => p.Title)
                .HasMaxLength(100);

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
