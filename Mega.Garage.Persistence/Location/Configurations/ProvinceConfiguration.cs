﻿using Mega.Garage.Persistence.Location.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Location.Configurations
{
    public class ProvinceConfiguration : EntityTypeConfiguration<Province>
    {
        public ProvinceConfiguration()
        {
            ToTable("Province", "location");

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
