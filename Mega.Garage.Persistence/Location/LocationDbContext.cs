using System.Data.Entity;
using Mega.Garage.Persistence.Location;
using Mega.Garage.Persistence.Location.Configurations;
using Mega.Garage.Persistence.Location.Entities;

namespace Mega.Garage.Persistence
{

    public partial class DataContext : DbContext
    {
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<Province> Provinces { get; set; }
        public virtual DbSet<Region> Regions { get; set; }

        public static void ConfigureLocation(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CityConfiguration());
            modelBuilder.Configurations.Add(new ProvinceConfiguration());
            modelBuilder.Configurations.Add(new RegionConfiguration());
        }
    }
}
