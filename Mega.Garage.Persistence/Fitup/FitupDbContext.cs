using System.Data.Entity;
using Mega.Garage.Persistence.Fitup.Configurations;
using Mega.Garage.Persistence.Fitup;
using Mega.Garage.Persistence.Fitup.Entities;

namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public virtual DbSet<CarGroups> FitUpCarGroups { get; set; }
        public virtual DbSet<CarModel> FitUpCarModels { get; set; }
        public virtual DbSet<Mapping> FitUpMapping { get; set; }

        public static void ConfigureFitup(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CarGroupsConfiguration());
            modelBuilder.Configurations.Add(new CarModelsConfiguration());
            modelBuilder.Configurations.Add(new MappingConfiguration());
        }
    }
}
