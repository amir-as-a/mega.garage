﻿using Mega.Garage.Persistence.Fitup.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Fitup.Configurations
{
    public class MappingConfiguration : EntityTypeConfiguration<Mapping>
    {
        public MappingConfiguration()
        {
            ToTable("Mapping", "fitup");
            HasOptional(e => e.CarGroup).WithMany().HasForeignKey(e => e.CarGroupId).WillCascadeOnDelete(false);
            HasOptional(e => e.CarModel).WithMany().HasForeignKey(e => e.CarModelId).WillCascadeOnDelete(false);
            HasOptional(e => e.Product).WithMany().HasForeignKey(e => e.ProductId).WillCascadeOnDelete(false);
            Property(e => e.ProductCode).HasMaxLength(200);
            Property(e => e.CatalogTitle).HasMaxLength(250);
            Property(e => e.Description);

            Property(e => e.RowVersion).IsRowVersion().IsFixedLength().IsConcurrencyToken();
        }
    }
}
