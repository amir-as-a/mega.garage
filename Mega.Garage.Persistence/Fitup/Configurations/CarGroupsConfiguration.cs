﻿using Mega.Garage.Persistence.Fitup.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Fitup.Configurations
{
    public class CarGroupsConfiguration : EntityTypeConfiguration<CarGroups>
    {
        public CarGroupsConfiguration()
        {
            ToTable("CarGroups", "fitup");
            Property(e => e.Title).HasMaxLength(50);
            Property(e => e.BrandCode).HasMaxLength(50);
            Property(e => e.SPic).HasMaxLength(50);
            Property(e => e.RowVersion).IsRowVersion().IsFixedLength().IsConcurrencyToken();
        }
    }
}
