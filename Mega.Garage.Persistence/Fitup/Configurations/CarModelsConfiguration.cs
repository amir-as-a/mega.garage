﻿using Mega.Garage.Persistence.Fitup.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Fitup.Configurations
{
    public class CarModelsConfiguration : EntityTypeConfiguration<CarModel>
    {
        public CarModelsConfiguration()
        {
            ToTable("CarModels", "fitup");
            Property(e => e.Title).HasMaxLength(50);
            Property(e => e.Code).HasMaxLength(50);
            HasOptional(e => e.CarGroup).WithMany(x => x.CarModel).HasForeignKey(e => e.CarGroupId).WillCascadeOnDelete(false);

            Property(e => e.RowVersion).IsRowVersion().IsFixedLength().IsConcurrencyToken();
        }
    }
}
