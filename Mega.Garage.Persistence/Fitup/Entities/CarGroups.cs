using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Fitup.Entities
{
    
    public class CarGroups : BaseEntity<int>
    {
        public string Title { get; set; }
        public string BrandCode { get; set; }
        public bool? Visible1 { get; set; }
        public bool? Visible2 { get; set; }
        public bool? Visible3 { get; set; }
        public string SPic { get; set; }
        public virtual IList<CarModel> CarModel { get; set; }
    }
}
