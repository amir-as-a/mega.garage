using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Fitup.Entities
{
    
    public partial class CarModel : BaseEntity<int>
    {
        public int? CarGroupId { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public bool? Visible1 { get; set; }
        public bool? Visible2 { get; set; }
        public bool? Visible3 { get; set; }
        public virtual CarGroups CarGroup { get; set; }

    }
}
