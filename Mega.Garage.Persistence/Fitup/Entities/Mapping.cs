using System.ComponentModel.DataAnnotations.Schema;
using Mega.Garage.Persistence.Order.Entities;

namespace Mega.Garage.Persistence.Fitup.Entities
{
    
    public class Mapping : BaseEntity<int>
    {
        public int? CarGroupId { get; set; }
        public int? CarModelId { get; set; }
        public long? ProductId { get; set; }
        public string ProductCode { get; set; }
        public string CatalogTitle { get; set; }
        public string Description { get; set; }

        public virtual CarGroups CarGroup { get; set; }
        public virtual CarModel CarModel { get; set; }
        public virtual Product Product { get; set; }
    }
}
