using System.Data.Entity;
using Mega.Garage.Persistence.Order.Configurations;
using Mega.Garage.Persistence.Order.Entities;


namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public virtual DbSet<Discount> Discounts { get; set; }
        public virtual DbSet<DiscountProduct> DiscountProducts { get; set; }
        public virtual DbSet<ProductList> ProductLists { get; set; }
        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<CampaignProduct> CampaignProducts { get; set; }
        public virtual DbSet<CampaignType> CampaignTypes { get; set; }
        public virtual DbSet<Cart> Carts { get; set; }
        public virtual DbSet<Error> Errors { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<InvoiceAdditive> InvoiceAdditives { get; set; }
        public virtual DbSet<InvoiceAdditiveType> InvoiceAdditiveTypes { get; set; }
        public virtual DbSet<InvoiceProcess> InvoiceProcesses { get; set; }
        public virtual DbSet<InvoiceProcessHistory> InvoiceProcessHistories { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }
        public virtual DbSet<OrderProcess> OrderProcesses { get; set; }
        public virtual DbSet<OrderProcessHistory> OrderProcessHistories { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<CartItem> CartItems { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<PaymentType> PaymentTypes { get; set; }
        public static void ConfigureOrder(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CampaignConfiguration());
            modelBuilder.Configurations.Add(new CampaignProductConfiguration());
            modelBuilder.Configurations.Add(new CampaignTypeConfiguration());
            modelBuilder.Configurations.Add(new CartConfiguration());
            modelBuilder.Configurations.Add(new CartItemConfiguration());
            modelBuilder.Configurations.Add(new DiscountConfiguration());
            modelBuilder.Configurations.Add(new DiscountProductConfiguration());
            modelBuilder.Configurations.Add(new ErrorConfiguration());
            modelBuilder.Configurations.Add(new InvoiceConfiguration());
            modelBuilder.Configurations.Add(new InvoiceAdditiveConfiguration());
            modelBuilder.Configurations.Add(new InvoiceAdditiveTypeConfiguration());
            modelBuilder.Configurations.Add(new InvoiceProcessConfiguration());
            modelBuilder.Configurations.Add(new InvoiceProcessHistoryConfiguration());
            modelBuilder.Configurations.Add(new OrderItemConfiguration());
            modelBuilder.Configurations.Add(new OrderProcessConfiguration());
            modelBuilder.Configurations.Add(new OrderProcessHistoryConfiguration());
            modelBuilder.Configurations.Add(new ProductConfiguration());
            modelBuilder.Configurations.Add(new ProductListConfiguration());
            modelBuilder.Configurations.Add(new CustomerConfiguration());
            modelBuilder.Configurations.Add(new PaymentTypeConfiguration());
        }
    }
}
