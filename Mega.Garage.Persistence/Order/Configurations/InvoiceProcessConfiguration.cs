﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class InvoiceProcessConfiguration : EntityTypeConfiguration<InvoiceProcess>
    {
        public InvoiceProcessConfiguration()
        {
            ToTable("InvoiceProcess", "sale");
            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Title).HasMaxLength(250);
            Property(p => p.Code).HasMaxLength(50);
        }
    }
}