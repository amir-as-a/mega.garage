﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class ProductConfiguration : EntityTypeConfiguration<Product>
    {
        public ProductConfiguration()
        {
            ToTable("Product", "sale");
            Property(p => p.Code).HasMaxLength(200);
            Property(p => p.CatalogTitle).HasMaxLength(250);
            Property(p => p.ShortTitle).HasMaxLength(255);
            Property(p => p.CatalogCode).HasMaxLength(50);
            Property(p => p.CatalogImage).HasMaxLength(250);
            Property(p => p.ColorImage).HasMaxLength(250);
            Property(p => p.ShareLinkAddress).HasMaxLength(500);
        }
    }
}