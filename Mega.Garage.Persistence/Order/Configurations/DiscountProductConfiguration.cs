﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class DiscountProductConfiguration : EntityTypeConfiguration<DiscountProduct>
    {
        public DiscountProductConfiguration()
        {
            ToTable("DiscountProduct", "sale");
        }
    }
}