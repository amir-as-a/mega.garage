﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class ErrorConfiguration : EntityTypeConfiguration<Error>
    {
        public ErrorConfiguration()
        {
            ToTable("Error", "sale");
            Property(p => p.Title).HasMaxLength(250);
            Property(p => p.Code).HasMaxLength(50);
        }
    }
}