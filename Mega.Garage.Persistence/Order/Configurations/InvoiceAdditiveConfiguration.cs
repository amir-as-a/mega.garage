﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class InvoiceAdditiveConfiguration : EntityTypeConfiguration<InvoiceAdditive>
    {
        public InvoiceAdditiveConfiguration()
        {
            ToTable("InvoiceAdditive", "sale");
        }
    }
}