﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class InvoiceConfiguration : EntityTypeConfiguration<Invoice>
    {
        public InvoiceConfiguration()
        {
            ToTable("Invoice", "sale");
            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.InvoiceCode).HasMaxLength(150);
            Property(p => p.CustomerFirstName).HasMaxLength(150);
            Property(p => p.CustomerLastName).HasMaxLength(150);
            Property(p => p.CustomerMobile).HasMaxLength(150);
            Property(p => p.OrderNumber).HasMaxLength(250).IsRequired();
            Property(p => p.Ip).HasMaxLength(50).HasColumnName("ip");
            HasRequired(x => x.PaymentType).WithMany().HasForeignKey(x => x.PaymentTypeId);
            HasRequired(x => x.InvoiceProcess).WithMany().HasForeignKey(x => x.CurrentProcessId);
        }
    }
}