﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            ToTable("Customer", "sale");
            Property(p => p.FirstName).HasMaxLength(150);
            Property(p => p.LastName).HasMaxLength(150);
            Property(p => p.Mobile).HasMaxLength(20);
            Property(p => p.Email).HasMaxLength(50);
            Property(p => p.HomePhone).HasMaxLength(50);
            Property(p => p.NationalCode).HasMaxLength(12);
            Property(p => p.ip).HasMaxLength(20);
            Property(p => p.CarGroupCode).HasColumnName("CarGroup");
            HasMany(p => p.Invoices);
        }
    }
}