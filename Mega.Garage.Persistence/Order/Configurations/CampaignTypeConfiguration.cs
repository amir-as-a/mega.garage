﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class CampaignTypeConfiguration : EntityTypeConfiguration<CampaignType>
    {
        public CampaignTypeConfiguration()
        {
            ToTable("CampaignType", "sale");
            Property(p => p.Title).HasMaxLength(50);
            Property(p => p.Code).HasMaxLength(50);
           
        }
    }
}