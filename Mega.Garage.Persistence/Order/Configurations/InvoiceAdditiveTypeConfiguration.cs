﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class InvoiceAdditiveTypeConfiguration : EntityTypeConfiguration<InvoiceAdditiveType>
    {
        public InvoiceAdditiveTypeConfiguration()
        {
            ToTable("InvoiceAdditiveType", "sale");
            Property(p => p.Title).HasMaxLength(250);
        }
    }
}