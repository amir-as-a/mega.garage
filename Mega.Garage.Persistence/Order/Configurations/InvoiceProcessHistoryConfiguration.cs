﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class InvoiceProcessHistoryConfiguration : EntityTypeConfiguration<InvoiceProcessHistory>
    {
        public InvoiceProcessHistoryConfiguration()
        {
            ToTable("InvoiceProcessHistory", "sale");
        }
    }
}