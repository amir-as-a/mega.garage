﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{ 
    public class CartItemConfiguration : EntityTypeConfiguration<CartItem>
    {
        public CartItemConfiguration()
        {
            ToTable("CartItem", "sale");
            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.ProductTitle).HasMaxLength(250);
        }
    }
}