﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class DiscountConfiguration : EntityTypeConfiguration<Discount>
    {
        public DiscountConfiguration()
        {
            ToTable("Discount", "sale");
            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Title).HasMaxLength(250);
        }
    }
}