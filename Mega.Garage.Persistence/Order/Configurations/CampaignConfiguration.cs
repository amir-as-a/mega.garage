﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class CampaignConfiguration : EntityTypeConfiguration<Campaign>
    {
        public CampaignConfiguration()
        {
            ToTable("Campaign", "sale");
            Property(p => p.Title).HasMaxLength(250).IsRequired();
            Property(p => p.CampaignImage).HasMaxLength(250);
            Property(p => p.WebsiteLink).HasMaxLength(500);
            HasOptional(x => x.CampaignType).WithMany().HasForeignKey(x => x.CampaignTypeId);
        }
    }
}