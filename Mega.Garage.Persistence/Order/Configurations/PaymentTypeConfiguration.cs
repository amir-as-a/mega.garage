﻿using Mega.Garage.Persistence.Order.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Persistence.Order.Configurations
{
  
    public class PaymentTypeConfiguration : EntityTypeConfiguration<PaymentType>
    {
        public PaymentTypeConfiguration()
        {
            ToTable("PaymentType", "sale");
            Property(p => p.Title).HasMaxLength(250).IsRequired();
            Property(p => p.Code).HasMaxLength(50).IsRequired();
            Property(p => p.Description).IsRequired();

        }
    }
}
