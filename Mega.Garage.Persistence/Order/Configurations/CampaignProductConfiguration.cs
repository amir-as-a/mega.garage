﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class CampaignProductConfiguration : EntityTypeConfiguration<CampaignProduct>
    {
        public CampaignProductConfiguration()
        {
            ToTable("CampaignProduct", "sale");
        }
    }
}