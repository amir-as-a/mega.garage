﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class OrderProcessConfiguration : EntityTypeConfiguration<OrderProcess>
    {
        public OrderProcessConfiguration()
        {
            ToTable("OrderProcess", "sale");
            Property(p => p.Code).HasMaxLength(50);
        }
    }
}