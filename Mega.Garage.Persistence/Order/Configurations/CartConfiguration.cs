﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class CartConfiguration : EntityTypeConfiguration<Cart>
    {
        public CartConfiguration()
        {
            ToTable("Cart", "sale");
            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Ip).HasMaxLength(50).HasColumnName("ip");
            Property(p => p.CookieToken).HasMaxLength(100);
        }
    }
}