﻿using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class OrderItemConfiguration : EntityTypeConfiguration<OrderItem>
    {
        public OrderItemConfiguration()
        {
            ToTable("OrderItem", "sale");
            HasRequired(p => p.OrderProcess)
                .WithMany()
                .HasForeignKey(p => p.OrderStatusId);
        }
    }
}