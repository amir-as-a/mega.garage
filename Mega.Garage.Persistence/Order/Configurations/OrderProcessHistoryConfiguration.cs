﻿using Mega.Garage.Persistence.Entities;
using Mega.Garage.Persistence.Order.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Order.Configurations
{
    public class OrderProcessHistoryConfiguration : EntityTypeConfiguration<OrderProcessHistory>
    {
        public OrderProcessHistoryConfiguration()
        {
            ToTable("OrderProcessHistory", "sale");
        }
    }
}