using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public partial class InvoiceProcessHistory : BaseEntity<int>
    {
        public int? InvoiceId { get; set; }
        public int? NewInvoiceProcessId { get; set; }
        public int? InvoiceHistoryStatus { get; set; }
        public int? OldInvoiceProcessId { get; set; }
    }
}
