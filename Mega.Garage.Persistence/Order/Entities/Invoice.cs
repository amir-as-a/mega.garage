﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public partial class Invoice : BaseEntity<int>
    {
        [Display(Name = "ردیف سبد خرید")]
        public int? CartId { get; set; }
        [Display(Name = "مشتری")]
        public int CustomerId { get; set; }
        [Display(Name = "نام مشتری")]
        public string CustomerFirstName { get; set; }
        [Display(Name = "نام خانوادگی مشتری")]
        public string CustomerLastName { get; set; }
        [Display(Name = "تلفن همراه مشتری")]
        public string CustomerMobile { get; set; }
        [Display(Name = "ردیف نمایندگی")]
        public int? DealerId { get; set; }
        [Display(Name = "کد فاکتور")]
        public string InvoiceCode { get; set; }
        [Display(Name = "تاریخ فاکتور")]
        public DateTime InvoiceDate { get; set; }
        [Display(Name = "تاریخ انقضاء")]
        public DateTime ExpireDate { get; set; }
        [Display(Name = "شماره سفارش")]
        public string OrderNumber { get; set; }
        [Display(Name = "هزینه های اضافه")]
        public bool? HasAdditive { get; set; }
        [Display(Name = "هزینه های اضافه شده")]
        public double? AdditiveAmount { get; set; }
        [Display(Name = "مبلغ کل")]
        public double? TotalAmount { get; set; }
        [Display(Name = "تعداد کل")]
        public int? TotalQty { get; set; }
        [Display(Name = "کد تخفیف")]
        public string DiscountCode { get; set; }
        [Display(Name = "تخفیف کل")]
        public double? TotalDiscount { get; set; }
        [Display(Name = "قیمت نهایی")]
        public double? FinalPrice { get; set; }
        [Display(Name = "آی پی")]
        public string Ip { get; set; }
        [Display(Name = "ردیف پردازش جاری")]
        public int CurrentProcessId { get; set; }
        [Display(Name = "تحویل")]
        public bool? HasDelivery { get; set; }
        [Display(Name = "محل تحویل")]
        public int? DeliveryPrice { get; set; }
        [Display(Name = "نوع پرداخت")]
        public int? PaymentTypeId { get; set; }
        [Display(Name = "پرداخت شده")]
        public bool? IsPayed { get; set; }
        [Display(Name = "حساب کاربر تایید شده")]
        public bool? IsAccountingAccepted { get; set; }
        [Display(Name = "قیمت نهایی حساب شده")]
        public double? AccountingFinalPrice { get; set; }
        [Display(Name = "وضعیت پرداخت")]
        public string PaymentStatus { get; set; }
        [Display(Name = "تایید شده")]
        public bool? IsConfirmed { get; set; }
        [Display(Name = "تایید شده توسط")]
        public string ConfirmedBy { get; set; }
        [Display(Name = "نوع مشتری")]
        public string ProductGroup { get; set; }
        public string ReserveJsonData { get; set; }
        public virtual Customer Customers { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; } = new HashSet<OrderItem>();
        public virtual PaymentType PaymentType { get; set; }
        public virtual InvoiceProcess InvoiceProcess { get; set; }
    }
}
