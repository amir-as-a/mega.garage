using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public partial class OrderProcessHistory : BaseEntity<int>
    {
        public int OrderId { get; set; }
        public int OrderProcessId { get; set; }
    }
}
