﻿using System;
using System.ComponentModel;

namespace Mega.Garage.Persistence.Order.Entities
{
    public class ProductList : BaseEntity<int>
    {
        [DisplayName("کد محصول")]
        public int? ProductId { get; set; }
        [DisplayName("قیمت")]
        public double? Price { get; set; }
        [DisplayName("درصد نماینده")]
        public double? DealerSharePercent { get; set; }
        [DisplayName("از تاریخ")]
        public DateTime? FromDate { get; set; }
        [DisplayName("تا تاریخ")]
        public DateTime? ToDate { get; set; }
    }
}
