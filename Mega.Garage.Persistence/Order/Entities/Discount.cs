﻿using System;
using System.ComponentModel;

namespace Mega.Garage.Persistence.Order.Entities
{
    public class Discount : BaseEntity<int>
    {
        [DisplayName("عنوان تخفیف")]
        public string Title { get; set; }
        [DisplayName("کد تخفیف")]
        public string Code { get; set; }
        [DisplayName("از تاریخ")]
        public DateTime? FromDate { get; set; }
        [DisplayName("تا تاریخ")]
        public DateTime? ToDate { get; set; }
        [DisplayName("درصد تخفیف")]
        public double? Percent { get; set; }
        [DisplayName("مقدار تخفیف")]
        public double? AmountPrice { get; set; }
        [DisplayName("محدودیت تعداد استفاده")]
        public int? NumberOfUsageLimit { get; set; }
        [DisplayName("سقف مقدار استفاده")]
        public int? MaximumAmountLimit { get; set; }
        [DisplayName("حداقل مبلغ سبد خرید")]
        public int? MinimumBasketSize { get; set; }
        [DisplayName("ابطال شده")]
        public bool HasExpired { get; set; }
        [DisplayName("تعداد استفاده شده")]
        public int? NumberOfUsed { get; set; }
        [DisplayName("قابل استفاده فقط برای")]
        public bool? IsJustForTransportation { get; set; }
        [DisplayName("قابل استفاده برای خرید از وب")]
        public bool IsForWeb { get; set; }
        [DisplayName("قابل استفاده برای خرید از اپلیکیشن موبایل")]
        public bool IsForMobile { get; set; }
        [DisplayName("قابل استفاده برای خرید آنلاین")]
        public bool IsJustForOnlinePayment { get; set; }
    }
}
