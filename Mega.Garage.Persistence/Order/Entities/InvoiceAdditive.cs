using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class InvoiceAdditive : BaseEntity<int>
    {
        public int InvoiceId { get; set; }
        public int? InvoiceAdditiveTypeId { get; set; }
        public double? Amount { get; set; }
        public string ReserveJsonData { get; set; }
    }
}
