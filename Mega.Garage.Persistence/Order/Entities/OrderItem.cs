﻿using Mega.Garage.Persistence.Order.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class OrderItem : BaseEntity<int>
    {
        [Display(Name = "فاکتور")]
        public int InvoiceId { get; set; }
        [Display(Name = "محصول")]
        public long? ProductId { get; set; }
        [Display(Name = "قیمت پایه")]
        public double BasePrice { get; set; }
        [Display(Name = "واحد قیمت")]
        public double UnitPrice { get; set; }
        [Display(Name = "تخفیف")]
        public double Discount { get; set; }
        [Display(Name = "ردیف جزئیات")]
        public string OrderDetailNumber { get; set; }
        [Display(Name = "کد تخفیف")]
        public string DicountCode { get; set; }
        [Display(Name = "شناسه یکتا")]
        public Guid Guid { get; set; }
        [Display(Name = "تحویل داده شده")]
        public bool IsDelivered { get; set; }
        [Display(Name = "هزینه حمل")]
        public int? DeliveryPrice { get; set; }
        [Display(Name = "تاریخ تحویل")]
        public DateTime? DeliveryDate { get; set; }
        [Display(Name = "ارسال شده")]
        public bool IsShipped { get; set; }
        [Display(Name = "تاریخ ارسال")]
        public DateTime? ShipDate { get; set; }
        [Display(Name = "لغو شده")]
        public bool IsCancelled { get; set; }
        [Display(Name = "علت لغو")]
        public int? CancelReasonErrorID { get; set; }
        [Display(Name = "وضعیت کد")]
        public int OrderStatusId { get; set; }
        [Display(Name = "نمایندگی")]
        public int DealerId { get; set; }
        [Display(Name = "شناسه فرایند سفارش")]
        public int CurrentOrderProcessId { get; set; }
        public string ReserveJsonData { get; set; }
        public virtual Invoice Invoice { get; set; }
        public virtual Product Product { get; set; }
        public virtual OrderProcess OrderProcess { get; set; }
    }
}
