using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class Error : BaseEntity<int>
    {
        public string Title { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
