using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class OrderProcess : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }

}
