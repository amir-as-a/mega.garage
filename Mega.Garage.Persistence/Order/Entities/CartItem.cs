using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class CartItem : BaseEntity<int>
    {
        public int CartId { get; set; }
        public long? ProductId { get; set; }
        public int? ProductVariationId { get; set; }
        public DateTime? AddToBasketDateTime { get; set; }
        public string Guid { get; set; }
        public int OrderQty { get; set; }
        public double? UnitPrice { get; set; }
        public double LineTotal { get; set; }
        public double? DiscountAmount { get; set; }
        public double? DiscountPercent { get; set; }
        public decimal FinalPrice { get; set; }
        public bool? IsCanceled { get; set; }
        public string ProductTitle { get; set; }
        public string ReserveJsonData { get; set; }
    }
}
