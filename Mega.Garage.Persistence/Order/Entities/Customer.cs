﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class Customer : BaseEntity<int>
    {
        [DisplayName("شماره کاربری")]
        public int UserId { get; set; }
        [DisplayName("نام")]
        public string FirstName { get; set; }
        [DisplayName("نام خانوادگی")]
        public string LastName { get; set; }
        [DisplayName("موبایل")]
        public string Mobile { get; set; }
        [DisplayName("ایمیل")]
        public string Email { get; set; }
        [DisplayName("تلفن منزل")]
        public string HomePhone { get; set; }
        [DisplayName("مرد هست؟")]
        public bool IsMale { get; set; }
        [DisplayName("کد سامانه ثبت نامی")]
        public int ClientId { get; set; }
        [DisplayName("حقوقی هست؟")]
        public bool IsLegalUser { get; set; }
        [DisplayName("کد ملی")]
        public string NationalCode { get; set; }
        [DisplayName("آدرس")]
        public string ip { get; set; }
        [DisplayName("وضعیت تأیید")]
        public bool IsAccepted { get; set; }
        [DisplayName("اطلاعات رزرو")]
        public string ReserveJsonData { get; set; }
        [DisplayName("گروه خودرو کاربر")]
        public int CarGroupCode { get; set; }
        public virtual ICollection<Invoice> Invoices { get; } = new HashSet<Invoice>();
    }

}
