using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class CampaignProduct : BaseEntity<int>
    {
        public int? CampaignId { get; set; }
        public long? ProductId { get; set; }
        public int? MaxOrderQty { get; set; }
        public int? MinOrderQty { get; set; }
        public int? Qty { get; set; }
        public string ReserveJsonData { get; set; }
    }
}
