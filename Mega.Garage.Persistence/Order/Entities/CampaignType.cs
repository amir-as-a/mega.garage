﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class CampaignType : BaseEntity<int>
    {
        [Display(Name = "عنوان")]
        public string Title { get; set; }
        [Display(Name = "کد")]
        public string Code { get; set; }
    }
}
