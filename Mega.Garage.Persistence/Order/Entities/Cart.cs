using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class Cart : BaseEntity<int>
    {
        public string Ip { get; set; }
        public int? UserId { get; set; }
        public int? CustomerId { get; set; }
        public string CookieToken { get; set; }
        public string Guid { get; set; }
        public DateTime ValidEndDate { get; set; }
        public double TotalAmount { get; set; }
        public double? TotalDiscountAmount { get; set; }
        public bool? IsProcessed { get; set; }
        public bool? IsCanceled { get; set; }
        public int? CancelReasonErrorID { get; set; }
    }
}
