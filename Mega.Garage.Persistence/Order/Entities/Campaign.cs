﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class Campaign : BaseEntity<int>
    {
        [Display(Name = "عنوان")]
        public string Title { get; set; }
        [Display(Name = "کد")]
        public string Code { get; set; }
        [Display(Name = "نوع کمپین")]
        public int? CampaignTypeId { get; set; }
        [Display(Name = "تصویر کمپین")]
        public string CampaignImage { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "کمپین اینترنتی")]
        public bool IsOnsite { get; set; }
        [Display(Name = "تاریخ شروع")]
        public DateTime? StartDate { get; set; }
        [Display(Name = "تاریخ پایان")]
        public DateTime? EndDate { get; set; }
        [Display(Name = "تایید شده")]
        public bool IsConfirmed { get; set; }
        [Display(Name = "تاریخ تایید")]
        public DateTime? ConfirmDate { get; set; }
        [Display(Name = "تایید شده توسط")]
        public string ConfirmedBy { get; set; }
        [Display(Name = "فروش ویژه؟")]
        public bool IsSpecialSale { get; set; }
        [Display(Name = "لینک وب سایت")]
        public string WebsiteLink { get; set; }
        public string ReserveJsonData { get; set; }
        public virtual CampaignType CampaignType { get; set; }
        public bool IsSpecialGarage { get; set; }
    }
}
