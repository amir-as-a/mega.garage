using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
   
    public class InvoiceAdditiveType : BaseEntity<int>
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public bool? IsDeductive { get; set; }
        public string Scope { get; set; }
        public string ReserveJsonData { get; set; }
    }
}
