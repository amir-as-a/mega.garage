using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public partial class InvoiceProcess : BaseEntity<int>
    {
        public string Title { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
    }

}
