﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class Product : BaseEntity<long>
    {
        [DisplayName("شناسه")]
        public string Code { get; set; }
        [DisplayName("کاتالوگ")]
        public int? CatalogId { get; set; }
        [DisplayName("عنوان محصول")]
        public string CatalogTitle { get; set; }
        [DisplayName("عنوان مختصر محصول")]
        public string ShortTitle { get; set; }
        [DisplayName("توضیحات")]
        public string CatalogDescription { get; set; }
        [DisplayName("کد محصول")]
        public string CatalogCode { get; set; }
        [DisplayName("عکس محصول")]
        public string CatalogImage { get; set; }
        [DisplayName("نمایش فروشنده")]
        public bool ShowSeller { get; set; }
        [DisplayName("فروشنده")]
        public int? SellerId { get; set; }
        [DisplayName("نمایش گارانتی")]
        public bool ShowGuarantee { get; set; }
        [DisplayName("گارانتی")]
        public int? GuaranteeId { get; set; }
        [DisplayName("قیمت پایه")]
        public int? BasePrice { get; set; }
        [DisplayName("تخفیف")]
        public int? BaseDiscount { get; set; }
        [DisplayName("رنگ")]
        public int? ColorId { get; set; }
        [DisplayName("هزینه افزاینده رنگ")]
        public int? ColorAdditiveCost { get; set; }
        [DisplayName("عکس رنگی محصول")]
        public string ColorImage { get; set; }
        [DisplayName("سایز")]
        public int? SizeId { get; set; }
        [DisplayName("هزینه افزاینده سایز")]
        public int? SizeAdditiveCost { get; set; }
        [DisplayName("قیمت محصول")]
        public int? UnitPrice { get; set; }
        [DisplayName("تعداد موجودی")]
        public int? AvailableQty { get; set; }
        [DisplayName("تعداد بازدید")]
        public long? ViewCount { get; set; }
        [DisplayName("تعداد بازدید 2")]
        public long? VisitCount { get; set; }
        [DisplayName("تعداد کلیک")]
        public long? ClickCount { get; set; }
        [DisplayName("تعداد لایک")]
        public long? LikeCount { get; set; }
        [DisplayName("لینک به اشتراک گذاری")]
        public string ShareLinkAddress { get; set; }
        [DisplayName("اطلاعات اضافی")]
        public string ReserveJsonData { get; set; }
        [DisplayName("قابل مشاهده در سایت")]
        public bool IsOnSite { get; set; }
        [DisplayName("رزرو 1")]
        public bool? Reserve1 { get; set; }
        [DisplayName("رزرو 2")]
        public bool? Reserve2 { get; set; }

        public virtual ICollection<OrderItem> OrderItem { get; } = new HashSet<OrderItem>();
    }
}
