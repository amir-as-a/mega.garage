namespace Mega.Garage.Persistence.Order.Entities
{
    public class DiscountProduct : BaseEntity<int>
    {
        public int? DiscountId { get; set; }
        public int? CategoryId { get; set; }
        public int? ProductId { get; set; }
    }
}
