using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Order.Entities
{
    
    public class PaymentType : BaseEntity<int>
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
