﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Persistence
{
    public class BaseEntity<PK> where PK : struct
    {
        [DisplayName("شناسه")]
        public PK Id { get; set; }
        [DisplayName("تاریخ ثبت")]
        public DateTime CreatedDate { get; set; }
        [DisplayName("تاریخ ویرایش")]
        public DateTime? ModifiedDate { get; set; }
        [DisplayName("ایجاد شده توسط")]
        public int CreatedBy { get; set; }
        [DisplayName("ویرایش شده توسط")]
        public int ModifiedBy { get; set; }
        [DisplayName("وضعیت")]
        public bool IsActive { get; set; }
        [Timestamp]
        [DisplayName("برچسب اعتباری")]
        public byte[] RowVersion { get; set; }
    }
}
