using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.General.Entities
{
   
    public class Setting : BaseEntity<int>
    {
        public string Code { get; set; }
        public string KeyTitle { get; set; }
        public string ValueTitle { get; set; }
        public string Description { get; set; }
        public string ValueDataType { get; set; }
    }
}
