using System.Data.Entity;
using Mega.Garage.Persistence.General;
using Mega.Garage.Persistence.General.Configurations;
using Mega.Garage.Persistence.General.Entities;

namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public virtual DbSet<Setting> GeneralSettings { get; set; }

        public static void ConfigureGeneral(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SettingsConfiguration());
        }
    }
}
