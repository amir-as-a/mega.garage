﻿using Mega.Garage.Persistence.General.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.General.Configurations
{
    public class SettingsConfiguration : EntityTypeConfiguration<Setting>
    {
        public SettingsConfiguration()
        {
            ToTable("Settings", "general");

            Property(p => p.Code)
                .HasMaxLength(50);

            Property(p => p.KeyTitle)
                .HasMaxLength(150);

            Property(p => p.ValueTitle);

            Property(p => p.Description);

            Property(p => p.ValueDataType)
                .HasMaxLength(150);

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}

