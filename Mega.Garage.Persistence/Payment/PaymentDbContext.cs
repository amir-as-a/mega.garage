using System.Data.Entity;
using Mega.Garage.Persistence.Entities;
using Mega.Garage.Persistence.Payment;
using Mega.Garage.Persistence.Payment.Configurations;
using Mega.Garage.Persistence.Payment.Entities;

namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public virtual DbSet<IPG> IPGs { get; set; }
        public virtual DbSet<PaymentStatus> PaymentStatuss { get; set; }
        public virtual DbSet<UserPayment> UserPayments { get; set; }
        public virtual DbSet<UserPaymentHistory> UserPaymentHistorys { get; set; }

        public static void ConfigurePayment(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new IPGConfiguration());
            modelBuilder.Configurations.Add(new UserPaymentConfiguration());
            modelBuilder.Configurations.Add(new UserPaymentHistoryConfiguration());
            modelBuilder.Configurations.Add(new PaymentStatusConfiguration());
        }
    }
}
