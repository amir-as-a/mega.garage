﻿using Mega.Garage.Persistence.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Payment.Configurations
{
    public class PaymentStatusConfiguration : EntityTypeConfiguration<PaymentStatus>
    {
        public PaymentStatusConfiguration()
        {
            ToTable("PaymentStatus", "payment");
            Property(p => p.Code).HasMaxLength(10).IsRequired();
            Property(p => p.Title).HasMaxLength(50).IsRequired();
            
        }
    }
}
