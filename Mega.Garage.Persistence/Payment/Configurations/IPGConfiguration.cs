﻿using Mega.Garage.Persistence.Payment.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Payment.Configurations
{
    public class IPGConfiguration : EntityTypeConfiguration<IPG>
    {
        public IPGConfiguration()
        {
            ToTable("IPG", "payment");
            Property(p => p.CreatedDate).IsRequired();
        }
    }
}
