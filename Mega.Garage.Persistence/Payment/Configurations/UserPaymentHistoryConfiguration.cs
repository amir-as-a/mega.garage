﻿using Mega.Garage.Persistence.Payment.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Payment.Configurations
{
    public class UserPaymentHistoryConfiguration : EntityTypeConfiguration<UserPaymentHistory>
    {
        public UserPaymentHistoryConfiguration()
        {
            ToTable("UserPaymentHistory", "Payment");

        }
    }
}
