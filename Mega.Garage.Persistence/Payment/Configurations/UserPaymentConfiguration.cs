﻿using Mega.Garage.Persistence.Payment.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Payment.Configurations
{
    public class UserPaymentConfiguration : EntityTypeConfiguration<UserPayment>
    {
        public UserPaymentConfiguration()
        {
            ToTable("UserPayment", "payment");
            Property(p => p.SalesCode).HasMaxLength(100).IsRequired();
            Property(p => p.ReferalCode).HasMaxLength(100);
            Property(p => p.HasSplit).IsRequired();
            HasRequired(x => x.IPG).WithMany().HasForeignKey(x => x.IPGId);
            HasRequired(x => x.Invoice).WithMany().HasForeignKey(x => x.InvoiceId);
            HasRequired(x => x.PaymentStatus).WithMany().HasForeignKey(x => x.PaymentStatusId);
        }
    }
}
