﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Entities
{
    
    public class PaymentStatus : BaseEntity<int>
    {
        [Display(Name = "شناسه")]
        public string Code { get; set; }
        [Display(Name = "عنوان")]
        public string Title { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
    }
}
