﻿using Mega.Garage.Persistence.Entities;
using Mega.Garage.Persistence.Order.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Payment.Entities
{
    
    public class UserPayment : BaseEntity<int>
    {
        [Display(Name = "کد فاکتور ")]
        public string SalesCode { get; set; }
        [Display(Name = "کد ارجاع")]
        public string ReferalCode { get; set; }
        [Display(Name = "وضعیت بانکی")]
        public string ResponseCode { get; set; }
        [Display(Name = "شماره پیگیری سفارش بانکی")]
        public string SaleReferenceId { get; set; }
        [Display(Name = "شناسه درگاه بانکی")]
        public int IPGId { get; set; }
        [Display(Name = "شناسه فاکتور")]
        public int InvoiceId { get; set; }
        [Display(Name = "تاریخ پرداخت فاکتور")]
        public DateTime PaymentDate { get; set; }
        [Display(Name = "وضعیت پرداخت")]
        public int PaymentStatusId { get; set; }
        [Display(Name = "آیا تسهیم دارد")]
        public bool HasSplit { get; set; }

        public virtual IPG IPG { get; set; }
        public virtual Invoice Invoice { get; set; }
        public virtual PaymentStatus PaymentStatus { get; set; }
        public string GaragesCode { get; set; }
        public string GarageReferenceId { get; set; }
    }
}
