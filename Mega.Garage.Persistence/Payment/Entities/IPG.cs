﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Payment.Entities
{
  
    public class IPG : BaseEntity<int>
    {
        [Display(Name = "عنوان درگاه")]
        public string Name { get; set; }
        [Display(Name = "نام کاربری")]
        public string UserName { get; set; }
        [Display(Name = "رمز عبور")]
        public string Password { get; set; }
        [Display(Name = "شناسه ترمینال")]
        public string TerminalId { get; set; }
        [Display(Name = "آدرس بازگشتی")]
        public string CallBackURL { get; set; }
    }
}
