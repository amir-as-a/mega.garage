﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Payment.Entities
{
    
    public class UserPaymentHistory : BaseEntity<int>
    {
        [Display(Name = "شناسه کد پرداخت کاربر")]
        public int UserPaymentId { get; set; }
        [Display(Name = "از وضعیت")]
        public int FromStatus { get; set; }
        [Display(Name = "به وضعیت")]
        public int ToStatus { get; set; }
        [Display(Name = "توضیحانت")]
        public string Description { get; set; }
    }
}
