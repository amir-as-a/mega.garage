using System.Data.Entity;
using Mega.Garage.Persistence.Dealer;
using Mega.Garage.Persistence.Dealer.Configurations;
using Mega.Garage.Persistence.Dealer.Entities;
using Mega.Garage.Persistence.Dealer.Entities.Configurations;

namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public virtual DbSet<BookingCapacity> BookingCapacities { get; set; }
        public virtual DbSet<Booking> Bookings { get; set; }
        public virtual DbSet<Dealer.Entities.Dealer> Dealers { get; set; }
        public virtual DbSet<DealerLogin> DealerLogins { get; set; }
        public virtual DbSet<DealerInventory> DealerInventories { get; set; }
        public virtual DbSet<DealerService> DealerServices { get; set; }
        public virtual DbSet<DealerTransactionHistory> DealerTransactionHistories { get; set; }
        public virtual DbSet<TimeBox> TimeBoxes { get; set; }
        public virtual DbSet<TimeBoxType> TimeBoxTypes { get; set; }

        public static void ConfigureDealer(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BookingCapacityConfiguration());
            modelBuilder.Configurations.Add(new BookingConfiguration());
            modelBuilder.Configurations.Add(new DealerConfiguration());
            modelBuilder.Configurations.Add(new DealerLoginConfiguration());
            modelBuilder.Configurations.Add(new DealerInventoryConfiguration());
            modelBuilder.Configurations.Add(new DealerServiceConfiguration());
            modelBuilder.Configurations.Add(new DealerTransactionHistoryConfiguration());
            modelBuilder.Configurations.Add(new TimeBoxConfiguration());
            modelBuilder.Configurations.Add(new TimeBoxTypeConfiguration());
        }
    }
}
