﻿using Mega.Garage.Persistence.Dealer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Dealer.Configurations
{
    public class TimeBoxConfiguration : EntityTypeConfiguration<TimeBox>
    {
        public TimeBoxConfiguration()
        {
            ToTable("TimeBox", "dealer");

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.Code)
                .HasMaxLength(50);

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
