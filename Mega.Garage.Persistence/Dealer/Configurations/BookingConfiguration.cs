﻿using Mega.Garage.Persistence.Dealer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Dealer.Configurations
{
    public class BookingConfiguration : EntityTypeConfiguration<Booking>
    {
        public BookingConfiguration()
        {
            ToTable("Booking", "dealer");

            Property(p => p.Day)
                .HasColumnType("date");                

            Property(p => p.CustomerFirstName)
                .HasMaxLength(50);

            Property(p => p.CustomerLastName)
                .HasMaxLength(50);

            Property(p => p.QrCodeImage)
                .HasMaxLength(50);            

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
