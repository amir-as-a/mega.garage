﻿using Mega.Garage.Persistence.Dealer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Dealer.Configurations
{
    public class DealerInventoryConfiguration : EntityTypeConfiguration<DealerInventory>
    {
        public DealerInventoryConfiguration()
        {
            ToTable("DealerInventory", "dealer");

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
