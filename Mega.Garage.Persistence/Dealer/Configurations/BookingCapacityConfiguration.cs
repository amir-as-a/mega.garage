﻿using Mega.Garage.Persistence.Dealer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Dealer.Configurations
{
    public class BookingCapacityConfiguration : EntityTypeConfiguration<BookingCapacity>
    {
        public BookingCapacityConfiguration()
        {
            ToTable("BookingCapacity", "dealer");           

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
