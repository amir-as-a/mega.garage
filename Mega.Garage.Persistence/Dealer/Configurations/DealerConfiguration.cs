﻿using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Dealer.Entities.Configurations
{ 
    public class DealerConfiguration : EntityTypeConfiguration<Dealer>
    {
        public DealerConfiguration()
        {
            ToTable("Dealer", "dealer");

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.Code)
                .HasMaxLength(50);

            Property(p => p.OwnerFirstName)
                .HasMaxLength(150);

            Property(p => p.OwnerLastName)
                .HasMaxLength(150);

            Property(p => p.OwnerPhone)
                .HasMaxLength(20);

            Property(p => p.OwnerNationalCode)
                .HasMaxLength(10);

            Property(p => p.Province)
                .HasMaxLength(50);

            Property(p => p.City)
                .HasMaxLength(50);

            Property(p => p.Phone)
                .HasMaxLength(15);

            Property(p => p.Image)
               .HasMaxLength(300);

            Property(p => p.Icon)
              .HasMaxLength(300);

            Property(p => p.DocumentImage)
              .HasMaxLength(300);

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
