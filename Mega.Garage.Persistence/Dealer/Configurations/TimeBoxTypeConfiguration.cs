﻿using Mega.Garage.Persistence.Dealer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Dealer.Configurations
{
    public class TimeBoxTypeConfiguration : EntityTypeConfiguration<TimeBoxType>
    {
        public TimeBoxTypeConfiguration()
        {
            ToTable("TimeBoxType", "dealer");

            Property(p => p.Title)
                .HasMaxLength(150);

            Property(p => p.Code)
                .HasMaxLength(50);
        }
    }
}
