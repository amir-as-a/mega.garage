﻿using Mega.Garage.Persistence.Dealer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Dealer.Configurations
{
    public class DealerServiceConfiguration : EntityTypeConfiguration<DealerService>
    {
        public DealerServiceConfiguration()
        {
            ToTable("DealerService", "dealer");

            Property(p => p.OrderDetailNumber)
                .HasMaxLength(50);

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
