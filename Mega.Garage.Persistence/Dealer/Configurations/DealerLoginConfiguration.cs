﻿using Mega.Garage.Persistence.Dealer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Dealer.Configurations
{
    public class DealerLoginConfiguration : EntityTypeConfiguration<DealerLogin>
    {
        public DealerLoginConfiguration()
        {
            ToTable("DealerLogin", "dealer");

            Property(p => p.UserName)
                .HasMaxLength(50);

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
