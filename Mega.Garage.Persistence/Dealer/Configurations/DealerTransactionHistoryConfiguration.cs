﻿using Mega.Garage.Persistence.Dealer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Dealer.Configurations
{
    public class DealerTransactionHistoryConfiguration : EntityTypeConfiguration<DealerTransactionHistory>
    {
        public DealerTransactionHistoryConfiguration()
        {
            ToTable("DealerTransactionHistory", "dealer");

            Property(p => p.RowVersion)
                .IsFixedLength();
        }
    }
}
