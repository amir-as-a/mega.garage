using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Dealer.Entities
{
    
    public class DealerService : BaseEntity<int>
    {
        public int? OrderItemId { get; set; }

        public int? InvoiceId { get; set; }

        public string OrderDetailNumber { get; set; }

        public int? DealerId { get; set; }

        public DateTime? ProcessDate { get; set; }

        public string Description { get; set; }
    }
}