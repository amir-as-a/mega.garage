﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Dealer.Entities
{
  
    public class DealerTransactionHistory : BaseEntity<long>
    {
        [DisplayName("نمایندگی")]
        public int? DealerId { get; set; }
        [DisplayName("محصول")]
        public int? ProductId { get; set; }
        [DisplayName("تعداد تراکنش")]
        public int? TransactionAmount { get; set; }
        [DisplayName("تاریخ تراکنش")]
        public DateTime? TransactionDate { get; set; }
        [DisplayName("تعداد نهایی")]
        public int? Qty { get; set; }
        public virtual Dealer Dealer { get; set; }
    }
}
