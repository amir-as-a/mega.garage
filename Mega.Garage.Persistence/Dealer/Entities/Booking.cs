﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Dealer.Entities
{
    
    public class Booking : BaseEntity<int>
    {
        [DisplayName("نمایندگی")]
        public int? DealerId { get; set; }

        [DisplayName("بازه زمانی")]
        public int? TimeBoxId { get; set; }

        [DisplayName("تاریخ")]
        public DateTime? Day { get; set; }

        [DisplayName("مشخصات رزرو")]
        public string DayDescription { get; set; }

        [DisplayName("فاکتور")]
        public int? InvoiceId { get; set; }

        [DisplayName("مشتری")]
        public int? CustomerId { get; set; }

        [DisplayName("نام مشتری")]
        public string CustomerFirstName { get; set; }

        [DisplayName("نام خانوادگی مشتری")]
        public string CustomerLastName { get; set; }

        [DisplayName("تعداد")]
        public int? Qty { get; set; }

        [DisplayName("رزرو شده")]
        public bool IsReserved { get; set; }

        [DisplayName("تصویر QR کد")]
        public string QrCodeImage { get; set; }

        [DisplayName("مراجعه شده")]
        public bool HasAttendance { get; set; }

        [DisplayName("تاریخ مراجعه")]
        public DateTime? AttendanceDate { get; set; }

        public virtual Dealer Dealer { get; set; }

        public virtual TimeBox TimeBox { get; set; }
    }
}

