using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Dealer.Entities
{
    
    public class DealerLogin : BaseEntity<int>
    {
        public int? DealerId { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string Token { get; set; }

        public DateTime? TokenExpireDate { get; set; }

        public string RegId { get; set; }

        public bool? IsLocked { get; set; }

        public double? Longitude { get; set; }

        public double? Latitude { get; set; }

        public string Description { get; set; }
    }
}
