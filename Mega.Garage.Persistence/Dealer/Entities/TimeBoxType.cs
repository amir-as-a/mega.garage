﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Dealer.Entities
{
    
    public class TimeBoxType : BaseEntity<int>
    {
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("شناسه")]
        public string Code { get; set; }
        [DisplayName(" توضیحات")]
        public string Description { get; set; }

        public virtual ICollection<TimeBox> TimeBox { get; } = new HashSet<TimeBox>();
    }
}
