﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Dealer.Entities
{
   
    public class TimeBox : BaseEntity<int>
    {
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("نوع بازه زمانی")]
        public int? TimeBoxTypeId { get; set; }
        [DisplayName("شناسه")]
        public string Code { get; set; }
        [DisplayName(" توضیحات")]
        public string Description { get; set; }
        public virtual TimeBoxType TimeBoxType { get; set; }
    }
}
