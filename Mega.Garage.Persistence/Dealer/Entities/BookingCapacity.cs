﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Dealer.Entities
{
   
    public class BookingCapacity : BaseEntity<int>
    {
        [DisplayName("نمایندگی")]
        public int? DealerId { get; set; }
        [DisplayName("بازه زمانی")]
        public int? TimeBoxId { get; set; }
        [DisplayName("حداکثر ظرفیت")]
        public int? MaxCapacity { get; set; }
        [DisplayName(" نمایش در سایت")]
        public bool IsOnSite { get; set; }

        public virtual Dealer Dealer { get; set; }

        public virtual TimeBox TimeBox { get; set; }
    }
}
