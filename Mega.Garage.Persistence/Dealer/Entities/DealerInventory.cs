﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Dealer.Entities
{
    
    public class DealerInventory : BaseEntity<int>
    {
        [DisplayName("نمایندگی")]
        public int? DealerId { get; set; }
        [DisplayName("محصول")]
        public int? ProductId { get; set; }
        [DisplayName("تعداد")]
        public int? Qty { get; set; }

        public virtual Dealer Dealer { get; set; }
    }
}
