﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Dealer.Entities
{
    
    public class Dealer : BaseEntity<int>
    {
        [DisplayName("نام نمایندگی")]
        public string Title { get; set; }
        [DisplayName("کد نمایندگی")]
        public string Code { get; set; }
        [DisplayName("نام مسئول")]
        public string OwnerFirstName { get; set; }
        [DisplayName("نام خانوادگی مسئول")]
        public string OwnerLastName { get; set; }
        [DisplayName("تلفن مسئول")]
        public string OwnerPhone { get; set; }
        [DisplayName("کدملی مسئول")]
        public string OwnerNationalCode { get; set; }
        [DisplayName("استان")]
        public string Province { get; set; }
        [DisplayName("شهر")]
        public string City { get; set; }
        [DisplayName("تلفن نمایندگی")]
        public string Phone { get; set; }
        [DisplayName("آدرس")]
        public string Address { get; set; }
        [DisplayName("طول جغرافیایی")]
        public double? Longitude { get; set; }
        [DisplayName("عرض جغرافیایی")]
        public double? Latitude { get; set; }
        [DisplayName("تصویر")]
        public string Image { get; set; }
        [DisplayName("لوگو")]
        public string Icon { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [DisplayName("نمایش در سایت")]
        public bool IsOnSite { get; set; }
        [DisplayName("تصویر مجوز نمایندگی")]
        public string DocumentImage { get; set; }

        [DisplayName("شماره شبا")]
        public string ShebaId { get; set; }

        [DisplayName("شماره شناسه بانکی")]
        public string BankAccountNumber { get; set; }

        [DisplayName("کد ای پی جی")]
        public string IPGBankId { get; set; }
    }
}
