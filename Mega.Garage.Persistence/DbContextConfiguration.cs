﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public DataContext(string ConnectionStringName) : base($"name={ConnectionStringName}")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /// We added this line to avoid comparsion between database and model
            /// As default EF UOW checks for model and tables
            Database.SetInitializer<GarageDbContext>(null);

            ///Catalog Module Table Configurations
            ConfigureCatalog(ref modelBuilder);

            ///Dealer Module Table Configurations
            DataContext.ConfigureDealer(ref modelBuilder);

            ///Order Module Table Configurations
            DataContext.ConfigureOrder(ref modelBuilder);

            ///Location Module Table Configurations
            DataContext.ConfigureLocation(ref modelBuilder);

            ///File Upload Module Table Configurations
            DataContext.ConfigureFileUpload(ref modelBuilder);

            ///On Site Module Table Configurations
            DataContext.ConfigureOnSite(ref modelBuilder);

            ///Account Module Table Configurations
            DataContext.ConfigureAccount(ref modelBuilder);

            ///Payment Module Table Configurations
            DataContext.ConfigurePayment(ref modelBuilder);

            ///Polling Module Table Configurations
            DataContext.ConfigurePolling(ref modelBuilder);

            ///General Module Table Configurations
            DataContext.ConfigureGeneral(ref modelBuilder);

            ///Fitup Module Table Configurations
            DataContext.ConfigureFitup(ref modelBuilder);

            base.OnModelCreating(modelBuilder);
        }
    }
}
