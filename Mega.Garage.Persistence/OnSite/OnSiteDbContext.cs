using Mega.Garage.Persistence.OnSite.Configurations;
using Mega.Garage.Persistence.OnSite.Entities;
using System.Data.Entity;

namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public virtual DbSet<LandingPage> LandingPages { get; set; }
        public virtual DbSet<ProductGroup> ProductGroups { get; set; }
        public virtual DbSet<ProductGroupProduct> ProductGroupProducts { get; set; }

        public static void ConfigureOnSite(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new LandingPageConfiguration());
            modelBuilder.Configurations.Add(new ProductGroupConfiguration());
            modelBuilder.Configurations.Add(new ProductGroupProductConfiguration());
        }
    }
}
