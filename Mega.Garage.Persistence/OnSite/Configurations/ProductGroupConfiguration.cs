﻿using Mega.Garage.Persistence.OnSite.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.OnSite.Configurations
{
    public class ProductGroupConfiguration : EntityTypeConfiguration<ProductGroup>
    {
        public ProductGroupConfiguration()
        {
            ToTable("ProductGroup", "onsite");

            Property(e => e.RowVersion)
                .IsFixedLength();

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.EnglishTitle)
                .HasMaxLength(250);

            Property(p => p.ThumbImagePath)
                .HasMaxLength(250);

            Property(p => p.ImagePath)
                .HasMaxLength(250);
        }
    }
}
