﻿using Mega.Garage.Persistence.OnSite.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.OnSite.Configurations
{
    public class ProductGroupProductConfiguration : EntityTypeConfiguration<ProductGroupProduct>
    {
        public ProductGroupProductConfiguration()
        {
            ToTable("ProductGroupProducts", "onsite");

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
