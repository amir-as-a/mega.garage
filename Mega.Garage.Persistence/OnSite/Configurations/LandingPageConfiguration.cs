﻿using Mega.Garage.Persistence.OnSite.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.OnSite.Configurations
{
    public class LandingPageConfiguration : EntityTypeConfiguration<LandingPage>
    {
        public LandingPageConfiguration()
        {
            ToTable("LandingPage", "onsite");

            Property(e => e.RowVersion)
                .IsFixedLength();

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.EnglishTitle)
                .HasMaxLength(250);
        }
    }
}
