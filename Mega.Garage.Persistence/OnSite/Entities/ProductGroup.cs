﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.OnSite.Entities
{
    
    public class ProductGroup : BaseEntity<int>
    {
        [DisplayName("عنوان گروه محصول")]
        public string Title { get; set; }
        [DisplayName("عنوان انگلیسی")]
        public string EnglishTitle { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [DisplayName("عکس Thumbnail")]
        public string ThumbImagePath { get; set; }
        [DisplayName("عکس")]
        public string ImagePath { get; set; }
        [DisplayName("صفحه لندینگ")]
        public int? LandingPageId { get; set; }
        [DisplayName("نمایش کامل یک طبقه")]
        public bool ShowFullCategoryProducts { get; set; }
        [DisplayName("طبقه انتخابی")]
        public int? SelectedCategoryId { get; set; }
    }
}
