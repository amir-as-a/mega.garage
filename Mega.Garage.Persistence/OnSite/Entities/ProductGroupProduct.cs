﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.OnSite.Entities
{
    
    public class ProductGroupProduct : BaseEntity<int>
    {
        [DisplayName("گروه محصول")]
        public int? ProductGroupId { get; set; }
        [DisplayName("محصول")]
        public long ProductId { get; set; }
        [DisplayName("نمایش محصول زمان اتمام")]
        public bool ShowWhenQtyFinished { get; set; }
    }
}
