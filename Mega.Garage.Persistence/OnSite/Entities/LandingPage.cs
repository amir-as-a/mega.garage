﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.OnSite.Entities
{
    
    public class LandingPage : BaseEntity<int>
    {
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("عنوان انگلیسی")]
        public string EnglishTitle { get; set; }
        [DisplayName("آدرس Url")]
        public string Url { get; set; }
        [DisplayName("کلمات کلیدی")]
        public string Keywords { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
    }
}
