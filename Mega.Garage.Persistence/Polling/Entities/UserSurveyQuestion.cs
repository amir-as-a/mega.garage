using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Polling.Entities
{
    
    public class UserSurveyQuestion : BaseEntity<long>
    {
        public int ResponseValue { get; set; }
        public bool IsOutlier { get; set; }

        public long? UserSurveyId { get; set; }
        public int QuestionId { get; set; }

        public virtual UserSurvey UserSurvey { get; set; }
        public virtual Question Question { get; set; }
    }
}
