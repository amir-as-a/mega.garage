﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Polling.Entities
{
    
    public class Survey : BaseEntity<long>
    {
        [Display(Name = "عنوان نظرسنجی")]
        public string Title { get; set; }
        [Display(Name = "کد")]
        public string Code { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }
        [Display(Name = "نوع سوال")]
        public int QuestionType { get; set; }
        [Display(Name = "تاریخ شروع نظرسنجی")]
        public DateTime StartDate { get; set; }
        [Display(Name = "تاریخ پایان نظرسنجی")]
        public DateTime EndDate { get; set; }
        [Display(Name = "آدرس اینترنتی")]
        public string LinkUrl { get; set; }
        [Display(Name = "کد نرم افزار مشتری")]
        public int ClientId { get; set; }
        [Display(Name = "نوع نظرسنجی")]
        public int SurveyTypeId { get; set; }

        public virtual SurveyType SurveyType { get; set; }
        public virtual ICollection<Question> Questions { get; set; } = new HashSet<Question>();
        public virtual ICollection<UserSurvey> UserSurveys { get; set; } = new HashSet<UserSurvey>();
    }
}
