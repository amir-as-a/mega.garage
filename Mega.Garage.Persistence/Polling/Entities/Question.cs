﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Polling.Entities
{
    public class Question : BaseEntity<int>
    {
        [Display(Name = "عنوان سوال")]
        public string Title { get; set; }
        [Display(Name = "آیا پاسخ منفی درست است؟")]
        public bool IsNegativeAnswerBetter { get; set; }
        [Display(Name = "دسته بندی")]
        public long? SurveyId { get; set; }

        public virtual Survey Survey { get; set; }
        public virtual ICollection<UserSurveyQuestion> UserSurveyQuestions { get; set; } = new HashSet<UserSurveyQuestion>();
    }
}
