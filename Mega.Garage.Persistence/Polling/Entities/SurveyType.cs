﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Polling.Entities
{
    
    public class SurveyType : BaseEntity<int>
    {
        [Display(Name = "عنوان")]
        public string Title { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }

        public virtual ICollection<Survey> Surveys { get; set; } = new HashSet<Survey>();
    }
}
