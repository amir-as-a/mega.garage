﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Polling.Entities
{
    
    public class UserSurvey : BaseEntity<long>
    {
        [Display(Name = "شناسه کاربر")]
        public long UserId { get; set; }
        [Display(Name = "شمار فاکتور")]
        public string OrderNumber { get; set; }
        [Display(Name = "کد یکتای نمایندگی")]
        public long DealerId { get; set; }
        [Display(Name = "تاریخ نمایش نظرسنجی به مشتری")]
        public DateTime? ShowDate { get; set; }
        [Display(Name = "تاریخ اتمام نظرسنجی توسط مشتری")]
        public DateTime? CompleteDate { get; set; }
        [Display(Name = "متن توضیحی مشتری")]
        public string Comment { get; set; }
        public long SurveyId { get; set; }

        public virtual Survey Survey { get; set; }
        public virtual ICollection<UserSurveyQuestion> UserSurveyQuestions { get; set; } = new HashSet<UserSurveyQuestion>();
    }
}
