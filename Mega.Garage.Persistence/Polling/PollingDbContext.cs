using System.Data.Entity;
using Mega.Garage.Persistence.Polling.Configurations;
using Mega.Garage.Persistence.Polling;
using Mega.Garage.Persistence.Polling.Entities;

namespace Mega.Garage.Persistence
{

    public partial class DataContext : DbContext
    {
        public virtual DbSet<SurveyType> SurveyTypes { get; set; }
        public virtual DbSet<Survey> Surveys { get; set; }
        public virtual DbSet<Question> Questions { get; set; }
        public virtual DbSet<UserSurvey> UserSurveys { get; set; }
        public virtual DbSet<UserSurveyQuestion> UserSurveyQuestions { get; set; }

        public static void ConfigurePolling(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new SurveyTypeConfiguration());
            modelBuilder.Configurations.Add(new SurveyConfiguration());
            modelBuilder.Configurations.Add(new QuestionConfiguration());
            modelBuilder.Configurations.Add(new UserSurveyConfiguration());
            modelBuilder.Configurations.Add(new UserSurveyQuestionConfiguration());
        }
    }
}
