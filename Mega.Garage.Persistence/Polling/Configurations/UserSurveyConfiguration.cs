﻿using Mega.Garage.Persistence.Polling.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Polling.Configurations
{
    public class UserSurveyConfiguration : EntityTypeConfiguration<UserSurvey>
    {
        public UserSurveyConfiguration()
        {
            ToTable("UserSurvey", "polling");
            Property(p => p.OrderNumber).HasMaxLength(250);
            HasRequired(x => x.Survey).WithMany(x => x.UserSurveys).HasForeignKey(x => x.SurveyId);
        }
    }
}
