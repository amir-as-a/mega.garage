﻿using Mega.Garage.Persistence.Polling.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Polling.Configurations
{
    public class SurveyConfiguration : EntityTypeConfiguration<Survey>
    {
        public SurveyConfiguration()
        {
            ToTable("Survey", "polling");
            Property(p => p.Title).HasMaxLength(150);
            Property(p => p.Code).HasMaxLength(50);
            HasRequired(x => x.SurveyType).WithMany(x => x.Surveys).HasForeignKey(x => x.SurveyTypeId);
        }
    }
}
