﻿using Mega.Garage.Persistence.Polling.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Polling.Configurations
{
    public class SurveyTypeConfiguration : EntityTypeConfiguration<SurveyType>
    {
        public SurveyTypeConfiguration()
        {
            ToTable("SurveyType", "polling");
            Property(p => p.Title).HasMaxLength(150);
        }
    }
}
