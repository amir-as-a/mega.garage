﻿using Mega.Garage.Persistence.Polling.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Polling.Configurations
{
    public class QuestionConfiguration : EntityTypeConfiguration<Question>
    {
        public QuestionConfiguration()
        {
            ToTable("Question", "polling");
            Property(p => p.Title).HasMaxLength(250);
            HasRequired(x => x.Survey).WithMany(x => x.Questions).HasForeignKey(x => x.SurveyId);
        }
    }
}
