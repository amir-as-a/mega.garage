﻿using Mega.Garage.Persistence.Polling.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Polling.Configurations
{
    public class UserSurveyQuestionConfiguration : EntityTypeConfiguration<UserSurveyQuestion>
    {
        public UserSurveyQuestionConfiguration()
        {
            ToTable("UserSurveyQuestion", "polling");
            HasRequired(x => x.UserSurvey).WithMany(x => x.UserSurveyQuestions).HasForeignKey(x => x.UserSurveyId);
            HasRequired(x => x.Question).WithMany(x => x.UserSurveyQuestions).HasForeignKey(x => x.QuestionId);
        }
    }
}
