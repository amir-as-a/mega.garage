﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{
    public partial class Catalog : BaseEntity<int>
    {
        public static object Services { get; set; }
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("عنوان انگلیسی")]
        public string EnglishTitle { get; set; }
        [DisplayName("عنوان مختصر ")]
        public string ShortTitle { get; set; }
        [DisplayName("شرکت")]
        public int? CompanyId { get; set; }
        [DisplayName("طبقه بندی")]
        public int? CategoryId { get; set; }
        [DisplayName("برند")]
        public int? BrandId { get; set; }
        [DisplayName("شناسه پیکره بندی")]
        public string ConfigCode { get; set; }
        [DisplayName("شناسه طبقه بندی کلی")]
        public string FullConfigCode { get; set; }
        [DisplayName("نوع کاتالوگ")]
        public int? CatalogTypeId { get; set; }
        [DisplayName("نوع تحویل محصول")]
        public int? CatalogShippingTypeId { get; set; }
        [DisplayName("تعداد بازدید ")]
        public long? CatalogVisitCount { get; set; }
        [DisplayName("واحد اندازه گیری")]
        public string CatalogWeightUnit { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [DisplayName("تصویر کوچک")]
        public string ThumbImagePath { get; set; }
        [DisplayName("تصویر")]
        public string ImagePath { get; set; }
        [DisplayName("امتیاز نمایشی")]
        public int? WebsiteBoostScore { get; set; }
        [DisplayName("ردیف")]
        public int? CategorySortId { get; set; }
        [DisplayName("فعال در فروش")]
        public bool IsOnSale { get; set; }
        [DisplayName("وضعیت کاتالوگ")]
        public int? CatalogStatusId { get; set; }
        [DisplayName("شناسه یکتا")]
        public string Guid { get; set; }
        [DisplayName("قیمت پایه")]
        public long? LastBasePrice { get; set; }
        [DisplayName("تاریخ آخرین قیمت")]
        public DateTime? LastBasePriceModified { get; set; }
        [DisplayName("دارای گارانتی")]
        public bool HasGuarantee { get; set; }
        [DisplayName("نوع گارانتی")]
        public int? GuaranteeTypeId { get; set; }
        [DisplayName("امتیاز کاربران")]
        public int? AverageUserRate { get; set; }
        [DisplayName("‍‍‍طبقه")]
        public virtual Category Category { get; set; }
        [DisplayName("‍‍‍برند")]
        public virtual Brand Brand { get; set; }
        [DisplayName("‍‍‍نوع کاتالوگ")]
        public virtual CatalogType CatalogType { get; set; }
        [DisplayName("‍‍‍وضعیت کاتالوگ")]
        public virtual CatalogStatus CatalogStatus { get; set; }
        public virtual ICollection<CatalogCategory> CatalogCategories { get; } = new HashSet<CatalogCategory>();
    }
}