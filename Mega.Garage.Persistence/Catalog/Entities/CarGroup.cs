using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{
   
    public class CarGroup : BaseEntity<int>
    {
        public int Code { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string ReservedJsonData { get; set; }
    }
}
