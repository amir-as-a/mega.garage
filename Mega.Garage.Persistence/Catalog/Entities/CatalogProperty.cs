﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{
   
    public partial class CatalogProperty : BaseEntity<int>
    {
        [DisplayName("کاتالوگ")]
        public int? CatalogId { get; set; }
        [DisplayName("مشخصه کاتالوگ")]
        public int? CatalogPropertyTypeId { get; set; }
        [DisplayName("نوع پیش فرض")]
        public bool IsDefaultVariation { get; set; }
        [DisplayName("نوع مشخصه")]
        public int? PropTypeId { get; set; }
        [DisplayName("مقدار مشحصه")]
        public int? PropValueId { get; set; }
        [DisplayName("شامل هزینه اضافه")]
        public bool HasAdditiveCost { get; set; }
        [DisplayName("نوع هزینه اضافه")]
        public int? AdditiveCostType { get; set; }
        [DisplayName("مقدار هزینه اضافه")]
        public float? AdditiveCostAmount { get; set; }
        [DisplayName("شناسه یکتا")]
        public string Guid { get; set; }
        public virtual Catalog Catalog { get; set; }
    }
}
