﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{
    
    public partial class ProductAttributeKey : BaseEntity<int>
    {
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("شناسه")]
        public string Code { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [DisplayName("طبقه اصلی")]
        public int? ParentProductAttributeKeyId { get; set; }
        [DisplayName("دارای مقدار")]
        public bool HasPropertyValue { get; set; }
        public virtual ICollection<ProductAttributeValue> ProductAttributeValues { get; } = new HashSet<ProductAttributeValue>();
    }
}
