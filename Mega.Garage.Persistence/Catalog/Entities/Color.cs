﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{
    
    public partial class Color : BaseEntity<int>
    {
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("عنوان انگلیسی ")]
        public string EnglishTitle { get; set; }
        [DisplayName("کدهگزا دسیمال")]
        public string ColorHexCode { get; set; }
    }
}
