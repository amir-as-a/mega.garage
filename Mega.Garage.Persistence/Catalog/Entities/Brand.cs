﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{
    
    public partial class Brand : BaseEntity<int>
    {
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("عنوان انگلیسی")]
        public string EnglishTitle { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [DisplayName("لوگو")]
        public string LogoImagePath { get; set; }
        [DisplayName("تصویر")]
        public string WideImagePath { get; set; }
        [DisplayName("کشور")]
        public string Country { get; set; }
    }
}
