﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{
   
    public partial class ProductAttributeValue : BaseEntity<int>
    {
        [DisplayName("مقدار")]
        public string Value { get; set; }
        [DisplayName("شناسه")]
        public string Code { get; set; }
        [DisplayName("مشخصه")]
        public int ProductAttributeKeyId { get; set; }
        [DisplayName("مقدارهگزادسیمال")]
        public string HexValue { get; set; }
        [DisplayName("مقدار ۱")]
        public string AdditionalField1 { get; set; }
        [DisplayName("مقدار ۲")]
        public string AdditionalField2 { get; set; }
        [DisplayName("مقدار ۳")]
        public string AdditionalField3 { get; set; }
        public virtual ProductAttributeKey ProductAttributeKey { get; set; }
    }
}
