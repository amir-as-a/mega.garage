﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{
    
    public partial class CatalogCategory : BaseEntity<int>
    {

        [DisplayName("طبقه بندی")]
        public int? CategoryId { get; set; }
        [DisplayName("کاتالوگ")]
        public int? CatalogId { get; set; }
        [DisplayName("ردیف")]
        public int? SortNumber { get; set; }
        [DisplayName("مقدار رزرو شده")]
        public string ReserveJsonData { get; set; }
        public virtual Category Category { get; set; }
        public virtual Catalog Catalog { get; set; }
    }
}
