﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{

    
    public partial class Category : BaseEntity<int>
    {
        [DisplayName("عنوان")]
        public string Title { get; set; }
        [DisplayName("عنوان انگلیسی")]
        public string EnglishTitle { get; set; }
        [DisplayName("توضیحات")]
        public string Description { get; set; }
        [DisplayName("طبقه اصلی")]
        public int? ParentCategoryId { get; set; }
        [DisplayName("شناسه طبقه")]
        public string CategoryCode { get; set; }
        [DisplayName("نوع طبقه")]
        public int? CategoryTypeId { get; set; }
        [DisplayName("تصویر")]
        public string WideImagePath { get; set; }
        [DisplayName("تصویر کوچک")]
        public string ThumbImagePath { get; set; }
        [DisplayName("ردیف")]
        public int? SortNumber { get; set; }
        [DisplayName("قابلیت تخصیص محصول")]
        public bool CanAssignProduct { get; set; }
        [DisplayName("‍‍‍پیوند")]
        public string AddressLink { get; set; }
        [DisplayName("‍‍‍شماره سطح")]
        public int? LevelNumber { get; set; }
        [DisplayName("‍‍‍قرارگیری در منوی اصلی")]
        public bool IsMainMenu { get; set; }
        public virtual ICollection<CatalogCategory> CatalogCategories { get; } = new HashSet<CatalogCategory>();
        [DisplayName("‍‍‍طبقات")]
        public virtual ICollection<Category> Categories { get; } = new HashSet<Category>();
        [DisplayName("‍‍‍طبقه پدر")]
        public virtual Category ParentCategory { get; set; }
    }
}

