using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Mega.Garage.Persistence.Catalog.Entities
{
    
    public partial class Company
    {
        public int Id { get; set; }

        [StringLength(150)]
        public string Title { get; set; }

        [StringLength(150)]
        public string EnglishTitle { get; set; }

        public string Description { get; set; }

        [StringLength(650)]
        public string LogoImagePath { get; set; }

        [StringLength(650)]
        public string WideImagePath { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        public bool? IsActive { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? CreatedBy { get; set; }

        public int? ModifiedBy { get; set; }

        [Column(TypeName = "timestamp")]
        [MaxLength(8)]
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
}
