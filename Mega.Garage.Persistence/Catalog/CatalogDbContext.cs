using System.Data.Entity;
using Mega.Garage.Persistence.Catalog;
using Mega.Garage.Persistence.Catalog.Configurations;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Persistence.Catalog.Entities.Configurations;

namespace Mega.Garage.Persistence
{
    public partial class DataContext : DbContext
    {
        public virtual DbSet<Brand> Brands { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CatalogCategory> CategoryCatalogs { get; set; }
        public virtual DbSet<Color> Colors { get; set; }
        public virtual DbSet<Catalog.Entities.Catalog> Catalogs { get; set; }
        public virtual DbSet<ProductAttributeKey> ProductAttributeKeys { get; set; }
        public virtual DbSet<ProductAttributeValue> ProductAttributeValues { get; set; }
        public virtual DbSet<CatalogProperty> CatalogProperties { get; set; }
        public virtual DbSet<CatalogStatus> CatalogStatus { get; set; }
        public virtual DbSet<CatalogType> CatalogTypes { get; set; }
        public virtual DbSet<CarGroup> CarGroups { get; set; }

        public static void ConfigureCatalog(ref DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new BrandConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new CatalogCategoryConfiguration());
            modelBuilder.Configurations.Add(new ColorConfiguration());
            modelBuilder.Configurations.Add(new ProductAttributeKeyConfiguration());
            modelBuilder.Configurations.Add(new ProductAttributeValueConfiguration());
            modelBuilder.Configurations.Add(new CatalogConfiguration());
            modelBuilder.Configurations.Add(new CatalogPropertyConfiguration());
            modelBuilder.Configurations.Add(new CatalogStatusConfiguration());
            modelBuilder.Configurations.Add(new CatalogTypeConfiguration());
            modelBuilder.Configurations.Add(new CarGroupConfiguration());
        }
    }
}
