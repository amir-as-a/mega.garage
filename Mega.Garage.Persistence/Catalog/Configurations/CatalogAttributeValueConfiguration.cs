﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class ProductAttributeValueConfiguration : EntityTypeConfiguration<ProductAttributeValue>
    {
        public ProductAttributeValueConfiguration()
        {
            ToTable("ProductAttributeValue", "catalog");

            Property(p => p.Code)
                .HasMaxLength(50);

            Property(p => p.HexValue)
                .HasMaxLength(50);

            Property(p => p.AdditionalField3)
                .HasMaxLength(50);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
