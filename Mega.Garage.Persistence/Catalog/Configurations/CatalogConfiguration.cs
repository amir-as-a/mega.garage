﻿using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Entities.Configurations
{
    public class CatalogConfiguration : EntityTypeConfiguration<Catalog>
    {
        public CatalogConfiguration()
        {
            ToTable("Catalog", "catalog");

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.EnglishTitle)
                .HasMaxLength(250);

            Property(p => p.ShortTitle)
                .HasMaxLength(255);

            Property(p => p.ConfigCode)
                .HasMaxLength(15);

            Property(p => p.FullConfigCode)
                .HasMaxLength(15);

            Property(p => p.CatalogWeightUnit)
                .HasMaxLength(50);

            Property(p => p.ThumbImagePath)
                .HasMaxLength(250);

            Property(p => p.ImagePath)
                .HasMaxLength(250);

            Property(p => p.Guid)
                .HasMaxLength(50);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
