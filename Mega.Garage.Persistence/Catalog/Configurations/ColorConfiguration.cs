﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class ColorConfiguration : EntityTypeConfiguration<Color>
    {
        public ColorConfiguration()
        {
            ToTable("Color", "catalog");

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.EnglishTitle)
                .HasMaxLength(250);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
