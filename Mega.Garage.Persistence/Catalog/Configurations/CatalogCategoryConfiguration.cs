﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class CatalogCategoryConfiguration : EntityTypeConfiguration<CatalogCategory>
    {
        public CatalogCategoryConfiguration()
        {
            ToTable("CatalogCategory", "catalog");

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
