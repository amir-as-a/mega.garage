﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class CatalogPropertyConfiguration : EntityTypeConfiguration<CatalogProperty>
    {
        public CatalogPropertyConfiguration()
        {
            ToTable("CatalogProperty", "catalog");

            Property(p => p.Guid)
                .HasMaxLength(50);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
