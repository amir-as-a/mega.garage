﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class CatalogTypeConfiguration : EntityTypeConfiguration<CatalogType>
    {
        public CatalogTypeConfiguration()
        {
            ToTable("CatalogType", "catalog");

            Property(p => p.Title)
                .HasMaxLength(50);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
