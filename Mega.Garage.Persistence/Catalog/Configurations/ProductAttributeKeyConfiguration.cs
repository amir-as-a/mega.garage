﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class ProductAttributeKeyConfiguration : EntityTypeConfiguration<ProductAttributeKey>
    {
        public ProductAttributeKeyConfiguration()
        {
            ToTable("ProductAttributeKey", "catalog");

            HasMany(e => e.ProductAttributeValues)
                .WithRequired(e => e.ProductAttributeKey)
                .WillCascadeOnDelete(false);

            Property(p => p.Code)
                .HasMaxLength(150);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
