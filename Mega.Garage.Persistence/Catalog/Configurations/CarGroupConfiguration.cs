﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class CarGroupConfiguration : EntityTypeConfiguration<CarGroup>
    {
        public CarGroupConfiguration()
        {
            ToTable("CarGroup", "catalog");

            Property(p => p.Title)
                .HasMaxLength(50);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
