﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class CatalogStatusConfiguration : EntityTypeConfiguration<CatalogStatus>
    {
        public CatalogStatusConfiguration()
        {
            ToTable("CatalogStatus", "catalog");

            Property(p => p.Title)
                .HasMaxLength(150);

            Property(p => p.StatusCode)
                .HasMaxLength(50);

            Property(e => e.RowVersion)
                .IsFixedLength();
        }
    }
}
