﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            ToTable("Category", "catalog");

            Property(e => e.RowVersion)
                .IsFixedLength();

            HasMany(e => e.Categories)
                .WithOptional(e => e.ParentCategory)
                .HasForeignKey(e => e.ParentCategoryId);

            Property(p => p.Title)
                .HasMaxLength(250);

            Property(p => p.EnglishTitle)
                .HasMaxLength(250);

            Property(p => p.CategoryCode)
                .HasMaxLength(15);

            Property(p => p.WideImagePath)
                .HasMaxLength(650);

            Property(p => p.ThumbImagePath)
                .HasMaxLength(650);

            Property(p => p.AddressLink)
                .HasMaxLength(50);
        }
    }
}
