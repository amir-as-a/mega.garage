﻿using Mega.Garage.Persistence.Catalog.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Mega.Garage.Persistence.Catalog.Configurations
{
    public class BrandConfiguration : EntityTypeConfiguration<Brand>
    {
        public BrandConfiguration()
        {
            ToTable("Brand", "catalog");

            Property(e => e.RowVersion)
                .IsFixedLength();

            Property(p => p.Title)
                .HasMaxLength(150);

            Property(p => p.LogoImagePath)
                .HasMaxLength(650);

            Property(p => p.WideImagePath)
                .HasMaxLength(650);

            Property(p => p.EnglishTitle)
                .HasMaxLength(150);

            Property(p => p.Country)
                .HasMaxLength(50);
        }
    }
}
