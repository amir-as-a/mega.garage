﻿using System.Web;
using System.Web.Optimization;

namespace Mega.Garage.Client
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/theme/js/jquery.min.js"
                      )
              );
            bundles.Add(new ScriptBundle("~/bundles/jquery-migrate").Include(
                     "~/theme/js/jquery-migrate-3.0.1.min.js"
                     )
             );
            bundles.Add(new ScriptBundle("~/bundles/jSc").Include(
                        "~/theme/js/popper.min.js",
                        "~/theme/js/bootstrap.min.js",
                        "~/theme/js/jquery.easing.1.3.js",
                        "~/theme/js/jquery.waypoints.min.js",
                        "~/theme/js/jquery.stellar.min.js",
                        "~/theme/js/owl.carousel.min.js",
                        "~/theme/js/jquery.magnific-popup.min.js",
                        "~/theme/js/aos.js",
                        "~/theme/js/jquery.animateNumber.min.js",
                        "~/theme/js/bootstrap-datepicker.js",
                        "~/theme/js/scrollax.min.js",
                        "~/theme/js/main.js",
                        "~/scripts/number-only.js"
                        )
                );
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/theme/css/open-iconic-bootstrap.min.css",
                      "~/theme/css/animate.css",
                      "~/theme/css/owl.carousel.min.css",
                      "~/theme/css/owl.theme.default.min.css",
                      "~/theme/css/magnific-popup.css",
                      "~/theme/css/aos.css",
                      "~/theme/css/ionicons.min.css",
                      "~/theme/css/bootstrap-datepicker.css",
                      "~/theme/css/jquery.timepicker.css",
                      "~/theme/fonts/flaticon/font/flaticon.css",
                      "~/theme/css/style.css"
                      ));
            
            BundleTable.EnableOptimizations = true;
        }
    }
}
