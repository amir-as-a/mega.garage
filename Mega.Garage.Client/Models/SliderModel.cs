﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mega.Garage.Client.Models
{
    public class SliderModel
    {
        public string LinkUrl { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
    }
}