﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mega.Garage.Client.Garage
{
    public class LoginViewModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

}