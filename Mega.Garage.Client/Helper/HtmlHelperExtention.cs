﻿using Mega.Garage.Common;
using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Client.Helper
{

    public static class HtmlHelperExtention
    {
        /// <summary>
        /// Show text instead of boolean property 
        /// </summary>
        /// <param name="helper">html helpter class</param>
        /// <param name="item">boolean item</param>
        /// <returns>text for show</returns>
        public static string DisplayForBoolean(this HtmlHelper helper, bool? item)
        {
            if (helper is null)
            {
                throw new ArgumentNullException(nameof(helper));
            }

            if (item is null)
            {
                return "نامشخص";
            }

            if (item.Value)
            {
                return "بلی";
            }
            else
            {
                return "خیر";
            }
        }
        public static string DisplayUserName(this HtmlHelper helper, int? item)
        {
            if (helper is null)
            {
                throw new ArgumentNullException(nameof(helper));
            }

            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            if (item.HasValue)
            {
                if (item == 1)
                    return "مدیر سایت";
            }
            return "نامشخص";
        }

        /// <summary>
        /// convert and show date as persian date instead of gregorian date
        /// </summary>
        /// <param name="helper">html helpter class</param>
        /// <param name="item">date object</param>
        /// <returns>text for show</returns>
        public static string DisplayForDate(this HtmlHelper helper, DateTime? item)
        {
            if (helper is null)
            {
                throw new ArgumentNullException(nameof(helper));
            }

            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            return DateUtility.GetPersianDate(item);
        }

        /// <summary>
        /// convert and show date as persian date and time instead of gregorian date
        /// </summary>
        /// <param name="helper">html helpter class</param>
        /// <param name="item">date object</param>
        /// <returns>text for show</returns>
        public static string DisplayForDateTime(this HtmlHelper helper, DateTime? item)
        {
            if (helper is null)
            {
                throw new ArgumentNullException(nameof(helper));
            }

            if (item is null)
            {
                throw new ArgumentNullException(nameof(item));
            }

            return DateUtility.GetPersianDateTime(item);
        }
        public static MvcHtmlString ColorPicker<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            if (html is null)
            {
                throw new ArgumentNullException(nameof(html));
            }

            if (expression is null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            if (!(expression.Body is MemberExpression memberExpression))
                return null;
            var name = memberExpression.Member.Name;

            var inputTag = new TagBuilder("input");
            inputTag.Attributes["id"] = $"txt{name}";
            inputTag.Attributes["type"] = "text";
            inputTag.Attributes["name"] = $"{name}";
            inputTag.Attributes["class"] = "form-control jscolor";
            inputTag.Attributes["autocomplete"] = "off";
            inputTag.Attributes["readonly"] = "readonly";

            if (html.ViewData.Model != null)
            {
                Func<TModel, TValue> method = expression.Compile();
                string value = method(html.ViewData.Model).ToString();
                if (!string.IsNullOrEmpty(value))
                {
                    inputTag.Attributes["value"] = value;
                }
            }
            inputTag.InnerHtml += "<script>jscolor.installByClassName(\"jscolor\");</script>";
            return MvcHtmlString.Create(inputTag.ToString(TagRenderMode.Normal));

        }
        public static MvcHtmlString EditorPersianDatePicker<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            if (html is null)
            {
                throw new ArgumentNullException(nameof(html));
            }

            if (expression is null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            if (!(expression.Body is MemberExpression memberExpression))
                return null;
            var name = memberExpression.Member.Name;


            var inputTag = new TagBuilder("input");
            inputTag.Attributes["id"] = $"txt{name}";
            inputTag.Attributes["type"] = "text";
            inputTag.Attributes["name"] = $"persian_{name}";
            inputTag.Attributes["class"] = "form-control";
            inputTag.Attributes["autocomplete"] = "off";
            inputTag.Attributes["readonly"] = "readonly";

            var hiddenTag = new TagBuilder("input");
            hiddenTag.Attributes["type"] = "hidden";
            hiddenTag.Attributes["name"] = name;

            if (html.ViewData.Model != null)
            {
                Func<TModel, TValue> method = expression.Compile();
                string value = method(html.ViewData.Model).ToString();
                if (!string.IsNullOrEmpty(value))
                {
                    var date = DateTime.Parse(value);
                    inputTag.Attributes["value"] = date.ToString("yyyy/MM/dd");
                    hiddenTag.Attributes["value"] = date.ToString("yyyy/MM/dd");
                }
            }


            var script = @"
<script type='text/javascript'>
    $(document).ready(function () {
        var dateConfig = { 
            format: 'YYYY/MM/DD', 
            autoClose: true, 
" +
(!inputTag.Attributes.ContainsKey("value") ? @" initialValue: false, " : "") +
(inputTag.Attributes.ContainsKey("value") ? @" initialValueType: 'gregorian', " : "") +
@"
            onSelect: function (unix) {
                var date = new Date(unix);
                var gregorianDate = date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate();
                $('[name=" + name + @"]').val(gregorianDate);
            }
        };
        $('#txt" + name + @"').pDatepicker(dateConfig);
    });
</script>
";

            var container = new TagBuilder("div");
            container.InnerHtml += inputTag;
            container.InnerHtml += hiddenTag;
            container.InnerHtml += script;
            return MvcHtmlString.Create(container.ToString(TagRenderMode.Normal));
        }
        public static MvcHtmlString ImagePreview<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression,string classNames = "")
        {
            if (html is null)
            {
                throw new ArgumentNullException(nameof(html));
            }

            if (expression is null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            if (!(expression.Body is MemberExpression memberExpression))
                return null;
            var name = memberExpression.Member.Name;

            var imageTag = new TagBuilder("img");
            imageTag.Attributes["id"] = $"img{name}";
            if (!string.IsNullOrEmpty(classNames))
            {
                imageTag.Attributes["class"] = classNames;
            }

            if (html.ViewData.Model != null)
            {
                string ImageAddress;
                try
                {
                    Func<TModel, TValue> method = expression.Compile();
                    string value = method(html.ViewData.Model).ToString();
                    if (!string.IsNullOrEmpty(value))
                    {
                        //var ImageData = Common.JSON.Desrialize<Logic.FileUpload.ViewModel.FileData>(value);
                        ImageAddress = Logic.FileUpload.Services.UploadService.GetCDNFilePath(value);

                    }
                    else
                    {

                        ImageAddress = "/Images/no-image.jpg";
                    }
                }
                catch
                {
                    ImageAddress = "/Images/no-image.jpg";
                }
                imageTag.Attributes["src"] = ImageAddress;
            }

            return MvcHtmlString.Create(imageTag.ToString(TagRenderMode.Normal));
        }
        public static MvcHtmlString FileUpload<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression)
        {
            if (html is null)
            {
                throw new ArgumentNullException(nameof(html));
            }

            if (expression is null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            if (!(expression.Body is MemberExpression memberExpression))
                return null;
            var name = memberExpression.Member.Name;

            var inputTag = new TagBuilder("input");
            inputTag.Attributes["type"] = "file";
            inputTag.Attributes["name"] = $"file{name}";
            inputTag.Attributes["class"] = "form-control";

            if (html.ViewData.Model != null)
            {
                Func<TModel, TValue> method = expression.Compile();
                string value = method(html.ViewData.Model)?.ToString();
                if (!string.IsNullOrEmpty(value))
                {
                    inputTag.Attributes["value"] = value;
                }
            }
            return MvcHtmlString.Create(inputTag.ToString(TagRenderMode.Normal));

        }


    }
}