﻿using Mega.Garage.Logic.Polling.Services;
using Mega.Garage.Logic.Polling.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Client.Controllers
{
    public class SurveyController : Controller
    {
        public ActionResult Poll(int userServeyId) {
            var vm = UserSurveyService.GetUserSurveyByUserServeyId(userServeyId);
            if (vm != null)
            {
                if (vm.UserSurvey != null && vm.UserSurvey.ShowDate.HasValue && vm.UserSurvey.CompleteDate.HasValue)
                    return View("Review", vm);
                else if (vm.UserSurvey != null && vm.UserSurvey.ShowDate.HasValue && !vm.UserSurvey.CompleteDate.HasValue)
                {
                    return View("Insert", vm);
                }
                else
                {
                    var onclickAdd = UserSurveyService.AddUserSurvey(new Persistence.Polling.Entities.UserSurvey
                    {
                        ShowDate = DateTime.Now,
                        OrderNumber = vm.UserSurvey.OrderNumber,
                        SurveyId = vm.UserSurvey.Id,
                        DealerId = vm.UserSurvey.DealerId,
                        UserId = vm.UserSurvey.UserId
                    });
                    vm = new UserSurveyViewModel()
                    {
                        Questions = QuestionService.GetAllValidQuestion(DateTime.Now, 1, vm.UserSurvey.Id),
                        UserSurvey = new Persistence.Polling.Entities.UserSurvey { Id = onclickAdd.Id }
                    };
                    return View("Insert", vm);
                }
            }
            else
                throw new ArgumentException();

        }
        // GET: Survey
        public ActionResult Index(string orderNumber, long surveyId, long dealerId, long userId)
        {
            if (string.IsNullOrEmpty(orderNumber) && surveyId < 0 && dealerId < 0 && userId < 0)
                throw new ArgumentException();

            var vm = UserSurveyService.GetUserSurveyByOrderNumber(orderNumber);
            if (vm != null)
            {
                if (vm.UserSurvey != null && vm.UserSurvey.ShowDate.HasValue && vm.UserSurvey.CompleteDate.HasValue)
                    return View("Review", vm);
                else if (vm.UserSurvey != null && vm.UserSurvey.ShowDate.HasValue && !vm.UserSurvey.CompleteDate.HasValue)
                {
                    vm = new UserSurveyViewModel()
                    {
                        Questions = QuestionService.GetAllValidQuestion(DateTime.Now, 1, surveyId),
                        UserSurvey = vm.UserSurvey
                    };
                    return View("Insert", vm);
                }
                else
                {
                    var onclickAdd = UserSurveyService.AddUserSurvey(new Persistence.Polling.Entities.UserSurvey
                    {
                        ShowDate = DateTime.Now,
                        OrderNumber = orderNumber,
                        SurveyId = surveyId,
                        DealerId = dealerId,
                        UserId = userId
                    });
                    vm = new UserSurveyViewModel()
                    {
                        Questions = QuestionService.GetAllValidQuestion(DateTime.Now, 1, surveyId),
                        UserSurvey = new Persistence.Polling.Entities.UserSurvey { Id = onclickAdd.Id }
                    };
                    return View("Insert", vm);
                }
            }
            else
                throw new ArgumentException();

        }
        [HttpPost]
        public ActionResult Insert(UserSurveyViewModel vm)
        {
            if (vm.UserSurvey != null)
            {
                var userSurvey = UserSurveyService.AddUserSurvey(vm.UserSurvey);
                if (userSurvey != null)
                    UserSurveyService.AddUserSurveyQuestion(userSurvey.Id, vm.UserSurveyQuestions);
            }
            return RedirectToAction("Index", new { orderNumber = vm.UserSurvey.OrderNumber });
        }
    }
}