﻿using Mega.Garage.Client.Models;
using Mega.Garage.Common;
using Mega.Garage.Logic.OnSite.Services;
using Mega.Garage.Logic.Order.Services;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Mega.Garage.Client.Controllers
{
    public class HomeController : BaseController
    {
        private const string CookieName = "AUTH";
        public ActionResult Index()
        {
            var productGroup = ProductGroupService.GetProductGroupByEnglishTitle("Iranecar Customer");

            var singleProductList = ProductGroupService.GetProductByProductGroupEnglishTitle("Single Product");
            var productList = ProductGroupService.GetProductByProductGroupEnglishTitle("Iranecar Customer");

            ViewBag.SingleProductList = singleProductList.Data;
            ViewBag.ProductGroup = productGroup;
            return View(productList.Data);
        }

        public ActionResult Landing(string name)
        {
            if (Common.Cookie.IsCookieSet("AUTH"))
            {
                var productGroup = ProductGroupService.GetProductGroupByEnglishTitle("snapp products");
                var productList = ProductGroupService.GetProductByProductGroupEnglishTitle("snapp products");

                ViewBag.ProductGroup = productGroup;
                return View(productList);
            }
            else
            {
                return RedirectToAction("register", "user", new { redirect = "/Landing/snapp" });
            }
        }

        public ActionResult _UserMobile()
        {
            if (Common.Cookie.IsCookieSet("AUTH"))
            {
                var mobile = Common.Cryptography.Decrypt(Common.Cookie.ReadCookie("AUTH"));
                ViewBag.Customer = CustomerService.GetCustomerByMobile(mobile);
                ViewBag.MobileNumber = mobile;
            }
            return View();
        }

    }
}