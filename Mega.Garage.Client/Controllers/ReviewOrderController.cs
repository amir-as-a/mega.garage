﻿using Mega.Garage.Client.Garage;
using Mega.Garage.Client.Models;
using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Logic.OnSite.Services;
using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Logic.Order.ViewModel;
using Mega.Garage.Logic.Payment.Services;
using Mega.Garage.Persistence.Order.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZXing;
using ZXing.Common;
using ZXing.QrCode.Internal;
using ZXing.Rendering;

namespace Mega.Garage.Client.Controllers
{
    public class ReviewOrderController : BaseController
    {
        public ActionResult Index()
        {
            HttpCookie cookie = Request.Cookies["Cart"];
            List<CartViewModel> carts = new List<CartViewModel>();
            if (cookie != null)
                carts = JsonConvert.DeserializeObject<List<CartViewModel>>(cookie.Value);

            if (carts.Count <= 0)
                return RedirectToAction("index", "home");

            if (!Common.Cookie.IsCookieSet("AUTH"))
                return RedirectToAction("register", "user", new { redirect = "/reviewOrder" });

            ViewBag.ProductGroup = ProductGroupService.GetProductGroupById(carts.FirstOrDefault().ProductGroupId);

            List<long> productIds = carts.Select(x => x.ProductId).ToList();
            IEnumerable<Product> products = ProductService.GetProductsByIdList(productIds);

            var mobileNumber = Common.Cryptography.Decrypt(Common.Cookie.ReadCookie("AUTH"));
            var customer = CustomerService.GetCustomerByMobile(mobileNumber);
            ViewBag.Customer = customer;

            ViewBag.CarGroup = CarGroupService.GetAllCarGroups().ToList();
            ViewBag.Dealers = DealerService.GetAllDealers().ToList();

            var price = 0;
            foreach (var item in carts)
            {
                price += (item.Count * products.FirstOrDefault(x => x.Id == item.ProductId)?.UnitPrice.Value ?? 0);
            }
            ViewBag.Price = price;

            if (Common.Cookie.IsCookieSet("Discount"))
            {
                string code = Common.Cryptography.Decrypt(Common.Cookie.ReadCookie("Discount"));
                var discount = DiscountService.CheckDiscount(code, price);
                if (discount.Status == MegaStatus.Successfull)
                {
                    ViewBag.Discount = discount.Data;
                }
            }

            ViewBag.Products = products;
            return View(carts);
        }

        [HttpPost]
        public ActionResult Index(InvoiceViewModel model)
        {
            if (Common.Cookie.IsCookieSet("Discount"))
            {
                string code = Common.Cryptography.Decrypt(Common.Cookie.ReadCookie("Discount"));
                model.DiscountCode = code;
            }

            HttpCookie cookie = Request.Cookies["Cart"];
            var carts = JsonConvert.DeserializeObject<List<CartViewModel>>(cookie.Value);
            model.Carts = carts;

            try
            {
                var invoice = InvoiceService.CreateInvoice(model);
                if (invoice.Status == MegaStatus.Successfull && invoice.Data != null && invoice.Data.Id > 0)
                {
                    /// بعد از ثبت سفارش در دیتابیس نیاز است کوکی های مربوط به سبد خرید و کد تخفیف از یو آی کاربر منقضی گردد
                    Common.Cookie.ExpireCookie("Cart");
                    Common.Cookie.ExpireCookie("Discount");

                    /// model.Payment == 1 پرداخت در محل
                    /// model.Payment == 2 پرداخت اینترنتی
                    /// در صورتی که پرداخت در محل باشد سفارش ثبت می شود و کاربر یه صفحه مربوط به مشاهده جزئیات سفارش منتقل می گردد
                    /// در صورتی که سفارش پرداخت در محل باشد کاربر به اکشن مربوط به انتقال به درگاه منتقل می گردد

                    if (model.Payment == 1)
                    {
                        TempData["Messages"] = new List<Message>() { new Message() { Type = MessageType.Success, Content = $"فاکتور شما با شماره {invoice.Data.OrderNumber} موفقیت ثبت شد." } };
                        return RedirectToAction("Check", new { Id = invoice.Data.OrderNumber });
                    }
                    else if (model.Payment == 2)
                    {
                        // Select bank action
                        return RedirectToAction("MellatTunnel", new { orderNumber = invoice.Data.OrderNumber });
                    }
                }
                else
                {
                    return ReturnException(invoice.Messages, RedirectToAction("Index"));
                }
            }
            catch (Exception ex)
            {
                return ReturnException(ex, RedirectToAction("index"));
            }

            return Index();
        }

        //public ActionResult Payment(string id)
        //{
        //    var GarageCode = $"{id}{DateTime.Now:yyMMddHHmmss}";

        //    var invoice = InvoiceService.GetInvoiceByOrderNumber(id);

        //    if (invoice != null && invoice.PaymentTypeId == 2 && invoice.IsPayed != true)
        //    {
        //        if (invoice.DealerId != null)
        //        {
        //            var dealer = DealerService.GetDealerById(invoice.DealerId.Value);

        //            if (!string.IsNullOrEmpty(dealer.BankAccountNumber))
        //            {
        //                var bankObject =
        //                    MellatMerchantWebService
        //                    .PaymentRequest(
        //                        price: int.Parse(invoice.FinalPrice.ToString()),
        //                        orderid: long.Parse(invoice.OrderNumber),
        //                        GarageCode: long.Parse(GarageCode),
        //                        dealerShenase: dealer.BankAccountNumber);

        //                if (bankObject.Status == MegaStatus.Successfull)
        //                {
        //                    PaymentService.AddUserPayment(new Persistence.Payment.Entities.UserPayment()
        //                    {
        //                        GaragesCode = GarageCode,
        //                        ReferalCode = bankObject.Data.ReferalCode,
        //                        ResponseCode = bankObject.Data.ResCode,
        //                        IPGId = 1,
        //                        InvoiceId = invoice.Id,
        //                        PaymentDate = DateTime.Now,
        //                        PaymentStatusId = PaymentService.GetPaymentStatusByCode(Logic.Enums.PaymentStatus.PaymentRequest).Id,
        //                        HasSplit = false
        //                    });

        //                    return Redirect("/ReviewOrder/MellatTunnel?orderNumber=" + id);
        //                }
        //                else
        //                {
        //                    TempData["Messages"] = new List<Message>() {
        //                          new Message() { Type = MessageType.Error, Content = "خطایی از سمت بانک صادر شد."
        //                        }
        //                    };
        //                }
        //            }
        //            else
        //            {
        //                TempData["Messages"] = new List<Message>() {
        //                        new Message() { Type = MessageType.Error, Content = "برای این اتوسرویس نمی توان پرداخت آنلاین ثبت کرد."
        //                    }
        //                };
        //            }
        //        }
        //        else
        //        {
        //            TempData["Messages"] = new List<Message>() {
        //                new Message() { Type = MessageType.Error, Content = "درصوت درخواست پرداخت آنلاین بایستی اتوسرویس مشخص شده باشد."
        //                }
        //            };
        //        }
        //    }
        //    else
        //    {
        //        TempData["Messages"] = new List<Message>() {
        //                new Message() { Type = MessageType.Error, Content = "برای این سفارش نمی توانید پرداخت آنلاین ثبت کنید."
        //            }
        //        };
        //    };

        //    return View();
        //}

        //public ActionResult MellatTunnel(string orderNumber)
        //{
        //    var invoiceId = InvoiceService.GetInvoiceByOrderNumber(orderNumber)?.Id;
        //    if (invoiceId.HasValue)
        //    {
        //        var response = Logic.Payment.Services.PaymentService.GetByInvoiceId(invoiceId.Value);
        //        if (response != null)
        //            return View(response);
        //    }
        //    TempData["Messages"] = new List<Message>() { new Message() { Type = MessageType.Error, Content = "شماره فاکتور اشتباه می باشد" } };
        //    return RedirectToAction("index", "home");
        //}

        [HttpPost]
        //public ActionResult BankCallBack()
        //{
        //    string GarageReferenceId = (Request.Params["GarageReferenceId"] ?? "").ToString();
        //    string GarageOrderId = (Request.Params["GarageOrderId"] ?? "").ToString();
        //    string ResCode = (Request.Params["ResCode"] ?? "").ToString();
            //var response = Logic.Payment.Services.PaymentService.BankCallBack(GarageReferenceId, GarageOrderId, ResCode);
            //if (response.Status == MegaStatus.Successfull)
            //{
            //    TempData["Messages"] = new List<Message>() { new Message() { Type = MessageType.Success, Content = response.Messages.FirstOrDefault() } };
            //    return RedirectToAction("Check", new { id = response.Data.orderNumber });
            //}
            //else
            //{
            //    TempData["Messages"] = new List<Message>() { new Message() { Type = MessageType.Error, Content = response.Messages.FirstOrDefault() } };
            //    return RedirectToAction("PaymentError", new { ResCode, GarageOrderId });
            //}
       // }
        public FileResult QrCode(string id)
        {
            BarcodeWriter barcodeWriter = new BarcodeWriter();
            EncodingOptions encodingOptions = new EncodingOptions() { Width = 500, Height = 500, Margin = 0, PureBarcode = false };
            encodingOptions.Hints.Add(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            barcodeWriter.Renderer = new BitmapRenderer();
            barcodeWriter.Options = encodingOptions;
            barcodeWriter.Format = BarcodeFormat.QR_CODE;
            Bitmap bitmap = barcodeWriter.Write(id);
            Bitmap logo = new Bitmap(Server.MapPath("~/theme/images/logo_speedy.png"));
            Graphics g = Graphics.FromImage(bitmap);
            g.DrawImage(logo, new Point((bitmap.Width - logo.Width) / 2, (bitmap.Height - logo.Height) / 2));


            return File(Common.Image.ImageToByte(bitmap), "image/jpeg");
        }

        public ActionResult Check(string id)
        {
            var invoice = InvoiceService.GetInvoiceByOrderNumber(id);

            var orderItems = OrderService.GetAllOrderItemsByInvoiceId(invoice.Id);
            var productIds = orderItems.Select(x => x.ProductId.Value).ToList();
            var products = ProductService.GetProductsByIdList(productIds);

            ViewBag.OrderItems = orderItems;
            ViewBag.Products = products;

            if (invoice.DealerId != null && invoice.DealerId > 0)
                ViewBag.Dealer = DealerService.GetDealerById(invoice.DealerId.Value);

            return View(invoice);
        }
        public ActionResult PaymentError()
        {
            return View();
        }
        public ActionResult Print(string id)
        {
            var invoice = InvoiceService.GetInvoiceByOrderNumber(id);

            var orderItems = OrderService.GetAllOrderItemsByInvoiceId(invoice.Id);
            var productIds = orderItems.Select(x => x.ProductId.Value).ToList();
            var products = ProductService.GetProductsByIdList(productIds);

            ViewBag.InvoiceDetail = invoice;
            ViewBag.OrderItems = orderItems;
            ViewBag.Products = products;

            return View(invoice);
        }
    }
}