﻿using FluentValidation;
using Mega.Garage.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Message = Mega.Garage.Client.Models.Message;

namespace Mega.Garage.Client.Controllers
{
    public class BaseController : Controller
    {
        protected ActionResult ReturnException(Exception ex, RedirectToRouteResult redirect = null)
        {
            if (ex is ValidationException)
            {

                var messages = new List<Message>() {
                    new Message { Type = MessageType.Error, Content = "عملیات ناموفق." },
                };

                foreach (var item in ((ValidationException)ex).Errors)
                {
                    if (messages.Count(message => message.Content == item.ErrorMessage) == 0)
                    {
                        var message = new Message { Type = MessageType.Warning, Content = item.ErrorMessage };
                        messages.Add(message);
                    }
                }

                TempData["Messages"] = messages.Distinct().ToList();
            }
            else
            {
                if (Common.Cookie.IsCookieSet("AUTH"))
                {
                    var mobile = Common.Cryptography.Decrypt(Common.Cookie.ReadCookie("AUTH"));
                    if (mobile == "09357214976")
                    {
                        TempData["Messages"] = new List<Message>() { new Message { Type = MessageType.Error, Content = ex.Message }, new Message { Type = MessageType.Warning, Content = ex.InnerException?.Message } };
                    }
                    else
                    {
                        TempData["Messages"] = new List<Message>() { new Message { Type = MessageType.Error, Content = "خطایی رخ داده است." } };
                    }
                }
                else
                {
                    TempData["Messages"] = new List<Message>() { new Message { Type = MessageType.Error, Content = "خطایی رخ داده است." } };
                }
            }

            if (redirect == null)
                return View();
            else
                return redirect;
        }

        protected ActionResult ReturnException(IEnumerable<string> messages, RedirectToRouteResult redirect = null)
        {

            var Messages = new List<Message>() {
                    new Message { Type = MessageType.Error, Content = "عملیات ناموفق." },
                };

            foreach (var item in messages)
            {
                var message = new Message { Type = MessageType.Warning, Content = item };
                Messages.Add(message);
            }

            TempData["Messages"] = Messages.Distinct().ToList();

            if (redirect == null)
                return View();
            else
                return redirect;
        }
    }
}