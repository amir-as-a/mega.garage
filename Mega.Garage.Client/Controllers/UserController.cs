﻿using Mega.Garage.Client.Garage;
using Mega.Garage.Client.Models;
using Mega.Garage.Logic.Account.Services;
using Mega.Garage.Logic.Account.Servies;
using Mega.Garage.Logic.Account.ViewModel;
using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Logic.Order.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Mega.Garage.Client.Controllers
{
    public class UserController : BaseController
    {
        private const string CookieName = "AUTH";
        // GET: User
        [HttpGet]
        [ActionName("Profile")]
        public ActionResult ProfileData()
        {
            var mobile = Common.Cryptography.Decrypt(Common.Cookie.ReadCookie("AUTH"));
            var customer = CustomerService.GetCustomerByMobile(mobile);
            ViewBag.CarGroup = CarGroupService.GetAllCarGroups().ToList();

            return View(customer);
        }

        [HttpGet]
        public ActionResult Register(string redirect)
        {
            TempData["Redirect"] = TempData["Redirect"] ?? redirect;
            ViewBag.NeedDisable = false;
            bool hasRegistrationCookie = Common.Cookie.IsCookieSet(CookieName);
            if (hasRegistrationCookie)
            {
                string CookieValue = Common.Cookie.ReadCookie(CookieName);
                if (!string.IsNullOrEmpty(CookieValue))
                {
                    if (Common.Cryptography.TryDecrypt(CookieValue, out string MobileNo))
                    {
                        if (Common.Validate.IsMobile(MobileNo))
                        {
                            return Redirect("/");
                        }
                    }
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Register(string redirect, string mobile)
        {
            ViewBag.NeedDisable = false;
            if (string.IsNullOrEmpty(mobile))
                return ReturnException(new string[] { "وارد کردن شماره تلفن اجباری است." });

            if (mobile == "09357214976") // این برای شماره عابدی هارد کد شده که دیگه اس ام اس فرستاده نشه و مستقیم لاگین بشه
            {
                string EncryptedMobile = Common.Cryptography.Encrypt("09357214976");
                Common.Cookie.SetCookie(CookieName, EncryptedMobile);
                Session["MobileNo"] = "09357214976";

                if (!string.IsNullOrEmpty(redirect))
                    return Redirect(redirect);
                else
                    return Redirect("~/");
            }

            TempData["Redirect"] = redirect;
            try
            {
                OtpService.RegisterOtp(mobile);
                TempData["MobileNumber"] = mobile;
                TempData["Messages"] = new List<Message>() { new Message { Type = MessageType.Success, Content = "کد پیامک ارسال شده را در کادر زیر وارد کنید." } };
                TempData["Verify"] = true;
                ViewBag.NeedDisable = false;

                return View(new { redirect });
            }
            catch (Exception ex)
            {
                return ReturnException(ex);
            }
        }

        [HttpPost]
        public ActionResult Verify(OtpVerifyViewModel otpVerifyViewModel, string redirect)
        {
            TempData["Redirect"] = redirect ?? TempData["Redirect"];

            ViewBag.NeedDisable = false;
            TempData["MobileNumber"] = otpVerifyViewModel.Mobile;
            TempData["Verify"] = true;

            try
            {
                var verifyResult = OtpService.Verify(otpVerifyViewModel);
                if (verifyResult.Status == MegaStatus.Successfull)
                {
                    string EncryptedMobile = Common.Cryptography.Encrypt(otpVerifyViewModel.Mobile);
                    Common.Cookie.SetCookie(CookieName, EncryptedMobile);
                    Session["MobileNo"] = otpVerifyViewModel.Mobile;

                    if (!string.IsNullOrEmpty(TempData["Redirect"]?.ToString()))
                        return Redirect(TempData["Redirect"].ToString());
                    else
                        return Redirect("~/");
                }
                else
                {
                    return ReturnException(verifyResult.Messages, RedirectToAction("Register", new { redirect }));
                }
            }
            catch (Exception ex)
            {
                return ReturnException(ex, RedirectToAction("Register"));
            }
        }

        public ActionResult Archive()
        {
            if (!Common.Cookie.IsCookieSet(CookieName))
                return HttpNotFound();

            var mobileNumber = Common.Cryptography.Decrypt(Common.Cookie.ReadCookie(CookieName));
            var customer = CustomerService.GetCustomerByMobile(mobileNumber);
            List<Persistence.Order.Entities.Invoice> invoiceList = new List<Persistence.Order.Entities.Invoice>();

            if (customer != null)
                invoiceList = InvoiceService.GetInvoiceListByCusomerId(customer.Id).OrderByDescending(item => item.InvoiceDate).ToList();

            ViewBag.Customer = customer;
            return View(invoiceList);
        }

        public ActionResult Logout()
        {
            Common.Cookie.ExpireCookie(CookieName);
            Common.Cookie.ExpireCookie("Cart");
            Common.Cookie.ExpireCookie("Discount");

            return Redirect("/");
        }

    }
}