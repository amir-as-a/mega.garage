﻿using Mega.Garage.Logic.OnSite.Services;
using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Logic.Order.ViewModel;
using Mega.Garage.Persistence.Order.Entities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Client.Controllers
{
    public class CartController : BaseController
    {
        public ActionResult Index()
        {
            HttpCookie cookie = Request.Cookies["Cart"];
            List<CartViewModel> carts = new List<CartViewModel>();
            if (cookie != null)
                carts = JsonConvert.DeserializeObject<List<CartViewModel>>(cookie.Value);

            List<long> productIds = carts.Select(x => x.ProductId).ToList();
            IEnumerable<Product> products = ProductService.GetProductsByIdList(productIds);


            var price = 0;
            foreach (var item in carts)
            {
                price += (item.Count * products.SingleOrDefault(x => x.Id == item.ProductId)?.UnitPrice.Value ?? 0);
            }
            ViewBag.Price = price;

            if (Common.Cookie.IsCookieSet("Discount"))
            {
                string code = Common.Cryptography.Decrypt(Common.Cookie.ReadCookie("Discount"));
                var discount = DiscountService.CheckDiscount(code, price);
                if (discount.Status == MegaStatus.Successfull)
                {
                    ViewBag.Discount = discount.Data;
                }
            }

            ViewBag.Products = products;
            return View(carts);
        }

        public JsonResult AddToCart(long id, int productGroupId)
        {
            /// Read Data From Cookie with a unique name
            string CookieValue = Common.Cookie.ReadCookie(Utility.Cart.CartCookieName);
            List<CartViewModel> carts = new List<CartViewModel>();

            /// if data in cookie is not null we deserialize all data from it to a local list viewmodel
            if (CookieValue != null)
                carts = Common.JSON.Desrialize<List<CartViewModel>>(CookieValue);

            /// if this product already exists in Cookie storage we just count it up
            /// else we will add this product to list
            if (carts.Count(cart => cart.ProductId == id) > 0)
            {
                carts.FirstOrDefault(product => product.ProductId == id).Count += 1;
            }
            else
            {
                if (carts.Count > 0)
                {
                    //if (carts.FirstOrDefault().ProductGroupId == productGroupId)
                    //{
                    carts.Add(new CartViewModel() { ProductId = id, ProductGroupId = productGroupId, Count = 1 });
                    //}
                    //else
                    //{
                    //    return Json(new { status = "error", message = "امکان افزودن به سبد خرید از دو نوع مختلف فروش وجود ندارد" }, JsonRequestBehavior.AllowGet);
                    //}
                }
                else
                {
                    carts.Add(new CartViewModel() { ProductId = id, ProductGroupId = productGroupId, Count = 1 });
                }
            }

            /// result of all business will be saved in a Cookie storage with a unique name
            Common.Cookie.SetCookie(Utility.Cart.CartCookieName, Common.JSON.Serialize(carts));

            return Json(new { status = "Done" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(long id)
        {

            string CookieValue = Common.Cookie.ReadCookie(Utility.Cart.CartCookieName);
            List<CartViewModel> carts = new List<CartViewModel>();

            /// first of all we check for Cookie exists
            if (CookieValue != null)
                carts = Common.JSON.Desrialize<List<CartViewModel>>(CookieValue);

            var selectedProduct = carts.FirstOrDefault(cart => cart.ProductId == id);
            /// if a record exists in list of products we will remove it and then update the Cookie for further activities
            if (selectedProduct != null)
            {
                carts.Remove(selectedProduct);
                Common.Cookie.SetCookie(Utility.Cart.CartCookieName, Common.JSON.Serialize(carts));
            }
            return Json(new { status = "Done" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ChangeCount(long productId, int count)
        {
            string CookieValue = Common.Cookie.ReadCookie(Utility.Cart.CartCookieName);
            List<CartViewModel> carts = new List<CartViewModel>();
            /// if data in cookie is not null we deserialize all data from it to a local list viewmodel
            if (CookieValue != null)
                carts = Common.JSON.Desrialize<List<CartViewModel>>(CookieValue);

            /// if this product already exists in Cookie storage we just count it up
            /// else we will add this product to list
            if (carts.Count(cart => cart.ProductId == productId) > 0)
            {
                carts.FirstOrDefault(product => product.ProductId == productId).Count = count;
            }

            Common.Cookie.SetCookie(Utility.Cart.CartCookieName, Common.JSON.Serialize(carts));

            return Json(new { status = "Done" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckDiscount(long price, string discountCode)
        {
            if (!string.IsNullOrEmpty(discountCode))
            {
                var result = DiscountService.CheckDiscount(discountCode, price);

                if (result.Status == MegaStatus.Successfull)
                {
                    Common.Cookie.SetCookie("Discount", Common.Cryptography.Encrypt(discountCode));
                }
                else
                {
                    TempData["DicountCodeError"] = result.Messages[0];
                    Common.Cookie.ExpireCookie("Discount");
                }
            }
            else
            {
                Common.Cookie.ExpireCookie("Discount");
            }
            return Redirect("/cart/index#coupon_box");
        }

        public JsonResult GetCartSummary()
        {
            double discount = 0;
            var price = 0;

            HttpCookie cookie = Request.Cookies["Cart"];
            List<CartViewModel> carts = new List<CartViewModel>();
            if (cookie != null)
                carts = JsonConvert.DeserializeObject<List<CartViewModel>>(cookie.Value);

            List<long> productIds = carts.Select(x => x.ProductId).ToList();
            IEnumerable<Product> products = ProductService.GetProductsByIdList(productIds);


            foreach (var item in carts)
            {
                price += (item.Count * products.SingleOrDefault(x => x.Id == item.ProductId)?.UnitPrice.Value ?? 0);
            }

            if (Common.Cookie.IsCookieSet("Discount"))
            {
                string code = Common.Cryptography.Decrypt(Common.Cookie.ReadCookie("Discount"));
                var discountResult = DiscountService.CheckDiscount(code, price);
                if (discountResult.Status == MegaStatus.Successfull)
                {
                    discount = discountResult.Data;
                }
            }

            return Json(new
            {
                price,
                discount
            }, JsonRequestBehavior.AllowGet);
        }

    }
}