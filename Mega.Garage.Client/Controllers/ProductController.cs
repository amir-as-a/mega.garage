﻿using Mega.Garage.Logic.OnSite.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Client.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index(int Id)
        {
            ViewBag.productList = ProductGroupService.GetProductByProductGroupEnglishTitle("Iranecar Customer");

            var ProductDetail = Logic.Order.Services.ProductService.GetProductById(Id);
            return View(ProductDetail);
        }
    }
}