﻿using Mega.Garage.Logic.Order.ViewModel;
using System.Collections.Generic;

namespace Mega.Garage.Client.Utility
{
    public class Cart
    {
        public const string CartCookieName = "Cart";
        public static int GetTotalItemsInBasket()
        {
            string CookieValue = Common.Cookie.ReadCookie("Cart");
            if (CookieValue == null)
            {
                return 0;
            }
            var carts = Common.JSON.Desrialize<List<CartViewModel>>(CookieValue);
            return carts.Count;
        }
        public static bool CartTotalPriceWithoutDiscount(out int TotalPriceWithoutDiscount, out int TotalPriceWithDiscount)
        {
            int _TotalPriceWithoutDiscount = 0;
            int _TotalPriceWithDiscount = 0;

            string CookieValue = Common.Cookie.ReadCookie("Cart");
            if (CookieValue == null)
            {
                TotalPriceWithoutDiscount = 0;
                TotalPriceWithDiscount = 0;
                return false;
            }
            var carts = Common.JSON.Desrialize<List<CartViewModel>>(CookieValue);
            carts.ForEach(item =>
            {
                var productEntity = Logic.Order.Services.ProductService.GetProductById(item.ProductId);
                if (productEntity != null)
                {
                    if (productEntity.BasePrice.HasValue)
                    {
                        _TotalPriceWithoutDiscount += (productEntity.BasePrice.Value * item.Count);
                    }
                    if (productEntity.UnitPrice.HasValue)
                    {
                        _TotalPriceWithDiscount += (productEntity.UnitPrice.Value * item.Count);
                    }
                }
            });
            TotalPriceWithoutDiscount = _TotalPriceWithoutDiscount;
            TotalPriceWithDiscount = _TotalPriceWithDiscount;
            return true;
        }
    }
}