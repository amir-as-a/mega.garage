﻿using FluentFTP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mega.Garage.Common
{
    public class FTPClient
    {
        //protected readonly string userName = "speedy_file_ftp";
        //protected readonly string password = "2B7q8#@!45#ED*";
        //protected readonly string host = "185.209.243.62";

        protected readonly string userName = "speedy";
        protected readonly string password = "A9C853169469";
        protected readonly string host = "185.129.169.91";

        protected readonly int ftpPort = 21;
        protected readonly System.Net.NetworkCredential networkCredential = null;
        public FTPClient()
        {
            this.networkCredential = new System.Net.NetworkCredential(userName, password);
        }

        public static bool UploadFile(string filePath, byte[] fileArray)
        {
            string PathPrefix = "/SpeedyAdmin/";
            FTPClient fTPClient = new FTPClient();
            var client = new FluentFTP.FtpClient(fTPClient.host, fTPClient.ftpPort, fTPClient.networkCredential)
            {
                DataConnectionType = FluentFTP.FtpDataConnectionType.AutoActive
            };

            var Flag = client.Upload(fileArray, (PathPrefix + filePath).Replace("//","/"), FluentFTP.FtpRemoteExists.Overwrite, true);
            client.Dispose();
            return Flag.IsSuccess();
        }
        public static void CreateDirectory(string filePath)
        {
            FTPClient fTPClient = new FTPClient();
            var client = new FluentFTP.FtpClient(fTPClient.host, fTPClient.ftpPort, fTPClient.networkCredential)
            {
                DataConnectionType = FluentFTP.FtpDataConnectionType.AutoActive
            };
            client.CreateDirectory(filePath, true);
            client.Dispose();
        }

    }
}
