﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace Mega.Garage.Common
{
    public class Image
    {
        public static Color GetMostUsedColor(Bitmap bit)
        {
            return GetMostUsedColors(bit).ToList().OrderByDescending(x => x.Value).FirstOrDefault().Key;
        }
        public static List<KeyValuePair<Color, int>> GetMostUsedColors(Bitmap bit)
        {
            Dictionary<Color, int> countDictionary = new Dictionary<Color, int>();

            for (int wid = 0; wid < bit.Width; wid++)
            {
                for (int he = 0; he < bit.Height; he++)
                {
                    Color currentColor = bit.GetPixel(wid, he);
                    int currentCount = (countDictionary.ContainsKey(currentColor) ? countDictionary[currentColor] : 0);
                    if (currentCount == 0)
                    {
                        countDictionary.Add(currentColor, 1);
                    }
                    else
                    {
                        countDictionary[currentColor] = currentCount + 1;
                    }
                }
            }
            List<KeyValuePair<Color, int>> l = countDictionary.OrderByDescending(o => o.Value).Take(5).ToList();
            return l;
        }
        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                try
                {
                    img.Save(ms, img.RawFormat);
                    return ms.ToArray();
                }
                catch
                {
                    img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    return ms.ToArray();
                }
            }
        }
        public static System.Drawing.Image ByteToImage(byte[] Byte)
        {
            if (Byte == null)
            {
                return null;
            }
            else
            {
                using (MemoryStream ms = new MemoryStream(Byte))
                {
                    return System.Drawing.Image.FromStream(ms);
                }
            }
        }
        public static void DrawImage(HttpResponse response, System.Drawing.Image img)
        {
            response.ContentType = GetMimeType(img);
            byte[] imgBytes = ImageToByte(img);
            using (MemoryStream ms = new MemoryStream(imgBytes))
            {
                img.Dispose();
                ms.WriteTo(response.OutputStream);
            }
        }
        public static System.Drawing.Image ResizeImage(byte[] imgbyte, int Width = -1, int Height = -1)
        {
            System.Drawing.Image img = ByteToImage(imgbyte);
            string MimeType = GetMimeType(img);
            using (MemoryStream mc = new MemoryStream())
            {
                System.Drawing.Bitmap bmp;
                if (Width == -1 && Height == -1)
                    bmp = new Bitmap(img);
                else
                {
                    decimal RateW = (decimal)img.Width / (decimal)img.Height;
                    decimal RateH = (decimal)img.Height / (decimal)img.Width;
                    if (Width <= 0)
                    {
                        Width = int.Parse((Math.Round((RateW * Height))).ToString());
                    }
                    if (Height <= 0)
                    {
                        Height = int.Parse((Math.Round((RateH * Width))).ToString());
                    }
                    bmp = new Bitmap(img, new Size(Width, Height));
                }
                
                bmp.Save(mc, img.RawFormat);
                return bmp;
            }
        }
        public static void DrawImage(HttpResponse response, byte[] imgbyte, int Width = -1, int Height = -1)
        {
            System.Drawing.Image img = ByteToImage(imgbyte);
            string MimeType = GetMimeType(img);
            response.ContentType = GetMimeType(img);
            using (MemoryStream mc = new MemoryStream())
            {
                System.Drawing.Bitmap bmp;
                if (Width == -1 && Height == -1)
                    bmp = new Bitmap(img);
                else
                {
                    decimal RateW = (decimal)img.Width / (decimal)img.Height;
                    decimal RateH = (decimal)img.Height / (decimal)img.Width;
                    if (Width <= 0)
                    {
                        Width = int.Parse((Math.Round((RateW * Height))).ToString());
                    }
                    if (Height <= 0)
                    {
                        Height = int.Parse((Math.Round((RateH * Width))).ToString());
                    }
                    bmp = new Bitmap(img, new Size(Width, Height));
                }
                bmp.Save(mc, System.Drawing.Imaging.ImageFormat.Jpeg);
                img.Dispose();
                mc.WriteTo(response.OutputStream);
            }
        }
        public static string GetMimeType(System.Drawing.Image i)
        {
            foreach (System.Drawing.Imaging.ImageCodecInfo codec in System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders())
            {
                if (codec.FormatID == i.RawFormat.Guid)
                    return codec.MimeType;
            }
            return string.Empty;
        }
        public static bool IsFileAnImage(string fileName)
        {
            return (
                       new System.Text.RegularExpressions.Regex(
                           "(.jpg|.png|.bmp|.gif|.jpeg)",
                           System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.IgnorePatternWhitespace)
                           .IsMatch(fileName));
        }
        public static bool IsFileAnImage(byte[] imgstream)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(imgstream, false))
                {
                    using (var img = System.Drawing.Image.FromStream(ms))
                    {
                        // ReSharper disable ObjectCreationAsStatement
                        new Bitmap(img);
                        // ReSharper restore ObjectCreationAsStatement
                        return true;
                    }
                }
            }
            catch
            {
                return false;
            }
        }
        public string ImageToBase64(System.Drawing.Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                string base64String = Convert.ToBase64String(ms.ToArray());
                return base64String;
            }
        }
        public System.Drawing.Image Base64ToImage(string str)
        {
            byte[] byt = Convert.FromBase64String(str);
            using (MemoryStream ms = new MemoryStream(byt))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
        public class PixleToCentiMeter
        {
            internal static bool ImageCheckerinCm(Stream str, double wh)
            {
                string err;
                return ImageCheckerinCm(new Bitmap(str), wh, wh, out err);
            }
            internal static bool ImageCheckerinCm(Stream str, double wh, out string err)
            {
                return ImageCheckerinCm(new Bitmap(str), wh, wh, out err);
            }
            internal static bool ImageCheckerinCm(Bitmap bitmap, double wh, out string err)
            {
                return ImageCheckerinCm(bitmap, wh, wh, out err);
            }
            internal static bool ImageCheckerinCm(string path, double wh, out string err)
            {
                return ImageCheckerinCm(path, wh, wh, out err);
            }
            internal static bool ImageCheckerinCm(string path, double w, double h, out string err)
            {
                return ImageCheckerinCm(path, w, h, 1.0, out err);
            }
            internal static bool ImageCheckerinCm(Bitmap bitmap, double w, double h, out string err)
            {
                return ImageCheckerinCm(bitmap, w, h, 1.0, out err);
            }
            internal static bool ImageCheckerinCm(string path, double w, double h, double disMatchRange, out string err)
            {
                Bitmap bitmap = new Bitmap(path);
                return ImageCheckerinCm(bitmap, w, h, disMatchRange, out err);
            }
            internal static bool ImageCheckerinCm(Bitmap bitmap, double w, double h, double disMatchRange, out string err)
            {
                double a = System.Math.Round((bitmap.HorizontalResolution + bitmap.VerticalResolution) / 2, 2);
                double width = System.Math.Round((bitmap.Width * 2.54) / a, 1);
                double height = System.Math.Round((bitmap.Height * 2.54) / a);
                if (w + disMatchRange < width || w - disMatchRange > width)
                {
                    if (w + disMatchRange < width)
                        err = "عرض تصویر علامت نمی تواند بیشتر از " + (w + disMatchRange) + " سانتیمتر باشد.";
                    else
                        err = "عرض تصویر علامت نمی تواند کمتر از " + (w - disMatchRange) + " سانتیمتر باشد.";
                    return false;
                }
                //else
                if (h + disMatchRange < height || h - disMatchRange > height)
                {
                    if (h + disMatchRange < height)
                        err = "طول تصویر علامت نمی تواند بیشتر از " + (h + disMatchRange) + " سانتیمتر باشد.";
                    else
                        err = "طول تصویر علامت نمی تواند کمتر از " + (h - disMatchRange) + " سانتیمتر باشد.";
                    return false;
                }
                // ReSharper disable RedundantIfElseBlock
                else
                // ReSharper restore RedundantIfElseBlock
                {
                    err = null;
                    return true;
                }
            }
        }
        public static System.Drawing.Image DrawText(String text, System.Drawing.Color textColor, System.Drawing.Color backColor)
        {
            int Height = 250;
            int Width = 250;
            System.Drawing.Image img = new System.Drawing.Bitmap(1, 1);
            System.Drawing.Graphics drawing = System.Drawing.Graphics.FromImage(img);
            System.Drawing.Font font = new System.Drawing.Font("Arial", 150);
            System.Drawing.SizeF textSize = drawing.MeasureString(text, font);
            img.Dispose();
            drawing.Dispose();
            img = new System.Drawing.Bitmap(Width, Height);
            drawing = System.Drawing.Graphics.FromImage(img);
            drawing.Clear(backColor);
            drawing.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            drawing.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            drawing.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
            drawing.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            System.Drawing.Brush textBrush = new System.Drawing.SolidBrush(textColor);
            drawing.DrawString(text.Split(' ')[0], font, textBrush, 70, -80);
            drawing.DrawString(text.Split(' ')[1], font, textBrush, 30, 30);
            drawing.Save();
            textBrush.Dispose();
            drawing.Dispose();
            return img;
        }
    }
}
