﻿using Mega.Garage.Logic.General.Services;
using Mega.Garage.Presentation.Areas.General.Data;
using Mega.Garage.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.General.Controllers
{
    public class SettingsController : BaseController
    {
        public ActionResult Index()
        {
            ViewBag.MandatoryDealer = SettingsService.IsMandatoryDealer();
            ViewBag.SendSmsToDealer = SettingsService.IsSendSmsToDealer();
            return View();
        }

        [HttpPost]
        public ActionResult Index(SettingsViewModel model)
        {
            SettingsService.SaveMandatoryDealer(model.MandatoryDealer);
            SettingsService.SaveSendSmsToDealer(model.SendSmsToDealer);
            return Index();
        }

    }
}