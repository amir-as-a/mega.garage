﻿namespace Mega.Garage.Presentation.Areas.General.Data
{
    public class SettingsViewModel
    {
        public bool MandatoryDealer { get; set; }
        public bool SendSmsToDealer { get; set; }
    }
}