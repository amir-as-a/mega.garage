﻿using Mega.Garage.Logic.Account.Services;
using Mega.Garage.Persistence.Account.Entities;
using Mega.Garage.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Account.Controllers
{
    public class UserController : BaseController
    {
        // GET: Account
        public ActionResult Index()
        {
            var Account = UserService.GetAllUsers();
            return View(Account.ToList());
        }

        public ActionResult Modify(int? id)
        {
            if (id != null && id > 0)
            {
                var dealer = UserService.GetUserById((int)id);
                return View(dealer);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(User user)
        {
            if (user.Id > 0)
                UserService.UpdateUser(user);
            else
                UserService.AddUser(user);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserService.DeleteUser(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ResetPassword(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(int id, string newPassword, string confirmNewPassword)
        {
            UserService.ResetPassword(id, newPassword, confirmNewPassword);
            return Index();
        }
    }
}