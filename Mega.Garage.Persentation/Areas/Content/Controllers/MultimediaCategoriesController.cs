﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class MultimediaCategoriesController : BaseController
//    {
//        // GET: MultimediaCategories
//        public ActionResult Index()
//        {
//            var multimediaCategories = MultiMediaService.GetAllMultimediaCategories();
//            return View(multimediaCategories.ToList());
//        }

//        // GET: MultimediaCategories/Details/5

//        // GET: MultimediaCategories/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: MultimediaCategories/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] MultimediaCategory multimediaCategory)
//        {
//            if (ModelState.IsValid)
//            {
//                MultiMediaService.AddMultimediaCategory(multimediaCategory);
//                return RedirectToAction("Index");
//            }

//            return View(multimediaCategory);
//        }

//        // GET: MultimediaCategories/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var multimediaCategory = MultiMediaService.GetMultimediaCategoryById((int)id);
//            if (multimediaCategory == null)
//            {
//                return HttpNotFound();
//            }
//            return View(multimediaCategory);
//        }

//        // POST: MultimediaCategories/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] MultimediaCategory multimediaCategory)
//        {
//            if (ModelState.IsValid)
//            {
//                MultiMediaService.UpdateMultimediaCategory(multimediaCategory);
//                return RedirectToAction("Index");
//            }
//            return View(multimediaCategory);
//        }

//        // GET: MultimediaCategories/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var multimediaCategory = MultiMediaService.GetMultimediaCategoryById((int)id);
//            if (multimediaCategory == null)
//            {
//                return HttpNotFound();
//            }
//            return View(multimediaCategory);
//        }

//        // POST: MultimediaCategories/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var multimediaCategory = MultiMediaService.GetMultimediaCategoryById(id);
//            MultiMediaService.DeleteMultimediaCategory(id);
//            return RedirectToAction("Index");
//        }

//    }
//}