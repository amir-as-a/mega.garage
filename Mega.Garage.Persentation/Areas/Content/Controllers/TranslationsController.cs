﻿//using Mega.Garage.Common;
//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Logic.Content.Validation;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Areas.Content.Models;
//using Mega.Garage.Presentation.Controllers;
//using Mega.Garage.Presentation.Model;
//using System;
//using System.Collections.Generic;
//using System.Net;
//using System.Web.Mvc;
//using System.Linq;
//using FluentValidation;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class TranslationsController : BaseController
//    {
//        // GET: Translations
//        public ActionResult Index()
//        {
//            ViewBag.Messages = TempData["Messages"];
//            return View();
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Index([Bind(Include = "Search, Translations")] TranslationIndex viewModel)
//        {
//            if (!ModelState.IsValid)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var result = TranslationService.GetTranslationsBySearchQuery(viewModel.Search);
//            if (result == null)
//            {
//                return HttpNotFound();
//            }
//            viewModel.Translations = result;
//            return View(viewModel);
//        }

//        public ActionResult Create()
//        {
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title");
//            return View("Create");
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Description,LanguageId,ComponentPath,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Translation translation)
//        {
//            if (ModelState.IsValid)
//            {

//                try
//                {
//                    TranslationService.AddTranslation(translation);

//                    TempData["Messages"] = new List<Message>() { new Message { Type = MessageType.Success, Content = "عملیات با موفقیت انجام شد." } };
//                    return RedirectToAction("Index");
//                }
//                catch (Exception ex)
//                {
//                    return ReturnException(ex);
//                }
//            }

//            TempData["Messages"] = new List<Message>() { new Message { Type = MessageType.Error, Content = "عملیات ناموفق." } };
//            return RedirectToAction("Index");
//        }

//        public ActionResult Edit(int? id)
//        {
//            var translation = TranslationService.GetTranslationById((int)id);

//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", translation.LanguageId);
//            return View("Edit", translation);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Description,LanguageId,ComponentPath,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Translation translation)
//        {
//            if (ModelState.IsValid)
//            {
//                try
//                {
//                    TranslationService.UpdateTranslation(translation);

//                    TempData["Messages"] = new List<Message>() { new Message { Type = MessageType.Success, Content = "عملیات با موفقیت انجام شد." } };
//                    return RedirectToAction("Index");
//                }
//                catch (Exception ex)
//                {
//                    return ReturnException(ex);
//                }
//            }

//            TempData["Messages"] = new List<Message>() { new Message { Type = MessageType.Error, Content = "عملیات ناموفق." } };
//            return RedirectToAction("Index");
//        }

//        public ActionResult Delete(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var tag = TranslationService.GetTranslationById((int)id);
//            if (tag == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tag);
//        }

//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            TranslationService.DeleteTranslation(id);
//            return RedirectToAction("Index");
//        }

//        public PartialViewResult Search(string query)
//        {
//            if (string.IsNullOrEmpty(query))
//            {
//                ViewBag.Messages = new List<Message>() { new Message { Type = MessageType.Error, Content = "فیلد جستجو نباید خالی باشد." } };
//                return PartialView("_Search", new TranslationIndex { Translations = new List<Translation>() });
//                //return RedirectToAction("Index");
//            }

//            var translations = TranslationService.GetTranslationsBySearchQuery(query);
//            var result = new TranslationIndex
//            {
//                Translations = translations,
//                Search = query
//            };

//            return PartialView("_Search", result);
//        }


//    }
//}