﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class IndicesController : BaseController
//    {
//        // GET: Indices
//        public ActionResult Index()
//        {
//            var indices = EntryService.GetAllIndices();
//            return View(indices.ToList());
//        }

//        // GET: Indices/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var index = EntryService.GetIndexById((int)id);
//            if (index == null)
//            {
//                return HttpNotFound();
//            }
//            return View(index);
//        }

//        // GET: Indices/Create
//        public ActionResult Create()
//        {
//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            return View();
//        }

//        // POST: Indices/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,EnglishTitle,Description,MaxNumberOfIndexItems,ShowPublishDate,ShowIndex,MultimediaLibraryId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Index index)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.AddIndex(index);
//                return RedirectToAction("Index");
//            }

//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            return View(index);
//        }

//        // GET: Indices/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var index = EntryService.GetIndexById((int)id);
//            if (index == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            return View(index);
//        }

//        // POST: Indices/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,EnglishTitle,Description,MaxNumberOfIndexItems,ShowPublishDate,ShowIndex,MultimediaLibraryId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Index index)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.UpdateIndex(index);
//                return RedirectToAction("Index");
//            }
//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            return View(index);
//        }

//        // GET: Indices/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var index = EntryService.GetIndexById((int)id);
//            if (index == null)
//            {
//                return HttpNotFound();
//            }
//            return View(index);
//        }

//        // POST: Indices/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var index = EntryService.GetIndexById((int)id);
//            EntryService.DeleteIndex(id);
//            return RedirectToAction("Index");
//        }

//    }
//}