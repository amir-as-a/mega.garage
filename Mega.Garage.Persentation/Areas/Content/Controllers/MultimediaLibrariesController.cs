﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class MultimediaLibrariesController : BaseController
//    {

//        // GET: MultimediaLibraries
//        public ActionResult Index(string returnUrl = null, long id = 0, string actionName = null)
//        {
//            var multimediaLibraries = MultiMediaService.GetAllMultimediaLibraries();

//            if (id == 0)
//            {
//                ViewBag.RecievedId = 0;
//                return View(multimediaLibraries.ToList());
//            }

//            ViewBag.RecievedId = id;
//            ViewBag.ActionName = actionName;
//            ViewBag.ReturnUrl = returnUrl;

//            return View(multimediaLibraries.ToList());


//        }

//        // GET: MultimediaLibraries/Create
//        public ActionResult Create()
//        {
//            ViewBag.MultimediaCategoryId = new SelectList(MultiMediaService.GetAllMultimediaCategories(), "Id", "Title");
//            return View();
//        }

//        // POST: MultimediaLibraries/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,SubstituteText,Description,Width,Height,Link,MultimediaCategoryId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] MultimediaLibrary multimediaLibrary)
//        {
//            if (ModelState.IsValid)
//            {
//                MultiMediaService.AddMultimediaLibrary(multimediaLibrary);
//                return RedirectToAction("Index");
//            }

//            ViewBag.MultimediaCategoryId = new SelectList(MultiMediaService.GetAllMultimediaCategories(), "Id", "Title");
//            return View(multimediaLibrary);
//        }

//        // GET: MultimediaLibraries/Edit/5
//        public ActionResult Edit(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var multimediaLibrary = MultiMediaService.GetMultimediaLibraryById((long)id);
//            if (multimediaLibrary == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.MultimediaCategoryId = new SelectList(MultiMediaService.GetAllMultimediaCategories(), "Id", "Title");
//            return View(multimediaLibrary);
//        }

//        // POST: MultimediaLibraries/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,SubstituteText,Description,Width,Height,Link,MultimediaCategoryId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] MultimediaLibrary multimediaLibrary)
//        {
//            if (ModelState.IsValid)
//            {
//                MultiMediaService.UpdateMultimediaLibrary(multimediaLibrary);
//                return RedirectToAction("Index");
//            }
//            ViewBag.MultimediaCategoryId = new SelectList(MultiMediaService.GetAllMultimediaCategories(), "Id", "Title");
//            return View(multimediaLibrary);
//        }

//        // GET: MultimediaLibraries/Delete/5
//        public ActionResult Delete(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var multimediaLibrary = MultiMediaService.GetMultimediaLibraryById((long)id);
//            if (multimediaLibrary == null)
//            {
//                return HttpNotFound();
//            }

//            return View("Delete");
//        }

//        // POST: MultimediaLibraries/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long id)
//        {
//            var multimediaLibrary = MultiMediaService.GetMultimediaLibraryById((long)id);
//            MultiMediaService.DeleteMultimediaLibrary(id);
//            return RedirectToAction("Index");
//        }
//    }
//}