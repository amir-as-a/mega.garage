﻿
using Mega.Garage.Presentation.Controllers;
using System.Net;
using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
    //public class CategoriesController : BaseController
    //{
        // GET: Categories
        //public ActionResult Index()
        //{
        //    var categories = ContentCategoryService.GetDefinedCategories();
        //    return View(categories);
        //}

        // GET: Categories/Create
        //public ActionResult Create()
        //{
        //    ViewBag.CategoryParentId = new SelectList(ContentCategoryService.GetMainCategories(), "Id", "Title");
        //    return View();
        //}

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,Title,EnglishTitle,CategoryCode,Description,CategoryParentId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] ContentCategory category)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        ContentCategoryService.AddContentCategory(category);
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.CategoryParentId = new SelectList(ContentCategoryService.GetMainCategories(), "Id", "Title");
        //    return View(category);
        //}

        // GET: Categories/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    var category = ContentCategoryService.GetContentCategoryById((int)id);
        //    if (category == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.CategoryParentId = new SelectList(ContentCategoryService.GetMainCategories(), "Id", "Title", category.CategoryParentId);
        //    return View(category);
        //}

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,Title,EnglishTitle,CategoryCode,Description,CategoryParentId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] ContentCategory category)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        ContentCategoryService.UpdateCategory(category);
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.Categories = new SelectList(ContentCategoryService.GetMainCategories(), "Id", "Title", category.CategoryParentId);
        //    return View(category);
        //}

        // GET: Categories/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var category = ContentCategoryService.GetContentCategoryById((int)id);
//            if (category == null)
//            {
//                return HttpNotFound();
//            }

//            if (ContentCategoryService.CategoryIsInUse((int)id))
//                ViewBag.CategoryIsInUse = true;

//            else
//                ViewBag.CategoryIsInUse = false;

//            return View("Delete");
//        }

//        // POST: Categories/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var deleted = ContentCategoryService.DeleteCategory(id);
//            if (deleted)
//                return RedirectToAction("Index");
//            else
//            {
//                ViewBag.Deleted = false;
//                return View("Delete");
//            }
//        }

//    }
//}