﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class TablesController : BaseController
//    {
//        // GET: Tables
//        public ActionResult Index()
//        {
//            var tables = TableService.GetAllTables();
//            return View(tables.ToList());
//        }

//        // GET: Tables/Create
//        public ActionResult Create()
//        {
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title");
//            return View("Create");
//        }

//        // POST: Tables/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Description,LanguageId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Table table)
//        {
//            if (ModelState.IsValid)
//            {
//                TableService.AddTable(table);
//                return RedirectToAction("Index");
//            }

//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", table.LanguageId);
//            return View("Create", table);
//        }

//        // GET: Tables/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Table table = TableService.GetTableById((int)id);
//            if (table == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", table.LanguageId);
//            return View("Edit", table);
//        }

//        // POST: Tables/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Description,LanguageId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Table table)
//        {
//            if (ModelState.IsValid)
//            {
//                TableService.UpdateTable(table);
//                return RedirectToAction("Index", "Tables");
//            }
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", table.LanguageId);
//            return View("Edit", table);
//        }

//        // GET: Tables/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            Table table = TableService.GetTableById((int)id);
//            if (table == null)
//            {
//                return HttpNotFound();
//            }

//            return View("Delete");
//        }

//        // POST: Tables/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            TableService.DeleteTable(id);
//            return RedirectToAction("Index");
//        }

//    }
//}