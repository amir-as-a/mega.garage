﻿//using Mega.Garage.Logic.Catalog.Services;
//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class ProgressbarStepsController : BaseController
//    {
//        // GET: ProgressbarSteps
//        public ActionResult Index()
//        {
//            var progressbarSteps = ProgressbarService.GetAllProgressbarSteps();
//            return View(progressbarSteps.ToList());
//        }

//        // GET: ProgressbarSteps/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var progressbarStep = ProgressbarService.GetProgressbarStepById((int)id);
//            if (progressbarStep == null)
//            {
//                return HttpNotFound();
//            }
//            return View(progressbarStep);
//        }

//        // GET: ProgressbarSteps/Create
//        public ActionResult Create()
//        {
//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            ViewBag.ProgressbarId = new SelectList(ProgressbarService.GetAllProgressbars(), "Id", "Title");
//            return View();
//        }

//        // POST: ProgressbarSteps/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Description,MultimediaLibraryId,ProgressbarId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] ProgressbarStep progressbarStep)
//        {
//            if (ModelState.IsValid)
//            {
//                ProgressbarService.AddProgressbarStep(progressbarStep);
//                return RedirectToAction("Index");
//            }

//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            ViewBag.ProgressbarId = new SelectList(ProgressbarService.GetAllProgressbars(), "Id", "Title");
//            return View(progressbarStep);
//        }

//        // GET: ProgressbarSteps/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var progressbarStep = ProgressbarService.GetProgressbarStepById((int)id);
//            if (progressbarStep == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            ViewBag.ProgressbarId = new SelectList(ProgressbarService.GetAllProgressbars(), "Id", "Title");
//            return View(progressbarStep);
//        }

//        // POST: ProgressbarSteps/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Description,MultimediaLibraryId,ProgressbarId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] ProgressbarStep progressbarStep)
//        {
//            if (ModelState.IsValid)
//            {
//                ProgressbarService.UpdateProgressbarStep(progressbarStep);
//                return RedirectToAction("Index");
//            }
//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            ViewBag.ProgressbarId = new SelectList(ProgressbarService.GetAllProgressbars(), "Id", "Title");
//            return View(progressbarStep);
//        }

//        // GET: ProgressbarSteps/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var progressbarStep = ProgressbarService.GetProgressbarStepById((int)id);
//            if (progressbarStep == null)
//            {
//                return HttpNotFound();
//            }
//            return View(progressbarStep);
//        }

//        // POST: ProgressbarSteps/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var progressbarStep = ProgressbarService.GetProgressbarById((int)id);
//            ProgressbarService.DeleteProgressbarStep(id);
//            return RedirectToAction("Index");
//        }

//    }
//}