﻿
using Mega.Garage.Presentation.Controllers;
using System.Net;
using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class AlertsController : BaseController
//    {
//        // GET: Alerts
//        public ActionResult Index()
//        {
//            return View(AlertService.GetAllAlerts());
//        }

//        // GET: Alerts/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var alert = AlertService.GetAlertById((int)id);
//            if (alert == null)
//            {
//                return HttpNotFound();
//            }
//            return View(alert);
//        }

//        // GET: Alerts/Create
//        public ActionResult Create()
//        {
//            ViewBag.CategoryId = new SelectList(ContentCategoryService.GetAllEntryCategories(), "Id", "Title");
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title");
//            return View();
//        }

//        // POST: Alerts/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,EnglishTitle,Description,LanguageId,CategoryId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Alert alert)
//        {
//            if (ModelState.IsValid)
//            {
//                AlertService.AddAlert(alert);
//                return RedirectToAction("Index");
//            }

//            ViewBag.CategoryId = new SelectList(ContentCategoryService.GetAllContentCategories(), "Id", "Title", alert.CategoryId);
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", alert.LanguageId);
//            return View(alert);
//        }

//        // GET: Alerts/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var alert = AlertService.GetAlertById((int)id);
//            if (alert == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.CategoryId = new SelectList(ContentCategoryService.GetAllContentCategories(), "Id", "Title", alert.CategoryId);
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", alert.LanguageId);
//            return View(alert);
//        }

//        // POST: Alerts/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,EnglishTitle,Description,LanguageId,CategoryId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Alert alert)
//        {
//            if (ModelState.IsValid)
//            {
//                AlertService.UpdateAlert(alert);
//                return RedirectToAction("Index");
//            }
//            ViewBag.CategoryId = new SelectList(ContentCategoryService.GetAllContentCategories(), "Id", "Title", alert.CategoryId);
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", alert.LanguageId);
//            return View(alert);
//        }

//        // GET: Alerts/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var alert = AlertService.GetAlertById((int)id);
//            if (alert == null)
//            {
//                return HttpNotFound();
//            }
//            return View(alert);
//        }

//        // POST: Alerts/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            AlertService.DeleteAlert(id);
//            return RedirectToAction("Index");
//        }

//    }
//}