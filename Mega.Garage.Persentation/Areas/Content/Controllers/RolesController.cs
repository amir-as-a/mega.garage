﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class RolesController : BaseController
//    {

//        // GET: Roles
//        public ActionResult Index()
//        {
//            var roles = AuthorService.GetAllRoles();
//            return View(roles.ToList());
//        }

//        // GET: Roles/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var role = AuthorService.GetRoleById((int)id);
//            if (role == null)
//            {
//                return HttpNotFound();
//            }
//            return View(role);
//        }

//        // GET: Roles/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: Roles/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Role role)
//        {
//            if (ModelState.IsValid)
//            {
//                AuthorService.AddRole(role);
//                return RedirectToAction("Index");
//            }

//            return View(role);
//        }

//        // GET: Roles/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var role = AuthorService.GetRoleById((int)id);
//            if (role == null)
//            {
//                return HttpNotFound();
//            }
//            return View(role);
//        }

//        // POST: Roles/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Role role)
//        {
//            if (ModelState.IsValid)
//            {
//                AuthorService.UpdateRole(role);
//                return RedirectToAction("Index");
//            }
//            return View(role);
//        }

//        // GET: Roles/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var role = AuthorService.GetRoleById((int)id);
//            if (role == null)
//            {
//                return HttpNotFound();
//            }
//            return View(role);
//        }

//        // POST: Roles/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var role = AuthorService.GetRoleById(id);
//            AuthorService.DeleteRole(id);
//            return RedirectToAction("Index");
//        }
//    }
//}