﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class PositionsController : BaseController
//    {
//        // GET: Positions
//        public ActionResult Index()
//        {
//            var positions = EntryService.GetAllPositions();
//            return View(positions.ToList());
//        }

//        // GET: Positions/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var position = EntryService.GetPositionById((int)id);
//            if (position == null)
//            {
//                return HttpNotFound();
//            }
//            return View(position);
//        }

//        // GET: Positions/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: Positions/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Position position)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.AddPosition(position);
//                return RedirectToAction("Index");
//            }

//            return View(position);
//        }

//        // GET: Positions/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var position = EntryService.GetPositionById((int)id);
//            if (position == null)
//            {
//                return HttpNotFound();
//            }
//            return View(position);
//        }

//        // POST: Positions/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Position position)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.UpdatePosition(position);
//                return RedirectToAction("Index");
//            }
//            return View(position);
//        }

//        // GET: Positions/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var position = EntryService.GetPositionById((int)id);
//            if (position == null)
//            {
//                return HttpNotFound();
//            }
//            return View(position);
//        }

//        // POST: Positions/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var position = EntryService.GetPositionById(id);
//            EntryService.DeletePosition(id);
//            return RedirectToAction("Index");
//        }
//    }
//}