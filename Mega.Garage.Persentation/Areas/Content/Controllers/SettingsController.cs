﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class SettingsController : BaseController
//    {

//        // GET: Settings
//        public ActionResult Index()
//        {
//            var settings = SettingService.GetAllSettings();
//            return View(settings.ToList());
//        }

//        // GET: Settings/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var setting = SettingService.GetSettingById((int)id);
//            if (setting == null)
//            {
//                return HttpNotFound();
//            }
//            return View(setting);
//        }

//        // GET: Settings/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: Settings/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Value,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Setting setting)
//        {
//            if (ModelState.IsValid)
//            {
//                SettingService.AddSetting(setting);
//                return RedirectToAction("Index");
//            }

//            return View(setting);
//        }

//        // GET: Settings/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var setting = SettingService.GetSettingById((int)id);
//            if (setting == null)
//            {
//                return HttpNotFound();
//            }
//            return View(setting);
//        }

//        // POST: Settings/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Value,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Setting setting)
//        {
//            if (ModelState.IsValid)
//            {
//                SettingService.UpdateSetting(setting);
//                return RedirectToAction("Index");
//            }
//            return View(setting);
//        }

//        // GET: Settings/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var setting = SettingService.GetSettingById((int)id);
//            if (setting == null)
//            {
//                return HttpNotFound();
//            }
//            return View(setting);
//        }

//        // POST: Settings/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            SettingService.DeleteSetting(id);
//            return RedirectToAction("Index");
//        }

//    }
//}