﻿//using Mega.Garage.Logic.Catalog.Services;
//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Areas.Content.Models;
//using Mega.Garage.Presentation.Controllers;
//using System;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class EntriesController : BaseController
//    {
//        // GET: Entries
//        public ActionResult Index()
//        {
//            var entries = EntryService.GetDefinedEntries();
//            return View(entries.ToList());
//        }

//        // GET: Entries/Details/5
//        public ActionResult Details(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var entry = EntryService.GetEntryById((long)id);
//            if (entry == null)
//            {
//                return HttpNotFound();
//            }
//            return View(entry);
//        }

//        // GET: Entries/Create
//        public ActionResult Create()
//        {
//            var entry = new Entry();

//            entry = EntryService.AddEntry(entry);

//            EntryCreate entryCreate = new EntryCreate
//            {
//                EntryIndex = new EntryIndex
//                {
//                    Entry = EntryService.GetEntryById((long)entry.Id)
//                },
//                EntryTag = new EntryTag
//                {
//                    Entry = EntryService.GetEntryById((long)entry.Id)
//                },
//                IndexSelectList = new MultiSelectList(EntryService.GetAllIndices(), "Id", "Title"),
//                TagSelectList = new MultiSelectList(EntryService.GetAllTags(), "Id", "Title")
//            };

//            ViewBag.Code = Guid.NewGuid().ToString();
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", entryCreate.EntryIndex.Entry.LanguageId);
//            ViewBag.CategoryId = new SelectList(CategoryService.GetSubjectCategories(), "Id", "Title");
//            ViewBag.EntryTypeId = new SelectList(ContentCategoryService.GetAllEntryCategories(), "Id", "Title", entryCreate.EntryIndex.Entry.EntryTypeId);
//            ViewBag.AuthorId = new SelectList(AuthorService.GetAllAuthors(), "Id", "Title", entryCreate.EntryIndex.Entry.LanguageId);
//            ViewBag.SliderId = new SelectList(SliderService.GetAllSliders(), "Id", "Title", entryCreate.EntryIndex.Entry.SliderId);
//            ViewBag.EntryParentId = new SelectList(EntryService.GetAllEntries(), "Id", "Title", entryCreate.EntryIndex.Entry.EntryParentId);

//            return View("_IndexList", entryCreate);
//        }

//        // POST: Entries/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "EntryIndex,EntryTag,IndexSelectList,SelectedIndex,TagSelecList,SelectedTag")] EntryCreate entryCreate)
//        {
//            var indexList = entryCreate.SelectedIndex;
//            var tagList = entryCreate.SelectedTag;
//            if (ModelState.IsValid)
//            {
//                if (indexList != null)
//                {
//                    foreach (var index in indexList)
//                    {
//                        EntryService.AddEntryToIndex(entryCreate.EntryIndex.Entry.Id, index);
//                    }
//                }

//                if (tagList != null)
//                {
//                    foreach (var tag in tagList)
//                    {
//                        EntryService.AddTagToEntry(entryCreate.EntryTag.Entry.Id, tag);
//                    }
//                }

//                EntryService.UpdateEntry(entryCreate.EntryIndex.Entry);
//                return RedirectToAction("Index");
//            }

//            ViewBag.IndexList = entryCreate.IndexSelectList;
//            ViewBag.AuthorId = new SelectList(AuthorService.GetAllAuthors(), "Id", "Title", entryCreate.EntryIndex.Entry.AuthorId);
//            ViewBag.CategoryId = new SelectList(CategoryService.GetSubjectCategories(), "Id", "Title");
//            ViewBag.EntryTypeId = new SelectList(ContentCategoryService.GetAllEntryCategories(), "Id", "Title", entryCreate.EntryIndex.Entry.EntryTypeId);
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", entryCreate.EntryIndex.Entry.LanguageId);
//            ViewBag.SliderId = new SelectList(SliderService.GetAllSliders(), "Id", "Title", entryCreate.EntryIndex.Entry.SliderId);
//            ViewBag.EntryParentId = new SelectList(EntryService.GetAllEntries(), "Id", "Title", entryCreate.EntryIndex.Entry.EntryParentId);

//            return View("_IndexList", entryCreate);
//        }

//        // GET: Entries/Edit/5
//        public ActionResult Edit(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var entry = EntryService.GetEntryById((long)id);
//            if (entry == null)
//            {
//                return HttpNotFound();
//            }

//            ViewBag.EntryId = id;
//            ViewBag.AuthorId = new SelectList(AuthorService.GetAllAuthors(), "Id", "Title", entry.AuthorId);
//            ViewBag.CategoryId = new SelectList(CategoryService.GetSubjectCategories(), "Id", "Title");
//            ViewBag.EntryTypeId = new SelectList(ContentCategoryService.GetAllEntryCategories(), "Id", "Title", entry.EntryTypeId);
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", entry.LanguageId);
//            ViewBag.SliderId = new SelectList(SliderService.GetAllSliders(), "Id", "Title", entry.SliderId);
//            ViewBag.EntryParentId = new SelectList(EntryService.GetAllEntries(), "Id", "Title", entry.EntryParentId);

//            return View("Edit", entry);
//        }

//        // POST: Entries/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Code,Title,BriefTitle,EnglishTitle,BriefDescription,EnglishBriefDescription,FullText,HtmlCode,Link,VisitCount,CommentAllowed,CommentCount,ShowPublishDate,LanguageId,EntryTypeId,EntryParentId,AuthorId,CategoryId,VisibilityStatus,PublishStatus,SliderId,PublishDate,ExpirationDate,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Entry entry)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.UpdateEntry(entry);
//                return RedirectToAction("Index");
//            }

//            //ViewBag.IndexList = entryCreate.IndexSelectList;
//            ViewBag.AuthorId = new SelectList(AuthorService.GetAllAuthors(), "Id", "Title", entry.AuthorId);
//            ViewBag.CategoryId = new SelectList(CategoryService.GetSubjectCategories(), "Id", "Title");
//            ViewBag.EntryTypeId = new SelectList(ContentCategoryService.GetAllEntryCategories(), "Id", "Title", entry.EntryTypeId);
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title", entry.LanguageId);
//            ViewBag.SliderId = new SelectList(SliderService.GetAllSliders(), "Id", "Title", entry.SliderId);
//            ViewBag.EntryParentId = new SelectList(EntryService.GetAllEntries(), "Id", "Title", entry.EntryParentId);

//            return View("Edit", entry);
//        }

//        // GET: Entries/Delete/5
//        public ActionResult Delete(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var entry = EntryService.GetEntryById((long)id);
//            if (entry == null)
//            {
//                return HttpNotFound();
//            }
//            return View(entry);
//        }

//        // POST: Entries/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long id)
//        {
//            EntryService.DeleteEntry(id);
//            return RedirectToAction("Index");
//        }
//    }
//}