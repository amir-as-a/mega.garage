﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class SlidersController : BaseController
//    {

//        // GET: Sliders
//        public ActionResult Index()
//        {
//            return View(SliderService.GetAllSliders());
//        }

//        // GET: Sliders/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: Sliders/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        public JsonResult Create([Bind(Include = "Id,Title,SubstituteText,Description,Duration,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Slider slider)
//        {
//            object[] result = new object[2];
//            if (ModelState.IsValid)
//            {
//                Slider sliderEntity = SliderService.AddSlider(slider);
//                if (sliderEntity != null)
//                {
//                    result[0] = "true";
//                    result[1] = sliderEntity;
//                    return Json(result, JsonRequestBehavior.AllowGet);
//                }
//            }
//            result[0] = "false";
//            result[1] = null;
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }

//        // GET: Sliders/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var slider = SliderService.GetSliderById((int)id);
//            if (slider == null)
//            {
//                return HttpNotFound();
//            }
//            return View(slider);
//        }

//        // POST: Sliders/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,SubstituteText,Description,Duration,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Slider slider)
//        {
//            if (ModelState.IsValid)
//            {
//                SliderService.UpdateSlider(slider);
//                return RedirectToAction("Index");
//            }
//            return View(slider);
//        }

//        // GET: Sliders/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var setting = SliderService.GetSliderById((int)id);
//            if (setting == null)
//            {
//                return HttpNotFound();
//            }
//            return View(setting);
//        }

//        // POST: Sliders/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var slider = SliderService.GetSliderById((int)id);

//            SliderService.DeleteSlider(id);
//            return RedirectToAction("Index");
//        }

//    }
//}