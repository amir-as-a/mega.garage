﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class TableValuesController : BaseController
//    {
//        // GET: TableValues
//        public ActionResult Index(int tableId)
//        {
//            ViewBag.TableId = tableId;
//            var tableColumns = TableService.GetTableColumnByTableId(tableId);
//            var table = TableService.GetTableById(tableId);
//            ViewBag.TableTitle = table.Title;

//            if (tableColumns == null)
//            {
//                ViewBag.TableColumnCount = 0;
//                ViewBag.Massage = ("جدول هیچ محتوایی ندارد!");
//                return View();
//            }
//            else
//            {
//                ViewBag.TableColumn = tableColumns;
//            }

//            var tableRows = TableService.GetTableRowsByTableId(tableId);
//            ViewBag.TableRow = tableRows;

//            var tableValues = TableService.GetTableValueByTableId(tableId);
//            ViewBag.TableValue = tableValues;

//            if (tableValues == null)
//            {
//                ViewBag.Massage = ("هیچ داده ای برای نمایش وجود ندارد!");
//                return View();
//            }

//            return View(tableValues.ToList());
//        }

//        // GET: TableValues/Details/5
//        public ActionResult Details(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            TableValue tableValue = TableService.GetTableValueById((long)id);
//            if (tableValue == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tableValue);
//        }

//        // GET: TableValues/Create
//        public ActionResult Create(int tableId)
//        {
//            if (tableId == 0)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var existingTableValues = TableService.GetTableValueByTableId(tableId);

//            var tableColumns = TableService.GetTableColumnByTableId(tableId);

//            if (tableColumns != null)
//                ViewBag.ColumnId = new SelectList(tableColumns, "Id", "ColumnName");
//            else
//            {
//                ViewBag.Message = "ابتدا برای جدول ستون ایجاد کنید!";
//                ViewBag.TableColumnCount = 0;
//                return RedirectToAction("Create", "TableColumns", tableId);
//            }

//            var tableRow = new TableRow
//            {
//                TableId = tableId
//            };

//            tableRow = TableService.AddTableRow(tableRow);

//            var tableValues = new List<TableValue>();

//            foreach (var column in tableColumns)
//            {
//                var tableValue = new TableValue
//                {
//                    TableId = tableId,
//                    ColumnId = column.Id,
//                    RowId = tableRow.Id,
//                    Value = string.Empty,
//                    TableColumn = TableService.GetTableColumnById(column.Id),
//                    TableRow = TableService.GetTableRowById(tableRow.Id)
//                };
//                tableValues.Add(tableValue);
//            }
//            ViewBag.TableValue = tableValues;
//            return PartialView("Create", tableValues);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create()
//        {
//            var allTableValueKeys = Request.Form.AllKeys;
//            const int numberOfColumns = 5;
//            var numberOfTableColumns = Convert.ToInt32(allTableValueKeys.GetValue(numberOfColumns));
//            var numberOfProperties = allTableValueKeys.Length - 2;
//            var tableValues = new List<List<string>>();
//            var tableValue = new List<TableValue>();

//            const int startIndex = 1;

//            var formValues = new List<string>();

//            for (var i = 0; i < numberOfTableColumns; i++)
//            {
//                tableValues.Add(new List<string>(new string[numberOfProperties]));
//                tableValue.Add(new TableValue());
//            }

//            for (var i = startIndex; i < allTableValueKeys.Length - 1; i++)
//            {
//                formValues.Add(Request.Form[i]);
//                var value = formValues[i - 1].Split(',');

//                for (var j = 0; j < value.Length; j++)
//                {
//                    tableValues[j][i - 1] = value[j];
//                }
//            }

//            if (ModelState.IsValid)
//            {

//                for (var i = 0; i < tableValues.Count; i++)
//                {
//                    tableValue[i].TableId = Convert.ToInt32(tableValues[i][0]);
//                    tableValue[i].RowId = Convert.ToInt64(tableValues[i][1]);
//                    tableValue[i].ColumnId = Convert.ToInt64(tableValues[i][2]);
//                    tableValue[i].Value = tableValues[i][3];
//                    tableValue[i].TableColumn = TableService.GetTableColumnById(tableValue[i].ColumnId);
//                    tableValue[i].TableRow = TableService.GetTableRowById(tableValue[i].RowId);
//                }
//                TableService.AddTableValue(tableValue);
//                return RedirectToAction("Index", new { tableId = tableValue[0].TableId });
//            }
//            return View("Create", new List<TableValue>());
//        }

//        // GET: TableValues/Edit/5
//        public ActionResult Edit(long? rowId, int tableId)
//        {
//            if (rowId == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var tableValues = TableService.GetTableValueByRowId((long)rowId);
//            if (tableValues == null)
//            {
//                return HttpNotFound();
//            }

//            ViewBag.ColumnId = new SelectList(TableService.GetTableColumnByTableId(tableId), "Id", "ColumnName");
//            return View("Edit", tableValues);
//        }

//        // POST: TableValues/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit()
//        {
//            var allTableValueKeys = Request.Form.AllKeys;
//            const int numberOfColumns = 7;
//            var numberOfTableColumns = Convert.ToInt32(allTableValueKeys.GetValue(numberOfColumns));
//            var numberOfProperties = allTableValueKeys.Length - 2;
//            var tableValues = new List<List<string>>();
//            var tableValue = new List<TableValue>();

//            const int startIndex = 1;

//            var formValues = new List<string>();

//            for (var i = 0; i < numberOfTableColumns; i++)
//            {
//                tableValues.Add(new List<string>(new string[numberOfProperties]));
//                tableValue.Add(new TableValue());
//            }

//            for (var i = startIndex; i < allTableValueKeys.Length - 1; i++)
//            {
//                formValues.Add(Request.Form[i]);
//                var value = formValues[i - 1].Split(',');

//                for (var j = 0; j < value.Length; j++)
//                {
//                    tableValues[j][i - 1] = value[j];
//                }
//            }

//            if (ModelState.IsValid)
//            {
//                for (var i = 0; i < tableValues.Count; i++)
//                {
//                    tableValue[i].Id = Convert.ToInt64(tableValues[i][0]);
//                    tableValue[i].TableId = Convert.ToInt32(tableValues[i][1]);
//                    tableValue[i].RowId = Convert.ToInt64(tableValues[i][2]);
//                    tableValue[i].ColumnId = Convert.ToInt64(tableValues[i][3]);
//                    tableValue[i].RowVersion = Convert.FromBase64String(tableValues[i][4]);
//                    tableValue[i].Value = tableValues[i][5];
//                    tableValue[i].TableColumn = TableService.GetTableColumnById(tableValue[i].ColumnId);
//                    tableValue[i].TableRow = TableService.GetTableRowById(tableValue[i].RowId);
//                }
//                TableService.UpdateTableValue(tableValue);
//                return RedirectToAction("Index", new { tableId = tableValue[0].TableId });
//            }
//            //ViewBag.ColumnId = new SelectList(TableService.GetTableColumnByTableId(tableValue.TableId), "Id", "ColumnName");
//            return View("Create", new List<TableValue>());
//        }

//        // GET: TableValues/Delete/5
//        public ActionResult Delete(long? rowId, int tableId)
//        {
//            ViewBag.TableId = tableId;
//            if (rowId == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var tableValue = TableService.GetTableValueByRowId((long)rowId);
//            if (tableValue == null)
//            {
//                return HttpNotFound();
//            }

//            return View("Delete");
//        }

//        // POST: TableValues/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long rowId)
//        {
//            TableService.DeleteTableValue(rowId);
//            var value = TableService.GetTableIdByRowId(rowId);
//            return RedirectToAction("Index", "TableValues", new { tableId = value });
//        }

//    }
//}