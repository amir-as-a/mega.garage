﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class TableRowsController : BaseController
//    {
//        // GET: TableRows
//        public ActionResult Index()
//        {
//            var tables = TableService.GetAllTableRows();
//            return View(tables.ToList());
//        }

//        // GET: TableRows/Details/5
//        public ActionResult Details(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TableRow tableRow = TableService.GetTableRowById((long)id);
//            if (tableRow == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tableRow);
//        }

//        // GET: TableRows/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: TableRows/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,TableId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] TableRow tableRow)
//        {
//            if (ModelState.IsValid)
//            {
//                TableService.AddTableRow(tableRow);
//                return RedirectToAction("Index");
//            }

//            return View(tableRow);
//        }

//        // GET: TableRows/Edit/5
//        public ActionResult Edit(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TableRow tableRow = TableService.GetTableRowById((long)id);
//            if (tableRow == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tableRow);
//        }

//        // POST: TableRows/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,TableId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] TableRow tableRow)
//        {
//            if (ModelState.IsValid)
//            {
//                TableService.UpdateTableRow(tableRow);
//                return RedirectToAction("Index");
//            }
//            return View(tableRow);
//        }

//        // GET: TableRows/Delete/5
//        public ActionResult Delete(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TableRow tableRow = TableService.GetTableRowById((long)id);
//            if (tableRow == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tableRow);
//        }

//        // POST: TableRows/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long id)
//        {
//            TableService.DeleteTableRow((long)id);
//            return RedirectToAction("Index");
//        }
//    }
//}