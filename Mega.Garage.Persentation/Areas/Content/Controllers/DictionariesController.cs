﻿//using Mega.Garage.Logic.Catalog.Services;
//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Presentation.Controllers;
//using Mega.Garage.Persistence.Content;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class DictionariesController : BaseController
//    {
//        // GET: Dictionaries
//        public ActionResult Index()
//        {
//            var dictionaries = DictionaryService.GetAllDictionaries();
//            return View(dictionaries.ToList());
//        }

//        // GET: Dictionaries/Create
//        public ActionResult Create()
//        {
//            ViewBag.CategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title");
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title");
//            return View();
//        }

//        // POST: Dictionaries/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Key,Value,CategoryId,LanguageId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Persistence.Content.Entities.Dictionary dictionary)
//        {
//            if (ModelState.IsValid)
//            {
//                DictionaryService.AddDictionary(dictionary);
//                return RedirectToAction("Index");
//            }

//            ViewBag.CategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title", dictionary.CategoryId);
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title");
//            return View(dictionary);
//        }

//        // GET: Dictionaries/Edit/5
//        public ActionResult Edit(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var dictionary = DictionaryService.GetDictionaryById((long)id);
//            if (dictionary == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.CategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title", dictionary.CategoryId);
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title");
//            return View(dictionary);
//        }

//        // POST: Dictionaries/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Key,Value,CategoryId,LanguageId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Persistence.Content.Entities.Dictionary dictionary)
//        {
//            if (ModelState.IsValid)
//            {
//                DictionaryService.UpdateDictionary(dictionary);
//                return RedirectToAction("Index");
//            }
//            ViewBag.CategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title", dictionary.CategoryId);
//            ViewBag.LanguageId = new SelectList(LanguageService.GetAllLanguages(), "Id", "Title");
//            return View(dictionary);
//        }

//        // GET: Dictionaries/Delete/5
//        public ActionResult Delete(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            Persistence.Content.Entities.Dictionary dictionary = DictionaryService.GetDictionaryById((long)id);
//            if (dictionary == null)
//            {
//                return HttpNotFound();
//            }
//            return View(dictionary);
//        }

//        // POST: Dictionaries/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long id)
//        {
//            DictionaryService.DeleteDictionary(id);
//            return RedirectToAction("Index");
//        }

//    }
//}