﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Collections.Generic;
//using System.IO;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class SliderMediaController : BaseController
//    {
//        private string newDirectory = "\\Image\\";

//        // GET: SliderMedias
//        public ActionResult Index(int sliderId)
//        {
//            ViewBag.SliderId = sliderId;
//            var sliderMedia = SliderService.GetSliderMediaBySliderId(sliderId);
//            return PartialView("_Index", sliderMedia);
//        }

//        // GET: SliderMedias/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var sliderMedia = SliderService.GetSliderMediaById((int)id);
//            if (sliderMedia == null)
//            {
//                return HttpNotFound();
//            }

//            return View(sliderMedia);
//        }

//        // GET: SliderMedias/Create
//        public ActionResult Create(int sliderId)
//        {
//            if (sliderId == 0)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var sliderMedia = new SliderMedia
//            {
//                SliderId = sliderId,
//                MultimediaLibraryId = 0
//            };

//            ViewData["sliderMedia"] = sliderMedia;
//            return PartialView("_Create", sliderMedia);
//        }

//        // POST: SliderMedias/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Description,ShowButton,ButtonText,Link,SliderId,MultimediaLibraryId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] SliderMedia sliderMedia)
//        {
//            if (sliderMedia.MultimediaLibraryId == 0)
//            {
//                return PartialView("_Create", sliderMedia);
//            }

//            if (ModelState.IsValid)
//            {
//                SliderService.AddSliderMedia(sliderMedia);
//                return RedirectToAction("Edit", "Sliders", new { id = sliderMedia.SliderId });
//            }

//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            ViewBag.SliderId = new SelectList(SliderService.GetAllSliders(), "Id", "Title");
//            return PartialView("_Create", sliderMedia);
//        }

//        public ActionResult SelectSliderMedia(int sliderId)
//        {
//            return RedirectToAction("Index", "MultimediaLibraries", new { returnUrl = Request.UrlReferrer.ToString(), id = (long)sliderId, actionName = "Create" });
//        }

//        public ActionResult ReturnSliderMedia(int sliderMediaId, int sliderId, string returnUrl)
//        {
//            ViewBag.SliderMediaId = sliderMediaId;

//            return Redirect(returnUrl);
//        }

//        // GET: SliderMedias/Edit/5
//        public ActionResult Edit(int? id, int sliderId)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            SliderMedia sliderMedia = SliderService.GetSliderMediaById((int)id);

//            if (sliderMedia == null)
//            {
//                return HttpNotFound();
//            }

//            List<string> FilePathImages = new List<string>();

//            string DirectoryNew = Server.MapPath(newDirectory);
//            if (!Directory.Exists(DirectoryNew))
//                Directory.CreateDirectory(DirectoryNew);
//            string[] filePaths = Directory.GetFiles(DirectoryNew);
//            string fileName = string.Empty;
//            foreach (var item in filePaths)
//            {
//                fileName = Path.GetFileName(item);
//                FilePathImages.Add("/Image/" + fileName);
//            }

//            sliderMedia.ImagePathList = FilePathImages;
//            ViewBag.SliderTitle = sliderMedia.Slider.Title;
//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            ViewBag.SliderId = sliderId;
//            return PartialView("_Edit", sliderMedia);
//        }

//        // POST: SliderMedias/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Description,ShowButton,ButtonText,Link,SliderId,MultimediaLibraryId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] SliderMedia sliderMedia)
//        {
//            if (ModelState.IsValid)
//            {
//                SliderService.UpdateSliderMedia(sliderMedia);
//                return RedirectToAction("Edit", "Sliders", new { id = sliderMedia.SliderId });
//            }
//            ViewBag.MultimediaLibraryId = new SelectList(MultiMediaService.GetAllMultimediaLibraries(), "Id", "Title");
//            ViewBag.SliderId = new SelectList(SliderService.GetAllSliders(), "Id", "Title");
//            return PartialView("_Index");
//        }

//        // GET: SliderMedias/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var sliderMedia = SliderService.GetSliderMediaById((int)id);
//            if (sliderMedia == null)
//            {
//                return HttpNotFound();
//            }
//            return View(sliderMedia);
//        }

//        // POST: SliderMedias/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var sliderMedia = SliderService.GetSliderMediaById((int)id);
//            SliderService.DeleteSliderMedia(id);
//            return RedirectToAction("Index");
//        }

//        public JsonResult UploadImage(FormCollection formCollection, HttpPostedFileBase file)
//        {
//            object[] result = new object[3];
//            file = Request.Files["filename"];
//            var fileName = Path.GetFileName(file.FileName);
//            var strFileExtension = Path.GetExtension(fileName).ToUpper();
//            if (((strFileExtension != ".JPG") &&
//                 (strFileExtension != ".JPEG") &&
//                 (strFileExtension != ".JPE")) &&
//                (strFileExtension != ".GIF") &&
//                (strFileExtension != ".PNG"))
//            {

//                result[1] = "پسوند مجاز برای آپلود عکس jpeg و png و gif میباشد!";
//                return Json(result);
//            }

//            string strContentType = file.ContentType.ToUpper();
//            if ((strContentType != "IMAGE/JPEG") && // Firefox!
//                (strContentType != "IMAGE/PJPEG") && // Internet Explorer!
//                (strContentType != "IMAGE/GIF") &&
//                (strContentType != "IMAGE/PNG"))
//            {
//                result[1] = "پسوند فایل تغییر داده شده است!";
//                return Json(result);
//            }
//            if (file.ContentLength == 0)
//            {

//                result[1] = "در حال حاضر امکان آپلود فایل وجو ندارد!";
//                return Json(result);
//            }
//            if (file.ContentLength > 500 * 1024)
//            {

//                result[1] = "حداکثر حجم مجاز برای آپلود 500 کیلوبایت میباشد";
//                return Json(result);
//            }
//            System.Drawing.Image oImage = System.Drawing.Image.FromStream(file.InputStream);
//            if ((oImage.Width > 800) || (oImage.Height > 900))
//            {
//                result[1] = "&nbsp;دقت داشته باشید حتما عکس ها در اندازه 800 در 900 پیکسل باشد (طول=800 و عرض=900)!";
//                return Json(result);
//            }

//            var strRootRelativePath = "~/Image/" + fileName + "/";
//            if (System.IO.File.Exists(Server.MapPath(strRootRelativePath) + Path.GetFileName(file.FileName)))
//            {
//                result[1] = "تصویری با این نام در سرور وجود دارد";
//                return Json(result);
//            }

//            string newFilename = string.Empty;
//            string physicalPath1 = string.Empty;
//            newFilename = fileName;
//            if (!string.IsNullOrEmpty(fileName))
//            {
//                newFilename = "/Image/" + newFilename;
//                string newDirectory = "\\Image\\";
//                newDirectory = Server.MapPath(newDirectory);
//                physicalPath1 = Server.MapPath(newFilename);
//                if (!Directory.Exists(newDirectory))
//                    Directory.CreateDirectory(newDirectory);
//                file.SaveAs(physicalPath1);
//                result[0] = "true";
//                result[1] = newFilename;
//                result[2] = fileName;
//            }
//            return Json(result);
//        }
//        [HttpPost]
//        public JsonResult DeleteImage(string pathImage)
//        {
//            string[] result = new string[2];
//            if (string.IsNullOrEmpty(pathImage))
//            {
//                result[0] = "false";
//                result[1] = "لطفا دوباره سعی نمایید";
//                return Json(result);
//            }
//            string phsicalPath = Server.MapPath(pathImage);
//            System.IO.File.Delete(phsicalPath);
//            result[0] = "true";
//            result[1] = "با موفقیت حذف شد";
//            return Json(result);
//        }

//        [HttpPost]

//        public JsonResult SearchImage(string imageName)
//        {
//            string DirectoryNew = Server.MapPath(newDirectory);
//            string[] filePaths = Directory.GetFiles(DirectoryNew);
//            List<string> filePathFinal = new List<string>();
//            List<string> fileNameFinal = new List<string>();
//            object[] resultFile = new object[3];
//            foreach (var item in filePaths)
//            {
//                string fileName = Path.GetFileName(item);
//                bool result = fileName.Contains(imageName);
//                if (result)
//                {
//                    filePathFinal.Add("/Image/" + fileName);
//                    fileNameFinal.Add(fileName);
//                }

//            }
//            if (filePathFinal.Count == 0 || filePathFinal == null)
//            {
//                resultFile[0] = "false";
//                resultFile[1] = null;
//            }
//            else
//            {
//                resultFile[0] = "true";
//                resultFile[1] = filePathFinal;
//                resultFile[2] = fileNameFinal;
//            }
//            return Json(resultFile, JsonRequestBehavior.AllowGet);
//        }
//    }
//}