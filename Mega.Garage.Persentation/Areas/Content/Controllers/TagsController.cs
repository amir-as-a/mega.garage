﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Net;
//using System.Web;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class TagsController : BaseController
//    {

//        // GET: Tags
//        public ActionResult Index()
//        {
//            return View(EntryService.GetAllTags());
//        }

//        // GET: Tags/Details/5
//        public ActionResult Details(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var tag = EntryService.GetTagById((long)id);
//            if (tag == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tag);
//        }

//        // GET: Tags/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: Tags/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,EnglishTag,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Tag tag)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.AddTag(tag);

//                return RedirectToAction("Index");
//            }

//            return View(tag);
//        }

//        // GET: Tags/Edit/5
//        public ActionResult Edit(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var tag = EntryService.GetTagById((long)id);
//            if (tag == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tag);
//        }

//        // POST: Tags/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,EnglishTag,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Tag tag)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.UpdateTag(tag);
//                return RedirectToAction("Index");
//            }
//            return View(tag);
//        }

//        // GET: Tags/Delete/5
//        public ActionResult Delete(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var tag = EntryService.GetTagById((long)id);
//            if (tag == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tag);
//        }

//        // POST: Tags/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long id)
//        {
//            var tag = EntryService.GetTagById((long)id);
//            EntryService.DeleteTag(id);
//            return RedirectToAction("Index");
//        }
//    }
//}