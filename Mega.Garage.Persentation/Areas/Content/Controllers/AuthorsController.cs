﻿
//using Mega.Garage.Presentation.Controllers;
//using System.Collections.Generic;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class AuthorsController : BaseController
//    {
//        // GET: Authors
//        public ActionResult Index()
//        {
//            var model = AuthorService.GetAllAuthors();

//            ViewBag.Roles = new SelectList(AuthorService.GetAllRoles(), "Id", "Title");
//            return View(model);
//        }

//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Index(IEnumerable<Author> model)
//        {
//            var roleId = int.Parse(Request.Form["Roles"]);
//            model = AuthorService.GetAuthorsByRoleId(roleId);

//            ViewBag.Roles = new SelectList(AuthorService.GetAllRoles(), "Id", "Title");
//            return View(model);
//        }

//        // GET: Authors/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var author = AuthorService.GetAuthorById((int)id);
//            if (author == null)
//            {
//                return HttpNotFound();
//            }
//            return View(author);
//        }

//        // GET: Authors/Create
//        public ActionResult Create()
//        {
//            ViewBag.Roles = new SelectList(AuthorService.GetAllRoles(), "Id", "Title");
//            return View();
//        }

//        // POST: Authors/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,UserId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Author author)
//        {
//            if (ModelState.IsValid)
//            {
//                var result = AuthorService.AddAuthor(author);
//                AuthorService.AddRoleToAuthor(result.Id, int.Parse(Request.Form["Roles"]));
//                return RedirectToAction("Index");
//            }

//            return View(author);
//        }

//        // GET: Authors/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var author = AuthorService.GetAuthorById((int)id);
//            if (author == null)
//            {
//                return HttpNotFound();
//            }
//            return View(author);
//        }

//        // POST: Authors/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,UserId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Author author)
//        {
//            if (ModelState.IsValid)
//            {
//                AuthorService.UpdateAuthor(author);
//                return RedirectToAction("Index");
//            }
//            return View(author);
//        }

//        // GET: Authors/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var author = AuthorService.GetAuthorById((int)id);
//            if (author == null)
//            {
//                return HttpNotFound();
//            }
//            return View(author);
//        }

//        // POST: Authors/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            AuthorService.DeleteAuthor(id);
//            return RedirectToAction("Index");
//        }

//    }
//}