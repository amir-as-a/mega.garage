﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class EntryTypesController : BaseController
//    {
//        // GET: EntryTypes
//        public ActionResult Index()
//        {
//            var entryTypes = EntryService.GetAllEntryTypes();
//            return View(entryTypes.ToList());
//        }

//        // GET: EntryTypes/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var entryType = EntryService.GetEntryTypeById((int)id);
//            if (entryType == null)
//            {
//                return HttpNotFound();
//            }
//            return View(entryType);
//        }

//        // GET: EntryTypes/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: EntryTypes/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] EntryType entryType)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.AddEntryType(entryType);
//                return RedirectToAction("Index");
//            }

//            return View(entryType);
//        }

//        // GET: EntryTypes/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var entryType = EntryService.GetEntryTypeById((int)id);
//            if (entryType == null)
//            {
//                return HttpNotFound();
//            }
//            return View(entryType);
//        }

//        // POST: EntryTypes/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] EntryType entryType)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.UpdateEntryType(entryType);
//                return RedirectToAction("Index");
//            }
//            return View(entryType);
//        }

//        // GET: EntryTypes/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var entryType = EntryService.GetEntryTypeById((int)id);
//            if (entryType == null)
//            {
//                return HttpNotFound();
//            }
//            return View(entryType);
//        }

//        // POST: EntryTypes/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var entryType = EntryService.GetEntryTypeById((int)id);
//            EntryService.DeleteEntryType(id);
//            return RedirectToAction("Index");
//        }
//    }
//}