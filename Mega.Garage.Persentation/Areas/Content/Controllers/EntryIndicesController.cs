﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class EntryIndicesController : BaseController
//    {
//        // GET: EntryIndices
//        public ActionResult Index(long entryId)
//        {
//            var entryIndices = EntryService.GetEntryIndexByEntryId(entryId);
//            ViewBag.EntryId = entryId;
//            var entry = EntryService.GetEntryById((long)entryId);
//            ViewBag.EntryTitle = entry.Title;
//            return View(entryIndices.ToList());
//        }

//        // GET: EntryIndices/Details/5
//        public ActionResult Details(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var EntryIndex = EntryService.GetEntryIndexById((long)id);
//            if (EntryIndex == null)
//            {
//                return HttpNotFound();
//            }
//            return View(EntryIndex);
//        }

//        // GET: EntryIndices/Create
//        public ActionResult Create(long entryId)
//        {
//            var entry = EntryService.GetEntryById(entryId);
//            if (entry.Title != null)
//                ViewBag.EntryTitle = entry.Title;
//            else
//                ViewBag.EntryTitle = "";
//            ViewBag.EntryId = entry.Id;
//            ViewBag.IsExist = "";
//            ViewBag.IndexId = new SelectList(EntryService.GetAllIndices(), "Id", "Title");
//            return View("Create");
//        }

//        // POST: EntryIndices/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Link,Description,EntryId,IndexId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] EntryIndex entryIndex)
//        {
//            if (ModelState.IsValid)
//            {
//                var entryIsExistInIndex = EntryService.EntryIsExistInIndex((long)entryIndex.EntryId, (int)entryIndex.IndexId);
//                if (!entryIsExistInIndex)
//                {
//                    EntryService.AddEntryIndex(entryIndex);
//                    ViewBag.IsExist = "";
//                    return RedirectToAction("Index", new { entryId = (long)entryIndex.EntryId });
//                }
//                else
//                {
//                    ViewBag.IsExist = "نوشته قبلا به این فهرست اضافه شده است!";
//                    ViewBag.IndexId = new SelectList(EntryService.GetAllIndices(), "Id", "Title");
//                    return RedirectToAction("Create", new { entryId = (long)entryIndex.EntryId });
//                }
//            }

//            var entry = EntryService.GetEntryById((long)entryIndex.EntryId);
//            if (entry.Title != null)
//                ViewBag.EntryTitle = entry.Title;
//            else
//                ViewBag.EntryTitle = "";

//            ViewBag.IndexId = new SelectList(EntryService.GetAllIndices(), "Id", "Title");

//            return View("Create", entryIndex);
//        }

//        // GET: EntryIndices/Edit/5
//        public ActionResult Edit(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            var entryIndex = EntryService.GetEntryIndexById((long)id);
//            if (entryIndex == null)
//            {
//                return HttpNotFound();
//            }

//            var entry = EntryService.GetEntryById((long)entryIndex.EntryId);
//            if (entry.Title != null)
//                ViewBag.EntryTitle = entry.Title;
//            else
//                ViewBag.EntryTitle = "";

//            ViewBag.EntryId = entry.Id;

//            var index = EntryService.GetIndexById((int)entryIndex.IndexId);
//            ViewBag.IndexTitle = index.Title;

//            return View(entryIndex);
//        }

//        // POST: EntryIndices/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Link,Description,EntryId,IndexId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] EntryIndex entryIndex)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.UpdateEntryIndex(entryIndex);
//                return RedirectToAction("Index", new { entryId = (long)entryIndex.EntryId });
//            }

//            var entry = EntryService.GetEntryById((long)entryIndex.EntryId);
//            if (entry.Title != null)
//                ViewBag.EntryTitle = entry.Title;
//            else
//                ViewBag.EntryTitle = "";

//            var index = EntryService.GetIndexById((int)entryIndex.IndexId);
//            ViewBag.IndexTitle = index.Title;

//            return View(entryIndex);
//        }

//        // GET: EntryIndices/Delete/5
//        public ActionResult Delete(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var EntryIndex = EntryService.GetEntryIndexById((long)id);
//            if (EntryIndex == null)
//            {
//                return HttpNotFound();
//            }
//            return View(EntryIndex);
//        }

//        // POST: EntryIndices/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long id)
//        {
//            var entryId = EntryService.GetEntryIndexById((long)id);
//            var entry = EntryService.GetEntryById((long)entryId.EntryId);

//            EntryService.DeleteEntryIndex(id);

//            return RedirectToAction("Index", new { entryId = (long)entry.Id });
//        }
//    }
//}