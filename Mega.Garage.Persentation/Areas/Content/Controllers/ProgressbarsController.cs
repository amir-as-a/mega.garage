﻿//using Mega.Garage.Logic.Catalog.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class ProgressbarsController : BaseController
//    {
//        // GET: Progressbars
//        public ActionResult Index()
//        {
//            var progressbars = ProgressbarService.GetAllProgressbars();
//            return View(progressbars.ToList());
//        }

//        // GET: Progressbars/Details/5
//        public ActionResult Details(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var progressbar = ProgressbarService.GetProgressbarById((int)id);
//            if (progressbar == null)
//            {
//                return HttpNotFound();
//            }
//            return View(progressbar);
//        }

//        // GET: Progressbars/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: Progressbars/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,Description,ShowProgressbar,FlowId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Progressbar progressbar)
//        {
//            if (ModelState.IsValid)
//            {
//                ProgressbarService.AddProgressbar(progressbar);
//                return RedirectToAction("Index");
//            }

//            return View(progressbar);
//        }

//        // GET: Progressbars/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var progressbar = ProgressbarService.GetProgressbarById((int)id);
//            if (progressbar == null)
//            {
//                return HttpNotFound();
//            }
//            return View(progressbar);
//        }

//        // POST: Progressbars/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,Description,ShowProgressbar,FlowId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Progressbar progressbar)
//        {
//            if (ModelState.IsValid)
//            {
//                ProgressbarService.UpdateProgressbar(progressbar);
//                return RedirectToAction("Index");
//            }
//            return View(progressbar);
//        }

//        // GET: Progressbars/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var progressbar = ProgressbarService.GetProgressbarById((int)id);
//            if (progressbar == null)
//            {
//                return HttpNotFound();
//            }
//            return View(progressbar);
//        }

//        // POST: Progressbars/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            var progressbar = ProgressbarService.GetProgressbarById(id);
//            ProgressbarService.DeleteProgressbar(id);
//            return RedirectToAction("Index");
//        }

//    }
//}