﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class EntryTagsController : BaseController
//    {
//        // GET: EntryTags
//        public ActionResult Index(long entryId)
//        {
//            var entryIndices = EntryService.GetEntryTagByEntryId(entryId);
//            ViewBag.EntryId = entryId;
//            return View(entryIndices.ToList());
//        }

//        // GET: EntryTags/Details/5
//        public ActionResult Details(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var entryTag = EntryService.GetEntryTagById((long)id);
//            if (entryTag == null)
//            {
//                return HttpNotFound();
//            }
//            return View(entryTag);
//        }

//        // GET: EntryTags/Create
//        public ActionResult Create(long entryId)
//        {
//            var entry = EntryService.GetEntryById((long)entryId);
//            ViewBag.EntryId = entryId;
//            ViewBag.EntryTitle = entry.Title;
//            ViewBag.TagId = new SelectList(EntryService.GetAllTags(), "Id", "Title");
//            return View();
//        }

//        // POST: EntryTags/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,EntryId,TagId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] EntryTag entryTag)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.AddEntryTag(entryTag);
//                return RedirectToAction("Index");
//            }

//            ViewBag.EntryId = new SelectList(EntryService.GetAllEntries(), "Id", "Title");
//            ViewBag.TagId = new SelectList(EntryService.GetAllTags(), "Id", "Title");
//            return View(entryTag);
//        }

//        // GET: EntryTags/Edit/5
//        public ActionResult Edit(long entryId)
//        {
//            if (entryId == 0)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var entryTag = EntryService.GetEntryTagByEntryId((long)entryId);
//            if (entryTag == null)
//            {
//                return HttpNotFound();
//            }

//            var entry = EntryService.GetEntryById((long)entryId);
//            ViewBag.EntryId = entryId;
//            ViewBag.EntryTitle = entry.Title;
//            ViewBag.TagId = new SelectList(EntryService.GetAllTags(), "Id", "Title");
//            return View(entryTag);
//        }

//        // POST: EntryTags/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,EntryId,TagId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] EntryTag entryTag)
//        {
//            if (ModelState.IsValid)
//            {
//                EntryService.UpdateEntryTag(entryTag);
//                return RedirectToAction("Index");
//            }
//            ViewBag.EntryId = new SelectList(EntryService.GetAllEntries(), "Id", "Title");
//            ViewBag.TagId = new SelectList(EntryService.GetAllTags(), "Id", "Title");
//            return View(entryTag);
//        }

//        // GET: EntryTags/Delete/5
//        public ActionResult Delete(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var entryTag = EntryService.GetEntryTagById((long)id);
//            if (entryTag == null)
//            {
//                return HttpNotFound();
//            }
//            return View(entryTag);
//        }

//        // POST: EntryTags/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long id)
//        {
//            var entryTag = EntryService.GetEntryTagById((long)id);
//            EntryService.DeleteEntryTag(id);
//            return RedirectToAction("Index");
//        }
//    }
//}