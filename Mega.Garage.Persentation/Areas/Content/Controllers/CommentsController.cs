﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class CommentsController : BaseController
//    {
//        // GET: Comments
//        public ActionResult Index()
//        {
//            var comments = CommentService.GetAllComments();
//            return View(comments);
//        }

//        // GET: Comments/Create
//        public ActionResult Create()
//        {
//            ViewBag.EntryId = new SelectList(EntryService.GetAllEntries(), "Id", "Title");
//            return View();
//        }

//        // POST: Comments/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,UserFullName,UserEmail,UserUrl,UserIp,FullText,PublishDate,NotifyOnPublish,LikeCount,DislikeCount,VisitCount,CommentParentId,EntryId,CommentStatus,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Comment comment)
//        {
//            comment.UserIp = Request.UserHostAddress;
//            comment.UserUrl = Request.RawUrl;
//            if (ModelState.IsValid)
//            {
//                CommentService.AddComment(comment);
//                return RedirectToAction("Index");
//            }

//            ViewBag.CommentStatus = new SelectList(CommentService.GetAllCommentStatus(), "Id", "Title", comment.CommentStatus);
//            ViewBag.EntryId = new SelectList(EntryService.GetAllEntries(), "Id", "Title", comment.EntryId);
//            return View(comment);
//        }

//        // GET: Comments/Edit/5
//        public ActionResult Edit(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var comment = CommentService.GetCommentById((long)id);
//            if (comment == null)
//            {
//                return HttpNotFound();
//            }

//            ViewBag.EntryId = new SelectList(EntryService.GetAllEntries(), "Id", "Title", comment.EntryId);
//            return View(comment);
//        }

//        // POST: Comments/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit(/*[Bind(Include = "Id,UserFullName,UserEmail,UserUrl,UserIp,FullText,PublishDate,NotifyOnPublish,LikeCount,DislikeCount,VisitCount,CommentParentId,EntryId,CommentStatus,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Comment comment*/)
//        {
//            //if (ModelState.IsValid)
//            //{
//            //    _commentService.UpdateComment(comment);
//            //    return RedirectToAction("Index");
//            //}
//            ////ViewBag.CommentStatus = new SelectList(_commentStatusService.GetAllCommentStatus(), "Id", "Title", comment.CommentStatus);
//            //ViewBag.EntryId = new SelectList(_entryService.GetAllEntries(), "Id", "Title", comment.EntryId);
//            //return View(comment);
//            return View();
//        }

//        // GET: Comments/Delete/5
//        public ActionResult Delete(long? id)
//        {
//            //if (id == null)
//            //{
//            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            //}
//            //var comment = _commentService.GetCommentById((long)id);
//            //if (comment == null)
//            //{
//            //    return HttpNotFound();
//            //}
//            //return View(comment);
//            return View();
//        }

//        // POST: Comments/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long id)
//        {
//            //_commentService.DeleteComment(id);
//            //return RedirectToAction("Index");
//            return RedirectToAction("Index");
//        }
//    }
//}