﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Linq;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class LanguagesController : BaseController
//    {
//        // GET: Languages
//        public ActionResult Index()
//        {
//            var languages = LanguageService.GetAllLanguages();
//            return View(languages.ToList());
//        }

//        // GET: Languages/Create
//        public ActionResult Create()
//        {
//            return View();
//        }

//        // POST: Languages/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,Title,LanguageCode,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Language language)
//        {
//            if (ModelState.IsValid)
//            {
//                LanguageService.AddLanguage(language);
//                return RedirectToAction("Index");
//            }

//            return View(language);
//        }

//        // GET: Languages/Edit/5
//        public ActionResult Edit(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var language = LanguageService.GetLanguageById((int)id);
//            if (language == null)
//            {
//                return HttpNotFound();
//            }
//            return View(language);
//        }

//        // POST: Languages/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,Title,LanguageCode,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] Language language)
//        {
//            if (ModelState.IsValid)
//            {
//                LanguageService.UpdateLanguage(language);
//                return RedirectToAction("Index");
//            }
//            return View(language);
//        }

//        // GET: Languages/Delete/5
//        public ActionResult Delete(int? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            var language = LanguageService.GetLanguageById((int)id);
//            if (language == null)
//            {
//                return HttpNotFound();
//            }
//            return View(language);
//        }

//        // POST: Languages/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(int id)
//        {
//            LanguageService.DeleteLanguage(id);
//            return RedirectToAction("Index");
//        }

//    }
//}