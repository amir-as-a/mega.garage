﻿//using Mega.Garage.Logic.Content.Services;
//using Mega.Garage.Persistence.Content.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System.Net;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Content.Controllers
//{
//    public class TableColumnsController : BaseController
//    {
//        // GET: TableColumns
//        public ActionResult Index(int tableId)
//        {
//            ViewBag.TableId = tableId;
//            var tableColumns = TableService.GetTableColumnByTableId(tableId);
//            var table = TableService.GetTableById(tableId);
//            ViewBag.TableTitle = table.Title;

//            if (tableColumns == null)
//                return RedirectToAction("Create", "TableColumns", new { tableId = table.Id });

//            return View("Index", tableColumns);
//        }

//        // GET: TableColumns/Details/5
//        public ActionResult Details(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TableColumn tableColumn = TableService.GetTableColumnById((long)id);
//            if (tableColumn == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tableColumn);
//        }

//        // GET: TableColumns/Create
//        public ActionResult Create(int tableId)
//        {
//            if (tableId == 0)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            ViewBag.ColumnTypeId = new SelectList(TableService.GetAllTableColumnTypes(), "Id", "TypeName");
//            var table = TableService.GetTableById(tableId);
//            ViewBag.TableTitle = table.Title;

//            var tableColumn = new TableColumn
//            {
//                TableId = tableId
//            };
//            return View("Create", tableColumn);
//        }

//        // POST: TableColumns/Create
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Create([Bind(Include = "Id,ColumnName,ColumnTypeId,Description,TableId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] TableColumn tableColumn)
//        {
//            if (ModelState.IsValid)
//            {
//                TableService.AddTableColumn(tableColumn);
//                //_tableValueService.AddTableValue();
//                return RedirectToAction("Index", "TableColumns", new { tableId = tableColumn.TableId });
//            }

//            ViewBag.TableId = new SelectList(TableService.GetAllTables(), "Id", "Title", tableColumn.TableId);
//            ViewBag.ColumnTypeId = new SelectList(TableService.GetAllTableColumnTypes(), "Id", "TypeName", tableColumn.ColumnTypeId);
//            return View("Create", tableColumn);
//        }

//        // GET: TableColumns/Edit/5
//        public ActionResult Edit(long? id, int tableId)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }

//            TableColumn tableColumn = TableService.GetTableColumnById((long)id);
//            if (tableColumn == null)
//            {
//                return HttpNotFound();
//            }
//            ViewBag.TableId = tableId;
//            ViewBag.ColumnTypeId = new SelectList(TableService.GetAllTableColumnTypes(), "Id", "TypeName", tableColumn.ColumnTypeId);
//            return View("Edit", tableColumn);
//        }

//        // POST: TableColumns/Edit/5
//        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
//        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
//        [HttpPost]
//        [ValidateAntiForgeryToken]
//        public ActionResult Edit([Bind(Include = "Id,ColumnName,ColumnTypeId,Description,TableId,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] TableColumn tableColumn)
//        {
//            ViewBag.TableId = tableColumn.TableId;
//            if (ModelState.IsValid)
//            {
//                TableService.UpdateTableColumn(tableColumn);
//                return RedirectToAction("Index", "TableColumns", new { tableId = tableColumn.TableId });
//            }

//            ViewBag.ColumnTypeId = new SelectList(TableService.GetAllTableColumnTypes(), "Id", "TypeName", tableColumn.ColumnTypeId);
//            return View("Index");
//        }

//        // GET: TableColumns/Delete/5
//        public ActionResult Delete(long? id)
//        {
//            if (id == null)
//            {
//                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
//            }
//            TableColumn tableColumn = TableService.GetTableColumnById((long)id);
//            if (tableColumn == null)
//            {
//                return HttpNotFound();
//            }
//            return View(tableColumn);
//        }

//        // POST: TableColumns/Delete/5
//        [HttpPost, ActionName("Delete")]
//        [ValidateAntiForgeryToken]
//        public ActionResult DeleteConfirmed(long id)
//        {
//            var column = TableService.GetTableColumnById(id);
//            TableService.DeleteTableColumn(id);

//            return RedirectToAction("Index", "TableColumns", new { tableId = column.TableId });
//        }

//    }
//}