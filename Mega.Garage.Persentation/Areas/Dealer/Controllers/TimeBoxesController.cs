﻿using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Persistence.Dealer.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Dealer.Controllers
{
    public class TimeBoxesController : BaseController
    {
        public ActionResult Index()
        {
            var timeBoxs = TimeBoxService.GetAllTimeBoxes();
            return View(timeBoxs.ToList());
        }

        public ActionResult Modify(int? id)
        {
            ViewBag.TimeBoxTypeId = new SelectList(TimeBoxService.GetAllTimeBoxTypes(), "Id", "Title");
            if (id != null)
            {
                var timeBox = TimeBoxService.GetTimeBoxById((int)id);
                if (timeBox == null)
                    return HttpNotFound();
                return View(timeBox);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(TimeBox timeBox)
        {
            if (timeBox.Id > 0)
                TimeBoxService.UpdateTimeBox(timeBox);
            else
                TimeBoxService.AddTimeBox(timeBox);

            return View("index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TimeBoxService.DeleteTimeBox(id);
            return RedirectToAction("Index");
        }
    }
}