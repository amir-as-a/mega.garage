﻿using Mega.Garage.Logic;
using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Logic.Dealer.ViewModel;
using Mega.Garage.Persistence.Dealer.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Dealer.Controllers
{
    public class DealerLoginController : BaseController
    {

        public ActionResult Index(DealerLoginSearchViewModel parameters)
        {
            parameters.CurrentPage = int.Parse(Request["p"] ?? "1");
            parameters.PageSize = 25;
            PagedList<DealerLogin> data = Logic.Dealer.Services.DealerService.GetDealerLogins(parameters);

            ViewBag.PageSize = parameters.PageSize;
            ViewBag.CurrentPage = parameters.CurrentPage;
            ViewBag.TotalRecords = data.TotalRecords;

            ViewBag.Dealer = Logic.Dealer.Services.DealerService.GetAllDealers();

            return View(data.Records);
        }

        public ActionResult Modify(int? id)
        {
            if (id != null && id > 0)
            {
                var dealerLogin = Logic.Dealer.Services.DealerService.GetDealerLoginById((int)id);
                ViewBag.DealerId = new SelectList(Logic.Dealer.Services.DealerService.GetAllDealers(), "Id", "Title", dealerLogin.DealerId);

                return View(dealerLogin);
            }
            else
            {
                ViewBag.DealerId = new SelectList(Logic.Dealer.Services.DealerService.GetAllDealers(), "Id", "Title", null);
                return View(new DealerLogin());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(DealerLogin dealerLogin)
        {
            if (dealerLogin.Id > 0)
                Logic.Dealer.Services.DealerService.UpdateDealerLogin(dealerLogin);
            else
                Logic.Dealer.Services.DealerService.AddDealerLogin(dealerLogin);
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Logic.Dealer.Services.DealerService.DeleteDealerLogin(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ResetPassword(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassword(int id, string newPassword)
        {
            var dealerLogin = Logic.Dealer.Services.DealerService.GetDealerLoginById((int)id);
            dealerLogin.Password = newPassword;

            Logic.Dealer.Services.DealerService.UpdateDealerLogin(dealerLogin);

            return RedirectToAction("Index");
        }
    }
}