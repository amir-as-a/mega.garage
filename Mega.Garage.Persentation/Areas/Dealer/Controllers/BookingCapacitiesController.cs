﻿using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Persistence.Dealer.Entities;
using Mega.Garage.Presentation.Controllers;
using DealerService = Mega.Garage.Logic.Dealer.Services.DealerService;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Dealer.Controllers
{
    public class BookingCapacitiesController : BaseController
    {
        public ActionResult Index()
        {
            var bookings = BookingService.GetAllBookingCapacities();
            return View(bookings.ToList());
        }

        public ActionResult Modify(int? id)
        {
            ViewBag.DealerId = new SelectList(DealerService.GetAllDealers(), "Id", "Title");
            ViewBag.TimeBoxId = new SelectList(TimeBoxService.GetFreeTimeBoxes(), "Id", "Title");

            if (id != null)
            {
                var bookingCapacity = BookingService.GetBookingCapacityById((int)id);
                if (bookingCapacity == null)
                    return HttpNotFound();
                return View(bookingCapacity);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(BookingCapacity bookingCapacity)
        {
            if (bookingCapacity.Id > 0)
                BookingService.UpdateBookingCapacity(bookingCapacity);
            else
                BookingService.AddBookingCapacity(bookingCapacity);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BookingService.DeleteBookingCapacity(id);
            return RedirectToAction("Index");
        }
    }
}