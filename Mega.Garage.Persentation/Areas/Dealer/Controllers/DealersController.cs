﻿using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Logic.FileUpload.Services;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Dealer.Controllers
{
    public class DealersController : BaseController
    {

        public ActionResult Index()
        {
            var dealers = DealerService.GetAllDealers();
            return View(dealers.ToList());
        }

        public ActionResult Modify(int? id)
        {
            if (id != null && id > 0)
            {
                var dealer = DealerService.GetDealerById((int)id);
                return View(dealer);
            }
            else
            {
                return View(new Persistence.Dealer.Entities.Dealer());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Persistence.Dealer.Entities.Dealer dealer, HttpPostedFileBase fileImage, HttpPostedFileBase fileIcon, HttpPostedFileBase fileDocumentImage)
        {
            Persistence.Dealer.Entities.Dealer currentDealer = null;
            if (dealer.Id > 0)
                currentDealer = DealerService.GetDealerById(dealer.Id);

            if (fileIcon != null)
            {
                var iconFileData = UploadService.Upload(fileIcon);
                dealer.Icon = iconFileData.FileData.ToString();
            }
            else if (dealer.Id > 0)
            {
                dealer.Icon = currentDealer.Icon;
            }

            if (fileImage != null)
            {
                var imageFileData = UploadService.Upload(fileImage);
                dealer.Image = imageFileData.FileData.ToString();
            }
            else if (dealer.Id > 0)
            {
                dealer.Image = currentDealer.Image;
            }

            if (fileDocumentImage != null)
            {
                var documentImageFileData = UploadService.Upload(fileDocumentImage);
                dealer.DocumentImage = documentImageFileData.FileData.ToString();
            }
            else if (dealer.Id > 0)
            {
                dealer.DocumentImage = currentDealer.DocumentImage;
            }
            if (dealer.Id > 0)
                DealerService.UpdateDealer(dealer);
            else
                DealerService.AddDealer(dealer);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DealerService.DeleteDealer(id);
            return RedirectToAction("Index");
        }
    }
}