﻿using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Persistence.Dealer.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Dealer.Controllers
{
    public class TimeBoxTypesController : BaseController
    {
        // GET: TimeBoxTypes
        public ActionResult Index()
        {
            var timeBoxTypes = TimeBoxService.GetAllTimeBoxTypes();
            return View(timeBoxTypes.ToList());
        }

        // GET: TimeBoxTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var timeBoxType = TimeBoxService.GetTimeBoxTypeById((int)id);
            if (timeBoxType == null)
            {
                return HttpNotFound();
            }
            return View(timeBoxType);
        }

        // GET: TimeBoxTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TimeBoxTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Code,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] TimeBoxType timeBoxType)
        {
            if (ModelState.IsValid)
            {
                TimeBoxService.AddTimeBoxType(timeBoxType);
                return RedirectToAction("Index");
            }
            return View(timeBoxType);
        }

        // GET: TimeBoxTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var timeBoxType = TimeBoxService.GetTimeBoxTypeById((int)id);
            if (timeBoxType == null)
            {
                return HttpNotFound();
            }
            return View(timeBoxType);
        }

        // POST: TimeBoxTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Code,Description,CreatedDate,ModifiedDate,CreatedBy,ModifiedBy,IsActive,RowVersion")] TimeBoxType timeBoxType)
        {
            if (ModelState.IsValid)
            {
                TimeBoxService.UpdateTimeBoxType(timeBoxType);
                return RedirectToAction("Index");
            }
            return View(timeBoxType);
        }

        // GET: TimeBoxTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var timeBoxType = TimeBoxService.GetTimeBoxTypeById((int)id);
            if (timeBoxType == null)
            {
                return HttpNotFound();
            }
            return View(timeBoxType);
        }

        // POST: TimeBoxTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TimeBoxService.DeleteTimeBoxType(id);
            return RedirectToAction("Index");
        }

    }
}