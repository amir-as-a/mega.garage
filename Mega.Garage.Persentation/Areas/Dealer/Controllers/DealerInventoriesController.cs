﻿using Mega.Garage.Logic.Dealer.Services;
using DealerService = Mega.Garage.Logic.Dealer.Services.DealerService;
using Mega.Garage.Persistence.Dealer.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Dealer.Controllers
{
    public class DealerInventoriesController : BaseController
    {
        public ActionResult Index()
        {
            var dealerInventories = DealerService.GetAllDealerInventories();
            return View(dealerInventories.ToList());
        }

        public ActionResult Modify(int? id)
        {
            ViewBag.DealerId = new SelectList(DealerService.GetAllDealers(), "Id", "Title");
            if (id != null)
            {
                var dealerInventory = DealerService.GetDealerInventoryById((int)id);
                if (dealerInventory == null)
                    return HttpNotFound();

                return View(dealerInventory);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(DealerInventory dealerInventory)
        {
            if (dealerInventory.Id > 0)
                DealerService.UpdateDealerInventory(dealerInventory);
            else
                DealerService.AddDealerInventory(dealerInventory);
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DealerService.DeleteDealerInventory(id);
            return RedirectToAction("Index");
        }
    }
}