﻿using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Persistence.Dealer.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Web.Mvc;
using DealerService = Mega.Garage.Logic.Dealer.Services.DealerService;

namespace Mega.Garage.Presentation.Areas.Dealer.Controllers
{
    public class BookingsController : BaseController
    {
        public ActionResult Index()
        {
            var bookings = BookingService.GetAllBookings();
            return View(bookings.ToList());
        }

        [HttpGet]
        public ActionResult Modify(int? id)
        {
            ViewBag.DealerId = new SelectList(DealerService.GetAllDealers(), "Id", "Title");
            ViewBag.TimeBoxId = new SelectList(TimeBoxService.GetAllTimeBoxes(), "Id", "Title");

            if (id != null)
            {
                var booking = BookingService.GetBookingById((int)id);
                if (booking == null)
                    return HttpNotFound();
                else
                    return View(booking);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Booking booking)
        {
            if (booking.Id > 0)
                BookingService.UpdateBooking(booking);
            else
                BookingService.AddBooking(booking);
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BookingService.DeleteBooking(id);
            return RedirectToAction("Index");
        }
    }
}