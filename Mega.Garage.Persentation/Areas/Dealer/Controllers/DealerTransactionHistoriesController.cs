﻿using DealerService = Mega.Garage.Logic.Dealer.Services.DealerService;
using Mega.Garage.Persistence.Dealer.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Dealer.Controllers
{
    public class DealerTransactionHistoriesController : BaseController
    {

        public ActionResult Index()
        {
            var dealerTransactionHistories = DealerService.GetAllDealerTransactionHistories();
            return View(dealerTransactionHistories.ToList());
        }

        [HttpPost]
        public ActionResult Index(int dealerId)
        {
            var dealerTransactionHistories = DealerService.GetDealerTransactionHistoriesByDealerId(dealerId);
            return View(dealerTransactionHistories.ToList());
        }

        public ActionResult Modify(int? id)
        {
            ViewBag.DealerId = new SelectList(DealerService.GetAllDealers(), "Id", "Title");
            if (id != null)
            {
                var dealerTransactionHistory = DealerService.GetDealerTransactionHistoryById((int)id);
                if (dealerTransactionHistory == null)
                    return HttpNotFound();
                return View(dealerTransactionHistory);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(DealerTransactionHistory dealerTransactionHistory)
        {
            if (dealerTransactionHistory.Id > 0)
                DealerService.UpdateDealerTransactionHistory(dealerTransactionHistory);
            else
                DealerService.AddDealerTransactionHistory(dealerTransactionHistory);
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DealerService.DeleteDealerTransactionHistory(id);
            return RedirectToAction("Index");
        }
    }
}