﻿using Mega.Garage.Logic.OnSite.Services;
using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Persistence.OnSite.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.OnSite.Controllers
{
    public class ProductGroupProductsController : BaseController
    {
        public ActionResult Index(int id)
        {
            var productGroup = ProductGroupService.GetProductGroupById(id);
            ViewBag.Products = ProductService.GetAllProducts().ToList();
            ViewBag.ProductGroupProducts = ProductGroupService.GetProductGroupProductsByProductGroupId(id);

            return View(productGroup);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductGroupProduct productGroupProduct)
        {
            if (ModelState.IsValid)
                ProductGroupService.AddProductGroupProduct(productGroupProduct);

            return RedirectToAction("Index", new { Id = productGroupProduct.ProductGroupId });
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var productGroupProduct = ProductGroupService.GetProductGroupProductById(id);
            ProductGroupService.DeleteProductGroupProduct(id);
            return RedirectToAction("Index", new { Id = productGroupProduct.ProductGroupId });
        }
    }
}