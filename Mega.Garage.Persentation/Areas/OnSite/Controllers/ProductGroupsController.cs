﻿using Mega.Garage.Logic.OnSite.ViewModel;
using Mega.Garage.Persistence.OnSite.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Web.Mvc;
using Mega.Garage.Logic.OnSite.Services;
using Mega.Garage.Logic.Catalog.Services;
using System.Web;
using Mega.Garage.Logic.FileUpload.Services;
using ClosedXML.Extensions;

namespace Mega.Garage.Presentation.Areas.OnSite.Controllers
{
    public class ProductGroupsController : BaseController
    {

        public ActionResult Index(ProductGroupSearchViewModel parameters)
        {
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            parameters.CurrentPage = CurrentPage;
            parameters.PageSize = 25;
            ViewBag.PageSize = parameters.PageSize;
            var productGroups = ProductGroupService.GetAllProductGroups(parameters);
            ViewBag.TotalRecords = productGroups.TotalRecords;
            return View(productGroups.Records);
        }

        public ActionResult Modify(int? id)
        {
            if (id != null && id > 0)
            {
                var productGroup = ProductGroupService.GetProductGroupById((int)id);
                if (productGroup == null)
                    return HttpNotFound();

                ViewBag.LandingPageId = new SelectList(LandingPageService.GetAllLandingPages(), "Id", "Title", productGroup.LandingPageId);
                ViewBag.SelectedCategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title", productGroup.SelectedCategoryId);
                return View(productGroup);
            }
            else
            {
                ViewBag.LandingPageId = new SelectList(LandingPageService.GetAllLandingPages(), "Id", "Title");
                ViewBag.SelectedCategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title");
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(ProductGroup productGroup, HttpPostedFileBase fileThumbImagePath, HttpPostedFileBase fileImagePath)
        {
            ViewBag.LandingPageId = new SelectList(LandingPageService.GetAllLandingPages(), "Id", "Title", productGroup.LandingPageId);
            ViewBag.SelectedCategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title", productGroup.SelectedCategoryId);

            ProductGroup entity = null;
            if (productGroup.Id > 0)
                entity = ProductGroupService.GetProductGroupById(productGroup.Id);

            if (fileThumbImagePath != null)
            {
                var LogoFileData = UploadService.Upload(fileThumbImagePath);
                productGroup.ThumbImagePath = LogoFileData.FileData.ToString();
            }
            else if (productGroup.Id > 0)
            {
                productGroup.ThumbImagePath = entity.ThumbImagePath;
            }

            if (fileImagePath != null)
            {
                var LogoFileData = UploadService.Upload(fileImagePath);
                productGroup.ImagePath = LogoFileData.FileData.ToString();
            }
            else if (productGroup.Id > 0)
            {
                productGroup.ImagePath = entity.ImagePath;
            }

            if (productGroup.Id > 0)
                ProductGroupService.UpdateProductGroup(productGroup);
            else
                ProductGroupService.AddProductGroup(productGroup);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductGroupService.DeleteProductGroup(id);
            return RedirectToAction("Index");
        }

        public ActionResult Report(ProductGroupSearchViewModel parameters)
        {
            parameters.CurrentPage = 1;
            parameters.PageSize = int.MaxValue;
            var productGroups = ProductGroupService.GetAllProductGroups(parameters);
            var report = ProductGroupService.GenerateExcelReport(productGroups.Records);

            using (var wb = report)
            {
                return wb.Deliver("BrandsReport.xlsx");
            }
        }
    }
}