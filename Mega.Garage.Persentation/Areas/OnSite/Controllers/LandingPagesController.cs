﻿using Mega.Garage.Logic.OnSite.Services;
using Mega.Garage.Persistence.OnSite.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.OnSite.Controllers
{
    public class LandingPagesController : BaseController
    {
        public ActionResult Index()
        {
            return View(LandingPageService.GetAllLandingPages().ToList());
        }

        public ActionResult Modify(int? id)
        {
            if (id != null)
            {
                LandingPage landingPage = LandingPageService.GetLandingPageById((int)id);
                if (landingPage == null)
                    return HttpNotFound();

                return View(landingPage);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(LandingPage landingPage)
        {
            if (landingPage.Id > 0)
                LandingPageService.UpdateLandingPage(landingPage);
            else
                LandingPageService.AddLandingPage(landingPage);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LandingPageService.DeleteLandingPage(id);
            return RedirectToAction("Index");
        }
    }
}
