﻿using Mega.Garage.Logic;
using Mega.Garage.Logic.Polling.Services;
using Mega.Garage.Persistence.Polling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Polling.Controllers
{
    public class QuestionController : Controller
    {
        // GET: Polling/Question
        public ActionResult Index(string fromDate, string toDate, int? surveyId, string title, string surveyTitle, string code, int? questionType, int? clientId)
        {
            DateTime? startDate = Common.DateUtility.TryGetDateTime(fromDate);
            DateTime? endDate = Common.DateUtility.TryGetDateTime(toDate);

            ViewBag.surveyId = new SelectList(QuestionService.GetAllSurveys(), "Id", "Title", surveyId ?? 0);
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            ViewBag.PageSize = 25;
            PagedList<Question> survey = 
                QuestionService.GetAllQuestionsWithFilter(startDate, endDate, title, surveyId, code, questionType, clientId, CurrentPage, ViewBag.PageSize);
            ViewBag.TotalRecords = survey.TotalRecords;

            return View(survey.Records);
        }

        public ActionResult Modify(int? id)
        {
            if (id != null && id > 0)
            {
                var question = QuestionService.GetQuestionById((int)id);
                ViewBag.SurveyId = new SelectList(QuestionService.GetAllSurveys(), "Id", "Title", question.SurveyId);
                return View(question);
            }
            else
            {
                ViewBag.SurveyId = new SelectList(QuestionService.GetAllSurveys(), "Id", "Title");
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Persistence.Polling.Entities.Question question)
        {
            if (question.Id > 0)
                QuestionService.UpdateQuestion(question);
            else
                QuestionService.AddQuestion(question);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuestionService.DeleteQuestion(id);
            return RedirectToAction("Index");
        }
    }
}