﻿using Mega.Garage.Logic.Polling.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Polling.Controllers
{
    public class SurveyTypeController : Controller
    {
        // GET: Polling/SurveyType
        public ActionResult Index()
        {
            var SurveyTypes = QuestionService.GetAllSurveyTypes();
            return View(SurveyTypes.ToList());
        }

        public ActionResult Modify(int? id)
        {
            if (id != null && id > 0)
            {
                var SurveyType = QuestionService.GetSurveyTypeById((int)id);
                return View(SurveyType);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Persistence.Polling.Entities.SurveyType surveyType)
        {
            if (surveyType.Id > 0)
                QuestionService.UpdateSurveyType(surveyType);
            else
                QuestionService.AddSurveyType(surveyType);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuestionService.DeleteSurveyType(id);
            return RedirectToAction("Index");
        }
    }
}