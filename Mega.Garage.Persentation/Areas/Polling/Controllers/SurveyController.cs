﻿using Mega.Garage.Logic;
using Mega.Garage.Logic.Polling.Services;
using Mega.Garage.Persistence.Polling.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Polling.Controllers
{
    public class SurveyController : Controller
    {
        // GET: Polling/Survey
        public ActionResult Index(string fromDate, string toDate, int? surveyTypeId, string title, string code, int? questionType, int? clientId)
        {
            DateTime? startDate = Common.DateUtility.TryGetDateTime(fromDate);
            DateTime? endDate = Common.DateUtility.TryGetDateTime(toDate);

            ViewBag.surveyTypeId = new SelectList(QuestionService.GetAllSurveyTypes(), "Id", "Title", surveyTypeId ?? 0);
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            ViewBag.PageSize = 25;
            PagedList<Survey> survey = QuestionService.GetAllSurveysWithFilter(startDate, endDate, surveyTypeId, title, code, questionType, clientId, CurrentPage, ViewBag.PageSize);
            ViewBag.TotalRecords = survey.TotalRecords;

            return View(survey.Records);
        }

        public ActionResult Modify(int? id)
        {
            if (id != null && id > 0)
            {
                var survey = QuestionService.GetSurveyById((long)id);
                ViewBag.SurveyTypeId = new SelectList(QuestionService.GetAllSurveyTypes(), "Id", "Title", survey.SurveyTypeId);
                return View(survey);
            }
            else
            {
                ViewBag.SurveyTypeId = new SelectList(QuestionService.GetAllSurveyTypes(), "Id", "Title");
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Persistence.Polling.Entities.Survey survey)
        {
            if (survey.Id > 0)
                QuestionService.UpdateSurvey(survey);
            else
                QuestionService.AddSurvey(survey);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuestionService.DeleteSurvey((long)id);
            return RedirectToAction("Index");
        }
    }
}