﻿using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{
    public class CatalogCategoriesController : BaseController
    {

        public ActionResult Index()
        {
            var catalogCategories = CatalogService.GetAllCatalogCategories();
            return View(catalogCategories);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Modify(CatalogCategory catalogCategory)
        {
            if (catalogCategory.Id == 0)
                CatalogService.AddCatalogCategory(catalogCategory);
            else
                CatalogService.UpdateCatalogCategory(catalogCategory);

            return RedirectToAction("Index");
        }

        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                ViewBag.CategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title");
                ViewBag.CatalogId = new SelectList(CatalogService.GetAllCatalogs(), "Id", "Title");
                return View();
            }
            else
            {
                var catalogCategory = CatalogService.GetCatalogCategoryById((int)id);
                if (catalogCategory == null)
                    return HttpNotFound();

                ViewBag.CategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title", catalogCategory.CategoryId);
                ViewBag.CatalogId = new SelectList(CatalogService.GetAllCatalogs(), "Id", "Title", catalogCategory.CatalogId);
                return View(catalogCategory);
            }
        }

        [HttpPost, ActionName("Delete"), ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CatalogService.DeleteCatalogCategory(id);
            return RedirectToAction("Index");
        }

    }
}