﻿using ClosedXML.Extensions;
using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Logic.Catalog.ViewModel;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{
    public class ColorsController : BaseController
    {
        public ActionResult Index(ColorSearchViewModel parameters)
        {
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            parameters.CurrentPage = CurrentPage;
            parameters.PageSize = 25;
            ViewBag.PageSize = parameters.PageSize;
            var colors = ColorService.GetAllColorsByPage(parameters);
            ViewBag.TotalRecords = colors.TotalRecords;
            return View(colors.Records);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Color color)
        {
            if (color.Id == 0)
            {
                ColorService.AddColor(color);
            }
            else
            {
                ColorService.UpdateColor(color);
            }
            return RedirectToAction("Index");
        }

        // GET: Colors/Edit/5
        public ActionResult Modify(int? id)
        {
            if (id == null)
                return View();
            else
            {
                var color = ColorService.GetColorById((int)id);
                if (color == null)
                {
                    return HttpNotFound();
                }
                return View(color);
            }
        }

        // POST: Colors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ColorService.DeleteColor(id);
            return RedirectToAction("Index");
        }
        public ActionResult Report(ColorSearchViewModel parameters)
        {
            parameters.CurrentPage = 1;
            parameters.PageSize = int.MaxValue;
            var colors = ColorService.GetAllColorsByPage(parameters);
            var report = ColorService.GenerateColorsExcelReport(colors.Records);

            using (var wb = report)
            {
                return wb.Deliver("ColorsReport.xlsx");
            }
        }
    }
}