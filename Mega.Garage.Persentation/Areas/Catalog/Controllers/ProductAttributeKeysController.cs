﻿using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{
    public class ProductAttributeKeysController : BaseController
    {
        public ActionResult Index()
        {
            var productAttributeKeys = ProductAttributeKeyService.GetAllProductAttributeKeys();
            return View(productAttributeKeys.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(ProductAttributeKey productAttributeKey)
        {
            if (productAttributeKey.Id == 0)
                ProductAttributeKeyService.AddProductAttributeKey(productAttributeKey);
            else
                ProductAttributeKeyService.UpdateProductAttributeKey(productAttributeKey);
            return RedirectToAction("Index");
        }

        public ActionResult Modify(int? id)
        {
            if (id == null)
                return View();
            else
            {
                var productAttributeKey = ProductAttributeKeyService.GetProductAttributeKeyById((int)id);
                if (productAttributeKey == null)
                    return HttpNotFound();

                return View(productAttributeKey);
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductAttributeKeyService.DeleteProductAttributeKey(id);
            return RedirectToAction("Index");
        }
    }
}