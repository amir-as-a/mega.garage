﻿using ClosedXML.Extensions;
using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Logic.Catalog.ViewModel;
using Mega.Garage.Logic.FileUpload.Services;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{
    public class CatalogsController : BaseController
    {
        public ActionResult Index(CatalogSearchViewModel parameters)
        {
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            parameters.CurrentPage = CurrentPage;
            parameters.PageSize = 25;
            ViewBag.PageSize = parameters.PageSize;
            var Catalogs = CatalogService.GetAllCatalogsByPage(parameters);
            ViewBag.TotalRecords = Catalogs.TotalRecords;
            return View(Catalogs.Records);
        }

        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                ViewBag.CategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title");
                ViewBag.BrandId = new SelectList(BrandService.GetAllBrands(), "Id", "Title");
                ViewBag.CatalogTypeId = new SelectList(CatalogService.GetAllCatalogTypes(), "Id", "Title");
                ViewBag.CatalogStatusId = new SelectList(CatalogService.GetAllCatalogStatuses(), "Id", "Title");
                ViewBag.CompanyId = new SelectList(CatalogService.GetAllCatalogStatuses(), "Id", "Title");
                return View();
            }
            else
            {
                var Catalog = CatalogService.GetCatalogById((int)id);
                if (Catalog == null)
                    return HttpNotFound();

                ViewBag.CategoryId = new SelectList(CategoryService.GetAllCategories(), "Id", "Title", Catalog.CategoryId);
                ViewBag.BrandId = new SelectList(BrandService.GetAllBrands(), "Id", "Title", Catalog.BrandId);
                ViewBag.CatalogTypeId = new SelectList(CatalogService.GetAllCatalogTypes(), "Id", "Title", Catalog.CatalogTypeId);
                ViewBag.CatalogStatusId = new SelectList(CatalogService.GetAllCatalogStatuses(), "Id", "Title", Catalog.CatalogStatusId);

                return View(Catalog);
            }

        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Modify(Persistence.Catalog.Entities.Catalog catalog, HttpPostedFileBase fileThumbImagePath, HttpPostedFileBase fileImagePath)
        {
            if (catalog.Id == 0)
            {
                if (fileThumbImagePath != null)
                    catalog.ThumbImagePath = UploadService.Upload(fileThumbImagePath).FileData.ToString();

                if (fileImagePath != null)
                    catalog.ImagePath = UploadService.Upload(fileImagePath).FileData.ToString();

                CatalogService.AddCatalog(catalog);
                return RedirectToAction("Index");
            }
            else
            {
                var currentCatalog = CatalogService.GetCatalogById(catalog.Id);

                if (fileThumbImagePath != null)
                    catalog.ThumbImagePath = UploadService.Upload(fileThumbImagePath).FileData.ToString();
                else
                    catalog.ThumbImagePath = currentCatalog.ThumbImagePath;

                if (fileImagePath != null)
                    catalog.ImagePath = UploadService.Upload(fileImagePath).FileData.ToString();
                else
                    catalog.ImagePath = currentCatalog.ImagePath;

                CatalogService.UpdateCatalog(catalog);
                return RedirectToAction("Index");
            }
        }

        [HttpPost, ActionName("Delete"), ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CatalogService.DeleteCatalog(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public JsonResult GetCatalog(int id)
        {
            var result = CatalogService.GetCatalogById(id);
            return Json(new
            {
                result.Id,
                result.ShortTitle,
                result.Title,
                result.ConfigCode,
                result.ImagePath
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetNewCatalogCode(int categoryId)
        {
            var code = CatalogService.GetNewCatalogCode(categoryId);
            return Json(code, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Report(CatalogSearchViewModel parameters)
        {
            parameters.CurrentPage = 1;
            parameters.PageSize = int.MaxValue;
            var catalogs = CatalogService.GetAllCatalogsByPage(parameters);
            var report = CatalogService.GenerateCatalogExcelReport(catalogs.Records);

            using (var wb = report)
            {
                return wb.Deliver("CatalogsReport.xlsx");
            }
        }
    }
}