﻿using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{
    public class ProductAttributeValuesController : BaseController
    {

        // GET: ProductAttributeValues
        public ActionResult Index(int keyId)
        {
            var productAttributeValues = ProductAttributeValueService.GetProductAttributeValuesByAttributeKeyId(keyId);
            return View(productAttributeValues.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(ProductAttributeValue productAttributeValue)
        {
            //
            return RedirectToAction("Index");
        }

        // GET: ProductAttributeValues/Edit/5
        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                return View();
            }
            var productAttributeValue = ProductAttributeValueService.GetProductAttributeValueById((int)id);
            if (productAttributeValue == null)
            {
                return HttpNotFound();
            }

            var productAttributeKeys = ProductAttributeValueService.GetAllProductAttributeValues();
            return View(productAttributeValue);
        }

        // POST: ProductAttributeValues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductAttributeValueService.DeleteProductAttributeValue(id);
            return RedirectToAction("Index");
        }
    }
}