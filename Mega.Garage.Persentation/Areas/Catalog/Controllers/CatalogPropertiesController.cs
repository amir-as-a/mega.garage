﻿using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{
    public class CatalogPropertiesController : BaseController
    {
        public ActionResult Index()
        {
            var catalogProperties = CatalogService.GetAllCatalogProperties().OrderByDescending(x => x.CreatedDate);
            return View(catalogProperties.ToList());
        }

        public ActionResult Modify(int? id)
        {
            ViewBag.CatalogId = new SelectList(CatalogService.GetAllCatalogs(), "Id", "Title");
            if (id == null)
                return View();
            else
            {
                var catalogProperty = CatalogService.GetCatalogPropertyById((int)id);
                if (catalogProperty == null)
                    return HttpNotFound();

                return View(catalogProperty);
            }
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Modify(CatalogProperty catalogProperty)
        {
            if (catalogProperty.Id > 0)
                CatalogService.UpdateCatalogProperty(catalogProperty);
            else
                CatalogService.AddCatalogProperty(catalogProperty);

            return RedirectToAction("Index");
        }
        
        [HttpPost, ActionName("Delete"), ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CatalogService.DeleteCatalogProperty(id);
            return RedirectToAction("Index");
        }
    }
}