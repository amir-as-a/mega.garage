﻿using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{
    public class CatalogTypesController : BaseController
    {
        public ActionResult Index()
        {
            var CatalogTypes = CatalogService.GetAllCatalogTypes();
            return View(CatalogTypes.ToList());
        }

        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                return View();
            }
            else
            {
                var CatalogType = CatalogService.GetCatalogTypeById((int)id);
                if (CatalogType == null)
                    return HttpNotFound();

                return View(CatalogType);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(CatalogType catalogType)
        {
            if (catalogType.Id == 0)
                CatalogService.AddCatalogType(catalogType);
            else
                CatalogService.UpdateCatalogType(catalogType);
            
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CatalogService.DeleteCatalogType(id);
            return RedirectToAction("Index");
        }
    }
}