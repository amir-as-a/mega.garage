﻿using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{
    public class CatalogStatusController : BaseController
    {
        public ActionResult Index()
        {
            var CatalogStatus = CatalogService.GetAllCatalogStatuses();
            return View(CatalogStatus.ToList());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(CatalogStatus CatalogStatus)
        {
            if (CatalogStatus.Id == 0)
                CatalogService.AddCatalogStatus(CatalogStatus);
            else
                CatalogService.UpdateCatalogStatus(CatalogStatus);
            return RedirectToAction("Index");
        }

        public ActionResult Modify(int? id)
        {
            if (id == null)
                return View();
            else
            {
                var CatalogStatus = CatalogService.GetCatalogStatusById((int)id);
                if (CatalogStatus == null)
                    return HttpNotFound();
                return View(CatalogStatus);
            }
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CatalogService.DeleteCatalogStatus(id);
            return RedirectToAction("Index");
        }
    }
}