﻿using ClosedXML.Extensions;
using FluentValidation;
using FluentValidation.Attributes;
using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Logic.Catalog.ViewModel;
using Mega.Garage.Logic.FileUpload.Services;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Presentation.Controllers;
using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{

    public class BrandsController : BaseController
    {
        // GET: Brands
        public ActionResult Index(BrandSearchViewModel parameters)
        {
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            parameters.CurrentPage = CurrentPage;
            parameters.PageSize = 25;
            ViewBag.PageSize = parameters.PageSize;
            var brands = BrandService.GetAllBrands(parameters);
            ViewBag.TotalRecords = brands.TotalRecords;
            return View(brands.Records);
        }

        // GET: Brands/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var brand = BrandService.GetBrandById((int)id);
            if (brand == null)
            {
                return HttpNotFound();
            }
            return View(brand);
        }

        // GET: Brands/Edit/5
        public ActionResult Modify(int? id)
        {
            if (id == null)
                return View();
            else
            {
                var brand = BrandService.GetBrandById(id.Value);
                if (brand == null)
                    return HttpNotFound();
                
                return View(brand);
            }
        }

        // POST: Brands/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Brand brand, HttpPostedFileBase LogoImage, HttpPostedFileBase PictureImage)
        {
            ///Insert
            if (brand.Id != 0)
            {
                var currentBrand = BrandService.GetBrandById(brand.Id);

                if (LogoImage != null)
                    brand.LogoImagePath = UploadService.Upload(LogoImage).FileData.ToString();
                else
                    brand.LogoImagePath = currentBrand?.LogoImagePath;

                if (PictureImage != null)
                    brand.WideImagePath = UploadService.Upload(PictureImage).FileData.ToString();
                else
                    brand.WideImagePath = currentBrand?.WideImagePath;

                BrandService.UpdateBrand(brand);
            }
            ///Update
            else
            {
                if (LogoImage != null)
                    brand.LogoImagePath = UploadService.Upload(LogoImage).FileData.ToString();

                if (PictureImage != null)
                    brand.WideImagePath = UploadService.Upload(PictureImage).FileData.ToString();

                BrandService.AddBrand(brand);
            }
            return RedirectToAction("Index");
        }

        // POST: Brands/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BrandService.DeleteBrand(id);
            return RedirectToAction("Index");
        }

        public ActionResult Report(BrandSearchViewModel parameters)
        {
            parameters.CurrentPage = 1;
            parameters.PageSize = int.MaxValue;
            var brands = BrandService.GetAllBrands(parameters);
            var report = BrandService.GenerateExcelReport(brands.Records);

            using (var wb = report)
            {
                return wb.Deliver("BrandsReport.xlsx");
            }
        }
    }
}