﻿using ClosedXML.Extensions;
using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Logic.Catalog.ViewModel;
using Mega.Garage.Logic.FileUpload.Services;
using Mega.Garage.Persistence.Catalog.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Catalog.Controllers
{
    public class CategoriesController : BaseController
    {
        public ActionResult Index(CategorySearchViewModel parameters)
        {
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            parameters.CurrentPage = CurrentPage;
            parameters.PageSize = 25;
            ViewBag.PageSize = parameters.PageSize;
            var categories = CategoryService.GetAllCategoriesByPage(parameters);
            ViewBag.TotalRecords = categories.TotalRecords;
            return View(categories.Records);
        }

        public ActionResult Modify(int? id)
        {
            if (id == null)
            {
                var categories = CategoryService.GetAllCategories();
                var categoryTypes = ProductAttributeKeyService.GetAllProductAttributeKeys();
                ViewBag.CategoryTypeId = new SelectList(categoryTypes, "Id", "Title", 8); // فیلد نوع طبقه لازم است به صورت پیش فرض "طبقه محصول" انتخاب شود و کاربر این فیلد را مشاهده نکند
                ViewBag.ParentCategoryId = new SelectList(categories, "Id", "Title");
                return View();
            }
            else
            {
                var category = CategoryService.GetCategoryById((int)id);
                if (category == null)
                {
                    return HttpNotFound();
                }
                var categories = CategoryService.GetAllCategories();
                var categoryTypes = ProductAttributeKeyService.GetAllProductAttributeKeys();
                ViewBag.CategoryTypeId = new SelectList(categoryTypes, "Id", "Title", 8); // فیلد نوع طبقه لازم است به صورت پیش فرض "طبقه محصول" انتخاب شود و کاربر این فیلد را مشاهده نکند
                ViewBag.ParentCategoryId = new SelectList(categories, "Id", "Title", category.ParentCategoryId);
                return View(category);
            }

        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Category category, HttpPostedFileBase fileWideImagePath, HttpPostedFileBase fileThumbImagePath)
        {

            if (category.Id == 0)
            {
                var categories = CategoryService.GetAllCategories();
                var categoryTypes = ProductAttributeKeyService.GetAllProductAttributeKeys();
                ViewBag.CategoryTypeId = new SelectList(categoryTypes, "Id", "Title", category.CategoryTypeId);
                ViewBag.ParentCategoryId = new SelectList(categories, "Id", "Title", category.ParentCategoryId);
                if (fileWideImagePath != null)
                {
                    var LogoFileData = UploadService.Upload(fileWideImagePath);
                    category.WideImagePath = LogoFileData.FileData.ToString();
                }
                if (fileThumbImagePath != null)
                {
                    var WideImageFileData = UploadService.Upload(fileThumbImagePath);
                    category.ThumbImagePath = WideImageFileData.FileData.ToString();
                }
                CategoryService.AddCategory(category);
                return RedirectToAction("Index");
            }
            else
            {
                var currentCategory = CategoryService.GetCategoryById(category.Id);
                if (fileWideImagePath != null)
                {
                    var LogoFileData = UploadService.Upload(fileWideImagePath);
                    category.WideImagePath = LogoFileData.FileData.ToString();
                }
                else
                {
                    category.WideImagePath = currentCategory.WideImagePath;
                }
                if (fileThumbImagePath != null)
                {
                    var WideImageFileData = UploadService.Upload(fileThumbImagePath);
                    category.ThumbImagePath = WideImageFileData.FileData.ToString();
                }
                else
                {
                    category.ThumbImagePath = currentCategory.ThumbImagePath;
                }
                CategoryService.UpdateCategory(category);
                return RedirectToAction("Index");
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoryService.DeleteCategory(id);
            return RedirectToAction("Index");
        }

        public ActionResult Report(CategorySearchViewModel parameters)
        {
            parameters.CurrentPage = 1;
            parameters.PageSize = int.MaxValue;
            var catalogs = CategoryService.GetAllCategoriesByPage(parameters);
            var report = CatalogService.GenerateCategoriesExcelReport(catalogs.Records);

            using (var wb = report)
            {
                return wb.Deliver("CatalogCategoriesReport.xlsx");
            }
        }
    }
}