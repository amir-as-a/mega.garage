﻿using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Persistence.Order.Entities;
using Mega.Garage.Presentation.Controllers;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Order.Controllers
{
    public class CampaignTypesController : BaseController
    {
        public ActionResult Index()
        {
            return View(CampaignService.GetAllCampaignTypes().ToList());
        }

        public ActionResult Modify(int? id)
        {
            if (id != null)
            {
                CampaignType campaignType = CampaignService.GetCampaignTypeById((int)id);
                if (campaignType == null)
                    return HttpNotFound();

                return View(campaignType);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(CampaignType campaignType)
        {
            if (campaignType.Id > 0)
                CampaignService.UpdateCampaignType(campaignType);
            else
                CampaignService.AddCampaignType(campaignType);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CampaignService.DeleteCampaignType(id);
            return RedirectToAction("Index");
        }
    }
}