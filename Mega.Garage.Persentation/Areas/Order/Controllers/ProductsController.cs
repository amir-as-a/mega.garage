﻿using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Logic.FileUpload.Services;
using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Logic.Order.ViewModel;
using Mega.Garage.Persistence.Order.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Order.Controllers
{
    public class ProductsController : BaseController
    {

        public ActionResult Index(ProductSearchViewModel parameters)
        {
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            parameters.CurrentPage = CurrentPage;
            parameters.PageSize = 25;
            ViewBag.PageSize = parameters.PageSize;
            var products = ProductService.GetPagedListAllProducts(parameters);
            ViewBag.TotalRecords = products.TotalRecords;
            return View(products.Records);
        }

        public ActionResult Modify(int? id)
        {
            if (id != null)
            {
                var product = ProductService.GetProductById((int)id);
                if (product == null)
                    return HttpNotFound();

                ViewBag.CatalogId = new SelectList(CatalogService.GetAllCatalogs(), "Id", "Title", product.CatalogId);
                ViewBag.ColorId = new SelectList(ColorService.GetAllColors(), "Id", "Title", product.ColorId);

                return View(product);
            }
            else
            {
                ViewBag.CatalogId = new SelectList(CatalogService.GetAllCatalogs(), "Id", "Title");
                ViewBag.ColorId = new SelectList(ColorService.GetAllColors(), "Id", "Title");

                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Product product, HttpPostedFileBase fileColorImage, HttpPostedFileBase fileCatalogImage)
        {
            Product currentProduct = null;
            if (product.Id > 0)
                currentProduct = ProductService.GetProductById(product.Id);

            if (fileCatalogImage != null)
            {
                var LogoCatalogImageData = UploadService.Upload(fileCatalogImage);
                product.CatalogImage = LogoCatalogImageData.FileData.ToString();
            }
            else if (product.Id > 0)
            {
                product.CatalogImage = currentProduct.CatalogImage;
            }

            if (fileColorImage != null)
            {
                var LogoFileData = UploadService.Upload(fileColorImage);
                product.ColorImage = LogoFileData.FileData.ToString();
            }
            else if (product.Id > 0)
            {
                product.ColorImage = currentProduct.ColorImage;
            }

            if (product.Id > 0)
                ProductService.UpdateProduct(product);
            else
                ProductService.AddProduct(product);

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductService.DeleteProduct(id);
            return RedirectToAction("Index");
        }
    }
}