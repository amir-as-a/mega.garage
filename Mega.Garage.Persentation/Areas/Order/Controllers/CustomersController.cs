﻿using ClosedXML.Extensions;
using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Logic.Order.ViewModel;
using Mega.Garage.Persistence.Order.Entities;
using Mega.Garage.Presentation.Controllers;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Order.Controllers
{
    public class CustomersController : BaseController
    {
        // GET: customers
        public ActionResult Index(CustomerSearchViewModel parameters)
        {
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            parameters.CurrentPage = CurrentPage;
            parameters.PageSize = 25;
            ViewBag.PageSize = parameters.PageSize;
            var customers = CustomerService.GetAllCustomers(parameters);
            ViewBag.TotalRecords = customers.TotalRecords;

            return View(customers.Records);
        }

        public ActionResult Report(CustomerSearchViewModel parameters)
        {
            parameters.PageSize = 0;
            var customers = CustomerService.GetAllCustomers(parameters);

            var report = CustomerService.GenerateExcelReport(customers.Records);

            using (var wb = report)
            {
                return wb.Deliver("CustomerReport.xlsx");
            }
        }

        // GET: customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var customer = CustomerService.GetCustomerById((int)id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Customer customer)
        {
            if (ModelState.IsValid)
            {
                CustomerService.AddCustomer(customer);
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        // GET: customers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = CustomerService.GetCustomerById((int)id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CustomerService.DeleteCustomer(id);
            return RedirectToAction("Index");
        }

        public JsonResult SearchCustomer(string term, string q, int? page)
        {
            if (page == null) page = 1;
            CustomerSearchViewModel parameters = new CustomerSearchViewModel()
            {
                PageSize = 20,
                CurrentPage = page.Value,
                Name = term
            };
            var customers = CustomerService.GetAllCustomers(parameters).Records;

            var data = new
            {
                results = customers.Select(x => new
                {
                    id = x.Id,
                    text = $"{x.FirstName} {x.LastName}"
                }),
                pagination = new
                {
                    more = (customers.Count() != 0)
                }
            };

            return Json(data, JsonRequestBehavior.AllowGet);
        }

    }
}