﻿using Mega.Garage.Logic;
using Mega.Garage.Logic.FileUpload.Services;
using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Persistence.Order.Entities;
using Mega.Garage.Presentation.Controllers;
using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Mega.Garage.Common;

namespace Mega.Garage.Presentation.Areas.Order.Controllers
{
    public class CampaignsController : BaseController
    {

        public ActionResult Index(string startDate, string endDate, string isSpecialGarage, string isOneSite)
        {
            DateTime? StartDate = Common.DateUtility.TryGetDateTime(startDate);
            DateTime? EndDate = Common.DateUtility.TryGetDateTime(endDate);
            bool? IsSpecialGarage = isSpecialGarage.ConvertStringToBoolian();
            bool? IsOneSite = isOneSite.ConvertStringToBoolian();

            int CurrentPage = int.Parse(Request["p"] ?? "1");
            ViewBag.PageSize = 25;
            PagedList<Campaign> campaigns = CampaignService.GetAllCampaignsWithFilter(StartDate, EndDate, IsOneSite, IsSpecialGarage, CurrentPage, ViewBag.PageSize);
            ViewBag.TotalRecords = campaigns.TotalRecords;
            return View(campaigns.Records);
        }

        public ActionResult Modify(int? id)
        {
            if (id != null)
            {
                Campaign campaign = CampaignService.GetCampaignById((int)id);
                if (campaign == null)
                    return HttpNotFound();
                ViewBag.CampaignTypeId = new SelectList(CampaignService.GetAllCampaignTypes(), "Id", "Title", campaign.CampaignTypeId);

                return View(campaign);
            }
            else
            {
                ViewBag.CampaignTypeId = new SelectList(CampaignService.GetAllCampaignTypes(), "Id", "Title");
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Campaign model, HttpPostedFileBase fileCampaignImage)
        {
            if (model.Id > 0)
            {
                var currentCampaign = CampaignService.GetCampaignById(model.Id);
                if (fileCampaignImage != null)
                {
                    var LogoCatalogImageData = UploadService.Upload(fileCampaignImage);
                    model.CampaignImage = LogoCatalogImageData.FileData.ToString();
                }
                else
                {
                    model.CampaignImage = currentCampaign.CampaignImage;
                }


                CampaignService.UpdateCampaign(model);
            }
            else
            {
                if (fileCampaignImage != null)
                {
                    var LogoCatalogImageData = UploadService.Upload(fileCampaignImage);
                    model.CampaignImage = LogoCatalogImageData.FileData.ToString();
                }
                CampaignService.AddCampaign(model);
            }

            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CampaignService.DeleteCampaign(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult ProductList(int id)
        {
            ViewBag.CampaignProductList = CampaignService.GetCampaignProductsByCampaignId(id);
            ViewBag.ProductList = ProductService.GetAllProducts().ToList();
            var campaign = CampaignService.GetCampaignById(id);

            return View(campaign);
        }

        [HttpPost]
        public ActionResult ProductList(CampaignProduct campaignProduct)
        {
            try
            {
                CampaignService.AddCampaignProduct(campaignProduct);
            }
            catch (Exception ex)
            {
                return ReturnException(ex, Redirect(campaignProduct.CampaignId.ToString()));
            }

            ViewBag.CampaignProductList = CampaignService.GetCampaignProductsByCampaignId(campaignProduct.CampaignId.Value).ToList();
            ViewBag.ProductList = ProductService.GetAllProducts().ToList();
            var campaign = CampaignService.GetCampaignById((int)campaignProduct.CampaignId);

            return View(campaign);
        }

        [HttpPost, ActionName("DeleteCampaignProduct")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCampaignProductConfirmed(int id)
        {
            CampaignProduct campaignProduct = CampaignService.GetCampaignProductById((int)id);
            CampaignService.DeleteCampaignProduct(id);
            return RedirectToAction("ProductList", new { Id = campaignProduct.CampaignId });
        }

    }
}