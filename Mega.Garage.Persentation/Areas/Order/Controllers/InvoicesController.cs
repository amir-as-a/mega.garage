﻿using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Logic.Order.ViewModel;
using Mega.Garage.Persistence.Order.Entities;
using Mega.Garage.Presentation.Controllers;
using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ClosedXML.Extensions;
using Mega.Garage.Logic;
using Mega.Garage.Logic.Payment.Services;
using System.Collections.Generic;

namespace Mega.Garage.Presentation.Areas.Order.Controllers
{
    public class InvoicesController : BaseController
    {
        // GET: Invoices
        public ActionResult Index(InvoiceSearchViewModel parameters, string P_FromDate, string P_ToDate)
        {
            if (!string.IsNullOrEmpty(P_FromDate))
            {
                parameters.FromDate = Common.DateUtility.GetDateTime(P_FromDate);
            }

            if (!string.IsNullOrEmpty(P_ToDate))
            {
                parameters.ToDate = Common.DateUtility.GetDateTime(P_ToDate);
            }
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            ViewBag.OrderNumber = parameters.OrderNumber;
            ViewBag.CustomerName = parameters.CustomerName;
            ViewBag.FromDate = parameters.FromDate;
            ViewBag.ToDate = parameters.ToDate;
            ViewBag.PageSize = 25;
            PagedList<FullReportByCustomerViewModel> fullOrderItemList = InvoiceService.FullReportByCustomerViewModel(parameters, CurrentPage, ViewBag.PageSize);
            ViewBag.TotalRecords = fullOrderItemList.TotalRecords;

            if (parameters.CustomerId.HasValue)
                ViewBag.Customer = CustomerService.GetCustomerById(parameters.CustomerId.Value);

            ViewBag.PaymentTypes = InvoiceService.GetAllPaymentTypes();
            ViewBag.InvoiceProcesses = InvoiceService.GetAllInvoiceProcesses();

            return View(fullOrderItemList.Records.ToList()[0]);
        }

        // GET: Invoices
        public ActionResult IndexByCustomer(InvoiceSearchViewModel parameters)
        {
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            ViewBag.PageSize = 25;
            var invoice = InvoiceService.GetAllInvoices(parameters).OrderByDescending(x => x.InvoiceDate);
            ViewBag.FromDate = parameters.FromDate;
            ViewBag.ToDate = parameters.ToDate;
            ViewBag.DiscountCode = parameters.DiscountCode;
            ViewBag.CustomerId = parameters.CustomerId;
            ViewBag.Customer = CustomerService.GetCustomerById((int)parameters.CustomerId);
            ViewBag.Invoices = invoice;
            PagedList<FullReportByCustomerViewModel> fullOrderItemList = InvoiceService.FullReportByCustomerViewModel(parameters, CurrentPage, ViewBag.PageSize);
            ViewBag.TotalRecords = fullOrderItemList.TotalRecords;

            ViewBag.InvoiceProcesses = InvoiceService.GetAllInvoiceProcesses().ToList();
            return View(fullOrderItemList.Records.ToList()[0]);
        }

        // GET: Invoices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = InvoiceService.GetInvoiceById((int)id);
            if (invoice == null)
            {
                return HttpNotFound();
            }

            ViewBag.InvoiceAdditives = InvoiceService.GetAllInvoiceAdditivesByInvoiceId((int)id);
            ViewBag.InvoiceAdditiveTypes = InvoiceService.GetAllInvoiceAdditiveTypes();
            ViewBag.OrderItems = OrderService.GetAllOrderItemsByInvoiceId((int)id).ToList();

            return View(invoice);
        }

        // GET: Invoices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Invoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    invoice.CurrentProcessId = 1;
                    InvoiceService.AddInvoice(invoice);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ReturnException(ex);
                }
            }

            return View(invoice);
        }

        // GET: Invoices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = InvoiceService.GetInvoiceById((int)id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: Invoices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    InvoiceService.UpdateInvoice(invoice);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ReturnException(ex);
                }
            }
            return View(invoice);
        }

        public ActionResult Report(InvoiceSearchViewModel parameters)
        {
            int CurrentPage = 1;
            ViewBag.PageSize = int.MaxValue;
            PagedList<FullReportByCustomerViewModel> fullReport = InvoiceService.FullReportByCustomerViewModel(parameters, CurrentPage, ViewBag.PageSize);
            var invoices = fullReport.Records.ToList()[0].Invoices;
            var report = InvoiceService.GenerateExcelReport(invoices);

            using (var wb = report)
            {
                return wb.Deliver("InvoiceReport.xlsx");
            }
        }

        // GET: Invoices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = InvoiceService.GetInvoiceById((int)id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: Invoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InvoiceService.DeleteInvoice(id);
            return RedirectToAction("Index");
        }

        public ActionResult History(int id)
        {
            ViewBag.InvoiceProcesses = InvoiceService.GetAllInvoiceProcesses().ToList();
            return View(InvoiceService.GetAllInvoiceProcessHistoriesByInvoiceId(id));
        }

        public ActionResult OrderItemsHistory(int id)
        {
            ViewBag.OrderProcess = OrderService.GetAllOrderProcesses().ToList();
            return View(OrderService.GetAllOrderProcessHistoriesByOrderItemId(id));
        }

        //public ActionResult Payments(int id)
        //{
        //    var Payments = PaymentService.GetUserPaymentsByInvoiceId(id);
        //    return View(Payments);
        //}
        public ActionResult IndexByDealer(InvoiceSearchViewModel parameters)
        {
            int CurrentPage = int.Parse(Request["p"] ?? "1");
            parameters.CurrentPage = CurrentPage;
            parameters.PageSize = 25;
            ViewBag.PageSize = parameters.PageSize;

            ViewBag.dealer = Logic.Dealer.Services.DealerService.GetDealersList();
            PagedList<Invoice> invoiceByDealer = InvoiceService.GetInvoiceListByDealerId(parameters);
            ViewBag.TotalRecords = invoiceByDealer.TotalRecords;
            return View(invoiceByDealer);
        }

    }
}