﻿using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Logic.Order.ViewModel;
using Mega.Garage.Persistence.Order.Entities;
using Mega.Garage.Presentation.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Order.Controllers
{

    public class DiscountController : BaseController
    {
        public ActionResult Index(string fromDate, string toDate, string code)
        {
            DateTime? FromDate = Common.DateUtility.TryGetDateTime(fromDate);
            DateTime? ToDate = Common.DateUtility.TryGetDateTime(toDate);

            int CurrentPage = int.Parse(Request["p"] ?? "1");
            ViewBag.PageSize = 25;
            var discounts = DiscountService.GetAllPagedListDiscounts(FromDate, ToDate, code, CurrentPage, ViewBag.PageSize);
            ViewBag.TotalRecords = discounts.TotalRecords;
            return View(discounts.Records);
        }

        public ActionResult Modify(int? id)
        {
            if (id != null)
            {
                Discount discount = DiscountService.GetDiscountById((int)id);
                if (discount == null)
                    return HttpNotFound();

                return View(discount);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Modify(Discount discount)
        {
            if (discount.Id > 0)
                DiscountService.UpdateDiscount(discount);
            else
                DiscountService.AddDiscount(discount);
            return RedirectToAction("Index");
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DiscountService.DeleteDiscount(id);
            return RedirectToAction("Index");
        }

        public ActionResult BulkInsert()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult BulkInsert(BulkDiscountViewModel vm)
        {
            DiscountService.AddMultiple(vm);
            return RedirectToAction("Index");
        }
    }
}