﻿using DocumentFormat.OpenXml.EMMA;
using Mega.Garage.Logic.Fitup.Services;
using Mega.Garage.Presentation.Controllers;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Areas.Fitup.Controllers
{
    public class MappingController : BaseController
    {
        public ActionResult Index(long id)
        {
            ViewBag.ProductData = MappingService.GetAllMappingByProductId(id);
            ViewBag.ProductId = id;

            var data = CarGroupService.GetAllCarGroupsWithModels();
            return View(data);
        }

        public JsonResult Add(long productId, int modelId)
        {
            var result = MappingService.AddMapping(productId, modelId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Remove(long productId, int modelId)
        {
            var result = MappingService.RemoveMapping(productId, modelId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


    }
}