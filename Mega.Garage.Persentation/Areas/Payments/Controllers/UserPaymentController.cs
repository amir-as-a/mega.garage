﻿//using Mega.Garage.Common;
//using Mega.Garage.Logic;
//using Mega.Garage.Logic.Payment.Services;
//using Mega.Garage.Logic.Payment.ViewModel;
//using Mega.Garage.Persistence.Payment.Entities;
//using Mega.Garage.Presentation.Controllers;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;

//namespace Mega.Garage.Presentation.Areas.Payments.Controllers
//{
//    public class UserPaymentController : BaseController
//    {
//        public ActionResult Index(PaymentSearchRequestViewModel parameters)
//        {
//            if (!parameters.FromDate.HasValue)
//            {
//                PersianCalendar calendar = new PersianCalendar();
//                var year = calendar.GetYear(DateTime.Now);
//                var month = calendar.GetMonth(DateTime.Now);
//                var fromDate = DateUtility.GetDateTime(year + "/" + month + "/01");
//                parameters.FromDate = fromDate;
//            }
//            ViewBag.FromDate = parameters.FromDate;

//            if (!parameters.ToDate.HasValue)
//            {
//                PersianCalendar calendar = new PersianCalendar();
//                var year = calendar.GetYear(DateTime.Now);
//                var month = calendar.GetMonth(DateTime.Now);
//                var toDate = DateUtility.GetDateTime(year + "/" + (month + 1) + "/01").AddMilliseconds(-1);
//                parameters.ToDate = toDate;
//            }
//            ViewBag.ToDate = parameters.ToDate;

//            ViewBag.PaymentStatus = PaymentService.GetAllPaymentStatus();

//            int CurrentPage = int.Parse(Request["p"] ?? "1");
//            parameters.CurrentPage = CurrentPage;
//            parameters.PageSize = 25;
//            ViewBag.PageSize = parameters.PageSize;
//            PagedList<UserPayment> payments = PaymentService.GetAllUserPayments(parameters);
//            ViewBag.TotalRecords = payments.TotalRecords;
//            return View(payments.Records);
//        }
//    }
//}