﻿using FluentValidation;
using Mega.Garage.Persentation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web.Mvc;
using System.Web.Routing;

namespace Mega.Garage.Presentation.Controllers
{
    [Logic.Account.Services.Authorization(Roles = "Admin")]
    public class BaseController : Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                List<string> Result = new List<string>();
                filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError;
                if (filterContext.Exception is ValidationException)
                {
                    ((ValidationException)filterContext.Exception).Errors.ToList().ForEach(error => { Result.Add(error.ErrorMessage); });
                }
                else {
                    Result.Add(filterContext.Exception.Message);
                    if (filterContext.Exception.InnerException != null)
                    {
                        Result.Add(filterContext.Exception.InnerException.Message);
                    }
                    Result.Add(filterContext.Exception.StackTrace);
                }
                filterContext.Result = new JsonResult()
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new MegaViewModel<int>()
                    {
                        Messages = Result,
                        Status = MegaStatus.Failed
                    }
                };
                filterContext.ExceptionHandled = true;
            }
        }

        protected ActionResult ReturnException(Exception ex, RedirectResult redirect = null)
        {
            if (ex is ValidationException)
            {

                var messages = new List<Message>() {
                    new Message { Type = MessageType.Error, Content = "عملیات ناموفق." },
                };

                foreach (var item in ((ValidationException)ex).Errors)
                {
                    var message = new Message { Type = MessageType.Warning, Content = item.ErrorMessage };
                    messages.Add(message);
                }

                TempData["Messages"] = messages.Distinct().ToList();
                if (redirect != null)
                    return redirect;

                return RedirectToAction("Index");
            }
            else
            {
                TempData["Messages"] = new List<Message>() { new Message { Type = MessageType.Error, Content = "خطایی رخ داده است." } };

                if (redirect != null)
                    return redirect;

                return RedirectToAction("Index");
            }
        }
    }
}