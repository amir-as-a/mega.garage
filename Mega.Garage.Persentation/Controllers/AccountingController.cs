﻿using Mega.Garage.Logic.Account.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mega.Garage.Presentation.Controllers
{
    public class AccountingController : Controller
    {
        public ActionResult LogIn()
        {

            if (UserService.IsAuthenticated() != null)
                return Redirect("/");
            else
                return View();
        }
        [HttpPost]
        public ActionResult Login(string userName, string password, string redirectTo, bool rememberMe = false)
        {
            var Result = new MegaViewModel<string>() { Status = MegaStatus.Unauthorized };
            if (string.IsNullOrEmpty(userName))
                Result.Messages.Add("لطفا نام کاربری را وارد کنید");
            if (string.IsNullOrEmpty(password))
                Result.Messages.Add("لطفا گذرواژه را وارد کنید");

            var UserData = UserService.GetUserData(userName, password);
            if (UserData == null)
                Result.Messages.Add("با این اطلاعات کاربری در سیستم ثبت نشده است");
            else
            {
                if (!UserData.IsActive)
                    Result.Messages.Add("این کاربر هنوز غیرفعال است");
                if (UserData.IsLocked)
                    Result.Messages.Add("این کاربر غیرفعال شده است");
            }


            if (Result.Messages.Count == 0)
            {
                UserService.SetAuthenticationCookie(userName, password, rememberMe);
                return Redirect((string.IsNullOrEmpty(redirectTo) ? "/" : redirectTo));
            }

            return View(Result);
        }

        public ActionResult LogOut()
        {
            UserService.Logout();
            return Redirect("~/Login");
        }
    }
}