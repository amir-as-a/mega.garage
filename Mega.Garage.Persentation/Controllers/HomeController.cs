﻿using System;
using System.Web.Mvc;
using System.Linq;
using System.Net;
using Mega.Garage.Logic.Order.Services;
using Mega.Garage.Logic.Dealer.Services;
using Mega.Garage.Logic.Catalog.Services;
using Mega.Garage.Common;
using Mega.Garage.Logic.Order.ViewModel;

namespace Mega.Garage.Presentation.Controllers
{
    [Logic.Account.Services.Authorization]
    public class HomeController : Controller
    {

        // GET: Home
        public ActionResult Index()
        {
            ViewBag.TotalCatalog = CatalogService.GetTotalCatalogs();
            ViewBag.TotalProducts = ProductService.GetTotalProducts();
            ViewBag.TotalInvoices = InvoiceService.GetTotalInvoices();
            ViewBag.TotalCustomers = CustomerService.GetTotalCustomers();
            ViewBag.LastInvoices = InvoiceService.GetAllInvoices(
                new Logic.Order.ViewModel.InvoiceSearchViewModel()
                {
                    FromDate = DateTime.Now.AddDays(-4),
                    ToDate = DateTime.Now
                }).OrderByDescending(x => x.InvoiceDate).ToList();
            ViewBag.ActiveProducts = ProductService.GetAllProducts();

            return View();
        }

        public JsonResult GetProductSellReport()
        {
            var productReport = OrderService.ProductSellReport();
            return Json(productReport, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDiscountCodeReport()
        {
            var discountCodeReport = InvoiceService.DiscountCodeReport();
            return Json(discountCodeReport, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTotalInvoicesCountPerMonthReport(int year)
        {
            var totalInvoicesCountPerMonthReport = InvoiceService.GetTotalInvoicesCountPerMonthReport(year).ToList();
            return Json(totalInvoicesCountPerMonthReport, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDealerServiceTotalSellPerMonthInAUniqueYear(int year)
        {
            var dealerServiceTotalSellPerMonthInAUniqueYear = DealerService.GetDealerServiceTotalSellPerMonthInAUniqueYear(year).ToList();
            return Json(dealerServiceTotalSellPerMonthInAUniqueYear, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInvoiceCountByPaymentTypeReport(string fromDate, string toDate)
        {
            var parameters = new InvoicePaymentReportRequestViewModel()
            {
                FromDate = DateUtility.GetDateTime(fromDate.toEnglishNumber()),
                ToDate = DateUtility.GetDateTime(toDate.toEnglishNumber())
            };

            var invoiceCountByPaymentTypeReport = InvoiceService.GetInvoiceCountByPaymentTypeReport(parameters).ToList();
            return Json(invoiceCountByPaymentTypeReport, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetInvoiceProcessStatusReport(string fromDate, string toDate)
        {
            var parameters = new InvoiceCurrentProcessRequestViewModel()
            {
                FromDate = DateUtility.GetDateTime(fromDate.toEnglishNumber()),
                ToDate = DateUtility.GetDateTime(toDate.toEnglishNumber())
            };

            var invoiceProcessStatusReport = InvoiceService.GetInvoiceProcessStatusReport(parameters).ToList();
            return Json(invoiceProcessStatusReport, JsonRequestBehavior.AllowGet);
        }

        //[Route("FileStorage/{FileId}/{FileName}")]
        public ActionResult Download(char Size, Guid FileId, string FileName)
        {
            var Result = new Logic.FileUpload.ViewModel.DownloadResult();
            switch (Size)
            {
                case 't':
                    Result = Logic.FileUpload.Services.UploadService.DownloadImage(FileId, FileName, 64, null);
                    break;
                case 's':
                    Result = Logic.FileUpload.Services.UploadService.DownloadImage(FileId, FileName, 124, null);
                    break;
                case 'm':
                    Result = Logic.FileUpload.Services.UploadService.DownloadImage(FileId, FileName, 320, null);
                    break;
                case 'l':
                    Result = Logic.FileUpload.Services.UploadService.DownloadImage(FileId, FileName, 800, null);
                    break;
                case 'o':
                    Result = Logic.FileUpload.Services.UploadService.Download(FileId, FileName);
                    break;
            }

            if (Result.IsAvailble)
            {
                return File(Result.FileArray, Result.MimeType);
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.NotFound);

            }
        }
    }
}